# 代码格式规范的辅助工具

## 快速生成代码注释头（doxygen）

Doxygen是一种开源跨平台的，以类似JavaDoc风格描述的文档系统，完全支持C、C++、Java、Objective-C和IDL语言，部分支持PHP、C#。注释的语法与Qt-Doc、KDoc和JavaDoc兼容。Doxygen可以从一套归档源文件开始，生成HTML格式的在线类浏览器，或离线的LATEX、RTF参考手册。
为了便于doxygen工具生成源码参考手册，在源码的注释中就必须先按照doxygen的注释格式规范进行注释的编写。  
Doxygen规范示例：

```c
/**
 * <A short one line description>
 *
 * <Longer description>
 * <May span multiple lines or paragraphs as needed>
 *
 * @param  Description of method's or function's input parameter
 * @param  ...
 * @return Description of the return value
 */
```

### vscode中的doxygen插件

Doxygen Documentation Generator是vscode中快捷生成doxygen风格注释的一款插件，通过其简单的配置，即可通过在文件头或者函数名上一行输入“/** ”加回车后自动生成头部注释。  
根据OPPO的注释规范，对vscode的doxygen插件配置如下：

```json
{
    "doxdocgen.generic.authorTag": "@author Your name",
    "doxdocgen.file.copyrightTag": [
        "Copyright (c) 2008-{year}, OPPO Mobile Comm Corp., Ltd"
    ],
    "doxdocgen.file.customTag": [
        "VENDOR_EDIT"
    ],
    "doxdocgen.file.fileOrder": [
        "copyright",
        "custom",
        "file",
        "brief",
        "version",
        "date",
        "author"
    ],
    "doxdocgen.generic.order": [
        "brief",
        "param",
        "return"
    ]
}
```

Vscode->配置->setting:setting.json，使用json格式进行描述，可在vscode的设置中点击“在setting.json中编辑”进行打开。把上面的json格式配置内容添加到setting.json中即可在vscode中使用自动生成头部注释。

生成的文件头部注释示例如下：

```c
/**
 * Copyright (c) 2008-2019, OPPO Mobile Comm Corp., Ltd
 * VENDOR_EDIT
 * @file mqtt_thread.c
 * @brief mqtt client management process.
 * @version 0.1
 * @date 2019-07-27
 * @author WatWu
 */
```

生成的函数头部注释示例如下：

```c
/**
 * @brief initialize a mqtt client
 * @param client_id     [divice id]
 * @param server_addr   [mqtt server address, ip or domain name]
 * @param server_port   [mqtt server port]
 * @param username      [mqtt user name]
 * @param password      [mqtt password]
 * @return int          [initialized mqtt client's file descriptor, mqtt connection is not initiated at this time]
 */
int mqtt_init_client(char *client_id, char *server_addr, uint16_t server_port, char *username, char *password)

```

## 快速格式化代码（astyle）

AStyle是一个开源的代码缩进、格式化和美化工具，可以处理C, C++, ObjectiveC, C#和 Java语言。

### 安装
1 vscode : install astyle 
2 sudo apt-get install astyle
3 vi .astylerc

到 SourceForge AStyle 项目下载需要的版本(3.1或以上)，并解压，到build目录选择自己使用的编译器(有clang，gcc，intel等编译器支持，这里选择gcc）。然后make && make install就可以了。

### 使用

AStyle是一个命令行工具，从命令行读取输入。

```shell
oppo@oppo:~$ astyle --help

                     Artistic Style 3.1
                     Maintained by: Jim Pattee
                     Original Author: Tal Davidson

Usage:
------
            astyle [OPTIONS] File1 File2 File3 [...]
            ......
```

其中[OPTIONS]选项可以统一保存到配置文件，这样在使用astyle命令时就可以不用再输入参数，同时也可以整合进vscode等编辑器中。  
Astyle的配置文件为“.astylerc”，保存到Linux下的当前用户目录中即可。

[OPTIONS]选项可以保存到配置文件，这样在使用astyle命令时就可以不用再输入参数，同时也可以整合进vscode等编辑器中。

Astyle的配置文件为“.astylerc”，保存到Linux下的当前用户目录中即可。  
“.astylerc”中的内容如下，该配置已符合OPPO编码规范的要求（要求astyle的版本为3.1或以上）：

```shell
--style=otbs
--indent-switches
--indent=spaces=4
--pad-oper
--align-reference=type
--align-pointer=type
--unpad-paren
--suffix=none
--attach-return-type
--indent-preproc-cond
```

### Astyle源码编译安装

下载路径：https://sourceforge.net/p/astyle/code/HEAD/tree/  
在目录（astyle-code-r672-trunk\AStyle\build\gcc）下依次执行：`make; sudo make instal;`  
查找Astyle版本为 3.2 说明安装成功

### vscode中的astyle插件

在vscode中安装插件“Astyle”后，在源码文件中右键或者按快捷键<Ctrl+shift+i>即可自动格式化当前文件。该插件调用的就是系统中安装好的astyle命令。

![astyle](./assets/astyle.png)
