#prjname
#gcc 
CC    		= /media/data/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-gcc
AR			= /media/data/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-ar

#CC    		= /home/ft123456/openwrt_widora/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-gcc
#AR			= /home/ft123456/openwrt_widora/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-ar
# CC     	= gcc
# AR 		= ar

TOP=.

#freeIot_ppmq

TARGET 			= freeIot_zigbee

FREEOSAL_PATH      =$(TOP)/3rd/freeOsal
FREEHOST_PATH      =$(TOP)/3rd/freeHost
FREELUA_PATH       =$(TOP)/3rd/freeLua
MBEDTLS            =$(TOP)/3rd/mbedtls
FLASH_DB_PATH      =$(TOP)/3rd/FlashDB
NET                =$(TOP)/libSrc
HTTP               =$(TOP)/libSrc/http
LUASRC             =$(TOP)/luaSrc

# UTPPMQ             =$(TOP)/ppmq

#option
CFLAGS			=-fPIC
CFLAGS 			+= -Wall -g -O0 -std=gnu11
#CFLAGS			+= -pg
#link
LFLAGS 			+= -lm -pthread -lrt -ldl


#freeosal inc
INC_FLAGS 		+= -I $(FREEOSAL_PATH)/inc
INC_FLAGS 		+= -I $(FREEOSAL_PATH)/hal
INC_FLAGS 		+= -I $(FREEOSAL_PATH)/osal

#freehost inc
INC_FLAGS 		+= -I $(FREEHOST_PATH)/inc
INC_FLAGS 		+= -I $(FREEHOST_PATH)/ncp
INC_FLAGS 		+= -I $(FREEHOST_PATH)/zbhci
INC_FLAGS 		+= -I $(FREEHOST_PATH)/api

#freelua inc
INC_FLAGS 		+= -I $(FREELUA_PATH)/lua_os
INC_FLAGS 		+= -I $(FREELUA_PATH)/lua/lua-5.4.3

#mbedtls
INC_FLAGS 		+= -I $(MBEDTLS)/include
INC_FLAGS 		+= -I $(MBEDTLS)/include/mbedtls


INC_FLAGS 		+= -I $(NET)
INC_FLAGS 		+= -I $(HTTP)
INC_FLAGS 		+= -I $(LUASRC)
# INC_FLAGS 		+= -I $(UTPPMQ)

#FlashDB       
INC_FLAGS       += -I $(FLASH_DB_PATH)/inc

DTLS           += 	aes.c		aesni.c 	aria.c  	asn1parse.c	asn1write.c	base64.c	\
	bignum.c	camellia.c	ccm.c		cipher.c	cipher_wrap.c	\
	cmac.c		ctr_drbg.c	des.c		dhm.c		ecdh.c		ecdsa.c		\
	ecjpake.c	ecp.c		ecp_curves.c	entropy.c	entropy_poll.c		\
	gcm.c		hmac_drbg.c	md.c		\
	md5.c		oid.c		padlock.c	platform_util.c\
	pem.c		pk.c		pk_wrap.c	pkcs12.c	pkcs5.c		pkparse.c	\
	pkwrite.c	platform.c	ripemd160.c	rsa.c		sha1.c		sha256.c	\
	sha512.c	threading.c	timing.c	version.c	chacha20.c  nist_kw.c\
	x509.c 		x509_crt.c	debug.c		net_sockets.c	chachapoly.c\
	ssl_cache.c	ssl_ciphersuites.c		ssl_cli.c	ssl_cookie.c	poly1305.c	\
	ssl_srv.c	ssl_ticket.c	ssl_tls.c	rsa_alt_helpers.c	x509write_csr.c\
	x509write_crt.c	x509_create.c


SRC_CODE  =$(wildcard $(NET)/*.c)
SRC_CODE  +=$(wildcard $(HTTP)/*.c) 
SRC_CODE  +=$(wildcard $(LUASRC)/*.c) 
SRC_CODE  +=$(wildcard $(FLASH_DB_PATH)/src/*.c) 
SRC_CODE  +=$(addprefix $(MBEDTLS)/library/,${DTLS})

# SRC_CODE  +=$(wildcard $(UTPPMQ)/*.c)


OBJ_CODE:=$(SRC_CODE:.c=.o)

APP = $(TOP)/prj/poll_main.c 
# APP = $(TOP)/gp_cbc.c 


SUBDIRS    = $(FREEOSAL_PATH) $(FREEHOST_PATH) $(FREELUA_PATH)

CONSTRAINED_LIBS  = $(FREEOSAL_PATH)/libfree_osal.a
CONSTRAINED_LIBS += $(FREEHOST_PATH)/libhost.a  
CONSTRAINED_LIBS += $(FREELUA_PATH)/liblua.a

PRJAPP_LIBS      = libfreeIot.a libfreeIot.so $(TARGET)

export CC AR CFLAGS LFLAGS

.PHONY: all clean rebuild ctags

all:$(SUBDIRS) $(PRJAPP_LIBS) $(TARGET)

%.o: %.c
#	@mkdir -p obj 
	@echo "building $<"
	${CC} -c -o $@ $< ${CFLAGS} $(INC_FLAGS)

$(SUBDIRS):ECHO	
	@echo "bulid begin ....................."
	make	-C $@
ECHO:
	@echo $(SUBDIRS)
	
libfreeIot.a:$(OBJ_CODE)
	$(AR) cDPrST  $@ $(CONSTRAINED_LIBS) $(OBJ_CODE)
	ranlib libfreeIot.a

libfreeIot.so:$(OBJ_CODE)
	$(CC) -shared -o $@ $(OBJ_CODE) -Wl,--whole-archive $(CONSTRAINED_LIBS) -Wl,--no-whole-archive

$(TARGET): libfreeIot.a $(APP)
# libiotsdk.a
	@echo "linking object to $<"
	${CC} -o $@ $(APP) libfreeIot.a  ${CFLAGS} ${INC_FLAGS} ${LFLAGS}

clean:
	-rm -rf $(CONSTRAINED_LIBS) $(PRJAPP_LIBS) $(OBJ_CODE)

cleanall:clean
	make -C $(FREEOSAL_PATH) clean
	make -C $(FREEHOST_PATH) clean
	make -C $(FREELUA_PATH) clean


ctags:
	@ctags -R `pwd`
	@echo "making tags file"
