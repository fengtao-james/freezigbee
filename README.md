# freeIot

##    此项目是针对嵌入式IOT的微服务的一个设计构想。
### 协议分层
<table><tr><td bgcolor=ondine>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp; RPC &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=ondine>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;COAP|HTTP&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=cyan>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;  UDP|TLS &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=orange>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;OSAL&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>

### 服务分层

<table><tr><td bgcolor=ondine>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Microservice &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=ondine>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;API RPC REST模型&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=cyan>&nbsp;&nbsp;&nbsp;&emsp;&emsp;  server LUA引擎 网络服务 MQTT http协议&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<table><tr><td bgcolor=orange>&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;OSAL 内存管理 消息队列 时间管理&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;</td></tr></table>


## 目录架构

### freeOsal
#### 一个轻量级的os，包含 消息队列，定时器，内存管理。
#### 目录结构
+ app 包含os的一些应用样列
+ hal 包含平台相关的 系统接口
+ osal os core的具体实现
### freeLua
#### 一个轻量级的 基于LUA的RPC，包含lua 引擎，rpc 协议解析器和编码器。
#### 目录结构
+ app lua的应用样列
+ lpeg 基于lua的正则表达式库
+ lua-5.4.3 lua引擎

### freeHost
#### 一个轻量级的 基于TLSR825 gateway os。
#### 目录结构
+ inc api inc
+ ncp ncp api
+ zbhci zigbee modle uart api

### prj
#### 应用程序目录。

### luaSrc
#### lua API lib c
+ lua_bus :485 bus
+ lua http :http
+ lua tcp: tcp
+ lua udp :udp

### lualib
#### lua API lib
+ lua_bus :485 bus lua api
+ lua tcp: tcp lua api
+ lua udp :udp lua api
+ lua mqtt :lua mqtt api
+ lua zcl :zcl lua api

### APP
#### 一些LUA应用程序样列
### makefile
#### 编译管理模块
### taobao
#### https://item.taobao.com/item.htm?spm=a1z10.1-c.w137644-11225537082.39.ba313863HZxkLv&id=712906343255&skuId=5160502762941
