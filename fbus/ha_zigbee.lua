package.path ="/usr/lib/lualib/?.lua"
-- package.path ="./lualib/?.lua"

local osal     = require "osal"

osal.include("/root/fzigbee")

local aux_core = require("aux_core")
local bus      = require("bus_cli")
local tpapi    = require("topicapi")

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr

local function bus_recv_data(addr,cmd,data)
   local info          = {Device = addr}
   local str           = data
   local atid          = 3

   if cmd > bus.read_input then
    return
   end

   local len,id,pid = string.unpack("B >H >H",data)

   str = string.sub(str,6)  -- 1 + 2 + 2

   while #str >= 2 do
    local code = string.unpack(">H",str)

    print("code:",code,atid)

    str = string.sub(str,3)  -- 3

    local val,name  = tpapi.get_val(pid,atid)

    if val == nil then

        break
    end

    atid = atid +1

    info[val] = code
    info.info = name
   end

    tpapi.report(info)
end

-- // "/dev/ttyUSB0"
-- // "/dev/ttyS0"
osal.start(function()
    
    local bus_index = 1

    bus.sys_start("/dev/ttyS0",57600)
    bus.recv_thread(bus_recv_data)

    pcallx(tpapi.start)

    osal.fork(function()
        bus.send(bus_index,bus.read_acdata,1,20)

        bus_index = bus_index + 1

        if bus_index > 20  then
            bus_index = 1
        end

        return 0
    end,100)
end)