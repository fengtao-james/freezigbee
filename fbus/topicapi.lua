local osal     = require "osal"
local aux_core = require "aux_core"
local hmqtt    = require("ha_mqtt")
local bus      = require("bus_cli")


local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr
local to_json     = osal.json_encode
local to_table    = osal.json_decode

local topicapi = {}

local gmacstr    = "EABD12325A0A8710"



local bus_list = {
    -- Power Configuration cluster
    {id = 0xF403, atid = 3,  val = "temper",     name = "SENSOR-TEMPER"   }, 
    {id = 0xF403, atid = 4,  val = "humidity",   name = "SENSOR-TEMPER"   }, 
    {id = 0xF00D, atid = 3,  val = "motion",     name = "SENSOR-PIR" },
    {id = 0xF00D, atid = 4,  val = "illumination", name = "SENSOR-PIR" },
    {id = 0xF028, atid = 3,  val = "smoke",        name = "SENSOR-SMOKE" }, 
    {id = 0xF101, atid = 4,  val = "CWPower",      name = "LIGHT-C" },
    {id = 0xF101, atid = 12,  val = "CCurrent",      name = "LIGHT-C" },
    {id = 0xF10C, atid = 4,  val = "CWPower",      name = "LIGHT-CW" },
    {id = 0xF10C, atid = 12,  val = "CCurrent",      name = "LIGHT-CW" },
    {id = 0xF10C, atid = 13,  val = "WCurrent",      name = "LIGHT-CW" },
    {id = 0xF10D, atid = 3,  val = "RGBPower",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 4,  val = "CWPower",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 5,  val = "Dimmer",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 6,  val = "colorTemperatureMireds",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 7,  val = "hue",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 8,  val = "Voltage",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 9,  val = "RCurrent",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 10,  val = "GCurrent",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 11,  val = "BCurrent",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 12,  val = "CCurrent",     name = "LIGHT-RGBCW" },
    {id = 0xF10D, atid = 13,  val = "WCurrent",     name = "LIGHT-RGBCW" },
}


local function bus_tab_2_name(id,attid)
    local tb = nil

    osal.array_foreach(bus_list,function(tab)
        
        --print("cid :",cid,dtype,attid,tab.cid,tab.tp,tab.atid)
        if id == tab.id  and attid == tab.atid then
            tb = tab
        end
    end)
    return tb
end


local function bus_cmd(addr,cmd)
    if cmd.CWPower then

        bus.send(addr,bus.set_acdata,4,cmd.CWPower)
    end

    if cmd.RGBPower then

        bus.send(addr,bus.set_acdata,3,cmd.RGBPower)
    end

    if cmd.Dimmer then

        bus.send(addr,bus.set_acdata,5,cmd.Dimmer)
    end

    if cmd.colorTemperatureMireds then

        bus.send(addr,bus.set_acdata,6,cmd.colorTemperatureMireds)
    end

    if cmd.hue then

        bus.send(addr,bus.set_acdata,7,cmd.hue)
    end
    -- bus.send(addr,bus.set_acdata,)
end

local function _gw_online()
    hmqtt.push_retain("tele/".. gmacstr .."/LWT","Online")
end

local function _sensor_push(api,data)

    if not is_string(api) then

        error("sensor api error")
        return
    end

    hmqtt.report("tele/".. gmacstr .. api,to_json(data))
end


local function _topic_ack(api,data)

    if not is_string(api) then

        error("sensor api error")
        return
    end

    hmqtt.report("stat/".. gmacstr .. api,to_json(data))
end

local function _sub_topic_fn()

    hmqtt.subscribe("zbeacon/discovery",function(data)
        _gw_online()
    end)

    hmqtt.subscribe("cmnd/".. gmacstr .."/BusInfo",function(data)

        local cmd = to_table(data)

        if not is_string(cmd.Device) then
            return
        end

        bus.send(cmd.Device,bus.read_acdata,1,20)

        _topic_ack("/ZbInfoAck",{Id = cmd.Id,result = "ok"})
    end)

    hmqtt.subscribe("cmnd/".. gmacstr .."/BusSet",function(data)

        local cmd = to_table(data)

        if not is_integer(cmd.Device) then
            return
        end
        
        bus_cmd(cmd.Device,cmd)

        _topic_ack("/BusSetAck",{Id = cmd.Id,result = "ok"})
    end)

    hmqtt.subscribe("cmnd/".. gmacstr .."/SetAddr",function(data)

        local cmd = to_table(data)

        if not is_integer(cmd.Device) then
            return
        end
        
        bus.send(0,bus.set_acdata,1,cmd.Device)

        _topic_ack("/SetAddrAck",{Id = cmd.Id,result = "ok"})
    end)
end

function topicapi.get_val(id,attid)

    local tb = bus_tab_2_name(id,attid)

    if tb then
        return tb.val,tb.name
    end

    return nil
end


function topicapi.report(info)

    _sensor_push("/report",info)
end

function topicapi.start()

    hmqtt.start(function(flags,status)

        if status == 0  then
            print("mqtt connect ok!!!!")
            
            _sub_topic_fn()
            _gw_online()
        else
            print("mqtt connect error!!!!")
        end
    end,
    "tele/".. gmacstr .."/LWT",
    "Offline", gmacstr)
end

return topicapi

