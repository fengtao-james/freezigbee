/**
 * james
 * net_endpoint.h
 *
*/
#ifndef NET_CONFIG_H
#define NET_CONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

#define OC_PDU_SIZE 1024

#include "task_event.h"
#include "SmNetFun.h"

#define NET_IPV6_ADDRSTRLEN (46)
#define NET_IPV4_ADDRSTRLEN (16)
#define NET_IPV6_ADDRLEN (16)
#define NET_IPV4_ADDRLEN (4)

enum protocol {
    UDP     = 1 << 0,
    TCP     = 1 << 1,
};

enum transport_flags {
    SECURED = 1 << 0,
    CLIENT  = 1 << 1,
    CONNECT = 1 << 2,
    SERVER  = 1 << 3,
};

enum net_type {
    ENDPOINT_UP_NETWORK_EVENT,
    ENDPOINT_DOWN_NETWORK_EVENT,
    ENDPOINT_CHILD_UP_NETWORK_EVENT,
};

union sockaddr_all {
    struct sockaddr s;
    struct sockaddr_in v4;
    struct sockaddr_in6 v6;
};

typedef struct {
    enum protocol proto;
    enum transport_flags flags;
    union sockaddr_all addr;
} net_addr_t;

#ifdef __cplusplus
}
#endif

#endif /* FO_ENDPOINT_H */
