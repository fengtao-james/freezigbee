/*
// Copyright (c) 2019 Intel Corporation
//
//endpoint manager
// james
*/
#include "task_event.h"
#include "SmNetFun.h"
#include "net_config.h"

#include "net_udp.h"
#include "net_endpoint.h"
//--------------------------------------------------------------------

static int _udp_receive(int fd, uint8_t* data, union sockaddr_all* addr)
{
    int datalen = 0;
    int fromlen = sizeof(*addr);

    datalen = sm_udp_Recv(fd, (char*)data,
                          OC_PDU_SIZE, 0, &addr->s, &fromlen);
    return 	datalen;
}

static int net_udp_send(int fd, union sockaddr_all* addr, const uint8_t* data, int datalen)
{
    return sm_udp_Send(fd, (char*)data, datalen, (struct sockaddr*)&addr->s);
}

//--------------------------------------------------------------------
static int _udp_open(net_addr_t* addr)
{
    int fd = sm_create_Sock(0);

    if(fd > 0) {
        OSAL_DBG("net_endpoint_open_udp_client fd=%d", fd);
        if((int)addr->addr.v4.sin_addr.s_addr == -1) {
            sm_set_Sock_BoardCast(fd);
        }
    }
    return fd;
}

static void _udp_close(net_udp_t* ctx)
{
    osal_ep_unbind(&ctx->ep);

    sm_close_Socket(ctx->ep.fd);
    ctx->ep.fd = -1;
}

static void cb_on_recv(void* args, int fd)
{
    net_udp_t* ctx = args;
    union sockaddr_all addr;
    uint8_t buff[OC_PDU_SIZE];
    int len = _udp_receive(fd, buff, &addr);
    if(len > 0) {

        if(ctx->cb) {
            ctx->cb(ctx, &addr, buff, len);
        }
    }
}
/**
 * client open
 * 192.168.1.1:8000
*/
net_udp_t* net_udp_cli_open(const char* endpoint_str, udp_recv cb, void* args)
{
    net_addr_t addr;

    if(endpoint_str == NULL) {
        return NULL;
    }

    if(!net_string_to_endpoint(endpoint_str, &addr)) {

        net_udp_t* ctx = osal_mem_alloc(sizeof(*ctx));
        if(ctx == NULL) {
            return NULL;
        }

        ctx->args       = args;
        ctx->addr       = addr;
        ctx->addr.flags = CLIENT;
        ctx->cb         = cb;

        ctx->ep.fd      = _udp_open(&ctx->addr);
        ctx->ep.cb_send = NULL;
        ctx->ep.cb_recv = cb_on_recv;
        ctx->ep.args    = ctx;

        osal_ep_bind(&ctx->ep);
        OSAL_DBG("net_udp_new id=%d\n", ctx->ep.fd);

        return ctx;
    }
    OSAL_ERR("error url=%s\n", endpoint_str);
    return NULL;
}
/**
 *
 * stop
 *
*/
void net_udp_close(net_udp_t* ctx)
{
    OSAL_DBG("net_udp_free id=%d\n", ctx->ep.fd);
    if(ctx == NULL) {
        return ;
    }
    _udp_close(ctx);
    osal_mem_free(ctx);
}

int net_udp_cli_send(net_udp_t* point,  uint8_t* data, int datalen)
{
    return net_udp_send(point->ep.fd, &point->addr.addr, data, datalen);
}

net_udp_t* net_udp_srv_open(int port, udp_recv cb, void* args)
{
    net_udp_t* server = osal_mem_alloc(sizeof(*server));
    if(server == NULL) {
        return NULL;
    }

    net_addr_t addr = {0};

    server->args       = args;
    server->addr       = addr;
    server->addr.flags = SERVER;
    server->cb         = cb;

    server->ep.cb_send = NULL;
    server->ep.cb_recv = cb_on_recv;
    server->ep.fd      = sm_create_Sock(0);
    server->ep.args    = server;

    sm_bind_Sock(server->ep.fd, INADDR_ANY, port);
    sm_set_Sock_BoardCast(server->ep.fd);

    osal_ep_bind(&server->ep);
    OSAL_DBG("EP.fd=%d\n", server->ep.fd);
    return server;
}


int net_udp_srv_send(net_udp_t* point, union sockaddr_all* addr,  uint8_t* data, int datalen)
{
    return net_udp_send(point->ep.fd, addr, data, datalen);
}

int net_udp_srv_straddr_send(net_udp_t* point, const char* straddr, const uint8_t* data, int datalen)
{
    net_addr_t addr;

    if(straddr == NULL) {
        return -1;
    }

    if(!net_string_to_endpoint(straddr, &addr)) {

        return net_udp_send(point->ep.fd, &addr.addr, data, datalen);
    }

    return -1;
}

