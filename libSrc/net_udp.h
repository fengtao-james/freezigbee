/**
 * james
 * net_udp.h
 *
*/
#ifndef NET_UDP_H
#define NET_UDP_H

#ifdef __cplusplus
extern "C"
{
#endif

struct net_udp;

typedef void (*udp_recv)(struct net_udp* ctx, union sockaddr_all* addr, uint8_t* data, int len);

typedef struct net_udp {
    net_addr_t addr;
    struct osal_ep ep;
    void* args;
    udp_recv cb;
} net_udp_t;

int net_udp_cli_send(net_udp_t* ctx,  uint8_t* data, int datalen);
net_udp_t* net_udp_cli_open(const char* endpoint_str, udp_recv cb, void* args);

net_udp_t* net_udp_srv_open(int port, udp_recv cb, void* args);
int net_udp_srv_send(net_udp_t* point, union sockaddr_all* addr,  uint8_t* data, int datalen);
int net_udp_srv_straddr_send(net_udp_t* point, const char* straddr,  const uint8_t* data, int datalen);

void net_udp_close(net_udp_t* ctx);

#ifdef __cplusplus
}
#endif

#endif /* FO_ENDPOINT_H */
