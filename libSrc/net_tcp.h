/**
 * james
 * net_tcp.h
 *
*/
#ifndef NET_TCP_H
#define NET_TCP_H

#ifdef __cplusplus
extern "C"
{
#endif
struct net_tcp;

typedef void (*tcp_recv)(struct net_tcp* tcp, uint8_t* data, int len);
typedef void (*tcp_notify)(struct net_tcp* tcp, enum net_type type);
typedef void (*tcp_srv_notify)(struct net_tcp* tcp, int fd, union sockaddr_all* addr);


typedef struct net_tcp {
    net_addr_t addr;
    struct osal_ep ep;
    void* args;
    union {
        struct {
            tcp_recv recv;
            tcp_notify notify;
        } cli;
        tcp_srv_notify srv_notify;
    } h; //handle
} net_tcp_t;

net_tcp_t* net_tcp_client_open(const char* endpoint_str, tcp_recv recv, tcp_notify notify, void* args);
net_tcp_t* net_tcp_child_open(int fd, tcp_recv recv, tcp_notify notify, void* args);

int net_tcp_client_reset(net_tcp_t* point);
int net_tcp_client_restart(net_tcp_t* point);
int net_tcp_send(net_tcp_t* point, uint8_t* data, int len);
void net_tcp_close(net_tcp_t* node);

net_tcp_t* net_tcp_srv_open(int port, tcp_srv_notify notify, void* args);

#ifdef __cplusplus
}
#endif

#endif /* FO_ENDPOINT_H */
