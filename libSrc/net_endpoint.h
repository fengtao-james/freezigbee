/**
 * james
 * net_endpoint.h
 *
*/
#ifndef NET_ENDPOINT_H
#define NET_ENDPOINT_H

#ifdef __cplusplus
extern "C"
{
#endif

int net_endpoint_addr_cmp(union sockaddr_all* addr1, union sockaddr_all* addr2);
int net_string_to_endpoint(const char* endpoint_str, net_addr_t* addr);
void net_endpoint_to_string(union sockaddr_all* addr, char* ip, char* port);

#ifdef __cplusplus
}
#endif

#endif /* FO_ENDPOINT_H */
