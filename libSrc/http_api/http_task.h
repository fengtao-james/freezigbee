/*File : http_task.h
 *Auth : james
 *Date : 2019.8.20
 */
#ifndef _HTTP_TASK_H
#define _HTTP_TASK_H
#include "http_parser.h"
#include "type.h"

void http_task_req(char* url,char *head, char *body,http_parser_settings *cb);

#endif