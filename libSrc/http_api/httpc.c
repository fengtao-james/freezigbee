/*File : http.c
 *Auth : james
 *Date : 20190819
 */
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "httpc.h"
#include "task_event.h"
#include "fiot_endpoint.h"


#define BUFFER_SIZE 1024
#define HTTP_POST "POST /%s HTTP/1.1\r\nHOST: %s:%d\r\nAccept: */*\r\n"\
    "Content-Type:application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s"

#define HTTP_GET "GET /%s HTTP/1.1\r\nHOST: %s:%d\r\nAccept: */*\r\n\r\n"


static int http_tcpclient_create(const char* host, int port, int task)
{
    char uri[128] = {0};
    sprintf(uri, "tcp://%s:%d", host, port);
    return fO_endpoint_open(uri, 0, task);
}

static int http_parse_url(const char* url, char* host, char* file, int* port)
{
    char* ptr1, *ptr2;
    int len = 0;
    if(!url || !host || !file || !port) {
        return -1;
    }

    ptr1 = (char*)url;

    if(!strncmp(ptr1, "http://", strlen("http://"))) {
        ptr1 += strlen("http://");
    } else {
        return -1;
    }

    ptr2 = strchr(ptr1, '/');
    if(ptr2) {
        len = strlen(ptr1) - strlen(ptr2);
        memcpy(host, ptr1, len);
        host[len] = '\0';
        if(*(ptr2 + 1)) {
            memcpy(file, ptr2 + 1, strlen(ptr2) - 1);
            file[strlen(ptr2) - 1] = '\0';
        }
    } else {
        OSAL_DBG("ptr1=%s\n", ptr1);
        memcpy(host, ptr1, strlen(ptr1));
        host[strlen(ptr1)] = '\0';
    }
    //get host and ip
    ptr1 = strchr(host, ':');
    if(ptr1) {
        *ptr1++ = '\0';
        *port = atoi(ptr1);
    } else {
        *port = HTTPC_DEFAULT_PORT;
    }

    return 0;
}


// static int http_tcpclient_recv(int socket,char *lpbuff){
//     int recvnum = 0;
//     int nleft=BUFFER_SIZE*4;
//     int size=BUFFER_SIZE*4;
//     char *ptr=lpbuff;
//     int nTryTimes=0;
//     while(nleft > 0 && nTryTimes < 2)
//     {
//          if ((recvnum = recv(socket, ptr, nleft, 0)) < 0)
// 		{
//             break;
// 		}
//         else if (recvnum == 0)
//         {
//             break;
//         }
// 		OSAL_DBG("recvnum=%d\n",recvnum);
// 		nleft -= recvnum;
//         ptr   += recvnum;
//         ++nTryTimes;
//     }
//   //  recvnum = recv(socket, lpbuff,BUFFER_SIZE*4,0);
//     OSAL_DBG("recvnum=%d\n",size-nleft);
//     return size-nleft;
// }

static int http_tcpclient_send(int socket, char* buff, int size)
{
    fO_endpoint_push(socket, buff, size);
    return 0;
}

int http_post(const char* url, const char* post_str, int task)
{

    int fd = -1;
    char lpbuf[BUFFER_SIZE * 4] = {'\0'};
    char host_addr[128] = {'\0'};
    char file[BUFFER_SIZE] = {'\0'};
    int port = 0;

    if(!url || !post_str) {
        OSAL_DBG("      failed!\n");
        return -1;
    }

    if(http_parse_url(url, host_addr, file, &port)) {
        OSAL_DBG("http_parse_url failed!\n");
        return -1;
    }
    //OSAL_DBG("host_addr : %s\tfile:%s\t,%d\n",host_addr,file,port);

    fd = http_tcpclient_create(host_addr, port, task);
    if(fd < 0) {
        OSAL_DBG("http_tcpclient_create failed\n");
        return -1;
    }

    int len = sprintf(lpbuf, HTTP_POST, file, host_addr, port, (int)strlen(post_str), post_str);

    if(http_tcpclient_send(fd, lpbuf, len) < 0) {
        OSAL_DBG("http_tcpclient_send failed..\n");
        return -1;
    }
    OSAL_DBG("SEND REQ:\n%s\n", lpbuf);

    return fd;
}

int http_get(const char* url, int task)
{

    int fd = -1;
    char lpbuf[BUFFER_SIZE] = {0};
    char host_addr[128] = {0};
    char file[BUFFER_SIZE] = {0};
    int port = 0;

    if(!url) {
        OSAL_DBG("      failed!\n");
        return -1;
    }

    if(http_parse_url(url, host_addr, file, &port)) {
        OSAL_DBG("http_parse_url failed!\n");
        return -1;
    }
    OSAL_DBG("host_addr : %s\tfile:%s\t,%d\n", host_addr, file, port);

    fd =  http_tcpclient_create(host_addr, port, task);
    if(fd < 0) {
        OSAL_DBG("http_tcpclient_create failed\n");
        return -1;
    }

    int len = sprintf(lpbuf, HTTP_GET, file, host_addr, port);

    OSAL_DBG("lpbuf=%s\n", lpbuf);

    if(http_tcpclient_send(fd, lpbuf, len) < 0) {
        OSAL_DBG("http_tcpclient_send failed..\n");
        return -1;
    }
    OSAL_DBG("SEND HTTP REQ:\n%s\n", lpbuf);

    return fd;
}

