#include "http_parser.h"
#include "http_task.h"

#include "task_event.h"
#include "fiot_endpoint.h"

#define HTTP_MSG_OUTTIME  10000

typedef struct http_endpoint {
    net_tcp_t* netep;
    osal_endpoint_t info;
    http_parser_settings cb;
    http_parser parser;
} http_ep_t;


typedef struct http_prj {
    uint32_t task;
    OSAL_SLIST(http_list);
} http_prj_t;


static http_prj_t HTTP;

static void http_task_clear_key_value(http_prj_msg_t* node)
{
    memset(node->key, 0, 64);
    memset(node->value, 0, 128);
}


// int on_message_begin(http_parser* parser)
// {
//     http_prj_msg_t* node = parser->data;
//     node->msg_event = HTTP_MSG_TYPE_INFO_BEGIN;

//     // OSAL_DBG("***MESSAGE BEGIN******\n");
//     return 0;
// }

// int on_headers_complete(http_parser* parser)
// {
//     http_prj_msg_t* node = parser->data;
//     node->msg_event = HTTP_MSG_TYPE_INFO_MESSAGE;

//     // OSAL_DBG("***HEADERS COMPLETE***\n");
//     return 0;
// }

// int on_message_complete(http_parser* parser)
// {
//     http_prj_msg_t* node = parser->data;
//     // OSAL_DBG("***MESSAGE COMPLETE***\n");

//     node->msg_event = HTTP_MSG_TYPE_MESSAGE_COMPLET;
//     if(node->cb) {
//         node->cb(node->msg_event, NULL, NULL, 0, node->arg);
//     }
//     fO_endpoint_close(node->id);
//     osal_slist_remove(HTTP.http_list, node);
//     osal_mem_free(node);
//     return 0;
// }

// int on_url(http_parser* parser, const char* at, size_t length)
// {
//     // http_prj_msg_t *node=parser->data;
//     // OSAL_DBG("Url: %.*s\n", (int)length, at);
//     return 0;
// }


// int on_status(http_parser* parser, const char* at, size_t length)
// {
//     // OSAL_DBG("status: %.*s\n", (int)length, at);
//     http_prj_msg_t* node = parser->data;

//     if(node->cb) {
//         node->cb(node->msg_event, "status", at, length, node->arg);
//     }
//     node->msg_event = HTTP_MSG_TYPE_INFO_HEADER;
//     return 0;
// }

// int on_header_field(http_parser* parser, const char* at, size_t length)
// {

//     http_prj_msg_t* node = parser->data;
//     memcpy(node->key, at, length);
//     return 0;
// }

// int on_header_value(http_parser* parser, const char* at, size_t length)
// {

//     http_prj_msg_t* node = parser->data;
//     memcpy(node->value, at, length);

//     // OSAL_DBG("key=%s:: value:%s\n",node->key, node->value);

//     if(node->cb) {
//         node->cb(node->msg_event, node->key, node->value, length, node->arg);
//     }

//     http_task_clear_key_value(node);
//     return 0;
// }

// int on_body(http_parser* parser, const char* at, size_t length)
// {
//     // OSAL_DBG("Body: %.*s\n", (int)length, at);
//     http_prj_msg_t* node = parser->data;

//     if(node->cb) {
//         node->cb(node->msg_event, "Body", at, length, node->arg);
//     }
//     return 0;
// }

// static http_parser_settings settings_cb = { // http_parser cb
//     .on_message_begin = on_message_begin
//     , .on_header_field = on_header_field
//     , .on_header_value = on_header_value
//     , .on_url = on_url
//     , .on_status = on_status
//     , .on_body = on_body
//     , .on_headers_complete = on_headers_complete
//     , .on_message_complete = on_message_complete
// };

int http_prj_req(char* method, char* uri, const char* post_str, http_msg_rsp cb, void* arg, int outtime)
{
    int id = -1;

    if(0 == strcasecmp(method, "GET")) {
        id = http_get(uri, HTTP.task);
        if(id == -1) {
            return -1;
        }
    } else {
        id = http_post(uri, post_str, HTTP.task);
        if(id == -1) {
            return -1;
        }
    }

    http_prj_msg_t* node = osal_mem_alloc(sizeof(*node));
    if(node) {
        node->id = id;
        node->cb = cb;
        node->arg = arg;
        node->msg_event = HTTP_MSG_TYPE_INIT;
        http_parser_init(&node->parser, HTTP_RESPONSE);//  init parser=Response type
        node->parser.data = node;
        http_task_clear_key_value(node);
        if(outtime > HTTP_MSG_OUTTIME) {
            node->interval = outtime;
        } else {
            node->interval = HTTP_MSG_OUTTIME;
        }

        node->time = osal_clock_time() + node->interval;
        node->next = NULL;
        osal_slist_add(HTTP.http_list, node);
    }

    if(1 == osal_slist_length(HTTP.http_list)) {
        osal_timer_start_task(HTTP.task, 0, 3000);
    }
    return 0;
}


static void http_parser_recive_msg(void* data)
{
    osal_message_t* msg = data;
    http_prj_msg_t* node = hettp_parser_find(msg->id);
    if(node) {
        node->time = osal_clock_time() + node->interval;
        http_parser_execute(&node->parser, &settings_cb, (char*)msg->data, msg->length); // 执行解析过程
    }
    osal_mem_free(data);
}
static void http_parser_time_check(void)
{
    http_prj_msg_t* node = osal_slist_head(HTTP.http_list), *next;
    osal_clock_time_t now = osal_clock_time();
    while(node) {
        next = node->next;
        if(now >= node->time) {
            osal_slist_remove(HTTP.http_list, node);

            if(node->cb) {
                node->cb(HTTP_MSG_TYPE_OUTTIME, NULL, NULL, 0, node->arg);
            }
            osal_mem_free(node);
        }
        node = next;
    }

    OSAL_DBG("http_parser_time_check\n");
    if(osal_slist_length(HTTP.http_list)) {
        osal_timer_start_task(HTTP.task, 0, 3000);
    }
}


static void http_parser_event_rsp(const int msg_event, const char* key, const char* value, size_t length, void* arg)
{
    OSAL_DBG("event=%d -->key:%s::value: %.*s\n", msg_event, key, (int)length, value);
}

static void http_parser_event_init(void)
{
    HTTP.task = queue_context_new(http_parser_event_process, NULL);

    OSAL_SLIST_INIT(&HTTP, http_list);
    http_prj_req("GET", "http://192.168.1.1:80", NULL, http_parser_event_rsp, NULL, 0);
}


static int http_task_parse_url(const char* url, char* host, char* file, int* port)
{
    char* ptr1, *ptr2;
    int len = 0;
    if(!url || !host || !file || !port) {
        return -1;
    }

    ptr1 = (char*)url;

    if(!strncmp(ptr1, "http://", strlen("http://"))) {
        ptr1 += strlen("http://");
    } else {
        return -1;
    }

    ptr2 = strchr(ptr1, '/');
    if(ptr2) {
        len = strlen(ptr1) - strlen(ptr2);
        memcpy(host, ptr1, len);
        host[len] = '\0';
        if(*(ptr2 + 1)) {
            memcpy(file, ptr2 + 1, strlen(ptr2) - 1);
            file[strlen(ptr2) - 1] = '\0';
        }
    } else {
        OSAL_DBG("ptr1=%s\n", ptr1);
        memcpy(host, ptr1, strlen(ptr1));
        host[strlen(ptr1)] = '\0';
    }
    //get host and ip
    ptr1 = strchr(host, ':');
    if(ptr1) {
        *ptr1++ = '\0';
        *port = atoi(ptr1);
    } else {
        *port = 80;
    }
    return 0;
}
static http_ep_t* http_task_new()
{

}

static void http_recv(net_tcp_t* ep, uint8_t* data, int len)
{

}

static void http_notify(net_tcp_t* ep, int type)
{
    http_ep_t* http_ep = ep->info.ptr;

    if(ENDPOINT_UP_NETWORK_EVENT == type) {
        
        http_parser_init(&http_ep->parser, HTTP_RESPONSE);//  init parser=Response type
        http_ep->parser.data = http_ep;
       

        if(http_tcpclient_send(fd, lpbuf, len) < 0) {
            OSAL_DBG("http_tcpclient_send failed..\n");
            return -1;
        }       
    } else {

    }
}

#define HTTP_FORMAT "GET /%s HTTP/1.0\r\nHOST: %s:%d\r\nAccept: */*\r\n"

//#define HTTP_POST "POST /%s HTTP/1.0\r\nHOST: %s:%d\r\nAccept: */*\r\n"\
    "Content-Type:application/x-www-form-urlencoded\r\nContent-Length: %d\r\n\r\n%s"
//#define HTTP_GET "GET /%s HTTP/1.0\r\nHOST: %s:%d\r\nAccept: */*\r\n\r\n"

void http_task_req(char *method,char* url,char *head, char *body,http_parser_settings *cb)
{
    char host_addr[128] = {'\0'};
    char file[128] = {'\0'};
    int port = 0;

    if(http_parse_url(url, host_addr, file, &port)) {
        OSAL_DBG("http_parse_url failed %s!\n", url);
        return -1;
    }

    http_ep_t* ep = http_task_new();
    if(ep) {
        char lpbuf[2048] = {'\0'};
        int len = sprintf(lpbuf, HTTP_FORMAT, file, host_addr, port, (int)strlen(post_str), post_str);
        char uri[128] = {0};

        sprintf(uri, "tcp://%s:%d", host_addr, port);
        net_cb_t HTTP_CB = {
            http_recv,
            http_notify
        };
        ep->netep = fiot_tcp_new(url, &HTTP_CB);
        if(ep->netep == NULL) {
            OSAL_ERR("ERR NET EP");
        }
        
        ep->netep->info.ptr = ep;
        ep->netep->info.free = NULL;
    }
    return ep;
}

void http_task_close(http_ep_t* ep)
{

}