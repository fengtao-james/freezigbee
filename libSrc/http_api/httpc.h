/*File : httpc.h
 *Auth : james
 *Date : 2019.8.20
 */
#ifndef _HTTPC_H
#define _HTTPC_H

#define HTTPC_DEFAULT_PORT 80

int http_post(const char* url, const char* post_str, int task);
int http_get(const char* url, int task);

#endif
