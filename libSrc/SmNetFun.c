/**************************************************************
*SMNetFun.c
*
*����:ʵ��һЩ���糣�ú����ķ�װ
*
*����:ljm
*
*����ʱ��:2009-02-12
*
*
*�޸���־:
*
*
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include "unix_time.h"

#include "SmNetFun.h"

#define SM_RETURN_FAIL -1
#define SM_RETURN_OK    0


int sm_set_Sock_Attr(int fd, int bReuseAddr, int nSndTimeO, int nRcvTimeO, int nSndBuf, int nRcvBuf)
{
    int err_ret = SM_RETURN_OK;
    struct timeval sndTo, rcvTo;

    if(fd <= 0) {
        return SM_RETURN_FAIL;
    }

    sndTo.tv_sec  = nSndTimeO / 1000;
    sndTo.tv_usec = (nSndTimeO % 1000) * 1000;

    rcvTo.tv_sec  = nRcvTimeO / 1000;
    rcvTo.tv_usec = (nRcvTimeO % 1000) * 1000;

    if(bReuseAddr != 0 && setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void*)&bReuseAddr, sizeof(int)) < 0) {
        printf("SO_REUSEADDRSO_REUSEADDRSO_REUSEADDRSO_REUSEADDRSO_REUSEADDR\n");
        err_ret = SM_RETURN_FAIL;

    }

    if(nSndTimeO != 0 && setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (void*)&sndTo, sizeof(sndTo)) < 0) {
        err_ret = SM_RETURN_FAIL;
    }
    if(nSndTimeO != 0 && setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (void*)&rcvTo, sizeof(rcvTo)) < 0) {
        err_ret = SM_RETURN_FAIL;
    }

    if(nSndBuf != 0 && setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (void*)&nSndBuf, sizeof(nSndBuf)) < 0) {
        err_ret = SM_RETURN_FAIL;
    }
    if(nRcvBuf != 0 && setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (void*)&nRcvBuf, sizeof(nSndBuf)) < 0) {
        err_ret = SM_RETURN_FAIL;
    }
    return err_ret;
}

int sm_set_Sock_NoDelay(int fd)
{
    int opt = 1;

    if(fd <= 0) {
        return SM_RETURN_FAIL;
    }
    return setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char*)&opt, sizeof(opt));
}

int sm_set_Sock_KeepAlive(int fd)
{
    int opt = 1;

    if(fd <= 0) {
        return SM_RETURN_FAIL;
    }
    return setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char*)&opt, sizeof(opt));
}


int  sm_set_Sock_BoardCast(int fd)
{
    int    so_boardcast = 1;
    return setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &so_boardcast, sizeof(so_boardcast));
}

int  sm_set_Sock_MultiCast(int fd)
{
    int opt = 0;
    return setsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP, (char*)&opt,  sizeof(char));
}

int  sm_set_Sock_Multi_MemberShip(int fd, char* multiAddr, int interfaceIp)
{
    int    ret = 0;
    struct ip_mreq	ipmreq;

    memset(&ipmreq, 0, sizeof(ipmreq));
    //add multicast membership
    ipmreq.imr_multiaddr.s_addr = inet_addr(multiAddr);
    ipmreq.imr_interface.s_addr = interfaceIp;

    ret = setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&ipmreq,
                     sizeof(ipmreq));
    return ret;
}

int sm_set_Sock_Rm_Multi_MemberShip(int fd, char* multiAddr)
{
    int    ret = 0;
    struct ip_mreq	ipmreq;

    memset(&ipmreq, 0, sizeof(ipmreq));
    //add multicast membership
    ipmreq.imr_multiaddr.s_addr = inet_addr(multiAddr);
    ipmreq.imr_interface.s_addr = htonl(0);

    ret = setsockopt(fd , IPPROTO_IP, IP_DROP_MEMBERSHIP, (char*)&ipmreq,
                     sizeof(ipmreq));
    return ret;
}

int sm_tcpListen(int fd, int backlog)
{
    char*	ptr = NULL;

    if(fd <= 0) {
        return SM_RETURN_FAIL;
    }

    /*4can override 2nd argument with environment variable */
    if((ptr = getenv("LISTENQ")) != NULL) {
        backlog = atoi(ptr);
    }

    if(listen(fd, backlog) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

int sm_set_Sock_Block(int nSock)
{
    int bBlock = 0;
    if(nSock <= 0) {
        return SM_RETURN_FAIL;
    }

    if(ioctl(nSock, FIONBIO, &bBlock) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

int sm_set_Sock_NoBlock(int nSock)
{
    int bNoBlock = 1;

    if(nSock <= 0) {
        return SM_RETURN_FAIL;
    }

    if(ioctl(nSock, FIONBIO, &bNoBlock) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

static void
sm_set_socket_keepalive(int fd)
{
    int keepalive = 1;
    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (void*)&keepalive , sizeof(keepalive));
}

int sm_tcpBlock_Recv(int sockfd, unsigned char* rcvBuf, int bufSize, int rcvSize)
{
    int nleft = 0;
    int nread = 0;
    int nTryTimes = 0;
    char* ptr;

    ptr  = (char*)rcvBuf;
    nleft = rcvSize;

    if(sockfd <= 0 || rcvBuf == NULL || bufSize <= 0) {
        return SM_RETURN_FAIL;
    }

    if(rcvSize <= 0) {
        nleft = bufSize;
    }

    while(nleft > 0 && nTryTimes < SM_MAX_BLOCK_RECV_TIME) {
        if((nread = recv(sockfd, ptr, nleft, 0)) < 0) {
            if(errno == EINTR) {
                nread = 0;
            } else {
                return SM_RETURN_FAIL;
            }
        } else if(nread == 0) {
            break;
        }

        nleft -= nread;
        ptr   += nread;
        ++nTryTimes;
        if(rcvSize <= 0 && nread > 0) {
            break;
        }
    }

    if(rcvSize <= 0) {
        return nread;
    }
    return  rcvSize - nleft;
}

int sm_tcpBlock_Send(int fd, const void* vptr, int n)
{
    int nleft;
    int nwritten;
    int nTryTimes = 0;
    const char* ptr;

    if(fd <= 0 || vptr == NULL || n <= 0) {
        return SM_RETURN_FAIL;
    }

    ptr = vptr;
    nleft = n;
    while(nleft > 0 && nTryTimes < SM_MAX_BLOCK_SEND_TIME) {
        if((nwritten = send(fd, ptr, nleft, 0)) <= 0) {
            if(nwritten < 0 && errno == EINTR) {
                nwritten = 0;   /* and call write() again */
                nTryTimes = -1;
            } else {
                return (SM_RETURN_FAIL);    /* error */
            }
        }

        nleft -= nwritten;
        ptr += nwritten;
        ++nTryTimes;
    }
    return (n - nleft);
}

int sm_tcpBlock_Accept(int fd, struct sockaddr* sa, socklen_t* salenptr)
{
    int n = SM_RETURN_FAIL;
    char szIp[32] = {0};
again:
    if((n = accept(fd, sa, (socklen_t*)salenptr)) < 0) {
        if(errno == ECONNABORTED) {
            goto again;
        }
    }
    if(sa != NULL)
        printf("RECV REMOTE CONNECT %s\n",
               inet_ntop(AF_INET, &((struct   sockaddr_in*)sa)->sin_addr, szIp, sizeof(szIp)));
    return (n);
}

int sm_tcpBlock_Connect_Ex(struct sockaddr_in* addr, int* flag)
{
    struct sockaddr_in  r = *addr;
    int sockfd = sm_create_Sock(1);

    int addrLen = sizeof(struct sockaddr);

    if(sockfd < 0) {
        return -1;
    }

    if(connect(sockfd, (struct sockaddr*)&r, addrLen) == 0) {
        *flag = 0;
        return sockfd;
    }
    sm_close_Socket(sockfd);
    return -1;

}

int sm_tcpNoBlock_Recv(int sockfd, uint8_t* rcvBuf, int bufSize, int rcvSize)
{
    int		ret;
    int	dwRecved = 0;
    int  	nTryTime = 0;
    int                 nSize    = rcvSize;

    if(sockfd <= 0 || rcvBuf == NULL || bufSize <= 0) {
        return -1;
    }

    if(rcvSize <= 0) {
        nSize = bufSize;
    }

    while(dwRecved < nSize) {
        ret = recv(sockfd, rcvBuf + dwRecved, nSize - dwRecved, 0);
        if(0 == ret) {
            return -1;
        } else if(ret < 1) {
            if(ECONNRESET == errno) {
                return -1;
            } else if(EWOULDBLOCK == errno  || errno == EINTR || errno == EAGAIN) {
                if(nTryTime++ < SM_MAX_NOBLOCK_RECV_TIME) {
                    usleep(10000);
                    continue;
                } else {
                    break;
                }
            }

            return -1;
        }
        nTryTime = 0;
        dwRecved += ret;
        if(rcvSize <= 0) {
            break;
        }
    }
    return dwRecved;
}


int sm_tcpNoBlock_Send(int hSock, uint8_t* pbuf, int size, int* pBlock)
{
    int  block = 0;
    int  alllen = size;
    int  sended = 0;
    unsigned long nTime = unix_get_tick_count();

    if(hSock < 0 || pbuf == NULL || size <= 0) {
        return -1;
    }

    if(pBlock != NULL) {
        *pBlock = 0;
    }

    while(alllen > 0) {
        sended = send(hSock, pbuf, alllen, 0);
        if(0 == sended) {
            printf("sm_tcpNoBlock_Send send ret 0\n");
            return SM_RETURN_FAIL;
        } else if(sended < 1) {
            if(block > SM_MAX_NOBLOCK_SEND_TIME) {
                printf("sm_tcpNoBlock_Send send tims over %lu\n", unix_get_tick_count() - nTime);
                return SM_RETURN_FAIL;
            }
            if(errno == EWOULDBLOCK || errno == EINTR || errno == EAGAIN) {
                if(block++ < SM_MAX_NOBLOCK_SEND_TIME) {
                    usleep(1000);
                    continue;
                } else {
                    if(pBlock != NULL) {
                        *pBlock = 1;
                    }
                    printf("sm_tcpNoBlock_Send send tims over %lu\n", unix_get_tick_count() - nTime);
                    break;
                }
            }
            printf("sm_tcpNoBlock_Send send err\n");
            return SM_RETURN_FAIL;
        } else {
            // printf("sm_tcpNoBlock_Send len=%d\n", sended);
            if(pBlock != NULL) {
                *pBlock = 0;
            }
            pbuf += sended;
            alllen -= sended;
        }
    }

    if(alllen > 0) {
        printf("sm_tcpNoBlock_Send send err :%d\n", alllen);
        return SM_RETURN_FAIL;
    }
    return size;
}

int sm_tcpNoBlock_MTU_Send(int hSock, uint8_t* pbuf, int size, int mtu)
{
    int ret       = size;
    int sendLen   = 0;
    int nBlock    = 0;

    if(hSock <= 0 || pbuf == NULL || size <= 0) {
        return SM_RETURN_FAIL;
    }


    while(size > 0) {
        sendLen = size > mtu ? mtu : size;
        if(sendLen != sm_tcpNoBlock_Send(hSock, pbuf, sendLen, &nBlock)) {
            return SM_RETURN_FAIL;
        }

        pbuf += sendLen;
        size -= sendLen;
    }

    return ret;
}

int sm_tcpNoBlock_Accept(int fd, struct sockaddr* sa, socklen_t* salenptr)
{

    if(fd <= 0) {
        return SM_RETURN_FAIL;
    }

    int clientfd = sm_tcpBlock_Accept(fd, sa, salenptr);
    if(clientfd < 0) {
        return SM_RETURN_FAIL;
    }
    sm_set_socket_keepalive(clientfd);
    sm_set_Sock_NoBlock(clientfd);

    return clientfd;
}

int sm_udp_Recv(int sockfd, char* rcvBuf, int bufSize, int rcvSize, struct sockaddr* from, int* fromlen)
{
    int		ret;
    int	dwRecved = 0;
    int	nTryTime = 0;
    int             nSize    = rcvSize;

    if(rcvSize <= 0 || rcvBuf == NULL || bufSize <= 0) {
        nSize = bufSize;
    }

    while(dwRecved < nSize) {
        ret = recvfrom(sockfd, rcvBuf + dwRecved, nSize - dwRecved, 0, from, (socklen_t*)fromlen);
        if(0 == ret) {
            return SM_RETURN_FAIL;
        } else if(ret < 1) {
            if(ECONNRESET == errno) {
                return SM_RETURN_FAIL;
            } else if(EWOULDBLOCK == errno  || errno == EINTR || errno == EAGAIN) {
                if(nTryTime++ < SM_MAX_BLOCK_RECV_TIME) {
                    usleep(10000);
                    continue;
                } else {
                    break;
                }
            }

            return SM_RETURN_FAIL;
        }
        nTryTime = 0;
        dwRecved += ret;

        if(rcvSize <= 0) {
            break;
        }
    }
    return dwRecved;
}


int sm_udp_Send(int hSock, const void* pbuf, int size, struct sockaddr* distAddr)
{
    int  block  = 0;
    int  alllen = size;
    int  sended = 0;

    if(hSock <= 0 || pbuf == NULL || size <= 0 || distAddr == NULL) {
        printf("sm_udp_Send %d %d\n", hSock, size);
        return 0;
    }

    while(alllen > 0) {
        sended = sendto(hSock, pbuf, alllen, 0, distAddr, sizeof(struct sockaddr));
        if(0 == sended) {
            return SM_RETURN_FAIL;
        } else if(sended < 1) {
            if(block > SM_MAX_NOBLOCK_SEND_TIME) {
                return SM_RETURN_FAIL;
            }
            if(errno == EWOULDBLOCK || errno == EINTR || errno == EAGAIN) {
                if(block++ < SM_MAX_BLOCK_SEND_TIME) {
                    usleep(1000);
                    continue;
                } else {
                    break;
                }
            }
            return SM_RETURN_FAIL;
        } else {
            pbuf += sended;
            alllen -= sended;
        }
    }

    if(alllen > 0) {
        return SM_RETURN_FAIL;
    }
    return size;
}

unsigned short cal_chksum(unsigned short* addr, int len)
{
    int leave = len;
    int sum   = 0;
    unsigned short* w = addr;
    unsigned short answer = 0;

    while(leave > 1) {
        sum += *w++;
        leave -= 2;
    }

    if(leave == 1) {
        *(unsigned char*)(&answer) = *(unsigned char*)w;
        sum += answer;
    }

    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    answer = ~sum;

    return answer;
}


int sm_get_Host_Ip(int af, char* host)
{
    unsigned long dwIp = 0;
    int           ret  = 0;
    struct addrinfo hints, *res;

    if(host == NULL) {
        return SM_RETURN_FAIL;
    }
    if(strcmp(host, "255.255.255.255") == 0 || strcmp(host, "") == 0) {
        return SM_RETURN_FAIL;
    }

    bzero(&hints, sizeof(struct addrinfo));
    hints.ai_flags  = AI_CANONNAME;   /* always return canonical name */
    hints.ai_family = AF_UNSPEC;   /* AF_UNSPEC, AF_INET, AF_INET6, etc. */
    hints.ai_socktype = 0;  /* 0, SOCK_STREAM, SOCK_DGRAM, etc. */
    if((ret = getaddrinfo(host, NULL, &hints, &res)) != 0) {
        return SM_RETURN_FAIL;
    }
    dwIp = ((struct sockaddr_in*)(res->ai_addr))->sin_addr.s_addr;

    freeaddrinfo(res);

    return dwIp;
}


int sm_close_Socket(int pSock)
{
    if(pSock > 0) {
//		shutdown(*pSock, 2); //ljm close 2010-07-15
        close(pSock);
        return SM_RETURN_OK;
    }
    return SM_RETURN_FAIL;
}


int sm_create_Sock(int iType)
{
    return socket(AF_INET, iType == 1 ? SOCK_STREAM : SOCK_DGRAM, 0);
}

int sm_bind_Sock(int sockfd, int ip, int nPort)
{
    struct sockaddr_in addr_local;
    memset(&addr_local, 0, sizeof(struct sockaddr_in));

    addr_local.sin_family      = AF_INET;
    addr_local.sin_addr.s_addr = htonl(ip);
    addr_local.sin_port        = htons(nPort);

    sm_set_Sock_Attr(sockfd, 1, 0, 0, 0, 0);  //set the sock reuser_addr attribute
    if(bind(sockfd, (struct sockaddr*)&addr_local, sizeof(struct sockaddr_in)) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

int sm_sock_Set_LINGER(int sockfd)
{
    struct linger nLinger;

    if(sockfd <= 0) {
        return SM_RETURN_FAIL;
    }

    memset(&nLinger, 0, sizeof(struct linger));
    nLinger.l_onoff = 1;
    nLinger.l_linger = 0;

    if(setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (void*)&nLinger, sizeof(struct linger)) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

int sm_sock_Unset_LINGER(int sockfd)
{
    struct linger nLinger;

    if(sockfd <= 0) {
        return SM_RETURN_FAIL;
    }

    memset(&nLinger, 0, sizeof(struct linger));
    nLinger.l_onoff = 0;
    nLinger.l_linger = 0;

    if(setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (void*)&nLinger, sizeof(struct linger)) < 0) {
        return SM_RETURN_FAIL;
    }
    return SM_RETURN_OK;
}

int sm_sock_Bind_Interface(int sockfd, char* interfaceName)
{
    struct ifreq ifReq;
    int ret = 0;

    strncpy(ifReq.ifr_name, interfaceName, IFNAMSIZ);

    ret = setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE,
                     (char*)&ifReq, sizeof(ifReq));
    if(ret < 0) {
        printf("sm_sock_Bind_Interface bind to %s failed %s\n", interfaceName, strerror(errno));
    } else {
        printf("sm_sock_Bind_Interface bind to %s ok\n", interfaceName);
    }
    return ret;
}

int sm_get_Sock_Ip(int sock)
{
    struct sockaddr_in sockAddr;
    int addrLen = sizeof(struct sockaddr);

    if(0 != getsockname(sock, (struct sockaddr*)&sockAddr, (socklen_t*)&addrLen)) {
        return SM_RETURN_FAIL;
    }
    return sockAddr.sin_addr.s_addr;
}

int sm_get_Sock_Port(int sock)
{
    struct sockaddr_in sockAddr;
    int addrLen = sizeof(struct sockaddr);

    if(0 != getsockname(sock, (struct sockaddr*)&sockAddr, (socklen_t*)&addrLen)) {
        return SM_RETURN_FAIL;
    }
    return htons(sockAddr.sin_port);
}

unsigned long sm_ip_N2A(unsigned long ip, char* ourIp, int len)
{
    if(!ourIp) {
        return SM_RETURN_FAIL;
    }

    memset(ourIp, 0, len);
    //ip = ntohl(ip);

    inet_ntop(AF_INET, &ip, ourIp, len);
    return SM_RETURN_OK;
}

unsigned long  sm_ip_A2N(char* szIp)
{
    int nIp = 0;
    if(!szIp) {
        return 0xFFFFFFFF;
    }
    inet_pton(AF_INET, szIp, &nIp);
    return ntohl(nIp);
}

unsigned long sm_ip_Ext_A2N(char* pstrIP)
{
    #if 0
    unsigned long ret;
    struct hostent* hostinfo;
    struct sockaddr_in address;

    ret = htonl(sm_ip_A2N(pstrIP));

    if(0xFFFFFFFF == ret) {
        if(strcmp(pstrIP, "255.255.255.255") == 0) {
            return 0xFFFFFFFF;
        }

        if(!(hostinfo = gethostbyname(pstrIP))) {
            return 0;
        }

        address.sin_addr = *((struct in_addr*)(*(hostinfo->h_addr_list)));
        ret = address.sin_addr.s_addr;
    }

    return ret;
    #endif
    return 0;
}

void  reverse(char   s[])
{
    int   c,   i,   j;

    for(i   =   0,   j   =   strlen(s) - 1;   i   <   j;   i++,   j--) {
        c   =   s[i];
        s[i]   =   s[j];
        s[j]   =   c;
    }
}

void   SM_itoa(int   n,   char   s[])
{
    int   i,   sign;

    if((sign = n) < 0) {    /*   record   sign   */
        n = -n;    /*   make   n   positive   */
    }
    i = 0;
    do {            /*   generate   digits   in   reverse   order   */
        s[i++] = n % 10 + '0';     /*   get   next   digit   */
    } while((n /= 10) > 0);          /*   delete   it   */

    if(sign < 0) {
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

