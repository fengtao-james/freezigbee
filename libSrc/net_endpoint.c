/*
// Copyright (c) 2019 Intel Corporation
//
//endpoint manager
// james
*/
#include "task_event.h"
#include <netdb.h>
#include "net_config.h"
#include "net_endpoint.h"

#define FO_SCHEME_UDP   "udp://"
#define FO_SCHEME_UDPS  "udps://"
#define FO_SCHEME_TCP   "tcp://"
#define FO_SCHEME_TCPS  "tcps://"
#define FO_SCHEME_HTTP  "http://"
#define FO_SCHEME_HTTPS "https://"

int net_endpoint_addr_cmp(union sockaddr_all* addr1, union sockaddr_all* addr2)
{
    if(addr1->v4.sin_family == AF_INET
            && addr1->v4.sin_addr.s_addr == addr2->v4.sin_addr.s_addr
            && addr1->v4.sin_port == addr2->v4.sin_port) {
        return 0;
    } else if(addr1->v6.sin6_family == AF_INET6
              && addr1->v6.sin6_port == addr2->v6.sin6_port
              && (0 == memcmp(&addr1->v6.sin6_addr,
                              &addr2->v6.sin6_addr, sizeof(addr1->v6.sin6_addr)))) {
        return 0;
    }
    return 1;
}

//---------------------------------------------------------------------------
static int net_parse_endpoint_string(const char* endpoint_str, net_addr_t* addr)
{
    if(!endpoint_str || !addr) {
        return -1;
    }

    const char* address = NULL;
    addr->flags = 0;
    size_t len  = strlen(endpoint_str);

    if(len > strlen(FO_SCHEME_TCPS) &&
            memcmp(FO_SCHEME_TCPS, endpoint_str, strlen(FO_SCHEME_TCPS)) == 0) {
        address = endpoint_str + strlen(FO_SCHEME_TCPS);
        addr->flags = SECURED;
        addr->proto = TCP;
    } else if(len > strlen(FO_SCHEME_TCP) &&
              memcmp(FO_SCHEME_TCP, endpoint_str, strlen(FO_SCHEME_TCP)) == 0) {
        address = endpoint_str + strlen(FO_SCHEME_TCP);
        addr->proto = TCP;
        addr->flags = 0;
    } else if(len > strlen(FO_SCHEME_HTTP) &&
              memcmp(FO_SCHEME_HTTP, endpoint_str, strlen(FO_SCHEME_HTTP)) == 0) {
        address = endpoint_str + strlen(FO_SCHEME_HTTP);
        addr->proto = TCP;
        addr->flags = 0;
    } else if(len > strlen(FO_SCHEME_UDPS) &&
              memcmp(FO_SCHEME_UDPS, endpoint_str, strlen(FO_SCHEME_UDPS)) == 0) {
        address = endpoint_str + strlen(FO_SCHEME_UDPS);
        addr->flags = SECURED;
        addr->proto = UDP;
    } else if(len > strlen(FO_SCHEME_UDP) &&
              memcmp(FO_SCHEME_UDP, endpoint_str, strlen(FO_SCHEME_UDP)) == 0) {
        address = endpoint_str + strlen(FO_SCHEME_UDP);
        addr->proto = UDP;
        addr->flags = 0;
    } else {
        return -1;
    }

    len = strlen(endpoint_str) - (address - endpoint_str);

    /* Extract the port # if avilable */
    const char* p = NULL;
    p = memchr(address, ':', len);

    uint16_t port = 0;
    if(p) {
        /* Extract port # from string */
        port = (uint16_t)strtoul(p + 1, NULL, 10);
    } else {
        OSAL_WRN("PORT IS ERROR!");
    }
    /* At this point 'p' points to the location immediately following
    * the last character of the address
    */
    addr->addr.v4.sin_port = htons(port);
    int iplen = p - address;

    char domain[iplen + 1];
    strncpy(domain, address, iplen);
    domain[iplen] = '\0';

    OSAL_DBG("IP=%s\n", domain);
    addr->addr.v4.sin_addr.s_addr = sm_get_Host_Ip(0, domain);
    addr->addr.v4.sin_family      = AF_INET;

    return 0;
}

int net_string_to_endpoint(const char* endpoint_str, net_addr_t* addr)
{
    return net_parse_endpoint_string(endpoint_str, addr);
}

//-------------------------------------------------------------------------
void net_endpoint_to_string(union sockaddr_all* addr, char* ip, char* port)
{
    if(addr) {
        sm_ip_N2A(addr->v4.sin_addr.s_addr, ip, 22);

        if(port) {
            sprintf(port, "%d", ntohs(addr->v4.sin_port));
        }
        // OSAL_DBG("endpoint IP:%s port:%s", ip, port);
    }
}

