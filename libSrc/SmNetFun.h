#ifndef _SM_NET_FUN_H
#define _SM_NET_FUN_H

#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define LISTENQ 16
#define SM_MAX_BLOCK_SEND_TIME 3
#define SM_MAX_BLOCK_RECV_TIME 3

#define SM_MAX_NOBLOCK_RECV_TIME 500
#define SM_MAX_NOBLOCK_SEND_TIME 2000
#define SM_MAX_NOBLOCK_CHECK_SND_TIME 100 

int sm_set_Sock_Attr(int fd, int bReuseAddr, int nSndTimeO, int nRcvTimeO, int nSndBuf, int nRcvBuf);

int sm_set_Sock_NoDelay(int fd);

int sm_set_Sock_Block(int nSock);

int sm_set_Sock_NoBlock(int nSock);

int sm_tcpBlock_Connect_Ex(struct sockaddr_in* addr, int* flag);

int sm_tcpBlock_Recv(int sockfd, unsigned char* rcvBuf, int bufSize, int rcvSize);

int sm_tcpBlock_Send(int fd, const void* vptr, int n);

int sm_tcpBlock_Accept(int fd, struct sockaddr* sa, socklen_t* salenptr);

int sm_tcpNoBlock_Connect(const char* localHost, const char* localServ, const char* dstHost, const char* dstServ, int timeout_ms);

int sm_tcpNoBlock_Recv(int sockfd, uint8_t* rcvBuf, int bufSize, int rcvSize);

int sm_tcpNoBlock_Send(int hSock, uint8_t* pbuf, int size, int* pBlock);

int sm_tcpNoBlock_MTU_Send(int hSock, uint8_t* pbuf, int size, int mtu);

int sm_tcpNoBlock_Accept(int fd, struct sockaddr* sa, socklen_t* salenptr);

int sm_udp_Recv(int sockfd, char* rcvBuf, int bufSize, int rcvSize, struct sockaddr* from, int* fromlen);

int sm_udp_Send(int hSock, const void* pbuf, int size, struct sockaddr* distAddr);

int sm_create_Sock(int iType);

int sm_get_Host_Ip(int af, char* host);

int sm_close_Socket(int fd);
//
int sm_bind_Sock(int sockfd, int ip, int nPort);

int sm_get_Sock_Ip(int sock);

int sm_get_Sock_Port(int sock);

int sm_set_Sock_KeepAlive(int fd);

int  sm_set_Sock_BoardCast(int fd);

int  sm_set_Sock_MultiCast(int fd);

int  sm_set_Sock_Multi_MemberShip(int fd, char* multiAddr, int interfaceIp);

int sm_set_Sock_Rm_Multi_MemberShip(int fd, char* multiAddr);

int sm_sock_Bind_Interface(int sockfd, char* interfaceName);

unsigned long sm_ip_N2A(unsigned long ip, char* ourIp, int len);

unsigned long sm_ip_A2N(char* szIp);

unsigned long sm_ip_Ext_A2N(char* pstrIP);

void   SM_itoa(int   n,   char   s[]);

int sm_sock_Set_LINGER(int sockfd);

int sm_sock_Unset_LINGER(int sockfd);

int sm_tcpListen(int fd, int backlog);
#endif

