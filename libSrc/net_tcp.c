/*
// Copyright (c) 2019 Intel Corporation
//
//endpoint manager
// james
*/
#include "net_config.h"
#include "net_endpoint.h"
#include "net_tcp.h"

//------------------------------------------------------------------

static int _tcp_client_open(net_addr_t* addr)
{
    int status = 1;
    int fd = sm_tcpBlock_Connect_Ex((struct sockaddr_in*)&addr->addr.s, &status);

    if(fd > 0) {
        addr->flags = CLIENT;

        if(!status) {
            addr->flags |= CONNECT;
            OSAL_DBG("net_endpoint_open_tcp_client CONNECT=%02x\n", addr->flags);
        } else {
            OSAL_DBG("net_endpoint_open_tcp_client NOT CONNECT\n");
        }
    }
    return fd;
}

static void _tcp_close(net_tcp_t* point)
{
    osal_ep_unbind(&point->ep);
    sm_close_Socket(point->ep.fd);

    point->addr.flags &= ~(CONNECT);
    point->ep.fd       = -1;
}

static int _tcp_send(int fd, uint8_t* data, int datalen)
{
    return sm_tcpNoBlock_Send(fd, data, datalen, NULL);
}

static int _tcp_recv(int fd, uint8_t* data)
{
    return sm_tcpNoBlock_Recv(fd, data, OC_PDU_SIZE, 0);
}

static void cb_on_recv(void* args, int fd)
{
    net_tcp_t* point = args;
    if(0 == (point->addr.flags & CONNECT)) {
        return;
    }
    if(point->addr.flags & SERVER) {

        union sockaddr_all addr;
        socklen_t len = sizeof(addr);

        int client_fd = sm_tcpNoBlock_Accept(fd, &addr.s, &len);
        if(client_fd < 0) {
            OSAL_ERR("accept error");
            return;
        }

        if(point->h.srv_notify) {
            point->h.srv_notify(point, client_fd, &addr);
        }
        return;
    }

    uint8_t buff[OC_PDU_SIZE];
    int len = _tcp_recv(fd, buff);
    if(len < 0) {
        _tcp_close(point);
        if(point->h.cli.notify) {
            point->h.cli.notify(point, ENDPOINT_DOWN_NETWORK_EVENT);
        }
        return;
    }
    if(len > 0) {
        if(point->h.cli.recv) {
            point->h.cli.recv(point, buff, len);
        }
    }
}

/**
 * client open
 *
*/
net_tcp_t* net_tcp_client_open(const char* endpoint_str, tcp_recv recv, tcp_notify notify, void* args)
{
    net_addr_t addr;
    if(endpoint_str == NULL) {
        return NULL;
    }

    if(!net_string_to_endpoint(endpoint_str, &addr)) {
        int fd = _tcp_client_open(&addr);
        if(fd < 0) {
            OSAL_ERR("tcp open error", fd);
            return NULL;
        }

        net_tcp_t* point = osal_mem_alloc(sizeof(*point));
        if(point == NULL) {
            sm_close_Socket(fd);
            OSAL_ERR("tcp open error", fd);
            return NULL;
        }

        point->h.cli.recv       = recv;
        point->h.cli.notify     = notify;
        point->args             = args;

        point->ep.fd      = fd;
        point->ep.cb_send = NULL;
        point->ep.cb_recv = cb_on_recv;
        point->ep.args    = point;
        point->addr       = addr;

        point->addr.flags |= CLIENT;

        osal_ep_bind(&point->ep);

        return point;
    }
    OSAL_ERR("error url=%s\n", endpoint_str);
    return NULL;
}

void net_tcp_close(net_tcp_t* point)
{

    _tcp_close(point);
    osal_mem_free(point);
}

int net_tcp_client_reset(net_tcp_t* point)
{
    if(point->ep.fd > 0) {
        _tcp_close(point);
    }

    point->ep.fd = _tcp_client_open(&point->addr);

    if(point->ep.fd > 0) {
        osal_ep_bind(&point->ep);
        return 0;
    }

    return -1;
}


int net_tcp_send(net_tcp_t* point, uint8_t* data, int len)
{
    if(0 == (point->addr.flags & CONNECT)) {
        return -1;
    }
    int ret = _tcp_send(point->ep.fd, data, len);
    if(ret < 0) {
        _tcp_close(point);
        point->h.cli.notify(point, ENDPOINT_DOWN_NETWORK_EVENT);
    }
    return ret;
}
//---------------------------------------------------------------------

net_tcp_t* net_tcp_srv_open(int port, tcp_srv_notify notify, void* args)
{
    net_addr_t addr  = {0};
    int fd = sm_create_Sock(1);
    if(fd < 0) {
        OSAL_ERR("socet error");
        return NULL;
    }
    OSAL_DBG("tcp srv fd:%d", fd);

    if(0 > sm_bind_Sock(fd, INADDR_ANY, port)) {
        OSAL_ERR("bind error");
        sm_close_Socket(fd);
        return NULL;
    }

    if(0 > sm_tcpListen(fd, LISTENQ)) {
        OSAL_ERR("listen error");
        sm_close_Socket(fd);
        return NULL;
    }

    net_tcp_t* point = osal_mem_alloc(sizeof(*point));
    if(point == NULL) {
        OSAL_ERR("mem error");
        sm_close_Socket(fd);
        return NULL;
    }
    point->h.srv_notify     = notify;
    point->args             =  args;
    point->addr        = addr;
    point->addr.flags |= CONNECT | SERVER;

    point->ep.fd      = fd;
    point->ep.cb_send = NULL;
    point->ep.cb_recv = cb_on_recv;
    point->ep.args    = point;

    osal_ep_bind(&point->ep);
    return point;
}

net_tcp_t* net_tcp_child_open(int fd, tcp_recv recv, tcp_notify notify, void* args)
{
    net_addr_t addr = {0};
    if(fd < 0) {
        OSAL_ERR("fd is error")
        return NULL;
    }

    net_tcp_t* point = osal_mem_alloc(sizeof(*point));
    if(point == NULL) {
        OSAL_ERR("error mem");
        sm_close_Socket(fd);
        return NULL;
    }
    point->h.cli.recv       = recv;
    point->h.cli.notify     = notify;
    point->args             = args;
    point->addr             = addr;
    point->addr.flags       |= CONNECT | CLIENT;

    point->ep.fd      = fd;
    point->ep.cb_send = NULL;
    point->ep.cb_recv = cb_on_recv;
    point->ep.args    = point;

    osal_ep_bind(&point->ep);
    return point;

}

