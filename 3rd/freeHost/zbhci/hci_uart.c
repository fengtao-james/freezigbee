#include "task_event.h"
#include "attr_api.h"
#include "zbhci.h"

cycle_fifo_t uart_fifo = {0};

enum {
    start_step_head   = 1,
    start_step_length = 2
};

typedef struct {
    int start_step;
    uint16_t msgtype;
    uint16_t pktlen;
    uint8_t uartRxBuf[UART_RX_BUF_SIZE];
} zbhci_msg_t;

zbhci_msg_t zbhci_rx = {0};
zbhci_cb_t    hci_cb = {0};
//-------------------------------------------------

void uart_tx_start(uint8_t* buff, uint16_t Length)
{
    if(hci_cb.send_cb) {
        hci_cb.send_cb(buff, Length);
    }
}

/*
 * @breif:   crc8 calculate
 *
 * */
static uint8_t crc8Calculate(uint16_t type, uint16_t length, uint8_t* data)
{
    int n;
    uint8_t crc8;

    crc8  = (type   >> 0) & 0xff;
    crc8 ^= (type   >> 8) & 0xff;
    crc8 ^= (length >> 0) & 0xff;
    crc8 ^= (length >> 8) & 0xff;

    for(n = 0; n < length; n++)	{
        crc8 ^= data[n];
    }
    return crc8;
}

static void uart_txMsg(uint16_t type, uint16_t length, uint8_t* puint8_tData)
{
    int n;
    uint8_t uartTxBuf[UART_TX_BUF_SIZE] = {0};
    uint8_t crc8 = crc8Calculate(type, length, puint8_tData);

    uint8_t* p = uartTxBuf;
    *p++ = 0x55;
    *p++ = (type >> 0) & 0xff;
    *p++ = (type >> 8) & 0xff;
    *p++ = (length >> 0) & 0xff;
    *p++ = (length >> 8) & 0xff;
    *p++ = crc8;
    for(n = 0; n < length; n++) {
        *p++ = puint8_tData[n];
    }
    *p++ = 0xAA;

    uart_tx_start(uartTxBuf, p - uartTxBuf);
}
/*
 * @breif:   data send by HCI
 *
 * */
zbhciTx_e zbhciTx(uint16_t type, uint16_t length, uint8_t* data)
{
    uart_txMsg(type, length, data);
    return ZBHCI_TX_SUCCESS;
}

static void hci_cleardata(void)
{
    memset(&zbhci_rx, 0, sizeof(zbhci_rx));
    cycle_fifo_reset(&uart_fifo);
}

void uart_rx_start(uint8_t* buff, uint16_t length)
{
    uint8_t* p = zbhci_rx.uartRxBuf;

    uint16_t dlen = length + cycle_fifo_len(&uart_fifo);

    if(dlen > uart_fifo.size) {
        hci_cleardata();
        return;
    }

    cycle_fifo_put(&uart_fifo, buff, length);

    if(!zbhci_rx.start_step) {
        while(cycle_fifo_len(&uart_fifo)) {

            if(p[0] == ZBHCI_MSG_START_FLAG) {
                zbhci_rx.start_step = start_step_head;
                break;
            }
            cycle_fifo_get(&uart_fifo, p, 1);
        }
    }

    if(!zbhci_rx.start_step) {
        return;
    }

    if((zbhci_rx.start_step == start_step_head)) {
        if(cycle_fifo_len(&uart_fifo) < ZBHCI_MSG_HDR_LEN - 1) {
            return;
        } else {
            cycle_fifo_get(&uart_fifo, p + 1, ZBHCI_MSG_HDR_LEN - 1);

            zbhci_rx.msgtype    = ZNC_RTN_U16(p, 1);
            zbhci_rx.pktlen     = ZNC_RTN_U16(p, 3);
            zbhci_rx.start_step = start_step_length;
        }
    }

    if(zbhci_rx.pktlen > 128) {
        hci_cleardata();
        return;
    }

    if(zbhci_rx.pktlen <= cycle_fifo_len(&uart_fifo)) {

        cycle_fifo_get(&uart_fifo,
                       p + ZBHCI_MSG_HDR_LEN,
                       zbhci_rx.pktlen);

        // char test_buf[128] = {0};
        // OSAL_DBG("uart rx = %s", osal_log_hexstr(test_buf,
        //          zbhci_rx.uartRxBuf,
        //          zbhci_rx.pktlen + ZBHCI_MSG_HDR_LEN));

        zbhciCmdHandler(zbhci_rx.msgtype, zbhci_rx.pktlen , p + 6);
        memset(&zbhci_rx, 0, sizeof(zbhci_rx));
    }
}

void zbhci_uart_init(zbhci_cb_t* cb)
{
    cycle_fifo_init(&uart_fifo);
    hci_cb = *cb;
}