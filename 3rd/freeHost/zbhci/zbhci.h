/********************************************************************************************************
 * @file     zbhci.h
 *
 * @brief	 zbhci header file
 *
 * @author
 * @date     June. 10, 2017
 *
 * @par      Copyright (c) 2016, Telink Semiconductor (Shanghai) Co., Ltd.
 *           All rights reserved.
 *
 *			 The information contained herein is confidential and proprietary property of Telink
 * 		     Semiconductor (Shanghai) Co., Ltd. and is available under the terms
 *			 of Commercial License Agreement between Telink Semiconductor (Shanghai)
 *			 Co., Ltd. and the licensee in separate contract or the terms described here-in.
 *           This heading MUST NOT be removed from this file.
 *
 * 			 Licensees are granted free, non-transferable use of the information in this
 *			 file under Mutual Non-Disclosure Agreement. NO WARRENTY of ANY KIND is provided.
 *
 *******************************************************************************************************/
#ifndef ZBHCI_H
#define	ZBHCI_H

#include <stdint.h>

/** Macro to send a log message to the host machine
 *  First byte of the message is the level (0-7).
 *  Remainder of message is char buffer containing ascii message
 */

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

#define ZBHCI_MSG_STATUS_ERROR_START_CHAR    0xe0
#define ZBHCI_MSG_STATUS_MSG_OVERFLOW        0xe1
#define ZBHCI_MSG_STATUS_ERROR_END_CHAR      0xe2
#define ZBHCI_MSG_STATUS_BAD_MSG             0xe3
#define ZBHCI_MSG_STATUS_UART_EXCEPT         0xe4


#define	ZBHCI_MSG_START_FLAG				 0x55
#define	ZBHCI_MSG_END_FLAG					 0xAA
#define	ZBHCI_MSG_HDR_LEN					 0x07

#define UART_TX_BUF_SIZE    128
#define UART_RX_BUF_SIZE    128

typedef enum {
    ZBHCI_GET_MAC_ADDR_MODE,
    ZBHCI_NORMAL_MODE
} workingMode_e;

typedef enum {
    ZBHCI_CMD_BDB_COMMISSION_FORMATION  	= 0x0001,
    ZBHCI_CMD_BDB_COMMISSION_STEER      	,//= 0x0002,
    ZBHCI_CMD_BDB_COMMISSION_TOUCHLINK  	,//= 0x0003,
    ZBHCI_CMD_BDB_COMMISSION_FINDBIND   	,//= 0x0004,
    ZBHCI_CMD_BDB_FACTORY_RESET         	,//= 0x0005,
    ZBHCI_CMD_BDB_PRE_INSTALL_CODE      	,//= 0x0006,
    ZBHCI_CMD_BDB_CHANNEL_SET               ,//= 0x0007,
    ZBHCI_CMD_BDB_DONGLE_WORKING_MODE_SET   ,//= 0x0008,
    ZBHCI_CMD_BDB_NODE_DELETE               ,//= 0x0009,
    ZBHCI_CMD_BDB_TX_POWER_SET              ,//= 0x000A,
    ZBHCI_CMD_BDB_ALLOC_JOIN                ,//= 0x000B,
    ZBHCI_CMD_BDB_SYS_RESET         	    ,//= 0x000C,
    ZBHCI_CMD_BDB_ADD_GROUP         	    ,//= 0x000D,
    ZBHCI_CMD_BDB_DEL_GROUP         	    ,//= 0x000E,
    ZBHCI_CMD_BDB_DELALL_GROUP          	,//= 0x000F,
    ZBHCI_CMD_ACK  							= 0x8000,
    ZBHCI_CMD_BDB_NET_OK  					,
    ZBHCI_CMD_BDB_NET_NOT  					,

    ZBHCI_CMD_DISCOVERY_NWK_ADDR_REQ		= 0x0010,
    ZBHCI_CMD_DISCOVERY_IEEE_ADDR_REQ   	,//= 0x0011,
    ZBHCI_CMD_DISCOVERY_NODE_DESC_REQ		,//= 0x0012,
    ZBHCI_CMD_DISCOVERY_SIMPLE_DESC_REQ		,//= 0x0013,
    ZBHCI_CMD_DISCOVERY_MATCH_DESC_REQ		,//= 0x0014,
    ZBHCI_CMD_DISCOVERY_ACTIVE_EP_REQ		,//= 0x0015,
    ZBHCI_CMD_DISCOVERY_LEAVE_REQ			,//= 0x0016,

    ZBHCI_CMD_DISCOVERY_NWK_ADDR_RSP		= 0x8010,
    ZBHCI_CMD_DISCOVERY_IEEE_ADDR_RSP   	,//= 0x0011,
    ZBHCI_CMD_DISCOVERY_NODE_DESC_RSP		,//= 0x0012,
    ZBHCI_CMD_DISCOVERY_SIMPLE_DESC_RSP		,//= 0x0013,
    ZBHCI_CMD_DISCOVERY_MATCH_DESC_RSP		,//= 0x0014,
    ZBHCI_CMD_DISCOVERY_ACTIVE_EP_RSP		,//= 0x0015,

    ZBHCI_CMD_BINDING_REQ					= 0x0020,
    ZBHCI_CMD_UNBINDING_REQ					,//= 0x0021,

    ZBHCI_CMD_BINDING_RSP					= 0x8020,
    ZBHCI_CMD_UNBINDING_RSP					,//= 0x0021,

    ZBHCI_CMD_MGMT_LQI_REQ					= 0x0030,
    ZBHCI_CMD_MGMT_BIND_REQ					,//= 0x0031,
    ZBHCI_CMD_MGMT_LEAVE_REQ				,//= 0x0032,
    ZBHCI_CMD_MGMT_DIRECT_JOIN_REQ			,//= 0x0033,
    ZBHCI_CMD_MGMT_PERMIT_JOIN_REQ			,//= 0x0034,
    ZBHCI_CMD_MGMT_NWK_UPDATE_REQ			,//= 0x0035,

    ZBHCI_CMD_MGMT_LQI_RSP					= 0x8030,
    ZBHCI_CMD_MGMT_BIND_RSP					,//= 0x0031,
    ZBHCI_CMD_MGMT_LEAVE_RSP				,//= 0x0032,
    ZBHCI_CMD_MGMT_DIRECT_JOIN_RSP			,//= 0x0033,
    ZBHCI_CMD_MGMT_PERMIT_JOIN_RSP			,//= 0x0034,
    ZBHCI_CMD_MGMT_NWK_UPDATE_RSP			,//= 0x0035,

    ZBHCI_CMD_NODES_JOINED_GET_REQ			= 0x0040,
    // ZBHCI_CMD_NODES_TOGLE_TEST_REQ			= 0x0041,
    // ZBHCI_CMD_TXRX_PERFORMANCE_TEST_REQ		= 0x0042,
    // ZBHCI_CMD_AF_DATA_SEND_TEST_REQ			= 0x0044,
    ZBHCI_CMD_BDB_PANID_SET					= 0x0043,
    ZBHCI_CMD_NODES_JOINED_GET_RSP			= 0x8040,
    // ZBHCI_CMD_NODES_TOGLE_TEST_RSP			= 0x8041,
    // ZBHCI_CMD_TXRX_PERFORMANCE_TEST_RSP		= 0x8042,
    ZBHCI_CMD_NODES_DEV_ANNCE_IND			= 0x8043,
    // ZBHCI_CMD_AF_DATA_SEND_TEST_RSP			= 0x8044,

    ZBHCI_CMD_ZCL_CMD_REQ					= 0x0100,
    ZBHCI_CMD_ZCL_CMD_RSP					= 0x8100,

    ZBHCI_CMD_ZCL_OTA_IMAGE_NOTIFY			= 0x0190,
    ZBHCI_CMD_ZCL_OTA_IMAGE_START			= 0x0191,
    ZBHCI_CMD_ZCL_OTA_IMAGE_SET 			= 0x0192,
    ZBHCI_CMD_ZCL_OTA_IMAGE_END   			= 0x8190,
    ZBHCI_CMD_ZCL_OTA_BLOCK_REQ   			= 0x8191,
    ZBHCI_CMD_ZCL_OTA_UPDATE_REQ   			= 0x8192,
    ZBHCI_CMD_DATA_CONFIRM					= 0x8200,//data confirm
    ZBHCI_CMD_MAC_ADDR_IND					= 0x8201,
    ZBHCI_CMD_NODE_LEAVE_IND				= 0x8202
} teHCI_MsgType;

/** Status message */
typedef enum {
    ZBHCI_MSG_STATUS_SUCCESS,
    ZBHCI_MSG_STATUS_INCORRECT_PARAMETERS,
    ZBHCI_MSG_STATUS_UNHANDLED_COMMAND,
    ZBHCI_MSG_STATUS_BUSY,
    ZBHCI_MSG_STATUS_NO_MEMORY,
    ZBHCI_MSG_STATUS_STACK_ALREADY_STARTED,
} zbhci_msgStatus_e;

typedef enum {
    ZBHCI_TX_SUCCESS,
    ZBHCI_TX_BUFFERFULL,
    ZBHCI_TX_BUSY,
    ZBHCI_TX_FAILED,
    ZBHCI_TX_TOO_LONG,
} zbhciTx_e;

typedef enum {
    ZBHCI_ADDRMODE_BOUND,
    ZBHCI_ADDRMODE_GROUP,
    ZBHCI_ADDRMODE_SHORT,
    ZBHCI_ADDRMODE_IEEE,
    ZBHCI_ADDRMODE_BRC,
    ZBHCI_ADDRMODE_BOUNDNOACK,
    ZBHCI_ADDRMODE_SHORTNOACK,
    ZBHCI_ADDRMODE_IEEENOACK
} zbhciTxMode_e;


#define SUCCESS 0


typedef struct {
    uint16_t cmdId;
    uint16_t len;
    uint8_t  payload[1];
} zbhci_cmdHandler_t;


typedef struct {
    void (*send_cb)(uint8_t* data, uint16_t len);
    void (*recv_cb)(uint8_t* data, uint16_t len);
} zbhci_cb_t;

void zbhci_uart_init(zbhci_cb_t* cb);
void uart_tx_start(uint8_t* buff, uint16_t length);
void uart_rx_start(uint8_t* buff, uint16_t length);

zbhciTx_e zbhciTx(uint16_t type, uint16_t length, uint8_t* data);

void zbhci_setpanid(uint16_t panid);
void zbhci_setchannel(uint8_t channel);
void zbhci_sysreset(void);
void zbhci_netstart(void);
void zbhci_facreset(void);
void zbhci_add_gid(uint16_t gid);
void zbhci_del_gid(uint16_t gid);
void zbhci_delall_gid(void);
void zbhci_allwojoin(uint8_t t);
void zbhci_ActiveEpReq(uint16_t addr);
void zbhci_SimpleDescReq(uint16_t addr, uint16_t ep);
void zbhci_leavereq(const uint8_t* mac, uint16_t addr);

void zbhci_devcnt(void);

void zbhci_zclsend(uint16_t addr, uint8_t ep,
                   uint16_t cid, uint8_t len,
                   uint8_t* data);
void zbhci_ota_send(uint16_t addr, uint8_t ep);
void zbhci_ota_block_start(uint32_t sz);
void zbhci_ota_block_rsp(uint32_t index, const uint8_t* buf, uint8_t sz);

void zbhci_group_zclsend(uint16_t gid, uint8_t ep,
                         uint16_t cid, uint8_t len,
                         uint8_t* data);

void zbhci_addrreq(const uint8_t* mac);
void zbhci_ieeereq(uint16_t addr);

void zbhci_bindreq(const uint8_t* srcmac, uint8_t srcep,
                   uint16_t cid, uint16_t gid,
                   const uint8_t* desmac, uint8_t descep);

void zbhci_unbindreq(const uint8_t* srcmac, uint8_t srcep,
                     uint16_t cid, uint16_t gid,
                     const uint8_t* desmac, uint8_t descep);

void zbhciCmdHandler(uint16_t msgType, uint16_t msgLen, uint8_t* p);

#endif

