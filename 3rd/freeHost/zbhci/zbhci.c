/********************************************************************************************************
 * @file     zbhci.c
 *
 * @brief	 Zigbee Host communication interface which responsible for receive data from lower layer and uplayer. And resolve
 * 			the received message
 *
 * @author
 * @date     June. 10, 2017
 *
 * @par      Copyright (c) 2016, Telink Semiconductor (Shanghai) Co., Ltd.
 *           All rights reserved.
 *
 *			 The information contained herein is confidential and proprietary property of Telink
 * 		     Semiconductor (Shanghai) Co., Ltd. and is available under the terms
 *			 of Commercial License Agreement between Telink Semiconductor (Shanghai)
 *			 Co., Ltd. and the licensee in separate contract or the terms described here-in.
 *           This heading MUST NOT be removed from this file.
 *
 * 			 Licensees are granted free, non-transferable use of the information in this
 *			 file under Mutual Non-Disclosure Agreement. NO WARRENTY of ANY KIND is provided.
 *
 *******************************************************************************************************/

/**********************************************************************
 * INCLUDES
 */
#include "task_event.h"
#include "zbhci.h"
#include "attr_api.h"

void zbhci_add_gid(uint16_t gid)
{
    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);

    zbhciTx(ZBHCI_CMD_BDB_ADD_GROUP, pBuf - array, array);
}

void zbhci_del_gid(uint16_t gid)
{
    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);

    zbhciTx(ZBHCI_CMD_BDB_DEL_GROUP, pBuf - array, array);
}

void zbhci_delall_gid(void)
{
    zbhciTx(ZBHCI_CMD_BDB_DELALL_GROUP, 0, NULL);
}

void zbhci_allwojoin(uint8_t t)
{
    uint16_t addr = 0xfffc;

    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;
    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);
    *pBuf++ = t;
    if(t == 0) {
        *pBuf++ = 0;
    }else{
        *pBuf++ = 1;
    }

    zbhciTx(ZBHCI_CMD_MGMT_PERMIT_JOIN_REQ, pBuf - array, array);
}

void zbhci_setpanid(uint16_t panid)
{

    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;
    *pBuf++ = LO_UINT16(panid);
    *pBuf++ = HI_UINT16(panid);

    zbhciTx(ZBHCI_CMD_BDB_PANID_SET, pBuf - array, array);
}

void zbhci_setchannel(uint8_t channel)
{

    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;
    *pBuf++ = channel;

    zbhciTx(ZBHCI_CMD_BDB_CHANNEL_SET, pBuf - array, array);
}

void zbhci_netstart(void)
{
    zbhciTx(ZBHCI_CMD_BDB_COMMISSION_FORMATION, 0, NULL);
}

void zbhci_devcnt(void)
{
    uint8_t array[4];
    memset(array, 0, 4);

    uint8_t* pBuf = array;
    *pBuf++ = 0;
    *pBuf++ = 0;

    OSAL_DBG("dev cnt");

    zbhciTx(ZBHCI_CMD_NODES_JOINED_GET_REQ, pBuf - array, array);
}

void zbhci_facreset(void)
{
    zbhciTx(ZBHCI_CMD_BDB_FACTORY_RESET, 0, NULL);
}

void zbhci_sysreset(void)
{
    zbhciTx(ZBHCI_CMD_BDB_SYS_RESET, 0, NULL);
}

void zbhci_leavereq(const uint8_t* mac, uint16_t addr)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);
    memcpy(pBuf, mac, 8);
    pBuf += 8;

    zbhciTx(ZBHCI_CMD_DISCOVERY_LEAVE_REQ, pBuf - array, array);
}

static void zbhci_bindpackeg(uint16_t type, const uint8_t* srcmac, uint8_t srcep,
                             uint16_t cid, uint16_t gid,
                             const uint8_t* desmac, uint8_t descep)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;
    memcpy(pBuf, srcmac, 8);
    pBuf += 8;
    *pBuf++ = srcep;
    *pBuf++ = LO_UINT16(cid);
    *pBuf++ = HI_UINT16(cid);
    if(gid) {
        *pBuf++ = ZBHCI_ADDRMODE_GROUP;
        *pBuf++ = LO_UINT16(gid);
        *pBuf++ = HI_UINT16(gid);
    } else {
        *pBuf++ = ZBHCI_ADDRMODE_IEEE;
        memcpy(pBuf, desmac, 8);
        pBuf += 8;
        *pBuf++ = descep;
    }
    zbhciTx(type, pBuf - array, array);
}

void zbhci_bindreq(const uint8_t* srcmac, uint8_t srcep,
                   uint16_t cid, uint16_t gid,
                   const uint8_t* desmac, uint8_t descep)
{
    zbhci_bindpackeg(ZBHCI_CMD_BINDING_REQ, srcmac, srcep, cid, gid, desmac, descep);
}

void zbhci_unbindreq(const uint8_t* srcmac, uint8_t srcep,
                     uint16_t cid, uint16_t gid,
                     const uint8_t* desmac, uint8_t descep)
{
    zbhci_bindpackeg(ZBHCI_CMD_UNBINDING_REQ, srcmac, srcep, cid, gid, desmac, descep);
}

void zbhci_addrreq(const uint8_t* mac)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;
    memcpy(pBuf, mac, 8);

    zbhciTx(ZBHCI_CMD_DISCOVERY_NWK_ADDR_REQ, 8, array);
}

void zbhci_ieeereq(uint16_t addr)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);

    zbhciTx(ZBHCI_CMD_DISCOVERY_IEEE_ADDR_REQ, 2, array);
}


void zbhci_SimpleDescReq(uint16_t addr, uint16_t ep)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);
    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);
    *pBuf++ = ep;
    zbhciTx(ZBHCI_CMD_DISCOVERY_SIMPLE_DESC_REQ, pBuf - array, array);
}

void zbhci_ActiveEpReq(uint16_t addr)
{
    uint8_t array[128];
    memset(array, 0, 128);

    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);

    *pBuf++ = LO_UINT16(addr);
    *pBuf++ = HI_UINT16(addr);

    zbhciTx(ZBHCI_CMD_DISCOVERY_ACTIVE_EP_REQ, pBuf - array, array);
}

/**
 * head
 *   1     2 3   4     4 5
 *  mode   addr  ep    cid
 *
 * payload
 * 0      (1   2)     3(1)   4(2)  5(3)
 * type   manufCode   seq    cmd   data
*/
void zbhci_zclsend(uint16_t addr, uint8_t ep,
                   uint16_t cid, uint8_t len,
                   uint8_t* data)
{
    uint8_t array[128];
    memset(array, 0, 128);
    uint8_t* pBuf = array;

    *pBuf ++ = ZBHCI_ADDRMODE_SHORT;
    *pBuf ++ = LO_UINT16(addr);
    *pBuf ++ = HI_UINT16(addr);
    *pBuf ++ = ep;
    *pBuf ++ = LO_UINT16(cid);
    *pBuf ++ = HI_UINT16(cid);

    memcpy(pBuf, data, len);
    pBuf += len;

    zbhciTx(ZBHCI_CMD_ZCL_CMD_REQ, pBuf - array, array);
}

void zbhci_group_zclsend(uint16_t addr, uint8_t ep,
                         uint16_t cid, uint8_t len,
                         uint8_t* data)
{
    uint8_t array[128];
    memset(array, 0, 128);
    uint8_t* pBuf = array;

    *pBuf ++ = ZBHCI_ADDRMODE_GROUP;
    *pBuf ++ = LO_UINT16(addr);
    *pBuf ++ = HI_UINT16(addr);
    *pBuf ++ = ep;
    *pBuf ++ = LO_UINT16(cid);
    *pBuf ++ = HI_UINT16(cid);

    memcpy(pBuf, data, len);
    pBuf += len;
    zbhciTx(ZBHCI_CMD_ZCL_CMD_REQ, pBuf - array, array);
}


void zbhci_ota_send(uint16_t addr, uint8_t ep)
{
    uint8_t array[128];
    memset(array, 0, 128);
    uint8_t* pBuf = array;

    *pBuf ++ = ZBHCI_ADDRMODE_SHORT;
    *pBuf ++ = LO_UINT16(addr);
    *pBuf ++ = HI_UINT16(addr);
    *pBuf ++ = ep;
    *pBuf ++ = LO_UINT16(0x0019);
    *pBuf ++ = HI_UINT16(0x0019);

    zbhciTx(ZBHCI_CMD_ZCL_OTA_IMAGE_NOTIFY, pBuf - array, array);
}

void zbhci_ota_block_start(uint32_t sz)
{
    uint8_t array[128];
    memset(array, 0, 128);
    uint8_t* pBuf = array;

    *pBuf ++ = LO_UINT16(sz);
    *pBuf ++ = HI_UINT16(sz);
    *pBuf ++ = HI_UINT24(sz);
    *pBuf ++ = HI_UINT32(sz);
    zbhciTx(ZBHCI_CMD_ZCL_OTA_IMAGE_START, pBuf - array, array);
}

/**
 * index :code index
 * buf   :code buf
 * sz    :code len
 *
 * pack : index || sz || buf
*/
void zbhci_ota_block_rsp(uint32_t index, const uint8_t* buf, uint8_t sz)
{
    uint8_t array[UART_TX_BUF_SIZE] = {0};
    uint8_t* pBuf = array;

    *pBuf ++ = LO_UINT16(index);
    *pBuf ++ = HI_UINT16(index);
    *pBuf ++ = HI_UINT24(index);
    *pBuf ++ = HI_UINT32(index);
    *pBuf ++ = sz;

    memcpy(pBuf, buf, sz);
    pBuf += sz;
    zbhciTx(ZBHCI_CMD_ZCL_OTA_IMAGE_SET, pBuf - array, array);
}


