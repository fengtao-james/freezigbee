/********************************************************************************************************
 * @file     zbhciCmdProcess.c
 *
 * @brief	 process all the commands from HCI
 *
 * @author
 * @date     June. 12, 2017
 *
 * @par      Copyright (c) 2016, Telink Semiconductor (Shanghai) Co., Ltd.
 *           All rights reserved.
 *
 *			 The information contained herein is confidential and proprietary property of Telink
 * 		     Semiconductor (Shanghai) Co., Ltd. and is available under the terms
 *			 of Commercial License Agreement between Telink Semiconductor (Shanghai)
 *			 Co., Ltd. and the licensee in separate contract or the terms described here-in.
 *           This heading MUST NOT be removed from this file.
 *
 * 			 Licensees are granted free, non-transferable use of the information in this
 *			 file under Mutual Non-Disclosure Agreement. NO WARRENTY of ANY KIND is provided.
 *
 *******************************************************************************************************/

#include "task_event.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "attr_api.h"
#include "zcl_cb.h"
#include "zbhci.h"
#include "discover.h"
#include "device-table.h"
#include "ncp_network.h"

//-------------------------------------------------------------------
static void zbhci_NwkAddrRspMsg(uint8_t* data)
{
    uint8_t status = data[1];
    //data[2]-data[9] mac
    //data[10]-data[11] addr
    // OSAL_DBG("status :%d", status);
    device_newdevjoinhandler(ZNC_RTN_U16(data, 10), &data[2]);
}

static void zbhci_IeeeAddrRspMsg(uint8_t* data)
{
    uint8_t status = data[1];
    //data[2]-data[9] mac
    //data[10]-data[11] addr
    // OSAL_DBG("status :%d", status);
    device_newdevjoinhandler(ZNC_RTN_U16(data, 10), &data[2]);
}

static void zbhci_ActiveEpRspMsg(uint8_t* data)
{
    uint8_t status = data[1];
    //data[2]-data[3] addr
    //data[4] active_ep_count
    //data[5] active_ep_lst
    // OSAL_DBG("status :%d", status);
    device_activeeprsp(ZNC_RTN_U16(data, 2), &data[5], data[4]);
}

static void zbhci_SimpleDescRspMsg(uint8_t* data)
{
    uint8_t status = data[1];
    //data[2]-data[3] addr
    //data[4] len
    //data[5] endpoint;
    //data[6-7] app_profile_id;
    //data[8-9] app_dev_id;
    //data[10] app_dev_ver;
    //data[11] inclen;
    //data[12-12+n] incid;
    //data[13+n] outclen;
    //data[14-14+n+x] outcid;
    // OSAL_DBG("status :%d", status);
    device_simpledescres(ZNC_RTN_U16(data, 2), data[5], ZNC_RTN_U16(data, 8), &data[11]);
}

static void zbhci_devleave(uint8_t* data)
{
    //mac
    discover_leave_dev(&data[2]);
}

static void zbhci_net_not(void)
{
    uint8_t data[20] = {0};
    ncp_setnetinfo(0, data[0], 0, ZNC_RTN_U16(data, 9), &data[1]);
}

static void zbhci_net_ok(uint8_t* data)
{
    //data[0]     channel
    //data[1-8]   mac
    //data[9-10]  panid
    //data[11-17] expanid
    ncp_setnetinfo(1, data[0], 10, ZNC_RTN_U16(data, 9), &data[1]);
}

static void zbhci_bdbCmdHandler(zbhci_cmdHandler_t* cmdInfo)
{
    uint16_t cmdID = cmdInfo->cmdId;
    uint8_t* p = cmdInfo->payload;

    if(cmdID == ZBHCI_CMD_BDB_NET_NOT) {
        zbhci_net_not();
    } else if(cmdID == ZBHCI_CMD_BDB_NET_OK) {
        zbhci_net_ok(p);
    }
}

static void zbhci_discoveryCmdHandler(zbhci_cmdHandler_t* cmdInfo)
{
    uint16_t cmdID = cmdInfo->cmdId;
    uint8_t* p = cmdInfo->payload;

    if(cmdID == ZBHCI_CMD_DISCOVERY_NWK_ADDR_RSP) {
        zbhci_NwkAddrRspMsg(p);

    } else if(cmdID == ZBHCI_CMD_DISCOVERY_IEEE_ADDR_RSP) {

        zbhci_IeeeAddrRspMsg(p);

    } else if(cmdID == ZBHCI_CMD_DISCOVERY_SIMPLE_DESC_RSP) {

        zbhci_SimpleDescRspMsg(p);

    } else if(cmdID == ZBHCI_CMD_DISCOVERY_ACTIVE_EP_RSP) {
        zbhci_ActiveEpRspMsg(p);

    } else if(cmdID == ZBHCI_CMD_NODE_LEAVE_IND) {
        zbhci_devleave(p);
    }
}

static void zbhci_bindCmdHandler(zbhci_cmdHandler_t* cmdInfo)
{
    EUI64 mac = {0};
    uint8_t* p = cmdInfo->payload;
    uint16_t addr = ZNC_RTN_U16(p, 0);
    uint8_t sql = p[2];
    uint8_t status = p[3];

    // OSAL_DBG("status :%d sql:%d", status, sql);

    if(dev_table_GetEui64FromNodeId(addr, mac)) {
        zbhci_ieeereq(addr);
        return;
    }
}

/**
 * head
 *   1     2 3   4     4 5
 *  mode   addr  ep    cid
 * payload
 * 0      (1   2)     3(1)   4(2)  5(3)
 * type   manufCode   seq    cmd   data
*/
static void zbhci_zclCmdHandle(zbhci_cmdHandler_t* cmdInfo)
{
    EUI64 mac = {0};
    uint16_t manucode = 0;
    uint8_t* p = cmdInfo->payload;
    uint8_t rssi = *p++;
    uint8_t mode = *p++;
    uint16_t addr = ZNC_RTN_U16(p, 0);
    p += 2;
    uint8_t ep = *p++;
    uint16_t cid = ZNC_RTN_U16(p, 0);
    p += 2;
    uint8_t type = *p++;

    if(type & 0x04) {
        manucode = ZNC_RTN_U16(p, 0);
        p += 2;
    }
    uint8_t sql  = *p++;
    uint8_t cmd  = *p++;

    if(dev_table_GetEui64FromNodeId(addr, mac)) {
        zbhci_ieeereq(addr);
        return;
    }
    // OSAL_DBG("mode :%d", mode);

    zcl_cb_recv(mac, rssi, sql, ep, cid, type, manucode, cmd, p, cmdInfo->len - (p - cmdInfo->payload));
}

static void zbhci_otaCmdHandle(zbhci_cmdHandler_t* cmdInfo)
{
    EUI64 mac = {0};
    uint8_t* p = cmdInfo->payload;
    uint16_t addr = ZNC_RTN_U16(p, 0);
    p += 2;
    uint8_t ep     = *p++;
    uint8_t status = *p++;

    if(dev_table_GetEui64FromNodeId(addr, mac)) {
        zbhci_ieeereq(addr);
        return;
    }
    ota_cb_report(mac, ep, status);
}

static void zbhci_otaBlockHandle(zbhci_cmdHandler_t* cmdInfo)
{
    uint8_t* p = cmdInfo->payload;
    uint32_t index = ZNC_RTN_U32(p, 0);
    p += 4;
    uint8_t len = *p;

    ota_cb_block(index, len);
}

static void zbhci_otaUpdateHandle(zbhci_cmdHandler_t* cmdInfo)
{
    EUI64 mac = {0};
    uint8_t* p = cmdInfo->payload;
    uint16_t addr = ZNC_RTN_U16(p, 0);
    p += 2;
    uint8_t  ep     = *p++;
    uint32_t max    = ZNC_RTN_U32(p, 0);
    p += 4;
    uint32_t now    = ZNC_RTN_U32(p, 0);

    if(dev_table_GetEui64FromNodeId(addr, mac)) {
        zbhci_ieeereq(addr);
        return;
    }

    ota_cb_update(mac, ep, max, now);
}

static void zbhci_devAnnHandler(zbhci_cmdHandler_t* cmdInfo)
{
    uint8_t* p = cmdInfo->payload;
    //addr  mac
    uint16_t addr = ZNC_RTN_U16(p, 0);
    p += 2;
    device_newdevjoinhandler(addr, p);
}

static void zbhci_nodejoinHandle(zbhci_cmdHandler_t* cmdInfo)
{
    uint8_t* p = cmdInfo->payload;

    OSAL_DBG("node join cnt :%d", BUILD_UINT16(p[0], p[1]));
}

void zbhciCmdHandler(uint16_t msgType, uint16_t msgLen, uint8_t* p)
{
    uint8_t buff[128] = {0};
    uint8_t st = 0;

    zbhci_cmdHandler_t* cmdInfo = (zbhci_cmdHandler_t*)buff;

    cmdInfo->cmdId = msgType;
    cmdInfo->len   = msgLen;
    memcpy(cmdInfo->payload, p, msgLen);

    switch(msgType) {
        case ZBHCI_CMD_BDB_NET_NOT:
        case ZBHCI_CMD_BDB_NET_OK:
            zbhci_bdbCmdHandler(cmdInfo);
            break;

        case ZBHCI_CMD_DISCOVERY_NWK_ADDR_RSP:
        case ZBHCI_CMD_DISCOVERY_IEEE_ADDR_RSP:
        case ZBHCI_CMD_DISCOVERY_NODE_DESC_RSP:
        case ZBHCI_CMD_DISCOVERY_SIMPLE_DESC_RSP:
        case ZBHCI_CMD_DISCOVERY_MATCH_DESC_RSP:
        case ZBHCI_CMD_DISCOVERY_ACTIVE_EP_RSP:
        case ZBHCI_CMD_NODE_LEAVE_IND:
            zbhci_discoveryCmdHandler(cmdInfo);
            break;

        case ZBHCI_CMD_BINDING_RSP:
        case ZBHCI_CMD_UNBINDING_RSP:
            zbhci_bindCmdHandler(cmdInfo);
            break;

        case ZBHCI_CMD_NODES_DEV_ANNCE_IND:
            zbhci_devAnnHandler(cmdInfo);
            break;

        case ZBHCI_CMD_ZCL_CMD_RSP:
            zbhci_zclCmdHandle(cmdInfo);
            break;
        case ZBHCI_CMD_ZCL_OTA_IMAGE_END:
            zbhci_otaCmdHandle(cmdInfo);
            break;
        case ZBHCI_CMD_ZCL_OTA_BLOCK_REQ:
            zbhci_otaBlockHandle(cmdInfo);
            break;
        case ZBHCI_CMD_ZCL_OTA_UPDATE_REQ:
            zbhci_otaUpdateHandle(cmdInfo);
            break;
        case ZBHCI_CMD_NODES_JOINED_GET_RSP:
            zbhci_nodejoinHandle(cmdInfo);
            break;
        default:
            st = ZBHCI_MSG_STATUS_UNHANDLED_COMMAND;
            // OSAL_DBG("st :%d msgType:%x", st, msgType);
            break;
    }

}

