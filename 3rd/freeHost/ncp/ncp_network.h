#ifndef NCP_NETWORK_H
#define NCP_NETWORK_H
#include "zcl_cb.h"

void ncp_net_nodeidreq(const uint8_t *mac);
void ncp_net_ieeereq(uint16_t addr);
uint8_t *ncp_net_mac(void);
void ncp_netreset(void);
// 3.0
void ncp_StartNetwork(uint8_t radioTxPower);
void ncp_ZdoLeave(uint8_t* nullEui64);
void ncp_Zdo_addr_del(uint16_t addr);
void ncp_setpanid(uint16_t panid);
void ncp_setchannel(uint8_t channel);

void ncp_CloseNetwork(void);// close net
void ncp_OpenNetwork(void);//180s open net
void ncp_NetworkLeave(void);
void ncp_net_getinfo(ncp_info_t* info);
void ncp_ota_start(uint8_t* mac, uint8_t ep);
void ncp_setnetinfo(uint8_t isok,uint8_t channel,
                    uint8_t radioTxPower,
                    uint16_t panId,uint8_t *mac);
void zdo_api_bind(uint8_t* mac, uint8_t ep, uint16_t clusterId);
void ncp_zcl_unicastsend(const uint8_t*mac,uint8_t ep,uint16_t cid, uint8_t* data, uint16_t len);
void ncp_zcl_Broadcast(zcl_broadcast_t* msg, uint8_t* data, uint16_t len);
void ncp_zcl_groupsend(zcl_group_t *msg,uint8_t *data,uint16_t len);
#endif



