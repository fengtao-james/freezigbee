
#include "task_event.h"

#include "ncp_api.h"
#include "zcl_api.h"
#include "ncp_dev.h"
#include "zcl_cb.h"
#include "ncp_cb.h"
#include "zbhci.h"
#include "device-table.h"
#include "ncp_network.h"
#include "discover.h"
static zcl_cb_t zclctx = {0};

//-----------------------------------------------------------------------------------

void zcl_cb_recv(uint8_t* mac,
                 uint8_t rssi,
                 uint8_t sql,
                 uint8_t ep,
                 uint16_t cluster,
                 uint8_t control,
                 uint16_t manucode,
                 uint8_t cmd,
                 const uint8_t* buff, uint8_t len)
{
    int rc;
    zcldata_t zclcmd;

    zclcmd.control  = control;
    zclcmd.manucode = manucode;
    zclcmd.sql      = sql;
    zclcmd.cmd      = cmd;

    if(!device_info_handle(mac, cluster, control, cmd, buff, len)) {
        return;
    }
    if(zcl_is_specfic(control)) {
        rc = spec_ias_process(mac, ep, cluster, cmd, sql);
        if(rc == 0) {
            return;
        }
    }

    spec_process(mac, ep, cluster, rssi, &zclcmd, buff, len);

    if(zclctx.zcl_recv) {
        zclctx.zcl_recv(zclctx.args, mac, ep, cluster, rssi, &zclcmd, buff, len);
    }
}

void ota_cb_report(uint8_t* mac, uint8_t ep, uint8_t status)
{
    if(zclctx.ota_st) {
        zclctx.ota_st(zclctx.args, mac, ep, status);
    }
}
//-----------------------------------------------------------
void ota_cb_block(uint32_t index, uint8_t len)
{
    if(zclctx.ota_block) {
        zclctx.ota_block(zclctx.args, index, len);
    }
}

void ota_cb_update(uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now)
{
    if(zclctx.ota_update) {
        zclctx.ota_update(zclctx.args, mac, ep, max, now);
    }
}
//----------------------------------------------------------------
void zcl_cb_net_ready(void)
{
    if(zclctx.zcl_ready) {
        zclctx.zcl_ready(zclctx.args);
    }
}
void zcl_cb_net_close(void)
{
    if(zclctx.zcl_close) {
        zclctx.zcl_close(zclctx.args);
    }
}

void zcl_cb_online(ep_info_t* dev)
{
    if(zclctx.dev_online) {
        zclctx.dev_online(zclctx.args, dev->mac, dev->ep);
    }
}

void zcl_cb_nextep(ep_info_t* dev)
{
    if(zclctx.next_ep) {
        zclctx.next_ep(zclctx.args, dev->mac, dev->ep);
    }
}

void zcl_cb_rejoin(uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    if(zclctx.dev_rejoin) {
        zclctx.dev_rejoin(zclctx.args, mac, mfc, mlen, mode, len);
    }
}

void zcl_cb_bind(uint8_t index, const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen)
{
    if(zclctx.bind_info) {
        zclctx.bind_info(zclctx.args, index, input, inlen, output, outlen);
    }
}

void zcl_cb_left(uint8_t* mac)
{
    if(zclctx.dev_leave) {
        zclctx.dev_leave(zclctx.args, mac);
    }
}

void zcl_cb_info(uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    if(zclctx.dev_info) {
        zclctx.dev_info(zclctx.args, mac, mfc, mlen, mode, len);
    }
}

void zdo_cb_bind(uint8_t* mac, uint8_t sql, uint8_t status)
{
    if(zclctx.zdo_bind) {
        zclctx.zdo_bind(zclctx.args, mac, status);
    }
}

//-----------------------------------------------------------------------------------

// cmd
int zcl_check(uint8_t* mac, uint8_t ep)
{

    if(0xffff == dev_table_GetNodeIdFromEui64(mac)) {
        char mac_name[20] = {0};
        OSAL_ERR("error mac :%s", osal_log_hexstr(mac_name, mac, 8));
        return -1;
    }

    // if((ep != 0xff) && (0xffff == dev_table_GetIndexFromEui64AndEp(mac, ep))) {
    //     OSAL_ERR("error ep :%d", ep);
    //     return -1;
    // }

    return 0;
}

//--------------------------------------------------------------------------------------------


static uint8_t  _zcl_pack(uint8_t* buff,
                          zcl_data_t* cmd,
                          const void* data,
                          size_t len)
{
    uint8_t index = 0;
    uint8_t sql = cmd->sql;

    buff[index++] = cmd->control;
    if(cmd->control & 0x04) {
        buff[index++] = cmd->manucode & 0xff;
        buff[index++] = (cmd->manucode >> 8) & 0xff;
    }
    buff[index++] = sql;
    buff[index++] = cmd->cmd;

    if(len) {
        memcpy(&buff[index], data, len);
        index += len;
    }
    return index;
}

int zcl_send(const uint8_t* mac,
             uint8_t ep,
             uint16_t cid,
             uint8_t control,
             uint16_t manucode,
             uint8_t sql,
             uint8_t cmd,
             const void* buff,
             size_t len)
{

    if(len > 128) {
        OSAL_ERR("len is error");
        return -1;
    }
    if(mac == NULL) {
        OSAL_ERR("mac is nil");
        return -1;
    }

    if(!ncp_net_isready()) {
        return -1;
    }

    uint8_t data[128] = {0};
    zcl_cmd_t msg = {0};
    memcpy(msg.mac, mac, 8);
    msg.ep  = ep;
    msg.cid = cid;

    msg.data.control  = control;
    msg.data.manucode = manucode;
    msg.data.cmd      = cmd;
    msg.data.sql      = sql;

    if(zcl_check(msg.mac, msg.ep)) {
        return -1;
    }

    ncp_zcl_unicastsend(mac, ep, cid, data, _zcl_pack(data, &msg.data, buff, len));
    return 0;
}

int zcl_response(const uint8_t* mac,
                 uint8_t ep,
                 uint16_t cid,
                 uint8_t control,
                 uint16_t manucode,
                 uint8_t sql,
                 uint8_t cmd,
                 const void* buff,
                 size_t len)
{

    if(len > 128) {
        OSAL_ERR("len is error");
        return -1;
    }
    if(mac == NULL) {
        OSAL_ERR("mac is nil");
        return -1;
    }

    if(!ncp_net_isready()) {
        return -1;
    }

    uint8_t data[128] = {0};
    zcl_cmd_t msg = {0};
    memcpy(msg.mac, mac, 8);
    msg.ep  = ep;
    msg.cid = cid;

    msg.data.control  = control;
    msg.data.manucode = manucode;
    msg.data.cmd      = cmd;
    msg.data.sql      = sql;

    if(zcl_check(msg.mac, msg.ep)) {
        return -1;
    }

    ncp_zcl_unicastsend(mac, ep, cid, data, _zcl_pack(data, &msg.data, buff, len));

    return 0;
}

int zcl_broadcast(uint8_t ep,
                  uint16_t cid,
                  uint8_t control,
                  uint16_t manucode,
                  uint8_t sql,
                  uint8_t cmd,
                  const void* buff,
                  size_t len)
{
    if(len > 128) {
        return -1;
    }

    if(!ncp_net_isready()) {
        return -1;
    }

    uint8_t data[128] = {0};
    zcl_broadcast_t msg = {0};
    msg.ep = ep;
    msg.cid = cid;

    msg.data.control  = control;
    msg.data.manucode = manucode;
    msg.data.cmd      = cmd;
    msg.data.sql      = sql ;

    ncp_zcl_Broadcast(&msg, data, _zcl_pack(data, &msg.data, buff, len));

    return 0;
}

int zcl_groupsend(uint16_t gid,
                  uint8_t ep,
                  uint16_t cid,
                  uint8_t control,
                  uint16_t manucode,
                  uint8_t sql,
                  uint8_t cmd,
                  const void* buff,
                  size_t len)
{
    if(len > 128) {
        return -1;
    }
    if(gid == 0xffff) {
        return -1;
    }

    if(!ncp_net_isready()) {
        return -1;
    }

    uint8_t data[128] = {0};
    zcl_group_t msg = {0};
    msg.gid = gid;
    msg.ep  = ep;
    msg.cid = cid;

    msg.data.control = control;
    msg.data.manucode = manucode;
    msg.data.cmd = cmd;
    msg.data.sql = sql ;

    ncp_zcl_groupsend(&msg, data, _zcl_pack(data, &msg.data, buff, len));
    return 0;
}

int ota_start(const uint8_t* mac, uint8_t ep)
{
    if(zcl_check((uint8_t*)mac, ep)) {
        return -1;
    }
    ncp_ota_start((uint8_t*)mac, ep);

    return 0;
}

void ota_code_set(uint32_t index, const uint8_t* buf, uint8_t len)
{
    zbhci_ota_block_rsp(index, buf, len);
}

void ota_code_info(uint32_t fsz)
{
    zbhci_ota_block_start(fsz);
}
//-----------------------------------------------------------------
struct gw_ota {
    FILE* fld;
    size_t sz;
};

struct gw_ota _gw_ota = {0};

int zcl_ota_block(uint32_t index, uint8_t len)
{
    uint8_t buf[128] = {0};

    if(index > _gw_ota.sz) {
        OSAL_ERR("error index:%u::%u", index, _gw_ota.sz);
        return -1;
    }
    if(_gw_ota.fld == NULL) {
        OSAL_ERR("fld error");
        return -1;
    }

    fseek(_gw_ota.fld, index, SEEK_SET);
    uint8_t clen = fread(buf, 1, len,  _gw_ota.fld);
    if(clen == 0) {
        OSAL_ERR("clen error index:%u", index);
        return -1;
    }
    ota_code_set(index, buf, clen);

    if(clen != len) {
        fclose(_gw_ota.fld);

        _gw_ota.fld = NULL;
        _gw_ota.sz  = 0;
        return 0;
    }
    return _gw_ota.sz - index;
}

int zcl_ota_info(const char* path)
{
    _gw_ota.fld = fopen(path, "rb");
    if(_gw_ota.fld == NULL) {
        OSAL_ERR("path :%s error!!!", path);
        return -1;
    }
    fseek(_gw_ota.fld, 0, SEEK_END);
    _gw_ota.sz = (size_t) ftell(_gw_ota.fld);

    if(_gw_ota.sz == 0) {
        OSAL_ERR("sz error :%s !!!", path);
        fclose(_gw_ota.fld);
        _gw_ota.sz  = 0;
        _gw_ota.fld = NULL;
        return -1;
    } else {
        OSAL_DBG("code sz :%u", _gw_ota.sz);
    }

    ota_code_info(_gw_ota.sz);
    return 0;
}

//-----------------------------------------------------------------
void zcl_regist_handle(const zcl_cb_t* handle)
{
    zclctx = *handle;
}

int zcl_deldev(const uint8_t* mac)
{

    if(mac == NULL) {
        return -1;
    }

    ncp_ZdoLeave((uint8_t*)mac);
    return 0;
}

int  zcl_get_netstatus(void)
{

    return  ncp_get_netstatus();
}

//-------------------------------------------------------------------------------------------
#define ZCL_GLOBAL_TYPE  0
#define ZCL_SPECFIC_TYPE  1

int zcl_is_global(uint8_t control)
{
    return (control & 1) == ZCL_GLOBAL_TYPE;
}

int zcl_is_specfic(uint8_t control)
{
    return (control & 1) == ZCL_SPECFIC_TYPE;
}

/**
 * if eplist=NULL return ep num
*/
int zcl_get_dev_ep(uint8_t* eplist, size_t sz, const uint8_t* mac)
{
    if(mac == NULL) {
        return -1;
    }
    if((eplist == NULL) && (sz != 0)) {
        return -1;
    }

    return ncp_getdev_ep(eplist, sz, mac);
}

int zcl_get_ep_dev_info(ep_info_t* info, const uint8_t* mac, uint8_t ep)
{
    if(mac == NULL || (info == NULL)) {
        return -1;
    }
    return ncp_getDevices_ep(info, mac, ep);
}

int zcl_get_dev_info(dev_info_t* info, const uint8_t* mac)
{
    if(mac == NULL || (info == NULL)) {
        return -1;
    }

    return ncp_get_device_info(info, mac);
}

uint16_t zcl_get_dev_index(const uint8_t* mac)
{
    return ncp_get_device_index(mac);
}

int zcl_zdo_bind(const uint8_t mac[8],
                 uint8_t ep,
                 uint16_t cid)
{
    if(zcl_check((uint8_t*)mac, ep)) {
        return -1;
    }

    zdo_api_bind((uint8_t*)mac, ep, cid);

    return 0;
}

