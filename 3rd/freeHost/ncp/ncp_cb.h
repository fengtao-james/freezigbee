#ifndef SILABS_NCP_CB_H
#define SILABS_NCP_CB_H

typedef struct ncp_cmd {
    uint8_t type;
    uint16_t addr; //2 byte
    uint8_t ep;
    uint16_t profileId;
    uint16_t cid;//clusterId
    uint8_t len;
    uint8_t data[];
} ncp_cmd_t;

typedef struct ncp_recv {
    uint8_t rssi;
    ncp_cmd_t cmd;
} ncp_recv_t;

#define ZCL_BUFFER_SIZE 256

void ncp_api_start(void);
void ncp_api_leave(void);


/**
 * net status
*/
int ncp_get_netstatus(void);

void ncp_net_up_cb(void);
void ncp_net_down_cb(void);

int ncp_net_isready(void);

void ncp_getjoineddev(void);
#endif



