#ifndef _NCP_DEV_H
#define _NCP_DEV_H

#include "zcl_api.h"

int  ncp_getDevices(freezgb_mac devmac, uint16_t index);
int  dev_numDevices(freezgb_mac* devmac, size_t sz);

int ncp_getdev_ep(uint8_t* eplist, size_t sz, const uint8_t* mac);
int ncp_getDevices_ep(ep_info_t* info, const uint8_t* mac, uint8_t ep);
int ncp_get_device_info(dev_info_t* info, const uint8_t* mac);
uint16_t ncp_get_device_index(const uint8_t* mac);
#endif
