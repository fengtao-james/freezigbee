#ifndef SILABS_DISCOVER_H
#define SILABS_DISCOVER_H

#define DISCOVER_EM_NUM_MAX  (8)
enum {
    discover_active = 1,
    discover_simple,
    discover_info
};

typedef struct dev_base {
    struct dev_base* prev;
    struct dev_base* next;
    osal_etimer_t t;
    uint8_t ep[DISCOVER_EM_NUM_MAX];
    uint8_t eui64[8];
    uint16_t nodeId;
    uint8_t cont;
    uint8_t ev;
    uint8_t sql;
    uint8_t epindex;
    uint8_t epnum;
} dev_node_t;

void discover_clear_list(void);
void discover_del_dev(uint8_t* mac);
void discover_leave_dev(uint8_t* mac);
void device_newdevjoinhandler(uint16_t newNodeId, uint8_t* newNodeEui64);
void device_activeeprsp(uint16_t nodeid, uint8_t* list, uint8_t length);
void device_simpledescres(uint16_t nodeId, uint8_t ep, uint16_t did, uint8_t* message);
int device_info_handle(uint8_t* mac,
                       uint16_t cluster,
                       uint8_t control,
                       uint8_t cmd,
                       const uint8_t* buff,
                       uint8_t len);
#endif