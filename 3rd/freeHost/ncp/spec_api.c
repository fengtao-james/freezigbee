#include "task_event.h"
#include "spec_api.h"
#include "zcl_api.h"
#include "attr_api.h"
#include "zcl_cb.h"
#include "ncp_network.h"

typedef struct spec_ev {
    struct spec_ev* prev, *next;
    osal_etimer_t t;
    spec_ev_cb cb;
    uint8_t mac[8];
    uint8_t ep;
    uint8_t sql;//sql(0-127)
} spec_ev_t;

typedef struct spec_ctx_list {
    spec_ev_t* head;
} spec_ctx_list_t;

static spec_ctx_list_t specctx;

static  int spec_timer_cb(void* args)
{
    spec_ev_t* ev = args;
    spec_ev_t* item, *tmp1, *tmp2;

    ncp_net_nodeidreq(ev->mac);

    OSAL_LIST_HEAD_SAFE(specctx.head, item, tmp1, tmp2) {
        if(item == ev) {
            OSAL_LIST_HEAD_DELETE(specctx.head, item);
            ev->cb(ev->mac, ev->ep, ev->sql, spec_st_timeout, 0);
            osal_mem_free(item);
            return -1;
        }
    }

    return -1;
}

static int spec_ctx_rsp(const uint8_t* mac,
                        uint8_t sql,
                        uint8_t rssi)
{
    spec_ev_t* item, *tmp1, *tmp2;

    OSAL_LIST_HEAD_SAFE(specctx.head, item, tmp1, tmp2) {
        if(!memcmp(mac, item->mac, 8)
                && (sql == item->sql)) {
            OSAL_LIST_HEAD_DELETE(specctx.head, item);

            osal_timer_stop(&item->t);
            item->cb(item->mac, item->ep, item->sql, spec_st_ok, rssi);
            osal_mem_free(item);
            return 0;
        }
    }
    return -1;
}

int spec_ev_open_and_start(const uint8_t mac[8],
                           uint8_t ep,
                           uint8_t sql,
                           const spec_ev_cb cb)
{

    if(cb == NULL) {
        return -1;
    }
    spec_ev_t* ev = osal_mem_alloc(sizeof(*ev));
    if(ev == NULL) {
        return -1;
    }
    ev->cb  = cb;
    ev->ep  = ep;
    ev->sql = sql;
    memcpy(ev->mac, mac, 8);

    OSAL_LIST_HEAD_APPEND(specctx.head, ev);
    osal_timer_start(&ev->t, spec_timer_cb, ev, 5000);
    return 0;
}

void spec_ev_reset(void)
{
    spec_ev_t* item, *tmp1, *tmp2;

    OSAL_LIST_HEAD_SAFE(specctx.head, item, tmp1, tmp2) {

        OSAL_LIST_HEAD_DELETE(specctx.head, item);
        osal_timer_stop(&item->t);
        osal_mem_free(item);
    }
}



int spec_add_group_start(const uint8_t mac[8],
                         uint8_t ep,
                         uint8_t sql,
                         const spec_ev_cb cb,
                         uint16_t gid)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }
    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);

    if(zcl_send(mac, ep,
                ZCL_CLUSTER_GEN_GROUPS, 1,
                0, sql, ZCL_CMD_GROUP_ADD_GROUP,
                array, pBuf - array)) {
        return -1;
    }

    return spec_ev_open_and_start(mac, ep, sql, cb);
}

int  spec_remove_group(const uint8_t mac[8],
                       uint8_t ep,
                       uint8_t sql,
                       const spec_ev_cb cb,
                       uint16_t gid)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }
    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);
    if(zcl_send(mac,
                ep,
                ZCL_CLUSTER_GEN_GROUPS, 1,
                0,
                sql, ZCL_CMD_GROUP_REMOVE_GROUP,
                array, pBuf - array)) {

        return -1;
    }

    return spec_ev_open_and_start(mac, ep, sql, cb);
}

int spec_remove_all_group(const uint8_t mac[8],
                          uint8_t ep,
                          uint8_t sql,
                          const spec_ev_cb cb)
{
    if(zcl_send(mac,
                ep,
                ZCL_CLUSTER_GEN_GROUPS, 1,
                0,
                sql, ZCL_CMD_GROUP_REMOVE_ALL_GROUP,
                NULL, 0)) {

        return -1;
    }
    return spec_ev_open_and_start(mac, ep, sql, cb);
}

int spec_store_scenes_start(const uint8_t mac[8],
                            uint8_t ep,
                            uint8_t sql,
                            const spec_ev_cb cb,
                            uint16_t gid,
                            uint8_t sid)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);
    *pBuf++ = sid;

    if(zcl_send(mac,
                ep,
                ZCL_CLUSTER_GEN_SCENES, 1,
                0,
                sql, ZCL_CMD_SCENE_STORE_SCENE,
                array, pBuf - array)) {
        return -1;
    }
    return spec_ev_open_and_start(mac, ep, sql, cb);
}

int spec_remove_scenes_start(const uint8_t mac[8],
                             uint8_t ep,
                             uint8_t sql,
                             const spec_ev_cb cb,
                             uint16_t gid,
                             uint8_t sid)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }
    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);
    *pBuf++ = sid;

    if(zcl_send(mac, ep,
                ZCL_CLUSTER_GEN_SCENES, 1,
                0, sql, ZCL_CMD_SCENE_REMOVE_SCENE,
                array, pBuf - array)) {
        return -1;
    }

    return spec_ev_open_and_start(mac, ep, sql, cb);
}

int spec_remove_all_scenes_start(const uint8_t mac[8],
                                 uint8_t ep,
                                 uint8_t sql,
                                 const spec_ev_cb cb,
                                 uint16_t gid)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);

    if(zcl_send(mac, ep,
                ZCL_CLUSTER_GEN_SCENES, 1,
                0, sql, ZCL_CMD_SCENE_REMOVE_ALL_SCENE,
                array, pBuf - array)) {
        return -1;
    }
    return spec_ev_open_and_start(mac, ep, sql, cb);
}

//------------------------------------------------------------------------
//group send
//------------------------------------------------------------------------

int spec_store_group_scenes(uint16_t gid, uint8_t sid, uint8_t sql)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);
    *pBuf++ = sid;
    return zcl_groupsend(gid, 0xff, ZCL_CLUSTER_GEN_SCENES,
                         1, 0, sql, ZCL_CMD_SCENE_STORE_SCENE,
                         array, pBuf - array);
}

int spec_remove_group_scanes(uint16_t gid, uint8_t sql)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);

    return zcl_groupsend(gid, 0xff, ZCL_CLUSTER_GEN_SCENES,
                         1, 0, sql, ZCL_CMD_SCENE_REMOVE_SCENE,
                         array, pBuf - array);
}

int spec_call_group_scenes(uint16_t gid,  uint8_t sid, uint8_t sql)
{
    uint8_t array[4] = {0};
    uint8_t* pBuf = array;

    if(gid == 0xffff) {
        OSAL_ERR("error gid");
        return -1;
    }

    *pBuf++ = LO_UINT16(gid);
    *pBuf++ = HI_UINT16(gid);
    *pBuf++ = sid;
    return zcl_groupsend(gid, 0xff, ZCL_CLUSTER_GEN_SCENES,
                         1, 0, sql, ZCL_CMD_SCENE_RECALL_SCENE,
                         array, pBuf - array);
}


int spec_process(const uint8_t* mac,
                 uint8_t ep,
                 uint16_t clusterId,//clusterId
                 uint8_t rssi,
                 const zcldata_t* cmd,
                 const void* data,
                 uint8_t len)
{
    return spec_ctx_rsp(mac, cmd->sql, rssi);
}

void spec_ctx_init(void)
{
    specctx.head = NULL;
}

int spec_ias_process(const uint8_t* mac, uint8_t ep, uint16_t cid, uint8_t cmd, uint8_t sql)
{
    uint8_t array[2] = {0};
    uint8_t* pBuf = array;

    if(cid == ZCL_CLUSTER_SS_IAS_ZONE
            && cmd == ZCL_CMD_ZONE_ENROLL_REQ) {
        *pBuf++ = 0;
        *pBuf++ = 0;

        zcl_send(mac, ep,
                 ZCL_CLUSTER_SS_IAS_ZONE, 1,
                 0, sql, ZCL_CMD_ZONE_ENROLL_RSP,
                 array, pBuf - array);
        return 0;
    }

    return -1;
}



