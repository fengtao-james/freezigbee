
#include "task_event.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "attr_api.h"
#include "device-table.h"
#include "discover.h"
#include "zbhci.h"
#include "zcl_cb.h"
#include "ncp_network.h"


#define MAX_RELAY_COUNT      10
#define DISCOVER_MAX_TIMES   10
#define DISCOVER_POLL_TIME   600
#define DISCOVER_START_TIME  50

#define CLUSTER_IN                  0
#define CLUSTER_OUT                 1
#define NUMBER_OF_CLUSTER_IN_OUT    2


struct dev_base* ep_list  = NULL;
uint8_t dicover_sql       = 0x55;

static struct dev_base* discover_find_mac_eplist(const uint8_t* mac)
{
    struct dev_base* node;

    OSAL_LIST_HEAD_FOREACH(ep_list, node) {
        if(!memcmp(node->eui64, mac, 8)) {
            return node;
        }
    }

    return NULL;
}

static struct dev_base* discover_find_id_eplist(uint16_t id)
{
    struct dev_base* node;

    OSAL_LIST_HEAD_FOREACH(ep_list, node) {
        if(node->nodeId == id) {
            return node;
        }
    }
    return NULL;
}

static struct dev_base* discover_add_eplist(const uint8_t* mac, uint16_t id)
{
    struct dev_base* node = discover_find_mac_eplist(mac);
    if(node != NULL) {
        return NULL;
    }

    node = osal_mem_alloc(sizeof(*node));
    if(node == NULL) {
        return NULL;
    }
    memcpy(node->eui64, mac, 8);
    node->nodeId = id;
    node->cont = 0;
    node->ev   = discover_active;
    OSAL_LIST_HEAD_APPEND(ep_list, node);
    return node;
}

static void disover_del_eplist(struct dev_base* node)
{
    if(node == NULL) {
        return;
    }
    OSAL_LIST_HEAD_DELETE(ep_list, node);
    osal_timer_stop(&node->t);
    osal_mem_free(node);
}

static void _SendReadInfo(uint8_t* mac, uint8_t endpoint, uint8_t sql)
{
    uint8_t buff[32] = {0};
    uint16_t attrlist[] = {0x0004, 0x0005};

    zcl_send(mac, endpoint, 0x0000, 0, 0, sql,
             GLOBAL_CMD_READ, buff,
             attr_pack_read(buff, 32, attrlist, 2));
}

static int discover_timeout_process(void* args)
{
    struct dev_base* node = args;

    if(node->cont >= DISCOVER_MAX_TIMES) {
        char mac[32] = {0};
        OSAL_WRN("DISCOVER TIMEOUT MAC:%s",
                 osal_log_hexstr(mac, node->eui64, 8));
        if(node->ep[0] == 0) {
            discover_del_dev(node->eui64);
        } else {
            node->cont       = 0;
            node->ev         = discover_info;
            _SendReadInfo(node->eui64, node->ep[0], dicover_sql++);
        }
        return -1;
    }
    if(node->ev == discover_active) {
        zbhci_ActiveEpReq(node->nodeId);
    } else if(node->ev == discover_simple) {
        zbhci_SimpleDescReq(node->nodeId,
                            node->ep[node->epindex]);
    } else {
        _SendReadInfo(node->eui64, node->ep[0], dicover_sql++);
    }
    node->cont ++;

    return DISCOVER_POLL_TIME;
}

static void discover_timeout_eplist(struct dev_base* node, uint32_t t)
{
    osal_timer_start(&node->t, discover_timeout_process, node, t);
}

static void discover_clear_eplist(void)
{
    struct dev_base* item, *tmp1, *tmp2;

    OSAL_LIST_HEAD_SAFE(ep_list, item, tmp1, tmp2) {
        disover_del_eplist(item);
    }
}
//-------------------------------------------------------------------------------
static void discover_set_semple(struct dev_base* base,
                                const uint8_t* list,
                                uint8_t sz)
{
    dev_node_t* node =  base;
    node->cont       = 0;
    node->epindex    = 0;
    node->epnum      = sz;
    node->ev         = discover_simple;
    // OSAL_DBG("discover ep num :%d", sz);
    if(sz > 0) {
        memcpy(node->ep, list, sz);
    }
}

static void discover_next_devlist(dev_node_t* node)
{
    node->epindex++;
    node->cont = 0;

    if(node->epindex < node->epnum) {

    } else {
        node->ev         = discover_info;
    }
}
//-------------------------------------------------------------------------------
void discover_clear_list(void)
{
    discover_clear_eplist();
}

static void _SendCieAddressWrite(uint8_t* mac, uint8_t endpoint)
{
    uint8_t buff[32] = {0};

    int packlen = attr_pack_write(buff, 32, 0x0010, 0xf0, ncp_net_mac(), 8);

    zcl_send(mac, endpoint, 0x0500, 0, 0, 0, GLOBAL_CMD_WRITE, buff, packlen);
}

//-------------------------------------------------------------------------------
// We have a new endpoint.
static void newEndpointDiscovered(dev_tab_Entry* p_entry)
{
    if(p_entry->deviceId == DEVICE_ID_IAS_ZONE) {
        // write IEEE address to CIE address location
        _SendCieAddressWrite(p_entry->eui64, p_entry->endpoint);
    }

    dev_table_NewDeviceCallback(p_entry);
    dev_table_Save();
}

int device_info_handle(uint8_t* mac,
                       uint16_t cluster,
                       uint8_t control,
                       uint8_t cmd,
                       const uint8_t* buff,
                       uint8_t len)
{
    struct dev_base* item = discover_find_mac_eplist(mac);
    if(item == NULL) {
        return -1;
    }
    if(cluster != 0x0000) {
        return -1;
    }
    if(cmd != GLOBAL_CMD_READ_RSP) {
        return -1;
    }
    if(!zcl_is_global(control)) {
        return -1;
    }
    read_rsp_t rsp = {0};

    const uint8_t* first = buff;
    const uint8_t* last  = buff + len;
    const uint8_t* mfc   = NULL;
    const uint8_t* mod   = NULL;
    uint8_t mfclen = 0;
    uint8_t modlen = 0;

    while(!attr_unpack_readack(&rsp, &first, last)) {
        if(rsp.status) {
            return -1;
        }
        if(rsp.attr_id == 0x0004) {
            mfc    = rsp.data;
            mfclen = rsp.len;
        } else {
            mod    = rsp.data;
            modlen = rsp.len;
        }
    }
    disover_del_eplist(item);
    dev_info_set(mac, mfc, mfclen, mod, modlen);
    zcl_cb_info(mac, mfc, mfclen, mod, modlen);
    return 0;
}

void device_activeeprsp(uint16_t nodeid, uint8_t* list, uint8_t length)
{
    struct dev_base* item = discover_find_id_eplist(nodeid);
    if(item != NULL) {
        // char mac[32] = {0};
        // OSAL_WRN("ACTIVE ACK MAC:%s",
        //          osal_log_hexstr(mac, item->eui64, 8));
        discover_set_semple(item, list, length);
    }
}

void device_simpledescres(uint16_t nodeId, uint8_t ep, uint16_t did, uint8_t* message)
{
    dev_tab_Entry* pEntry = NULL;
    uint8_t clusterIndex = 0;
    uint16_t dindex;
    uint8_t i, currentClusterType, ClusterCount;
    uint8_t* p = message;

    dindex = dev_table_GetEpFromNodeIdAndEp(nodeId, ep);

    if(dindex == DEVICE_TABLE_NULL_INDEX) {

        dindex = dev_table_FindFreeDeviceTableIndex();
        if(dindex == DEVICE_TABLE_NULL_INDEX) {
            return;
        }
    }
    pEntry = dev_table_FindDeviceTableEntry(dindex);
    pEntry->deviceId = did;

    pEntry->nodeId   = nodeId;
    pEntry->endpoint = ep;
    ClusterCount     = *p++;

    for(clusterIndex = 0; clusterIndex < DEVICE_TABLE_CLUSTER_SIZE; clusterIndex++) {
        pEntry->clusterIds[clusterIndex] = ZCL_NULL_CLUSTER_ID;
    }
    clusterIndex = 0;


    for(currentClusterType = CLUSTER_IN; currentClusterType < NUMBER_OF_CLUSTER_IN_OUT; currentClusterType++) {

        for(i = 0; i < ClusterCount; i++) {
            pEntry->clusterIds[clusterIndex] = ZNC_RTN_U16(p, 0);
            clusterIndex++;
            p += 2;
        }
        if(currentClusterType == CLUSTER_IN) {
            pEntry->clusterOutStartPosition = clusterIndex;
            ClusterCount = *p++;
        }
    }

    dev_node_t* node = discover_find_id_eplist(nodeId);

    if(node != NULL && node->ep[node->epindex] == ep) {

        memcpy(pEntry->eui64, node->eui64, EUI64_SIZE);
        newEndpointDiscovered(pEntry);
        discover_next_devlist(node);
    }
}

// two things have to be handled here:
// 1.) new device joined
// 2.) node Id has been changed.
void device_newdevjoinhandler(uint16_t newNodeId, uint8_t* newNodeEui64)
{
    uint16_t deviceTableIndex = dev_table_GetFirstIndexFromEui64(newNodeEui64);
    dev_tab_Entry* dtb        = dev_table_Pointer(deviceTableIndex);

    // OSAL_DBG("NEW dev :%x", newNodeId);
    if(deviceTableIndex == DEVICE_TABLE_NULL_INDEX) {

        if(discover_find_mac_eplist(newNodeEui64) == NULL) {

            struct dev_base* node = discover_add_eplist(newNodeEui64, newNodeId);
            if(node != NULL) {
                discover_timeout_eplist(node, DISCOVER_START_TIME);
            }
        }
    } else {
        if(newNodeId != dtb->nodeId) {
            dev_table_UpdateNodeId(dtb->nodeId, newNodeId);
            uint16_t endpointIndex = dev_table_GetEpFromNodeIdAndEp(dtb->nodeId, dtb->endpoint);
            if(endpointIndex == DEVICE_TABLE_NULL_INDEX) {
                return;
            }
            dev_table_RejoinDeviceCallback(dtb->eui64);
            dev_table_Save();
        }
    }
}

void discover_leave_dev(uint8_t* mac)
{
    uint16_t index = dev_table_GetFirstIndexFromEui64(mac);

    {
        char buff[128] = {0};
        OSAL_DBG("leave :%s", osal_log_hexstr(buff, mac, 8));
    }

    if(index != DEVICE_TABLE_NULL_INDEX) {
        dev_table_DeleteEntry(index);
        dev_table_Save();

        dev_info_clear(mac);
        dev_table_DeviceLeftCallback(mac);
    }
    disover_del_eplist(discover_find_mac_eplist(mac));
}

void discover_del_dev(uint8_t* mac)
{
    uint16_t index = dev_table_GetFirstIndexFromEui64(mac);

    if(index != DEVICE_TABLE_NULL_INDEX) {
        zbhci_leavereq(mac, dev_table_GetNodeIdFromEui64(mac));

        dev_info_clear(mac);
        dev_table_DeleteEntry(index);
        dev_table_Save();
        dev_table_DeviceLeftCallback(mac);
    }
    disover_del_eplist(discover_find_mac_eplist(mac));
}
