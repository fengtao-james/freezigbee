#ifndef SILABS_ZCL_CB_H
#define SILABS_ZCL_CB_H

#include "ncp_api.h"
#include "zcl_api.h"

enum zcl_cb_type {
    zcl_cb_type_ready,
    zcl_cb_type_close,
    zcl_cb_type_recv,// ncp cmd
    zcl_cb_type_online,
    zcl_cb_type_nextep,
    zcl_cb_type_rejoin,
    zcl_cb_type_left,
    zcl_cb_type_cb,
    zcl_cb_type_args,
    zdo_cb_type_bind
};

typedef struct zcl_data {
    uint8_t control;
    uint16_t manucode;
    uint8_t sql;
    uint8_t cmd;
} zcl_data_t;

typedef struct zcl_cmd {
    uint8_t mac[ZIGBEE_MAC_SZ]; //8 byte
    uint8_t rssi;
    uint16_t cid;//clusterId
    uint8_t ep;
    zcl_data_t data;
} zcl_cmd_t;

typedef struct zcl_broadcast {
    uint16_t cid;//clusterId
    uint8_t ep;
    zcl_data_t data;
} zcl_broadcast_t;

typedef struct zcl_group {
    uint16_t gid;
    uint16_t cid;//clusterId
    uint8_t ep;
    zcl_data_t data;
} zcl_group_t;

typedef struct zdo_bind {
    uint8_t mac[8]; //8 byte
    uint16_t cid;//clusterId
    uint8_t ep;
} zdo_bind_t;

typedef struct zdo_bindrsp {
    uint8_t mac[8]; //8 byte
    uint8_t sql;
    uint8_t status;
} zdo_bindrsp_t;

int zcl_check(uint8_t* mac, uint8_t ep);
void zcl_ctx_init(void);
void spec_ctx_init(void);
int spec_ias_process(const uint8_t* mac, uint8_t ep, uint16_t cid, uint8_t cmd, uint8_t sql);

void zcl_cb_ev(int ev, void* data, int sz);
void zdo_cb_bind(uint8_t* mac, uint8_t sql, uint8_t status);
void zcl_cb_net_ready(void);
void zcl_cb_net_close(void);
void zcl_cb_rejoin(uint8_t* mac, const uint8_t* mfc,
                   uint8_t mlen, const uint8_t* mode, uint8_t len);
void zcl_cb_online(ep_info_t* dev);
void zcl_cb_nextep(ep_info_t* dev);
void zcl_cb_left(uint8_t* mac);
void zcl_cb_info(uint8_t* mac, const uint8_t* mfc,
                 uint8_t mlen, const uint8_t* mode, uint8_t len);
void zcl_cb_recv(uint8_t* mac,
                 uint8_t rssi,
                 uint8_t sql,
                 uint8_t ep,
                 uint16_t cluster,
                 uint8_t control,
                 uint16_t manucode,
                 uint8_t cmd,
                 const uint8_t* buff, uint8_t len);

int spec_process(const uint8_t* mac,
                 uint8_t ep,
                 uint16_t clusterId,//clusterId
                 uint8_t rssi,
                 const zcldata_t* cmd,
                 const void* data,
                 uint8_t len);
void ota_cb_report(uint8_t* mac, uint8_t ep, uint8_t status);
void ota_cb_block(uint32_t index, uint8_t len);
void ota_cb_update(uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now);

void zcl_cb_bind(uint8_t index, const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen);
#endif



