#ifndef _DEVICE_TABLE_H
#define _DEVICE_TABLE_H

// ZigBee device IDs
#define DEVICE_ID_ON_OFF_SWITCH        0x0000
#define DEVICE_ID_LEVEL_CONTROL_SWITCH 0x0001
#define DEVICE_ID_ON_OFF_OUTPUT        0x0002
#define DEVICE_ID_LEVEL_CONTROL_OUTPUT 0x0003
#define DEVICE_ID_SCENE_SELECTOR       0x0004
#define DEVICE_ID_CONFIG_TOOL          0x0005
#define DEVICE_ID_REMOTE_CONTROL       0x0006
#define DEVICE_ID_COMBINED_INTERFACE   0x0007
#define DEVICE_ID_RANGE_EXTENDER       0x0008
#define DEVICE_ID_MAINS_POWER_OUTLET   0x0009
#define DEVICE_ID_DOOR_LOCK            0x000a
#define DEVICE_ID_DOOR_LOCK_CONTROLLER 0x000b
#define DEVICE_ID_SIMPLE_SENSOR        0x000c
#define DEVICE_ID_CONSUMPTION_AWARENESS_DEVICE 0x000d
#define DEVICE_ID_HOME_GATEWAY         0x0050
#define DEVICE_ID_SMART_PLUG           0x0051
#define DEVICE_ID_WHITE_GOODS          0x0052
#define DEVICE_ID_METER_INTERFACE      0x0053

#define DEVICE_ID_ON_OFF_LIGHT         0x0100
#define DEVICE_ID_DIMMABLE_LIGHT       0x0101
#define DEVICE_ID_COLOR_DIMMABLE_LIGHT 0x0102
#define DEVICE_ID_ON_OFF_LIGHT_SWITCH  0x0103
#define DEVICE_ID_DIMMER_SWITCH        0x0104
#define DEVICE_ID_COLOR_DIMMER_SWITCH  0x0105
#define DEVICE_ID_LIGHT_SENSOR         0x0106
#define DEVICE_ID_OCCUPANCY_SENSOR     0x0107

#define DEVICE_ID_SHADE                0x0200
#define DEVICE_ID_SHADE_CONTROLLER     0x0201
#define DEVICE_ID_WINDOW_COVERING_DEVICE     0x0202
#define DEVICE_ID_WINDOW_COVERING_CONTROLLER 0x0203

#define DEVICE_ID_HEATING_COOLING_UNIT 0x0300
#define DEVICE_ID_THERMOSTAT           0x0301
#define DEVICE_ID_TEMPERATURE_SENSOR   0x0302
#define DEVICE_ID_PUMP                 0x0303
#define DEVICE_ID_PUMP_CONTROLLER      0x0304
#define DEVICE_ID_PRESSURE_SENSOR      0x0305
#define DEVICE_ID_FLOW_SENSOR          0x0306
#define DEVICE_ID_MINI_SPLIT_AC        0x0307

#define DEVICE_ID_IAS_CIE               0x0400
#define DEVICE_ID_IAS_ANCILLARY_CONTROL 0x0401
#define DEVICE_ID_IAS_ZONE              0x0402
#define DEVICE_ID_IAS_WARNING           0x0403

#define DEVICE_TAB_PATCH            "."

#define ZCL_NULL_CLUSTER_ID          0xffff
#define DEVICE_TABLE_CLUSTER_SIZE    20
#define EUI64_SIZE                   8
#define MILLISECOND_TICKS_PER_SECOND (1000)

typedef uint8_t EUI64[EUI64_SIZE];

typedef struct {
    uint16_t    deviceId;
    uint16_t    nodeId;
    EUI64       eui64;
    uint8_t     endpoint;
    uint8_t     clusterOutStartPosition;
    uint16_t clusterIds[DEVICE_TABLE_CLUSTER_SIZE];
} dev_tab_Entry;

typedef struct {
    uint8_t     used;
    EUI64       eui64;
    uint8_t     mid[32];
    uint8_t     mfd[32];
} dev_info_Entry;

typedef struct {
    uint16_t  inlen;
    uint16_t  outlen;
    uint8_t*     input;
    uint8_t*     output;
} dev_bind_Entry;


#define DEVICE_TABLE_DEVICE_TABLE_SIZE  512
#define DEVICE_TABLE_DEVICE_INFO_SIZE   256
#define DEVICE_TABLE_BIND_SIZE          128

#define DEVICE_TABLE_NULL_NODE_ID       0xffff
#define DEVICE_TABLE_NULL_INDEX         0xffff

void dev_table_DeviceLeftCallback(EUI64 nodeEui64);
void dev_table_NewDeviceCallback(dev_tab_Entry* dev);
void dev_table_RejoinDeviceCallback(EUI64 nodeEui64);

uint16_t dev_table_FindNextEp(uint16_t index);

int dev_table_GetEui64FromNodeId(uint16_t nodeid, EUI64 eui64);


uint16_t dev_table_GetNodeIdFromIndex(uint16_t index);


uint16_t dev_table_GetEpFromNodeIdAndEp(uint16_t nodeid,
                                        uint8_t endpoint);


uint16_t dev_table_GetIndexFromNodeId(uint16_t nodeid);


uint16_t dev_table_GetFirstIndexFromEui64(EUI64 eui64);


uint16_t dev_table_GetIndexFromEui64AndEp(EUI64 eui64,
        uint8_t endpoint);


uint16_t dev_table_GetNodeIdFromEui64(const EUI64 eui64);


void dev_table_MessageSentStatus(uint16_t nodeId,
                                 uint8_t status,
                                 uint16_t profileId,
                                 uint16_t clusterId);


void dev_table_SendLeave(uint16_t index);


void dev_table_MessageReceived(uint16_t nodeId);


void dev_table_InitiateRouteRepair(uint16_t nodeId);


dev_tab_Entry* dev_table_Pointer(uint16_t index);


dev_tab_Entry* dev_table_FindDeviceTableEntry(uint16_t index);


void device_newdevjoinhandler(uint16_t newNodeId, EUI64 newNodeEui64);


uint16_t dev_table_FindFirstEpIeee(EUI64 eui64);

int dev_table_MatchEui64(const EUI64 a, const EUI64 b);

void dev_table_DeleteEntry(uint16_t index);
void dev_table_Save(void);
void dev_table_Clear(void);
void dev_table_loaddev(const char* path);
uint16_t dev_table_FindFreeDeviceTableIndex(void);
void dev_table_UpdateNodeId(uint16_t currentNodeId, uint16_t newNodeId);

dev_info_Entry* dev_info_Pointer(uint16_t index);
void dev_info_clear(EUI64 mac);
void dev_info_online(void);
uint16_t dev_info_get_index(const uint8_t* mac);
void dev_info_set(uint8_t* mac , const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len);


void bind_info_set(uint8_t index , const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen);
void bind_info_all(void);
void bind_info_clear(void);
#endif //__DEVICE_TABLE_H
