#include "task_event.h"
#include "attr_api.h"


typedef struct attr_data {
    uint8_t type;
    uint8_t len;
    const uint8_t* data;
} attr_data_t;

#define ZIGBEE_LTOB16(_wdata) \
        (((_wdata&0x00ff)<<8) | ((_wdata>>8)&0x00ff))

#define ZIGBEE_LTOB32(_ndata) \
        (((_ndata&0x000000ff) << 24) \
        | ((_ndata&0x0000ff00)<<8) \
        | ((_ndata&0x00ff0000)>>8) \
        | ((_ndata&0xff000000)>>24))


uint8_t attr_api_getint_bytes(uint8_t dataType)
{
    uint8_t len;

    switch(dataType) {
        case ZCL_ATTRIBUTE_DATA8:
        case ZCL_ATTRIBUTE_BOOLEAN:
        case ZCL_ATTRIBUTE_BITMAP8:
        case ZCL_ATTRIBUTE_INT8:
        case ZCL_ATTRIBUTE_UINT8:
        case ZCL_ATTRIBUTE_ENUM8:
            len = 1;
            break;

        case ZCL_ATTRIBUTE_DATA16:
        case ZCL_ATTRIBUTE_BITMAP16:
        case ZCL_ATTRIBUTE_UINT16:
        case ZCL_ATTRIBUTE_INT16:
        case ZCL_ATTRIBUTE_ENUM16:
        case ZCL_ATTRIBUTE_SEMI_PREC:
        case ZCL_ATTRIBUTE_CLUSTER_ID:
        case ZCL_ATTRIBUTE_ATTR_ID:
            len = 2;
            break;

        case ZCL_ATTRIBUTE_DATA24:
        case ZCL_ATTRIBUTE_BITMAP24:
        case ZCL_ATTRIBUTE_UINT24:
        case ZCL_ATTRIBUTE_INT24:
            len = 3;
            break;

        case ZCL_ATTRIBUTE_DATA32:
        case ZCL_ATTRIBUTE_BITMAP32:
        case ZCL_ATTRIBUTE_UINT32:
        case ZCL_ATTRIBUTE_INT32:
        case ZCL_ATTRIBUTE_SINGLE_PREC:
        case ZCL_ATTRIBUTE_TOD:
        case ZCL_ATTRIBUTE_DATE:
        case ZCL_ATTRIBUTE_UTC:
        case ZCL_ATTRIBUTE_BAC_OID:
            len = 4;
            break;

        case ZCL_ATTRIBUTE_UINT40:
        case ZCL_ATTRIBUTE_INT40:
            len = 5;
            break;

        case ZCL_ATTRIBUTE_UINT48:
        case ZCL_ATTRIBUTE_INT48:
            len = 6;
            break;

        case ZCL_ATTRIBUTE_UINT56:
        case ZCL_ATTRIBUTE_INT56:
            len = 7;
            break;

        case ZCL_ATTRIBUTE_DOUBLE_PREC:
        case ZCL_ATTRIBUTE_IEEE_ADDR:
        case ZCL_ATTRIBUTE_UINT64:
        case ZCL_ATTRIBUTE_INT64:
            len = 8;
            break;

        case ZCL_ATTRIBUTE_128_BIT_SEC_KEY:
            len = 16;
            break;

        case ZCL_ATTRIBUTE_NO_DATA:
        case ZCL_ATTRIBUTE_UNKNOWN:
        // Fall through

        default:
            len = 0;
            break;
    }
    return (len);
}


int  attr_is_report(uint8_t cmd)
{
    return (cmd == GLOBAL_CMD_REPORT);
}

int  attr_is_writeack(uint8_t cmd)
{
    return (cmd == GLOBAL_CMD_WRITE_RSP);
}

int  attr_is_readack(uint8_t cmd)
{
    return (cmd == GLOBAL_CMD_READ_RSP);
}

int  attr_is_read(uint8_t cmd)
{
    return (cmd == GLOBAL_CMD_READ);
}

int  attr_is_defaultack(uint8_t cmd)
{
    return (cmd == GLOBAL_CMD_DEFAULT_RSP);
}


int attr_get_intattr(uint32_t* result, const void* data, size_t len, uint8_t type)
{
    size_t ret = 0;
    const uint8_t* info = data;
    ret = attr_api_getint_bytes(type);
    if(ret > len) {
        return -1;
    }
    if(ret == 1) {
        *result = info[0];
    } else if(ret == 2) {
        *result = ZNC_RTN_U16(info, 0);
    } else if(ret == 3) {
        *result = ZNC_RTN_U24(info, 0);
    } else if(ret == 4) {
        *result = ZNC_RTN_U32(info, 0);
    } else {
        return -1;
    }
    return 0;
}

int attr_set_intattr(uint8_t* buff, size_t maxsz, uint32_t result, uint8_t type)
{
    size_t ret = attr_api_getint_bytes(type);
    if(buff == NULL || (maxsz == 0)) {
        return ret;
    }
    if(maxsz < ret) {
        return 0;
    }
    if(ret == 1) {
        buff[0] = result & 0xff;
    } else if(ret == 2) {
        buff[0] = LO_UINT16(result);
        buff[1] = HI_UINT16(result);
    } else if(ret == 3) {
        buff[0] = LO_UINT16(result);
        buff[1] = HI_UINT16(result);
        buff[2] = HI_UINT24(result);
    } else if(ret == 4) {
        buff[0] = LO_UINT16(result);
        buff[1] = HI_UINT16(result);
        buff[2] = HI_UINT24(result);
        buff[3] = HI_UINT32(result);
    } else {
        return 0;
    }
    return ret;
}

//little endian
static int attr_api_getattr_length(uint8_t dataType, const uint8_t* pData , size_t len, uint8_t* bytes)
{
    uint16_t dataLen = 0;

    if(dataType == ZCL_ATTRIBUTE_LONG_CHAR_STR
            || dataType == ZCL_ATTRIBUTE_LONG_OCTET_STR) {
        if(len < 2) {
            return -1;
        }
        dataLen = BUILD_UINT16(pData[0], pData[1]);
        *bytes = 2;
    } else if(dataType == ZCL_ATTRIBUTE_CHAR_STR
              || dataType == ZCL_ATTRIBUTE_OCTET_STR) {

        if(len < 1) {
            return -1;
        }
        dataLen = pData[0];
        *bytes = 1;
    } else {
        dataLen = attr_api_getint_bytes(dataType);
        *bytes = 0;
    }
    return (dataLen);
}

static int _attr_get_data(attr_data_t* data, const uint8_t* pData, size_t len)
{
    uint8_t bytes = 0;
    int datalen;

    if(len < 1) {
        return -1;
    }
    data->type = pData[0];

    datalen = attr_api_getattr_length(data->type, pData + 1, len - 1, &bytes);
    if(datalen < 0) {
        return -1;
    }

    data->len = datalen;
    if(data->len > 0) {
        data->data = pData + (bytes + 1);
    } else {
        data->data = NULL;
    }
    return bytes + 1 + data->len; //1: type byte
}

//-------------------------------------------------------------------------------------
int attr_pack_read(uint8_t* buff, size_t maxsz, const uint16_t* attr_id, size_t num)
{
    size_t total = num * 2;

    if((NULL == buff) && (maxsz = 0)) {
        return total;
    }
    if((maxsz < total) || (buff == NULL) || (attr_id == NULL) || (num == 0)) {
        return -1;
    }
    size_t i;
    for(i = 0; i < num; i++) {
        buff[i * 2] = LO_UINT16(attr_id[i]);
        buff[i * 2 + 1] = HI_UINT16(attr_id[i]);
    }
    return total;
}
//---------------------------------------------------------------------------------------
int attr_unpack_read(uint16_t* attridlist, size_t maxsz, const void* buff, size_t len)
{
    if(buff == NULL) {
        return -1;
    }

    if((len % 2) != 0) {
        return -1;
    }
    size_t num = len / 2;

    if(maxsz < num) {
        return -1;
    }

    if((attridlist == NULL) && (maxsz == 0)) {
        return num;
    }

    size_t i;
    const uint8_t* data = buff;
    for(i = 0; i < num; i++) {
        attridlist[i] = ZNC_RTN_U16(data , (2 * i));
    }
    return num;
}

static int attr_api_getstring_len_byte(uint8_t dataType)
{
    if(dataType == ZCL_ATTRIBUTE_LONG_CHAR_STR
            || dataType == ZCL_ATTRIBUTE_LONG_OCTET_STR) {
        return 2;
    } else if(dataType == ZCL_ATTRIBUTE_CHAR_STR
              || dataType == ZCL_ATTRIBUTE_OCTET_STR) {
        return 1;
    }
    return 0;
}

int attr_pack_write(uint8_t* buff, size_t maxsz,
                    uint16_t attid,
                    uint8_t type,
                    const void* data, size_t len)
{
    int index = 3;
    int ret = attr_api_getstring_len_byte(type);
    size_t total = ret + len + index;
    if((NULL == buff) && (maxsz = 0)) {
        return total;
    }
    if((total > maxsz) || (buff == NULL) || (data == NULL) || (len == 0)) {
        return -1;
    }

    buff[0] = LO_UINT16(attid);
    buff[1] = HI_UINT16(attid);
    buff[2] = type;
    if(ret) {
        if(ret == 1) {
            buff[index] = len;
            index += 1;
        } else {
            buff[index] = LO_UINT16(len);
            buff[index + 1] = HI_UINT16(len);
            index += 2;
        }
    }
    memcpy(&buff[index], data, len);
    index += len;
    return index;
}

int attr_unpack_writeack(write_rsp_t* data, const uint8_t** first, const uint8_t* last)
{
    if((*first == NULL) || (last == NULL) || (data == NULL)) {
        return -1;
    }
    int len = last - *first;

    if(len < 3) {
        return -1;
    }
    data->attr_id = ZNC_RTN_U16(*first, 0);
    data->status = (*first)[2];

    *first += 3;
    return 0;
}

int attr_pack_readack(uint8_t* buff, size_t maxsz,
                      uint16_t attid,
                      uint8_t status,
                      uint8_t type,
                      const void* data, size_t len)
{
    int index = 3;//attrid +status

    int ret = 0;

    if(status == 0) {
        ret = attr_api_getstring_len_byte(type);
        index += 1; //type
    }

    size_t total = ret + len + index;

    if((NULL == buff) && (maxsz = 0)) {
        return total;
    }

    if((total > maxsz) || (buff == NULL)) {
        return -1;
    }

    if((status == 0) && ((data == NULL) || (len == 0))) {
        return -1;
    }

    buff[0] = LO_UINT16(attid);
    buff[1] = HI_UINT16(attid);
    buff[2] = status;

    if(status == 0) {
        buff[3] = type;
        if(ret) {
            if(ret == 1) {
                buff[index] = len;
                index += 1;
            } else {
                buff[index] = LO_UINT16(len);
                buff[index + 1] = HI_UINT16(len);
                index += 2;
            }
        }
        memcpy(&buff[index], data, len);
        index += len;
    }

    return index;

}


int attr_unpack_readack(read_rsp_t* ack, const uint8_t** first, const uint8_t* last)
{
    int cost = 0;

    if((*first == NULL) || (last == NULL) || (ack == NULL)) {
        return -1;
    }
    size_t len = last - *first;
    if(len < 3) {
        return -1;
    }
    ack->attr_id = ZNC_RTN_U16(*first, 0);
    ack->status = (*first)[2];
    cost += 3;
    if(ack->status == 0) {
        attr_data_t attr;
        int ret = _attr_get_data(&attr, &(*first)[3], len - 3);
        if(ret < 0) {
            return ret;
        } else {
            ack->type = attr.type;
            ack->len = attr.len;
            ack->data = attr.data;
            cost += ret;
        }
    } else {
        ack->type = 0;
        ack->len = 0;
        ack->data = NULL;
    }
    *first += cost;
    return 0;
}


int attr_unpack_report(report_t* data, const uint8_t** first, const uint8_t* last)
{
    attr_data_t attr;
    int cost = 0;
    int ret;

    if((*first == NULL) || (last == NULL) || (data == NULL)) {
        return -1;
    }

    size_t len = last - *first;
    if(len < 2) {
        return -1;
    }
    data->attr_id = ZNC_RTN_U16(*first, 0);
    cost += 2;

    ret = _attr_get_data(&attr, &(*first)[2], len - 2);
    if(ret < 0) {
        return ret;
    } else {
        data->type = attr.type;
        data->len = attr.len;
        data->data = attr.data;
    }
    cost += ret;
    *first += cost;
    return 0;
}


int attr_unpack_defaultack(const void* data, size_t len)
{
    const uint8_t* info = data;
    if(len < 1 || data == NULL) {
        return -1;
    }
    return info[0];
}

int attr_pack_config_report(uint8_t* buff, size_t maxsz,
                            uint8_t direction,
                            uint16_t attid,
                            uint8_t type,
                            uint16_t min,
                            uint16_t max,
                            uint32_t data,
                            uint16_t delay)
{
    size_t total = 0;

    if(direction == 0) {
        total = 8 +  attr_api_getint_bytes(type);
    } else {
        total = 5;//attrid direction delay
    }

    if((NULL == buff) && (maxsz = 0)) {
        return total;
    }

    if((maxsz < total) || (buff == NULL)) {
        return -1;
    }
    if(direction == 0) {
        buff[0] = direction;
        buff[1] = LO_UINT16(attid);
        buff[2] = HI_UINT16(attid);
        buff[3] = type;
        buff[4] = LO_UINT16(min);
        buff[5] = HI_UINT16(min);
        buff[6] = LO_UINT16(max);
        buff[7] = HI_UINT16(max);
        attr_set_intattr(&buff[8], maxsz - 8, data, type);
    } else {
        buff[0] = direction;
        buff[1] = LO_UINT16(attid);
        buff[2] = HI_UINT16(attid);
        buff[3] = LO_UINT16(delay);
        buff[4] = HI_UINT16(delay);
    }
    return total;
}

int attr_unpack_config_reportack(config_report_rsp_t* data, const uint8_t** first, const uint8_t* last)
{

    if((*first == NULL) || (last == NULL) || (data == NULL)) {
        return -1;
    }

    size_t len = last - *first;
    if(len < 4) {
        return -1;
    }
    data->status = (*first)[0];
    data->direction = (*first)[1];
    data->attr_id = ZNC_RTN_U16(*first, 2);

    *first += 4;
    return 0;
}

