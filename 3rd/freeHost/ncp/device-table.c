#include "task_event.h"
#include <stdint.h>
#include "device-table.h"
#include "zcl_cb.h"

static char dev_tab_patch[32] = DEVICE_TAB_PATCH;

static dev_tab_Entry  deviceTable[DEVICE_TABLE_DEVICE_TABLE_SIZE];
static dev_info_Entry deviceinfo[DEVICE_TABLE_DEVICE_INFO_SIZE];
static dev_info_Entry bindinfo[DEVICE_TABLE_BIND_SIZE];

static void clearDeviceTableIndex(uint16_t index);

dev_tab_Entry* dev_table_Pointer(uint16_t index)
{
    return &deviceTable[index];
}

dev_info_Entry* dev_info_Pointer(uint16_t index)
{
    return &deviceinfo[index];
}

dev_bind_Entry* bind_info_Pointer(uint16_t index)
{
    return &bindinfo[index];
}



uint16_t dev_table_GetNodeIdFromIndex(uint16_t index)
{
    if(index >= DEVICE_TABLE_DEVICE_TABLE_SIZE) {
        return 0xffff;
    }
    dev_tab_Entry* tb = dev_table_Pointer(index);

    return tb->nodeId;
}


static void checkNullEui64(const EUI64 eui64a, const EUI64 eui64b)
{
    uint8_t i;
    for(i = 0; i < EUI64_SIZE; i++) {
        if(eui64a[i] != 0xff
                || eui64b[i] != 0xff) {
            return;
        }
    }
}

static int matchEui64(const EUI64 a, const EUI64 b)
{
    checkNullEui64(a, b);

    if(memcmp(a, b, EUI64_SIZE) == 0) {
        return 0;
    } else {
        return -1;
    }
}

int dev_table_MatchEui64(const EUI64 eui64a, const EUI64 eui64b)
{
    return matchEui64(eui64a, eui64b);
}

static void unsetEui64(EUI64 eui64)
{
    uint8_t i;
    for(i = 0; i < 8; i++) {
        eui64[i] = 0xff;
    }
}

static void clearDeviceTableIndex(uint16_t index)
{
    uint8_t i;
    if(index >= DEVICE_TABLE_DEVICE_TABLE_SIZE) {
        return ;
    }
    dev_tab_Entry* tb = dev_table_Pointer(index);

    tb->nodeId = DEVICE_TABLE_NULL_NODE_ID;
    unsetEui64(tb->eui64);
    tb->endpoint = 0;

    for(i = 0; i < DEVICE_TABLE_CLUSTER_SIZE; i++) {
        tb->clusterIds[i] = ZCL_NULL_CLUSTER_ID;
    }

    tb->clusterOutStartPosition = 0;
}

void dev_table_DeleteEntry(uint16_t index)
{
    uint16_t currentIndex;

    while(index != DEVICE_TABLE_NULL_INDEX) {
        currentIndex = index;

        index = dev_table_FindNextEp(index);

        clearDeviceTableIndex(currentIndex);
    }
}

static void dev_table_Init(void)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        clearDeviceTableIndex(i);
    }
}


uint16_t dev_table_GetIndexFromEui64AndEp(EUI64 eui64, uint8_t endpoint)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(!matchEui64(deviceTable[i].eui64, eui64)
                && deviceTable[i].endpoint == endpoint) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

uint16_t dev_table_GetNodeIdFromEui64(const EUI64 eui64)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {

        if(!matchEui64(deviceTable[i].eui64, eui64)) {
            return deviceTable[i].nodeId;
        }
    }
    return DEVICE_TABLE_NULL_NODE_ID;
}

int dev_table_GetEui64FromNodeId(uint16_t nodeid, EUI64 eui64)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == nodeid) {
            memcpy(eui64, deviceTable[i].eui64, EUI64_SIZE);
            return 0;
        }
    }
    return -1;
}

uint16_t dev_table_GetIndexFromNodeId(uint16_t nodeid)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == nodeid) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

uint16_t dev_table_FindFreeDeviceTableIndex(void)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == DEVICE_TABLE_NULL_NODE_ID) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

uint16_t dev_table_GetEpFromNodeIdAndEp(uint16_t nodeid, uint8_t endpoint)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == nodeid
                && deviceTable[i].endpoint == endpoint) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

void dev_table_CopyDeviceTableEntry(uint16_t fromIndex, uint16_t toIndex)
{
    dev_tab_Entry* from = &(deviceTable[fromIndex]);
    dev_tab_Entry* to = &(deviceTable[toIndex]);

    memcpy(to, from, sizeof(dev_tab_Entry));
}

uint8_t dev_table_NumberOfEpsFromIndex(uint16_t index)
{
    uint8_t count = 0;
    uint16_t currentNodeId = dev_table_GetNodeIdFromIndex(index);
    uint16_t i;

    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == currentNodeId) {
            count++;
        }
    }
    return count;
}

static uint16_t findIndexFromNodeIdAndIndex(uint16_t nodeId, uint16_t index)
{
    uint16_t i;
    for(i = index; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(nodeId == dev_table_GetNodeIdFromIndex(i)) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

static uint16_t findIndexFromEui64AndIndex(EUI64 eui64, uint16_t index)
{
    uint16_t i;
    for(i = index; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(!matchEui64(eui64, deviceTable[i].eui64)) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

uint16_t dev_table_first_ep_id(uint16_t nodeId)
{
    return findIndexFromNodeIdAndIndex(nodeId, 0);
}

uint16_t dev_table_FindNextEp(uint16_t index)
{
    return findIndexFromEui64AndIndex(deviceTable[index].eui64, index + 1);
}

uint16_t dev_table_FindFirstEpIeee(EUI64 eui64)
{
    return findIndexFromEui64AndIndex(eui64, 0);
}

uint16_t dev_table_GetFirstIndexFromEui64(EUI64 eui64)
{
    return dev_table_FindFirstEpIeee(eui64);
}

int dev_table_AddNewEp(uint16_t index, uint8_t newEp)
{
    uint16_t newIndex = dev_table_FindFreeDeviceTableIndex();

    if(newIndex == DEVICE_TABLE_NULL_INDEX) {
        return -1;
    }

    dev_table_CopyDeviceTableEntry(index, newIndex);
    deviceTable[newIndex].endpoint = newEp;

    return 0;
}

uint16_t dev_table_FindIndexNodeIdEp(uint16_t nodeId, uint8_t endpoint)
{
    uint16_t i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        if(deviceTable[i].nodeId == nodeId
                && deviceTable[i].endpoint == endpoint) {
            return i;
        }
    }

    return DEVICE_TABLE_NULL_INDEX;
}

dev_tab_Entry* dev_table_FindDeviceTableEntry(uint16_t index)
{
    if(index >= DEVICE_TABLE_DEVICE_TABLE_SIZE) {
        return NULL;
    }
    return &(deviceTable[index]);
}

void dev_table_UpdateNodeId(uint16_t currentNodeId, uint16_t newNodeId)
{
    uint16_t index = dev_table_first_ep_id(currentNodeId);

    while(index != DEVICE_TABLE_NULL_INDEX) {
        deviceTable[index].nodeId = newNodeId;

        index = dev_table_FindNextEp(index);
    }
}

void dev_table_UpdateDeviceState(uint16_t index, uint8_t newState)
{
    while(index != DEVICE_TABLE_NULL_INDEX) {

        // deviceTable[index].state = newState;
        index = dev_table_FindNextEp(index);

    }
}
// Save/Load the devices
void dev_table_Save(void)
{
    FILE* fp;
    int i;
    uint8_t j;
    char patch[128] = {0};
    sprintf(patch, "%s/%s", dev_tab_patch, "devices.txt");
    // Save device table
    fp = fopen(patch, "w");

    for(i = 0;
            i < DEVICE_TABLE_DEVICE_TABLE_SIZE;
            i++) {
        dev_tab_Entry* dev = dev_table_Pointer(i);
        if(dev->nodeId != DEVICE_TABLE_NULL_NODE_ID) {
            fprintf(fp,
                    "%x %x %x ",
                    dev->nodeId,
                    dev->endpoint,
                    dev->deviceId);
            for(j = 0; j < 8; j++) {
                fprintf(fp, "%x ", dev->eui64[j]);
            }
            for(j = 0; j < DEVICE_TABLE_CLUSTER_SIZE; j++) {
                fprintf(fp, "%x ", dev->clusterIds[j]);
            }
            fprintf(fp, "%d ", dev->clusterOutStartPosition);
        }
    }

    // Write ffffffff to mark the end
    fprintf(fp, "\r\nffffffff\r\n");
    fclose(fp);
}

static void dev_table_Load(void)
{
    uint16_t i;
    uint16_t j;
    FILE* fp;
    unsigned int data, data2, data3;

    char patch[128] = {0};
    sprintf(patch, "%s/%s", dev_tab_patch, "devices.txt");
    fp = fopen(patch, "r");

    if(!fp) {
        return;
    }

    for(i = 0; i < DEVICE_TABLE_DEVICE_TABLE_SIZE && feof(fp) == false; i++) {
        dev_tab_Entry* dev = dev_table_Pointer(i);
        fscanf(fp, "%x %x %x", &data2, &data, &data3);
        dev->endpoint = (uint8_t) data;
        dev->nodeId   = (uint16_t) data2;
        dev->deviceId = (uint16_t) data3;

        if(dev->nodeId != DEVICE_TABLE_NULL_NODE_ID) {
            for(j = 0; j < 8; j++) {
                fscanf(fp, "%x", &data);
                dev->eui64[j] = (uint8_t) data;
            }
            for(j = 0; j < DEVICE_TABLE_CLUSTER_SIZE; j++) {
                fscanf(fp, "%x", &data);
                dev->clusterIds[j] = (uint16_t) data;
            }
            fscanf(fp, "%d", &data);
            dev->clusterOutStartPosition = (uint16_t) data;
        }
    }

    fclose(fp);

    // Set the rest of the device table to null.
    for(; i < DEVICE_TABLE_DEVICE_TABLE_SIZE; i++) {
        dev_tab_Entry* dev = dev_table_Pointer(i);
        dev->nodeId = DEVICE_TABLE_NULL_NODE_ID;
    }
}

//------------------------------------------------------------

void dev_info_Save(void)
{
    FILE* fp;
    int i;
    uint8_t j;
    char patch[128] = {0};
    sprintf(patch, "%s/%s", dev_tab_patch, "info.txt");

    fp = fopen(patch, "w");

    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {

        dev_info_Entry* dev = dev_info_Pointer(i);
        if(dev->used != 0xff) {
            fprintf(fp, "%x ", dev->used);

            for(j = 0; j < 8; j++) {
                fprintf(fp, "%x ", dev->eui64[j]);
            }
            for(j = 0; j < 32; j++) {
                fprintf(fp, "%x ", dev->mid[j]);
            }
            for(j = 0; j < 32; j++) {
                fprintf(fp, "%x ", dev->mfd[j]);
            }
        }
    }
    fprintf(fp, "\r\nffffffff\r\n");
    fclose(fp);
}


static void dev_info_Load(void)
{
    uint16_t i;
    uint16_t j;
    FILE* fp;
    unsigned int data;
    char patch[128] = {0};

    sprintf(patch, "%s/%s", dev_tab_patch, "info.txt");
    fp = fopen(patch, "r");

    if(!fp) {
        return;
    }

    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE && feof(fp) == false; i++) {

        dev_info_Entry* dev = dev_info_Pointer(i);
        fscanf(fp, "%x", &data);
        dev->used = (uint8_t) data;

        if(dev->used != 0xff) {
            for(j = 0; j < 8; j++) {
                fscanf(fp, "%x", &data);
                dev->eui64[j] = (uint8_t) data;
            }
            for(j = 0; j < 32; j++) {
                fscanf(fp, "%x", &data);
                dev->mid[j] = (uint16_t) data;
            }
            for(j = 0; j < 32; j++) {
                fscanf(fp, "%x", &data);
                dev->mfd[j] = (uint16_t) data;
            }
        }
    }
    fclose(fp);
    for(; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        dev->used = 0xff;
    }
}


void dev_info_set(uint8_t* mac , const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        if(dev->used != 0xff) {
            continue;
        }

        memcpy(dev->eui64, mac, 8);
        dev->used   = 1;
        dev->mfd[0] = mlen;
        dev->mid[0] = len;
        if(mlen) {
            memcpy(&dev->mfd[1], mfc, mlen);
        }
        if(len) {
            memcpy(&dev->mid[1], mode, len);
        }
        dev_info_Save();
        return;
    }
}


void dev_info_online(void)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        if(dev->used == 0xff) {
            continue;
        }
        // OSAL_DBG("mfd:%d s:%s  mid:%d  s:%s",dev->mfd[0],(char*)&dev->mfd[1],dev->mid[0]);
        zcl_cb_rejoin(dev->eui64, &dev->mfd[1],
                      dev->mfd[0], &dev->mid[1], dev->mid[0]);
    }
}
//-------------------------------------------------------------------------------------------------------------
void dev_bind_Save(void)
{
    FILE* fp;
    int i;
    uint8_t j;
    char patch[128] = {0};
    sprintf(patch, "%s/%s", dev_tab_patch, "bind.txt");

    fp = fopen(patch, "w");

    for(i = 0; i < DEVICE_TABLE_BIND_SIZE; i++) {

        dev_bind_Entry* dev = bind_info_Pointer(i);
        if(dev->inlen == 0) {
            continue;
        }
        fprintf(fp, "%x ", dev->inlen);
        fprintf(fp, "%s ", dev->input);
        fprintf(fp, "%x ", dev->outlen);
        fprintf(fp, "%s ", dev->output);
    }
    fprintf(fp, "\r\nffffffff\r\n");
    fclose(fp);
}

static void dev_bind_Load(void)
{
    uint16_t i;
    uint16_t j;
    FILE* fp;
    unsigned int data;
    char patch[128] = {0};

    sprintf(patch, "%s/%s", dev_tab_patch, "bind.txt");
    fp = fopen(patch, "r");

    if(!fp) {
        return;
    }

    for(i = 0; i < DEVICE_TABLE_BIND_SIZE && feof(fp) == false; i++) {

        dev_bind_Entry* info = bind_info_Pointer(i);
        fscanf(fp, "%x", &data);

        if(data >= 0xffff) {
            break;
        }
        if(data) {
            info->inlen = data;
            info->input = malloc(info->inlen + 1);
            if(info->input) {
                fscanf(fp, "%s", info->input);
            }
        }

        fscanf(fp, "%x", &data);
        if(data) {
            info->outlen = data;
            info->output = malloc(info->outlen + 1);
            if(info->output) {
                fscanf(fp, "%s", info->output);
            }
        }
    }
    fclose(fp);
}

void bind_info_set(uint8_t index , const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen)
{
    if(index > DEVICE_TABLE_BIND_SIZE) {
        return;
    }

    dev_bind_Entry* bind = bind_info_Pointer(index);

    if(bind->input) {
        free(bind->input);
    }
    if(bind->output) {
        free(bind->output);
    }

    bind->inlen  = inlen;
    bind->outlen = outlen;

    if(inlen) {
        bind->input = malloc(bind->inlen + 1);
        if(bind->input) {
            memcpy(bind->input, input, inlen);
        }
    }
    if(outlen) {
        bind->output = malloc(bind->outlen + 1);
        if(bind->output) {
            memcpy(bind->output, output, outlen);
        }
    }
    dev_bind_Save();
}

void bind_info_all(void)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_BIND_SIZE; i++) {

        dev_bind_Entry* bind = bind_info_Pointer(i);

        if(bind->inlen) {
            zcl_cb_bind(i, bind->input, bind->inlen, bind->output, bind->outlen);
        }
    }
}

void bind_info_clear(void)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_BIND_SIZE; i++) {

        dev_bind_Entry* bind = bind_info_Pointer(i);

        bind->inlen  = 0;
        bind->outlen = 0;

        if(bind->input) {
            free(bind->input);
        }
        if(bind->output) {
            free(bind->output);
        }
    }
    dev_bind_Save();
}

/**
 * if  devmac = NULL
 * return devnum
*/

int  dev_numDevices(freezgb_mac* devmac, size_t sz)
{
    uint8_t mac[8] = {0};
    size_t devnum = 0;
    uint16_t i;

    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {

        dev_info_Entry* dev = dev_info_Pointer(i);

        if(dev->used == 0xff) {
            continue;
        }

        if(devmac != NULL) {
            if(devnum < sz) {
                memcpy(devmac[devnum], dev->eui64, 8);
            } else {
                return devnum;
            }
        }
        devnum++;
    }
    return devnum;
}

void dev_info_clear(EUI64 mac)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        if(dev->used == 0xff) {
            continue;
        }

        if(!memcmp(dev->eui64, mac, 8)) {
            dev->used = 0xff;
            dev_info_Save();
            return;
        }
    }
}

uint16_t dev_info_get_index(const uint8_t* mac)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        if(dev->used == 0xff) {
            continue;
        }

        if(!memcmp(dev->eui64, mac, 8)) {
            return i;
        }
    }
    return DEVICE_TABLE_NULL_INDEX;
}

static void dev_info_init(void)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_DEVICE_INFO_SIZE; i++) {
        dev_info_Entry* dev = dev_info_Pointer(i);
        dev->used = 0xff;
    }
}

static void bind_info_init(void)
{
    int i;
    for(i = 0; i < DEVICE_TABLE_BIND_SIZE; i++) {
        dev_bind_Entry* bind = bind_info_Pointer(i);
        bind->inlen  = 0;
        bind->outlen = 0;

        if(bind->input) {
            free(bind->input);
        }
        if(bind->output) {
            free(bind->output);
        }
        bind->input  = NULL;
        bind->output = NULL;
    }
}


//---------------------------------------------------------------------------------------------------

void dev_table_loaddev(const char* path)
{
    if(path) {
        strcpy(dev_tab_patch, path);
    }
    dev_table_Init();
    dev_info_init();
    bind_info_init();

    dev_table_Load();
    dev_info_Load();
    dev_bind_Load();
}


void dev_table_Clear(void)
{
    dev_table_Init();
    dev_info_init();
    bind_info_init();

    dev_table_Save();
    dev_info_Save();
    dev_bind_Save();
}

