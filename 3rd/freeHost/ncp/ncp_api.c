#include <errno.h>
#include "task_event.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "ncp_dev.h"
#include "ncp_cb.h"
#include "zcl_cb.h"
#include "discover.h"
#include "zbhci.h"
#include "ncp_network.h"
#include "device-table.h"

int nec_net_status = 0;


//--------------------------------------------------------------------------------------------

void ncp_net_up_cb(void)
{
    OSAL_DBG("_net_up ");
    nec_net_status = net_state_ready;

    zcl_cb_net_ready();
}

void ncp_net_down_cb(void)
{
    OSAL_DBG("_net_down");
    nec_net_status = net_state_uninit;
    zcl_cb_net_close();
}

int ncp_get_netstatus(void)
{
    return nec_net_status;
}

int ncp_net_isready(void)
{
    return net_state_ready == nec_net_status;
}


void ncp_allowjoin(void)
{
    if(ncp_net_isready()) {
        ncp_OpenNetwork();//300s open net
    }
}

void ncp_unallowjoin(void)
{
    if(ncp_net_isready()) {
        ncp_CloseNetwork();
    }
}

void  ncp_creat(void)
{
    ncp_StartNetwork(10);
}

void ncp_destory(void)
{
    ncp_NetworkLeave();
    discover_clear_list();
}

int ncp_get_netinfo(ncp_info_t* info)
{
    if(info == NULL) {
        return -1;
    }

    ncp_net_getinfo(info);
    return 0;
}

int ncp_get_devnum(void)
{
    return dev_numDevices(NULL, 0);
}

int ncp_get_devmac(freezgb_mac mac, uint16_t index)
{
    if(mac == NULL) {
        OSAL_ERR("mac is nil");
        return -1;
    }
    return ncp_getDevices(mac, index);
}

int ncp_get_all_devmac(freezgb_mac* maclist, size_t sz)
{
    if(maclist == NULL && (sz != 0)) {
        return -1;
    }
    return dev_numDevices(maclist, sz);
}

void ncp_all_online(void)
{
    dev_info_online();
    bind_info_all();
}

int ncp_iszdo(uint16_t profileId)
{
    return 0x0104 == profileId;
}

int ncp_iszcl(uint16_t profileId)
{
    return profileId == 0x0104;
}

void ncp_set_log_cb(void (*log)(const char* data, size_t sz))
{
    osal_logstart(log);
}

void ncp_getjoineddev(void)
{
    zbhci_devcnt();
}

void ncp_poll(void)
{

}

void ncp_init(const char* path)
{
    dev_table_loaddev(path);
    spec_ctx_init();
}

