
#include "task_event.h"
#include "device-table.h"
#include "ncp_dev.h"
#include "zcl_cb.h"

static void _copyNodeinfo(dev_tab_Entry* dev, ep_info_t* info)
{
    uint8_t clusterIdIndex = 0;

    memcpy(info->mac, dev->eui64, 8);

    info->ep          = dev->endpoint;
    info->devid       = dev->deviceId;
    info->inclust_len = dev->clusterOutStartPosition;
    info->addr        = dev->nodeId;

    for(clusterIdIndex = 0; clusterIdIndex < DEVICE_TABLE_CLUSTER_SIZE; clusterIdIndex++) {

        if(dev->clusterIds[clusterIdIndex] == ZCL_NULL_CLUSTER_ID) {
            break;
        } else {
            if(clusterIdIndex < info->inclust_len) {
                info->inclust[clusterIdIndex] = dev->clusterIds[clusterIdIndex];
            } else {
                info->outclust[clusterIdIndex - info->inclust_len] = dev->clusterIds[clusterIdIndex];
            }
        }
    }
    info->outclust_len = clusterIdIndex - info->inclust_len;
}

//new dev online
void dev_table_NewDeviceCallback(dev_tab_Entry* dev)
{
    ep_info_t info;
    _copyNodeinfo(dev, &info);
    zcl_cb_nextep(&info);
}

/**
 * dev index
 * mac :dev
 *
*/
int  ncp_getDevices(freezgb_mac mac, uint16_t index)
{
    uint8_t ieee[8] = {0};
    int devnum = 0;
    uint16_t nodeIndex;

    for(nodeIndex = 0;
            nodeIndex < DEVICE_TABLE_DEVICE_TABLE_SIZE;
            nodeIndex++) {
        dev_tab_Entry* dev = dev_table_Pointer(nodeIndex);

        if(dev->nodeId != DEVICE_TABLE_NULL_NODE_ID) {
            if(memcmp(ieee, dev->eui64, 8)) {
                memcpy(ieee, dev->eui64, 8);

                if(devnum == index) {
                    memcpy(mac, dev->eui64, 8);
                    return 0;
                }
                devnum++;
            }
        }
    }
    return -1;
}

int ncp_getdev_ep(uint8_t* eplist, size_t sz, const uint8_t* mac)
{
    size_t i = 0;
    uint16_t index = dev_table_FindFirstEpIeee((uint8_t*)mac);

    if(index == DEVICE_TABLE_NULL_INDEX) {
        return -1;
    }

    while(index != DEVICE_TABLE_NULL_INDEX) {
        if(eplist != NULL) {
            if(i < sz) {
                dev_tab_Entry* dev = dev_table_Pointer(index);

                eplist[i] = dev->endpoint;
            } else {
                return i;
            }
        }
        i++;

        index = dev_table_FindNextEp(index);
    }
    return i;
}

int  ncp_getDevices_ep(ep_info_t* info, const uint8_t* mac, uint8_t ep)
{
    uint16_t index = dev_table_GetIndexFromEui64AndEp((uint8_t*)mac, ep);

    if(index != DEVICE_TABLE_NULL_NODE_ID) {
        _copyNodeinfo(dev_table_Pointer(index), info);
        return 0;
    }

    return -1;
}

int ncp_get_device_info(dev_info_t* info, const uint8_t* mac)
{
    uint16_t index = dev_info_get_index(mac);

    if(index != DEVICE_TABLE_NULL_NODE_ID) {
        dev_info_Entry* dev = dev_info_Pointer(index);

        memcpy(info->mac, dev->eui64, 8);
        memcpy(info->mfd, dev->mfd, 32);
        memcpy(info->mid, dev->mid, 32);
        return 0;
    }
    return -1;
}

uint16_t ncp_get_device_index(const uint8_t* mac)
{
    return dev_info_get_index(mac);
}

// dev online again
void dev_table_RejoinDeviceCallback(EUI64 nodeEui64)
{

}

void dev_table_DeviceLeftCallback(EUI64 nodeEui64)
{
    zcl_cb_left(nodeEui64);
}

static void ncp_publishdevicestatechange(EUI64 eui64, uint8_t state)
{
    // zcl_cb_cb()->change(eui64, state);
}

void dev_table_StateChangeCallback(uint16_t nodeId, uint8_t state)
{
    EUI64 nodeEui64;
    dev_table_GetEui64FromNodeId(nodeId, nodeEui64);
    ncp_publishdevicestatechange(nodeEui64, state);
}


void dev_table_ClearedCallback(void)
{

}

// void ncp_devicetableClear(void)
// {
//     dev_table_Clear();
// }



