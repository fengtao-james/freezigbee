
#include "task_event.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "zcl_cb.h"
#include "ncp_cb.h"

#include "zbhci.h"
#include "device-table.h"
#include "discover.h"
#include "ncp_network.h"


static ncp_info_t net_info = {0};
//--------------------------------------------------------------------------------------------

void ncp_NetworkLeave(void)
{
    dev_table_Clear();
    zbhci_facreset();
}
//-----------------------------------------------------------------------------------------------

void ncp_ZdoLeave(uint8_t* nullEui64)
{
    discover_del_dev(nullEui64);
}

void ncp_Zdo_addr_del(uint16_t addr)
{
    EUI64 ieee = {0};

    dev_table_GetEui64FromNodeId(addr, ieee);
    zbhci_leavereq(ieee, addr);
}

//-----------------------------------------------------------------------------------------------

static void ncp_AllowRejoin(uint8_t allow)
{
    zbhci_allwojoin((allow == 0) ? 0 : 180);
}

void ncp_OpenNetwork(void)
{
    ncp_AllowRejoin(1);
}

void ncp_CloseNetwork(void)
{
    ncp_AllowRejoin(0);
}

//-----------------------------------------------------------------------------------------------

void ncp_StartNetwork(uint8_t radioTxPower)
{
    zbhci_netstart();
}

//-----------------------------------------------------------------------------------------------

void ncp_SetPower(uint8_t power)
{

}

/**
 * ncp_zcl_unicastsend
 *
*/

void ncp_zcl_unicastsend(const uint8_t* mac, uint8_t ep, uint16_t cid, uint8_t* data, uint16_t len)
{
    uint16_t nodeId = dev_table_GetNodeIdFromEui64(mac);

    if(nodeId == 0xffff) {
        return ;
    }
    if(ep == 0) {
        ep = 0xff;
    }
    zbhci_zclsend(nodeId, ep, cid, len, data);
}

void ncp_zcl_Broadcast(zcl_broadcast_t* msg, uint8_t* data, uint16_t len)
{
    zbhci_zclsend(0xffff, msg->ep, msg->cid, len, data);
}

void ncp_ota_start(uint8_t* mac, uint8_t ep)
{
    uint16_t nodeId = dev_table_GetNodeIdFromEui64(mac);

    if(nodeId == 0xffff) {
        return ;
    }

    zbhci_ota_send(nodeId, ep);
}

void ncp_zcl_groupsend(zcl_group_t* msg, uint8_t* data, uint16_t len)
{
    zbhci_group_zclsend(msg->gid, msg->ep, msg->cid, len, data);
}

void zdo_api_bind(uint8_t* mac, uint8_t ep, uint16_t clusterId)
{

    EUI64 sourceEui;
    uint16_t target = dev_table_GetNodeIdFromEui64(mac);

    if(DEVICE_TABLE_NULL_INDEX == target) {
        return ;
    }

    memcpy(sourceEui, net_info.mac, 8);
    zbhci_bindreq(mac, ep,clusterId, 0, sourceEui, 1);
}

void ncp_setnetinfo(uint8_t isok, uint8_t channel,
                    uint8_t radioTxPower,
                    uint16_t panId, uint8_t* mac)
{
    net_info.power = radioTxPower;
    net_info.Channel = channel;
    net_info.panId = panId;

    memcpy(net_info.mac, mac, 8);
    if(isok) {
        ncp_net_up_cb();
    } else {
        dev_table_Clear();
        ncp_net_down_cb();
    }
}

void ncp_net_getinfo(ncp_info_t* info)
{
    *info = net_info;
}

void ncp_netreset(void)
{
    zbhci_sysreset();
}

uint8_t* ncp_net_mac(void)
{
    return net_info.mac;
}

void ncp_net_nodeidreq(const uint8_t* mac)
{
    zbhci_addrreq(mac);
}

void ncp_net_ieeereq(uint16_t addr)
{
    zbhci_ieeereq(addr);
}

void ncp_setpanid(uint16_t panid)
{
    zbhci_setpanid(panid);
}

void ncp_setchannel(uint8_t channel)
{
    zbhci_setchannel(channel);
}
