/***************************************************************************//**
 * @file
 * @brief Definitions for the zcl spec cmd api.
 * code: james
 ******************************************************************************/

#ifndef _SPEC_API_H
#define _SPEC_API_H
#ifdef __cplusplus
#define EXTERN_C        extern "C"
#define EXTERN_C_BEGIN  extern "C" {
#define EXTERN_C_END    }
#else
#define EXTERN_C
#define EXTERN_C_BEGIN
#define EXTERN_C_END
#endif
EXTERN_C_BEGIN

#define ZCL_CLUSTER_GEN_BASIC                             0x0000
#define ZCL_CLUSTER_GEN_POWER_CFG                         0x0001
#define ZCL_CLUSTER_GEN_DEVICE_TEMP_CONFIG                0x0002
#define ZCL_CLUSTER_GEN_IDENTIFY                          0x0003
#define ZCL_CLUSTER_GEN_GROUPS                            0x0004
#define ZCL_CLUSTER_GEN_SCENES                            0x0005
#define ZCL_CLUSTER_GEN_ON_OFF                            0x0006
#define ZCL_CLUSTER_GEN_ON_OFF_SWITCH_CONFIG              0x0007
#define ZCL_CLUSTER_GEN_LEVEL_CONTROL                     0x0008
#define ZCL_CLUSTER_GEN_ALARMS                            0x0009
#define ZCL_CLUSTER_GEN_TIME                              0x000A
#define ZCL_CLUSTER_GEN_LOCATION                          0x000B
#define ZCL_CLUSTER_GEN_ANALOG_INPUT_BASIC                0x000C
#define ZCL_CLUSTER_GEN_ANALOG_OUTPUT_BASIC               0x000D
#define ZCL_CLUSTER_GEN_ANALOG_VALUE_BASIC                0x000E
#define ZCL_CLUSTER_GEN_BINARY_INPUT_BASIC                0x000F
#define ZCL_CLUSTER_GEN_BINARY_OUTPUT_BASIC               0x0010
#define ZCL_CLUSTER_GEN_BINARY_VALUE_BASIC                0x0011
#define ZCL_CLUSTER_GEN_MULTISTATE_INPUT_BASIC            0x0012
#define ZCL_CLUSTER_GEN_MULTISTATE_OUTPUT_BASIC           0x0013
#define ZCL_CLUSTER_GEN_MULTISTATE_VALUE_BASIC            0x0014
#define ZCL_CLUSTER_GEN_COMMISSIONING                     0x0015
#define ZCL_CLUSTER_GEN_PARTITION			  0x0016
#define ZCL_CLUSTER_OTA                                   0x0019
#define ZCL_CLUSTER_GEN_POWER_PROFILE			  0x001A
#define ZCL_CLUSTER_GEN_APPLIANCE_CONTROL		  0x001B
#define ZCL_CLUSTER_GEN_POLL_CONTROL			  0x0020
#define ZCL_CLUSTER_GEN_GREEN_POWER		          0x0021
#define ZCL_CLUSTER_GEN_KEEP_ALIVE		          0x0025
#define ZCL_CLUSTER_GEN_DIAGNOSTICS                       0x0B05

// Closures Clusters
#define ZCL_CLUSTER_CLOSURES_SHADE_CONFIG                 0x0100
#define ZCL_CLUSTER_CLOSURES_DOOR_LOCK                    0x0101
#define ZCL_CLUSTER_CLOSURES_WINDOW_COVERING              0x0102

// HVAC Clusters
#define ZCL_CLUSTER_HVAC_PUMP_CONFIG_CONTROL              0x0200
#define ZCL_CLUSTER_HAVC_THERMOSTAT                       0x0201
#define ZCL_CLUSTER_HAVC_FAN_CONTROL                      0x0202
#define ZCL_CLUSTER_HAVC_DIHUMIDIFICATION_CONTROL         0x0203
#define ZCL_CLUSTER_HAVC_USER_INTERFACE_CONFIG            0x0204

// Lighting Clusters
#define ZCL_CLUSTER_LIGHTING_COLOR_CONTROL                0x0300
#define ZCL_CLUSTER_LIGHTING_BALLAST_CONFIG               0x0301

// Measurement and Sensing Clusters
#define ZCL_CLUSTER_MS_ILLUMINANCE_MEASUREMENT            0x0400
#define ZCL_CLUSTER_MS_ILLUMINANCE_LEVEL_SENSING_CONFIG   0x0401
#define ZCL_CLUSTER_MS_TEMPERATURE_MEASUREMENT            0x0402
#define ZCL_CLUSTER_MS_PRESSURE_MEASUREMENT               0x0403
#define ZCL_CLUSTER_MS_FLOW_MEASUREMENT                   0x0404
#define ZCL_CLUSTER_MS_RELATIVE_HUMIDITY                  0x0405
#define ZCL_CLUSTER_MS_OCCUPANCY_SENSING                  0x0406
#define ZCL_CLUSTER_MS_LEAF_WETNESS			  0x0407
#define ZCL_CLUSTER_MS_SOIL_MOISTURE			  0x0408
#define ZCL_CLUSTER_MS_ELECTRICAL_MEASUREMENT		  0x0B04

// Security and Safety (SS) Clusters
#define ZCL_CLUSTER_SS_IAS_ZONE                           0x0500
#define ZCL_CLUSTER_SS_IAS_ACE                            0x0501
#define ZCL_CLUSTER_SS_IAS_WD                             0x0502

// Protocol Interfaces
#define ZCL_CLUSTER_PI_GENERIC_TUNNEL                     0x0600
#define ZCL_CLUSTER_PI_BACNET_PROTOCOL_TUNNEL             0x0601
#define ZCL_CLUSTER_PI_ANALOG_INPUT_BACNET_REG            0x0602
#define ZCL_CLUSTER_PI_ANALOG_INPUT_BACNET_EXT            0x0603
#define ZCL_CLUSTER_PI_ANALOG_OUTPUT_BACNET_REG           0x0604
#define ZCL_CLUSTER_PI_ANALOG_OUTPUT_BACNET_EXT           0x0605
#define ZCL_CLUSTER_PI_ANALOG_VALUE_BACNET_REG            0x0606
#define ZCL_CLUSTER_PI_ANALOG_VALUE_BACNET_EXT            0x0607
#define ZCL_CLUSTER_PI_BINARY_INPUT_BACNET_REG            0x0608
#define ZCL_CLUSTER_PI_BINARY_INPUT_BACNET_EXT            0x0609
#define ZCL_CLUSTER_PI_BINARY_OUTPUT_BACNET_REG           0x060A
#define ZCL_CLUSTER_PI_BINARY_OUTPUT_BACNET_EXT           0x060B
#define ZCL_CLUSTER_PI_BINARY_VALUE_BACNET_REG            0x060C
#define ZCL_CLUSTER_PI_BINARY_VALUE_BACNET_EXT            0x060D
#define ZCL_CLUSTER_PI_MULTISTATE_INPUT_BACNET_REG        0x060E
#define ZCL_CLUSTER_PI_MULTISTATE_INPUT_BACNET_EXT        0x060F
#define ZCL_CLUSTER_PI_MULTISTATE_OUTPUT_BACNET_REG       0x0610
#define ZCL_CLUSTER_PI_MULTISTATE_OUTPUT_BACNET_EXT       0x0611
#define ZCL_CLUSTER_PI_MULTISTATE_VALUE_BACNET_REG        0x0612
#define ZCL_CLUSTER_PI_MULTISTATE_VALUE_BACNET_EXT        0x0613
#define ZCL_CLUSTER_PI_11073_PROTOCOL_TUNNEL              0x0614

// Smart Energy Clusters
#define ZCL_CLUSTER_SE_PRICE                              0x0700
#define ZCL_CLUSTER_SE_Demand_RSP_AND_LOAD_CONTROL        0x0701
#define ZCL_CLUSTER_SE_METERING                    	  0x0702
#define ZCL_CLUSTER_SE_MESSAGING                          0x0703
#define ZCL_CLUSTER_SE_SE_TUNNELING                       0x0704
#define ZCL_CLUSTER_SE_PREPAYMENT                         0x0705
#define ZCL_CLUSTER_GEN_KEY_ESTABLISHMENT                 0x0800



/** @addtogroup zcl_group_cmdId GROUP Command Ids
 * @{
 */
#define ZCL_CMD_GROUP_ADD_GROUP                           0x00
#define ZCL_CMD_GROUP_REMOVE_GROUP                        0x03
#define ZCL_CMD_GROUP_REMOVE_ALL_GROUP                    0x04

#define ZCL_CMD_GROUP_ADD_GROUP_RSP                       0x00
#define ZCL_CMD_GROUP_REMOVE_GROUP_RSP                    0x03
/** @} end of group zcl_group_cmdId */

#define ZCL_CMD_SCENE_ADD_SCENE                           0x00
#define ZCL_CMD_SCENE_REMOVE_SCENE                        0x02
#define ZCL_CMD_SCENE_REMOVE_ALL_SCENE                    0x03
#define ZCL_CMD_SCENE_STORE_SCENE                         0x04
#define ZCL_CMD_SCENE_RECALL_SCENE                        0x05

#define ZCL_CMD_SCENE_ADD_SCENE_RSP			  0x00
#define ZCL_CMD_SCENE_REMOVE_SCENE_RSP			  0x02
#define ZCL_CMD_SCENE_REMOVE_ALL_SCENE_RSP		  0x03
#define ZCL_CMD_SCENE_STORE_SCENE_RSP			  0x04

// Client generated command
#define ZCL_CMD_ZONE_ENROLL_RSP                           0x00
#define ZCL_CMD_INIT_NORMAL_OPERATION_MODE		  0x01
#define ZCL_CMD_INIT_TEST_MODE				  0x02

// Server generated command
#define ZCL_CMD_ZONE_STATUS_CHANGE_NOTIFICATION           0x00
#define ZCL_CMD_ZONE_ENROLL_REQ                           0x01


enum spec_status {
    spec_st_ok,
    spec_st_timeout
};

typedef void(*spec_ev_cb)(const uint8_t* mac,
                          uint8_t ep,
                          uint8_t sql,
                          enum spec_status status,
                          uint8_t rssi);

int spec_ev_open_and_start(const uint8_t mac[8],
                           uint8_t ep,
                           uint8_t sql,
                           const spec_ev_cb cb);

void spec_ev_reset(void);
/**
 * ctx : ev ctx
 * gid:group id
*/
int spec_add_group_start(const uint8_t mac[8],
                         uint8_t ep,
                         uint8_t sql,
                         const spec_ev_cb cb,
                         uint16_t gid);

/**
 * ctx : ev ctx
 * gid:group id
*/
int spec_remove_group_start(const uint8_t mac[8],
                            uint8_t ep,
                            uint8_t sql,
                            const spec_ev_cb cb,
                            uint16_t gid);

/**
 * ctx : ev ctx
*/
int spec_remove_all_group_start(const uint8_t mac[8],
                                uint8_t ep,
                                uint8_t sql,
                                const spec_ev_cb cb);

/**
 * ctx : ev ctx
 * gid :group id
 * sid :scenes id
*/
int spec_store_scenes_start(const uint8_t mac[8],
                            uint8_t ep,
                            uint8_t sql,
                            const spec_ev_cb cb,
                            uint16_t gid,
                            uint8_t sid);
/**
 * ctx : ev ctx
 * gid :group id
 * sid :scenes id
*/
int spec_remove_scenes_start(const uint8_t mac[8],
                             uint8_t ep,
                             uint8_t sql,
                             const spec_ev_cb cb,
                             uint16_t gid,
                             uint8_t sid);

/**
 * ctx : ev ctx
 * gid :group id
*/
int spec_remove_all_scenes_start(const uint8_t mac[8],
                                 uint8_t ep,
                                 uint8_t sql,
                                 const spec_ev_cb cb,
                                 uint16_t gid);



/**
 * gid :group id
 * sql :sqlnum
 * send for group
*/
int spec_remove_group_scanes(uint16_t gid, uint8_t sql);

/**
 * gid :group id
 * sid :scene id
 * sql :sqlnum
 * send for group
*/
int spec_store_group_scenes(uint16_t gid, uint8_t sid, uint8_t sql);

/**
 * gid :group id
 * sid :scene id
 * sql :sqlnum
 * send for group
*/
int spec_call_group_scenes(uint16_t gid,  uint8_t sid, uint8_t sql);


EXTERN_C_END
#endif