/*****************************************************************************
 * @file
 * @brief Definitions for the zcl api.
 * code: james
 ******************************************************************************/

#ifndef SILABS_NCP_API_H
#define SILABS_NCP_API_H
#include <stdint.h>
#include <stddef.h>
#ifdef __cplusplus
#define EXTERN_C        extern "C"
#define EXTERN_C_BEGIN  extern "C" {
#define EXTERN_C_END    }
#else
#define EXTERN_C
#define EXTERN_C_BEGIN
#define EXTERN_C_END
#endif
EXTERN_C_BEGIN

#ifndef FALSE
#define FALSE       0
#endif

#ifndef TRUE
#define TRUE       1
#endif

#define HOSTVER_STRING_LENGTH   14 // 13 characters + NULL (99.99.99-9999)

#define CLUSTE_MAX_SZ           10
#define ZIGBEE_MAC_SZ           8

/**
 * @brief ncp_creat open and start ncp
*/
void ncp_creat(void);

/**
 * @brief ncp_destory:leave ncp  network
*/
void ncp_destory(void);

/**
 * @brief ncp_get_netinfo:get ncp info
 * @param name :function name enum sys_cbindex
 * @param func :function
 * @return
*/
#define ZIGBEE_MAC_SZ           8

typedef struct ncp_info {
    char ncpStackVerString[HOSTVER_STRING_LENGTH];
    uint8_t power;
    uint8_t Channel;
    int panId;
    uint8_t mac[ZIGBEE_MAC_SZ];
    uint8_t expanid[ZIGBEE_MAC_SZ];
} ncp_info_t;

typedef uint8_t freezgb_mac[ZIGBEE_MAC_SZ];

int ncp_get_netinfo(ncp_info_t* info);

/**
 * @brief ncp_allowjoin:allow dev join 300s
 * @return :0 ok !0 error
*/
void ncp_allowjoin(void);//300s
/**
 * @brief ncp_unallowjoin:close dev join
 * @return :0 ok !0 error
*/
void ncp_unallowjoin(void);

/**
 * @brief freezgb_get_devnum:get all dev num
 * @return :0 no dev
 *         !0 dev num
*/
int ncp_get_devnum(void);

/**
 * @brief freezgb_get_devmac:get  dev mac from index
 * @return :-1 no dev
 *         0 dev OK
*/
int ncp_get_devmac(freezgb_mac mac, uint16_t index);

/**
 * @brief freezgb_get_all_devmac:get all dev mac
 * @param maclist if maclist nil return all dev num
 * @return :-1 no dev
 *         0 dev OK
*/
int ncp_get_all_devmac(freezgb_mac* maclist, size_t sz);


void ncp_all_online(void);

/**
 * @brief ncp_get_net:net status
 * @return :-1 error  >=0 net_state
*/
enum net_state {
    net_state_ready = 0,
    net_state_uninit,
    net_state_init
};

int  ncp_get_netstatus(void);

int ncp_iszdo(uint16_t profileId);

int ncp_iszcl(uint16_t profileId);

/**
 * @brief zcl_set_log_cb:set log cb
 * @param log :cb
 * @return 0:OK  !0 error
*/
void ncp_set_log_cb(void (*log)(const char* data, size_t sz));

/**
 * @brief ncp_poll
 * @return 0:OK  !0 error
*/
void ncp_poll(void);


void ncp_getjoineddev(void);
/**
 * @brief ncp_init
 * @return 0:OK  !0 error
*/
void ncp_init(const char* path);


int ncp_uart_start(const char* dev, const char* dev_path, int bps);
EXTERN_C_END
#endif
