/***************************************************************************//**
 * @file
 * @brief Definitions for the zcl attr api.
 * code: james
 ******************************************************************************/

#ifndef SILABS_ATTR_API_H
#define SILABS_ATTR_API_H
#include "zcl_api.h"
#ifdef __cplusplus
#define EXTERN_C        extern "C"
#define EXTERN_C_BEGIN  extern "C" {
#define EXTERN_C_END    }
#else
#define EXTERN_C
#define EXTERN_C_BEGIN
#define EXTERN_C_END
#endif
EXTERN_C_BEGIN
/*attr cmd*/
#define GLOBAL_CMD_READ                                    0x00
#define GLOBAL_CMD_READ_RSP                                0x01
#define GLOBAL_CMD_WRITE                                   0x02
#define GLOBAL_CMD_WRITE_UNDIVIDED                         0x03
#define GLOBAL_CMD_WRITE_RSP                               0x04
#define GLOBAL_CMD_WRITE_NO_RSP                            0x05
#define GLOBAL_CMD_CONFIG_REPORT                           0x06
#define GLOBAL_CMD_CONFIG_REPORT_RSP                       0x07
#define GLOBAL_CMD_READ_REPORT_CFG                         0x08
#define GLOBAL_CMD_READ_REPORT_CFG_RSP                     0x09
#define GLOBAL_CMD_REPORT                                  0x0a
#define GLOBAL_CMD_DEFAULT_RSP                             0x0b
#define GLOBAL_CMD_DISCOVER_ATTRS                          0x0c
#define GLOBAL_CMD_DISCOVER_ATTRS_RSP                      0x0d
#define GLOBAL_CMD_DISCOVER_CMDS_RECEIVED                  0x11
#define GLOBAL_CMD_DISCOVER_CMDS_RECEIVED_RSP              0x12
#define GLOBAL_CMD_DISCOVER_CMDS_GEN                       0x13
#define GLOBAL_CMD_DISCOVER_CMDS_GEN_RSP                   0x14
#define GLOBAL_CMD_DISCOVER_ATTRS_EXT                      0x15
#define GLOBAL_CMD_DISCOVER_ATTRS_EXT_RSP                  0x16


/*** Data Types ***/
#define ZCL_ATTRIBUTE_NO_DATA                            0x00    // No data
#define ZCL_ATTRIBUTE_DATA8                              0x08    // 8-bit data
#define ZCL_ATTRIBUTE_DATA16                             0x09    // 16-bit data
#define ZCL_ATTRIBUTE_DATA24                             0x0a    // 24-bit data
#define ZCL_ATTRIBUTE_DATA32                             0x0b    // 32-bit data
#define ZCL_ATTRIBUTE_DATA40                             0x0c    // 40-bit data
#define ZCL_ATTRIBUTE_DATA48                             0x0d    // 48-bit data
#define ZCL_ATTRIBUTE_DATA56                             0x0e    // 56-bit data
#define ZCL_ATTRIBUTE_DATA64                             0x0f    // 64-bit data
#define ZCL_ATTRIBUTE_BOOLEAN                            0x10    // Boolean
#define ZCL_ATTRIBUTE_BITMAP8                            0x18
#define ZCL_ATTRIBUTE_BITMAP16                           0x19
#define ZCL_ATTRIBUTE_BITMAP24                           0x1a
#define ZCL_ATTRIBUTE_BITMAP32                           0x1b
#define ZCL_ATTRIBUTE_BITMAP40                           0x1c
#define ZCL_ATTRIBUTE_BITMAP48                           0x1d
#define ZCL_ATTRIBUTE_BITMAP56                           0x1e
#define ZCL_ATTRIBUTE_BITMAP64                           0x1f
#define ZCL_ATTRIBUTE_UINT8                              0x20
#define ZCL_ATTRIBUTE_UINT16                             0x21
#define ZCL_ATTRIBUTE_UINT24                             0x22
#define ZCL_ATTRIBUTE_UINT32                             0x23
#define ZCL_ATTRIBUTE_UINT40                             0x24
#define ZCL_ATTRIBUTE_UINT48                             0x25
#define ZCL_ATTRIBUTE_UINT56                             0x26
#define ZCL_ATTRIBUTE_UINT64                             0x27
#define ZCL_ATTRIBUTE_INT8                               0x28
#define ZCL_ATTRIBUTE_INT16                              0x29
#define ZCL_ATTRIBUTE_INT24                              0x2a
#define ZCL_ATTRIBUTE_INT32                              0x2b
#define ZCL_ATTRIBUTE_INT40                              0x2c
#define ZCL_ATTRIBUTE_INT48                              0x2d
#define ZCL_ATTRIBUTE_INT56                              0x2e
#define ZCL_ATTRIBUTE_INT64                              0x2f
#define ZCL_ATTRIBUTE_ENUM8                              0x30
#define ZCL_ATTRIBUTE_ENUM16                             0x31
#define ZCL_ATTRIBUTE_SEMI_PREC                          0x38
#define ZCL_ATTRIBUTE_SINGLE_PREC                        0x39
#define ZCL_ATTRIBUTE_DOUBLE_PREC                        0x3a
#define ZCL_ATTRIBUTE_OCTET_STR                          0x41
#define ZCL_ATTRIBUTE_CHAR_STR                           0x42
#define ZCL_ATTRIBUTE_LONG_OCTET_STR                     0x43
#define ZCL_ATTRIBUTE_LONG_CHAR_STR                      0x44
#define ZCL_ATTRIBUTE_ARRAY                              0x48
#define ZCL_ATTRIBUTE_STRUCT                             0x4c
#define ZCL_ATTRIBUTE_SET                                0x50
#define ZCL_ATTRIBUTE_BAG                                0x51
#define ZCL_ATTRIBUTE_TOD                                0xe0
#define ZCL_ATTRIBUTE_DATE                               0xe1
#define ZCL_ATTRIBUTE_UTC                                0xe2
#define ZCL_ATTRIBUTE_CLUSTER_ID                         0xe8
#define ZCL_ATTRIBUTE_ATTR_ID                            0xe9
#define ZCL_ATTRIBUTE_BAC_OID                            0xea
#define ZCL_ATTRIBUTE_IEEE_ADDR                          0xf0
#define ZCL_ATTRIBUTE_128_BIT_SEC_KEY                    0xf1
#define ZCL_ATTRIBUTE_UNKNOWN                            0xff


/**
 * @brief attr_is_report:report attr
 * @param type :cmd->type
 * @param cmd :cmd->cmd
 * @return :1:OK 0:error
*/
int  attr_is_report(uint8_t cmd);

/**
 * @brief attr_is_writeack:attr write ack
 * @param type :cmd->type
 * @param cmd :cmd->cmd
 * @return :1:OK 0:error
*/
int  attr_is_writeack(uint8_t cmd);

/**
 * @brief attr_is_readack:attr read ack
 * @param type :cmd->type
 * @param cmd :cmd->cmd
 * @return :1:OK 0:error
*/
int  attr_is_readack(uint8_t cmd);

/**
 * @brief attr_is_read:attr read
 * @param type :cmd->type
 * @param cmd :cmd->cmd
 * @return :1:OK 0:error
*/
int  attr_is_read(uint8_t cmd);

/**
 * @brief attr_is_defaultdack:cmd default ack
 * @param type :cmd->type
 * @param cmd :cmd->cmd
 * @return :1:OK 0:error
*/
int  attr_is_defaultack(uint8_t cmd);

/**
 * @brief attr_unpack_report:attr report
 * @param data :report
 * @param first :buff first
 * @param last :buff last
 * @return : 0:OK
 *          -1:error
*/
typedef struct report {
    uint16_t attr_id;
    uint8_t type;
    uint8_t len;
    const uint8_t* data;
} report_t;

int attr_unpack_report(report_t* data, const uint8_t** first, const uint8_t* last);

/**
 * @brief attr_pack_read:attr read
 * @param buff :pack buff
 * @param maxsz :buff sz
 * @param attr_id :id list
 * @param num :id num
 * @return :0 :error !0 data len
*/
int attr_pack_read(uint8_t* buff, size_t maxsz, const uint16_t* attr_id, size_t num);

/**
 * @brief attr_unpack_read:attr read
 * @param attridlist :read_data_t list
 * @param maxsz :list num
 * @param buff :buff
 * @param len :buff len
 * @return :0 :error !0 data len
*/
int attr_unpack_read(uint16_t* attridlist, size_t maxsz, const void* buff, size_t len);

/**
 * @brief attr_pack_readack:attr read
 * @param list :buff packbuff
 * @param maxsz :buff sz
 * @param attid :attid
 * @param status :status
 * @param type :type
 * @param len :data len
 * @param data :data
 * @return :0 :error !0 data len
*/
int attr_pack_readack(uint8_t* buff, size_t maxsz,
                      uint16_t attid,
                      uint8_t status,
                      uint8_t type,
                      const void* data, size_t len);
/**
 * @brief attr_unpack_readack:attr read ack
 * @param data :read_rsp_t
 * @param first :buff first
 * @param last :buff last
 * @return : 0:OK
 *          -1:error
*/

typedef struct read_rsp {
    uint16_t attr_id;
    uint8_t status;// if status !=0 read error
    uint8_t type;
    uint8_t len;
    const uint8_t* data;
} read_rsp_t;

int attr_unpack_readack(read_rsp_t* data, const uint8_t** first, const uint8_t* last);

/**
 * @brief attr_pack_write:attr write
 * @param buff :pack buff
 * @param maxsz :maxsz
 * @param attid :attid
 * @param type :data type
 * @param len :data len
 * @param data :data
 * @return :0 :error !0 data len
*/
int attr_pack_write(uint8_t* buff, size_t maxsz,
                    uint16_t attid,
                    uint8_t type,
                    const void* data, size_t len);
/**
 * @brief attr_unpack_writeack:attr write ack
 * @param data :write_rsp_t
 * @param first :buff first
 * @param last :buff last
 * @return : 0:OK
 *          -1:error
*/

typedef struct write_rsp {
    uint16_t attr_id;
    uint8_t status;
} write_rsp_t;

int attr_unpack_writeack(write_rsp_t* data, const uint8_t** first, const uint8_t* last);

/**
 * @brief attr_unpack_defaultack:cmd ack
 * @param data :cmd->data
 * @param len :data len
 * @return :0: cmd ok !0:cmd error code
*/
int attr_unpack_defaultack(const void* data, size_t len);

/**
 * @brief attr_get_int:attr int num
 * @param data :attr_data_t
 * @param result :recv result
 * @return :0 OK -1 error
*/
int attr_get_intattr(uint32_t* result, const void* data, size_t len, uint8_t type);

/**
 * @brief attr_set_int: set attr int num
 * @param buff :attr_data_t
 * @param data :type :data type
 * @param result :set result
 * @return :0 :error !0 data len
*/
int attr_set_intattr(uint8_t* buff, size_t maxsz, uint32_t result, uint8_t type);

/**
 * @brief attr_pack_config_report: config report
 * @param buff :data
 * @param maxsz :sz
 * @param direction :direction
 * @param attid :attid
 * @param type :data type
 * @param min :min time
 * @param max :max time
 * @param data :change data
 * @param delay :delay
 * @return :-1 :error !0 data len
*/
int attr_pack_config_report(uint8_t* buff, size_t maxsz,
                            uint8_t direction,
                            uint16_t attid,
                            uint8_t type,
                            uint16_t min,
                            uint16_t max,
                            uint32_t data,
                            uint16_t delay);

/**
 * @brief attr_unpack_config_reportack: ack
 * @param data :config_report_rsp_t
 * @param first :start data
 * @param last :last data
 * @return :-1 :error 0 :ok
*/
typedef struct config_report_rsp {
    uint16_t attr_id;
    uint8_t direction;
    uint8_t status;
} config_report_rsp_t;

int attr_unpack_config_reportack(config_report_rsp_t* data,
                                 const uint8_t** first, const uint8_t* last);
EXTERN_C_END
#endif
