/*****************************************************************************
 * @file
 * @brief Definitions for the zcl api.
 * code: james
 ******************************************************************************/

#ifndef SILABS_ZCL_H
#define SILABS_ZCL_H

#include "ncp_api.h"
#ifdef __cplusplus
#define EXTERN_C        extern "C"
#define EXTERN_C_BEGIN  extern "C" {
#define EXTERN_C_END    }
#else
#define EXTERN_C
#define EXTERN_C_BEGIN
#define EXTERN_C_END
#endif
EXTERN_C_BEGIN


typedef struct zcldata {
    uint8_t control;
    uint16_t manucode;
    uint8_t sql;
    uint8_t cmd;
} zcldata_t;

typedef struct zcl_cb {
    void (*zcl_ready)(void* args); // sys is running
    void (*dev_online)(void* args, const uint8_t* mac, uint8_t ep); //dev add network
    void (*next_ep)(void* args, const uint8_t* mac, uint8_t ep); //dev report next endpoint
    void (*dev_rejoin)(void* args, const uint8_t* mac, const uint8_t* mfc,
                       uint8_t mlen, const uint8_t* mode, uint8_t len);//dev rejoin network
    void (*zcl_close)(void* args); // net close
    void (*dev_leave)(void* args, const uint8_t* mac);//leave network
    void (*dev_info)(void* args, const uint8_t* mac, const uint8_t* mfc,
                     uint8_t mlen, const uint8_t* mode, uint8_t len);
    void (*zdo_bind)(void* args, const uint8_t* mac, uint8_t status);
    void (*zcl_recv)(void* args,
                     const uint8_t* mac,
                     uint8_t ep,
                     uint16_t clusterId,//clusterId
                     uint8_t rssi,
                     const zcldata_t* cmd,
                     const void* data,
                     uint8_t len); //recv zcl msg
    void (*bind_info)(void* args, uint8_t index, const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen);
    void (*ota_st)(void* args, const uint8_t* mac, uint8_t ep, uint8_t status);
    void (*ota_block)(void* args, uint32_t index, uint8_t len);
    void (*ota_update)(void* args, const uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now);
    void* args;
} zcl_cb_t;

/**
 * @brief zcl_regist_handle open and start ncp
 * @param handle :user cb
 * @param path :file path
 * @param port :uart port
 * @param baudrate :uart baudrate
 * @param info :net info
*/
void zcl_regist_handle(const zcl_cb_t* handle);


/**
 * @brief zcl_deldev:leave dev,dev will use left cb report
 * @param mac :dev mac
 * @return :0 ok !0 error
*/
int zcl_deldev(const uint8_t* mac);


/**
 * @brief ota_start:ota start
 * @param mac :dev mac
 * @return :0 ok !0 error
*/

int ota_start(const uint8_t* mac, uint8_t ep);

/**
 * @brief ota_code_info:ota info
 * @param fsz :ota size
 * @return :
*/
void ota_code_info(uint32_t fsz);

/**
 * @brief zcl_ota_info:ota info
 * @param fsz :ota size
 * @return :
*/
int zcl_ota_info(const char* path);

/**
 * @brief zcl_ota_block:ota code
 * @param fsz :ota size
 * @return :
*/
int zcl_ota_block(uint32_t index, uint8_t len);

/**
 * @brief ota_code_set:ota info
 * @param index :ota index
 * @param buf :ota buff
 * @param len :ota buff len
 * @return :
*/
void ota_code_set(uint32_t index, const uint8_t* buf, uint8_t len);
/**
 * @brief zcl_send:zcl send msg
 * @param mac :dev mac
 * @param ep :dev ep
 * @param cid :zcl cluster id
 * @param control  zcl head
 * @param manucode menu
 * @param sql sql(0-127)
 * @param cmd :zcl cmd
 * @param buff :zcl data
 * @param len :data len
 * @return  -1 :cmd error
 *          =0:ok
*/
int zcl_send(const uint8_t* mac,
             uint8_t ep,
             uint16_t cid,
             uint8_t control,
             uint16_t manucode,
             uint8_t sql,
             uint8_t cmd,
             const void* buff,
             size_t len);
/**
 * @brief zcl_response:zcl send msg
 * @param mac :dev mac
 * @param ep :dev ep
 * @param cid :zcl cluster id
 * @param control  zcl head
 * @param manucode menu
 * @param sql sql(0-255)
 * @param cmd :zcl cmd
 * @param buff :zcl data
 * @param len :data len
 * @return  -1 :cmd error
 *          =0:ok
*/
int zcl_response(const uint8_t* mac,
                 uint8_t ep,
                 uint16_t cid,
                 uint8_t control,
                 uint16_t manucode,
                 uint8_t sql,
                 uint8_t cmd,
                 const void* buff,
                 size_t len);
/**
 * @brief zcl_broadcast:broadcast zcl
 * @param ep :dev ep
 * @param cid :zcl cluster id
 * @param manucode menu
 * @param sql sql(0-127)
 * @param cmd :zcl cmd
 * @param buff :zcl data
 * @param len :data len
 * @return  -1 :cmd error
 *          =0:ok
*/
int zcl_broadcast(uint8_t ep,
                  uint16_t cid,
                  uint8_t control,
                  uint16_t manucode,
                  uint8_t sql,
                  uint8_t cmd,
                  const void* buff,
                  size_t len);

/**
 * @brief zcl_groupsend:group zcl
 * @param ep :dev ep
 * @param cid :zcl cluster id
 * @param manucode menu
 * @param sql sql(0-127)
 * @param cmd :zcl cmd
 * @param buff :zcl data
 * @param len :data len
 * @return  -1 :cmd error
 *          =0:ok
*/
int zcl_groupsend(uint16_t gid,
                  uint8_t ep,
                  uint16_t cid,
                  uint8_t control,
                  uint16_t manucode,
                  uint8_t sql,
                  uint8_t cmd,
                  const void* buff,
                  size_t len);


int zcl_is_global(uint8_t control);

int zcl_is_specfic(uint8_t control);

typedef struct ep_info {
    uint8_t mac[ZIGBEE_MAC_SZ]; //8 byte
    uint16_t addr;//short addr
    uint16_t devid;
    uint8_t ep;
    uint8_t inclust_len;//inclust_list
    uint8_t outclust_len;//outclust_list
    uint16_t inclust[CLUSTE_MAX_SZ];
    uint16_t outclust[CLUSTE_MAX_SZ];
} ep_info_t;

/**
 * @brief freezgb_get_dev_ep:get ep info
 * @param eplist :eplist if eplistis nil return all ep num
 * @param sz  buff sz
 * @param mac :dev mac
 * @return ep num
*/
int zcl_get_dev_ep(uint8_t* eplist, size_t sz, const uint8_t* mac);

/**
 * @brief freezgb_get_dev_info:get dev info
 * @param epinfo :epinfo
 * @param mac  dev mac
 * @param ep :dev ep
 * @return 0:OK  !0 error
*/
int zcl_get_ep_dev_info(ep_info_t* epinfo, const uint8_t* mac, uint8_t ep);

/**
 * ctx :ctx
 * cid :cluster id
 *
*/
int zcl_zdo_bind(const uint8_t mac[8], uint8_t ep, uint16_t cid);


typedef struct dev_info {
    uint8_t     mac[ZIGBEE_MAC_SZ]; //8 byte
    uint8_t     mid[32];
    uint8_t     mfd[32];
} dev_info_t;

int zcl_get_dev_info(dev_info_t* info, const uint8_t* mac);

uint16_t zcl_get_dev_index(const uint8_t* mac);
EXTERN_C_END
#endif
