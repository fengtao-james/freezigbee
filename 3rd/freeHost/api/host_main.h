#ifndef HOST_MAIN_H
#define HOST_MAIN_H

void host_poll(void);
int host_start(const zcl_cb_t* handle, const char* dev, const char* path, int bps);
#endif