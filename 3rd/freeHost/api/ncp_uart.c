#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/select.h>

#include "task_event.h"
#include "zbhci.h"
#include "zcl_api.h"
#include "spec_api.h"

static const struct {
    int bps;
    speed_t posixBaud;
} baudTable[] = {
    { 600, B600    },
    { 1200, B1200   },
    { 2400, B2400   },
    { 4800, B4800   },
    { 9600, B9600   },
    { 19200, B19200  },
    { 38400, B38400  },
    { 57600, B57600  },
    { 115200, B115200 },
    { 230400, B230400 }
};

#define BAUD_TABLE_SIZE (sizeof(baudTable) / sizeof(baudTable[0]) )

static int setUartAttrDefault(int fdUart, int bps)
{
    size_t i;
    speed_t baud;
    struct termios tios;
    int status;
    tcgetattr(fdUart, &tios);//读取串口属性

    for(i = 0; i < BAUD_TABLE_SIZE; i++) {
        if(baudTable[i].bps == bps) {
            break;
        }
    }
    if(i < BAUD_TABLE_SIZE) {
        baud = baudTable[i].posixBaud;
    } else {
        return -1;
    }

    tcflush(fdUart, TCIOFLUSH);
    cfsetispeed(&tios, baud);
    cfsetospeed(&tios, baud);

    tios.c_cflag &= ~CSIZE;
    tios.c_cflag |= CS8;
    tios.c_cflag &= ~CSTOPB;
    tios.c_cflag &= ~CRTSCTS;
    tios.c_cflag &= ~PARENB;
    tios.c_cflag &= ~PARODD;
    tios.c_cflag |= (CLOCAL | CREAD);

    tios.c_iflag &= ~(IXON | IXOFF | IXANY);
    tios.c_iflag &= ~(INLCR | ICRNL | IGNCR);
    tios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    tios.c_oflag &= ~OPOST;
    tios.c_oflag &= ~(ONLCR | OCRNL);

    memset(tios.c_cc, _POSIX_VDISABLE, NCCS);  // disable all control chars
    tios.c_cc[VSTART] = CSTART;           // define XON and XOFF
    tios.c_cc[VSTOP] = CSTOP;
    tios.c_cc[VMIN] = 0;
    tios.c_cc[VTIME] = 0;

    tcsetattr(fdUart, TCSAFLUSH, &tios);
    sleep(1);
    return 0;
}

void uartcom_close(int serialFd)
{
    tcflush(serialFd, TCIOFLUSH);
    close(serialFd);
}

int uartcom_open(const char* szDevName, int bps)
{
    int fd = open(szDevName, O_RDWR | O_NOCTTY | O_NDELAY);
    if(fd < 0) {
        printf("Open Uart Port Faile\n");
        return -1;
    }
    if(setUartAttrDefault(fd, bps)) {
        printf("setUartAttrDefault Faile\n");
        uartcom_close(fd);
        return -1;
    }

    return fd;
}

void uartcon_send(int fd, unsigned char* msg, int len)
{
    while(len) {
        int nRet = -1;
        nRet = write(fd, msg, len);
        if(nRet == -1) {
            OSAL_ERR("send data error :%d", fd);
            return ;
        }
        len -= nRet;
        msg += nRet;
    }
}

int uart_read(int fd, unsigned char* buff, int sz)
{
    int nRet = 0;
    nRet = read(fd, buff, sz);
    if(nRet > 0) {
        return nRet;
    }
    return nRet;
}

/*-----------------------------------------------------------------------------*/
static void ep_recv_cb(void* args, int fd)
{
    uint8_t buff[128] = {0};
    int len = uart_read(fd, buff, 128);
    // {
    //     uint8_t strbuff[1024] = {0};
    //     OSAL_DBG("recv :%s", osal_log_hexstr(strbuff, buff, len));
    // }
    if(len) {
        int i;
        for(i = 0; i < len; i++) {
            uart_rx_start(&buff[i], 1);
        }
    }
}

static struct osal_ep ep_ctx = {
    .fd      = -1,
    .cb_send =  NULL,
    .cb_recv = ep_recv_cb,
    .args    = NULL
} ;

static void uart_msgsend(uint8_t* msg, uint16_t len)
{
    // {
    //     uint8_t buff[1024] = {0};
    //     OSAL_DBG("send :%s", osal_log_hexstr(buff, msg, len));
    // }
    if(ep_ctx.fd < 0) {
        return;
    }
    uartcon_send(ep_ctx.fd, msg, len);
}

static zbhci_cb_t hcicb = {
    .send_cb = uart_msgsend,
    .recv_cb = NULL
};

int ncp_uart_init(const char* dev_path, int bps)
{
    ep_ctx.fd  = uartcom_open(dev_path, bps);

    OSAL_DBG("fd:%d", ep_ctx.fd);

    if(ep_ctx.fd  > 0) {

        osal_ep_bind(&ep_ctx);
    } else {
        OSAL_DBG("fd is error :%d", ep_ctx.fd);
        // return -1;
    }

    zbhci_uart_init(&hcicb);
    return 0;
}

