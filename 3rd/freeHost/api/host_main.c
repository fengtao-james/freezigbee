/**
 * @file main.c
 * @brief host_main
 * @version 0.1
 * @date 2019-08-16
 * @author james
 */

#include "task_event.h"
#include "zcl_api.h"
#include "host_main.h"
#include "spec_api.h"
#include "ncp_uart.h"

//"/dev/ttyUSB0"
int host_start(const zcl_cb_t* handle, const char* dev, const char* path, int bps)
{
    if(ncp_uart_init(dev, bps)) {
        return -1;
    }

    ncp_init(path);
    zcl_regist_handle(handle);

    return 0;
}

void host_poll(void)
{
    osal_task_poll();
    osal_ep_poll(30);
}

