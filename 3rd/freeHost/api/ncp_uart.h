#ifndef SILABS_UART_H
#define SILABS_UART_H

int uartcom_open(const char* szDevName, int bps);
void uartcom_close(int serialFd);
void uartcon_send(int fd, unsigned char* msg, int len);
int uart_read(int fd, unsigned char* buff, int sz);

int ncp_uart_init(const char* dev_path, int bps);
#endif
