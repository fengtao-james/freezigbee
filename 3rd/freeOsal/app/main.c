/**
 * @file main.c
 * @brief freeOsal main
 * @version 0.1
 * @date 2019-08-16
 * @author james
 */

#include "task_event.h"
#include "unix_time.h"

int _timer_cb(void* args)
{
    OSAL_DBG("timer cb");
    return -1;
}

int _timer_cb1(void* args)
{
    OSAL_DBG("timer1 cb");
    return 0;
}

int _timer_cb2(void* args)
{
    OSAL_DBG("timer2 cb");
    return 0;
}

int main(int argc, char* argv[])
{
    osal_etimer_t t ;
    osal_etimer_t t1;
    osal_etimer_t t2;

    osal_os_init();
    osal_timer_start(&t,_timer_cb,NULL ,1000);
    osal_timer_start(&t1, _timer_cb1,NULL,300);
    osal_timer_start(&t2, _timer_cb2,NULL,500);
    while(1) {
        osal_task_poll();
        usleep(50*1000);
        // OSAL_DBG("poll");
    }

    return 0;
}



