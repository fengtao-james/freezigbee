/*
// Copyright (c) 2016 Intel Corporation
//LOG
*/
/**
  @file
*/
#include <syslog.h>

#include "task_event.h"
#include "unix_time.h"
// #include "uart_pub.h"


static void (*log_task)(const char* log, size_t len) = NULL;

void osal_log(char* log, size_t len)
{
    if(log_task) {
        log_task(log, len);
    }
}

void osal_logstart(void (*func)(const char* log, size_t len))
{
    log_task = func;
}

void osal_logstop(void)
{
    log_task = NULL;
}


void osal_log_debug(const char* level , const char* file ,
                    int line, const char* color, const char* msg, ...)
{
    va_list ap;
    char message[MAX_LOG_BUF] = {0};
    int nMessageLen = 0;
    int sz = 0;

    sz = snprintf(message, MAX_LOG_BUF, "%s[%lu(%s)]:<%s:%d>:", color, unix_get_tick_count(), level, file, line);

    va_start(ap, msg);
    nMessageLen = vsnprintf(message + sz, MAX_LOG_BUF - sz, msg, ap);
    va_end(ap);

    printf("%s", color);
    osal_log(message, nMessageLen + sz);
    printf("%s\n", LOG_RESET_COLOR);
}

void osal_log_printf(const char* level, const char* data)
{
    char message[MAX_LOG_BUF] = {0};
    int nMessageLen = 0;
    int sz = 0;
    char* color = LOG_COLOR_D;

    if(strstr(level, "D")) {
        color = LOG_COLOR_D;
    } else if(strstr(level, "I")) {
        color = LOG_COLOR_I;
    } else if(strstr(level, "W")) {
        color = LOG_COLOR_W;
    } else if(strstr(level, "E")) {
        color = LOG_COLOR_E;
    }

    sz = sprintf(message, "[%lu(%s)]: lua:", unix_get_tick_count(), level);

    nMessageLen = snprintf(message + sz, MAX_LOG_BUF - sz, "%s", data);

    printf("%s", color);
    osal_log(message, nMessageLen + sz);
    printf("%s\n", LOG_RESET_COLOR);

    // printf("%s[%s]%s\n",color,message,LOG_RESET_COLOR);
}
static void log_printf(const char* log, size_t len)
{
    printf("%s", log);
    // syslog(LOG_DEBUG, "%s", log);
    // os_printf("%s\n", log);
}
void osal_log_start(void)
{
    osal_logstart(log_printf);
}

char* osal_log_hexstr(char* out, const uint8_t* in, size_t sz)
{
    size_t i;
    for(i = 0; i < sz; i++) {
        sprintf(&out[i * 2], "%02hx", in[i]);
    }
    return out;
}

