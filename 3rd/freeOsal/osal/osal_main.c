/**
 * @file osal_main.c
 * @brief 
 * @version 0.1
 * @date 2021-08-12
 * @author WatWu
 */

#include "task_event.h"

void osal_os_init(void)
{
    // close interrupt
    HAL_DISABLE_INTERRUPTS();

    //osal os init
    osal_mem_init();

    //add task
    osal_timer_init();
    osal_task_init();
    osal_mem_kick();
    //alow interrupt
    HAL_ENABLE_INTERRUPTS();
}

