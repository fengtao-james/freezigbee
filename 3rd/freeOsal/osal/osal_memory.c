#include "task_event.h"


#ifdef OSAL_MEM_WATCH
#define OSAL_MEM_WATCH_MAX 128
typedef struct osal_memory_watch {
    void* prt;
    halDataAlign_t size;
    char func[32];
    int token;
} osal_memory_watch_t;

typedef struct osal_memory {
    osal_memory_watch_t slot[OSAL_MEM_WATCH_MAX];
    int slot_size;
    int handle_index;
    struct spinlock lock;
} osal_memory_t;

static osal_memory_t MEM_WATCH;

static int osal_mem_watch_register(const char* func, void* ptr, halDataAlign_t size)
{
    osal_memory_t* s = &MEM_WATCH;
    int i;
    SPIN_LOCK(s->lock)
    for(i = 0; i < s->slot_size; i++) {
        // int handle = (i + s->handle_index) & OSAL_HASHMAP_MASK;

        // int hash = handle & (s->slot_size - 1);

        // if(s->slot[hash].token == -1) {
        //     s->slot[hash].prt = ptr;
        //     s->slot[hash].size = size;
        //     strcpy(s->slot[hash].func, func);
        //     s->slot[hash].token = handle;
        //     s->handle_index = handle + 1;
        //     // OSAL_DBG("add f:%s %d sz:%d",func,hash,size);
        //     SPIN_UNLOCK(s->lock)
        //     return handle;
        // }
    }
    SPIN_UNLOCK(s->lock)
    OSAL_ERR("event is full");
    return -1;
}

static void  osal_mem_watch_retire(const char* func, void* ptr, halDataAlign_t size)
{
    osal_memory_t* s = &MEM_WATCH;
    int i;
    SPIN_LOCK(s->lock)
    for(i = 0; i < s->slot_size; i++) {
        if(s->slot[i].prt == ptr  && s->slot[i].token != -1) {
            s->slot[i].token = -1;
            // OSAL_DBG("del src:%s f:%s %d",func,s->slot[i].func,i);
             SPIN_UNLOCK(s->lock)
            return;
        }
    }
    SPIN_UNLOCK(s->lock)
    OSAL_ERR("can't find f:%s p:%p sz:%d", func, ptr, size);
}

void osal_mem_watch_check(void)
{
    int total = 0;
    int index = 0;

    osal_memory_t* s = &MEM_WATCH;
    int i;
    SPIN_LOCK(s->lock)
    for(i = 0; i < s->slot_size; i++) {
        osal_memory_watch_t* ctx = &s->slot[i];
        if(ctx->token != -1) {
            total += ctx->size;
            index++;
            OSAL_WRN("f:%s p:%p index:%d sz:%d", ctx->func, ctx->prt, i, ctx->size);
        }
    }
    SPIN_UNLOCK(s->lock)
    OSAL_ERR("TOTAL:%d ITEM:%d", total, index);
}

static void osal_mem_watch_init(void)
{
    osal_memory_t* map = &MEM_WATCH;

    map->handle_index = 1;
    map->slot_size = OSAL_MEM_WATCH_MAX;

    int i;
    for(i = 0; i < map->slot_size; i++) {
        map->slot[i].token = -1;
    }
    SPIN_INIT(map->lock)
}
#endif

#include <stdlib.h>
void osal_mem_kick(void)
{

}
void _osal_mem_free(const char* func, void* ptr){
    #ifdef OSAL_MEM_WATCH
    osal_mem_watch_retire(func, ptr,0);
    #endif
    free(ptr);
}
void* _osal_mem_alloc(const char* func, halDataAlign_t size){
    void*ptr= malloc(size);
    
    #ifdef OSAL_MEM_WATCH
    osal_mem_watch_register(func,ptr,size);
    #endif
    return ptr;
}
void osal_mem_init(void){
    #ifdef OSAL_MEM_WATCH
    osal_mem_watch_init();
    #endif
}
void* osal_mem_lalloc(void* ptr, halDataAlign_t nsize){
    if(nsize == 0) {
        free(ptr);
        return NULL;
    } else {
        return realloc(ptr, nsize);
    }
}
void* osal_mem_calloc(halDataAlign_t numElements, halDataAlign_t sizeOfElement){
    return calloc(numElements,sizeOfElement);
}
