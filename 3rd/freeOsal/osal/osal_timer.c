#include "task_event.h"



struct osal_timer_action {
    osal_etimer_t* head;
    uint32_t next_expiration;
} ;

struct osal_timer_action ACT;

static void osal_timer_executecb(void)
{
    osal_etimer_t* head, * item, *temp1, *temp2;

    head = NULL;
    OSAL_LIST_HEAD_SAFE(ACT.head, item, temp1, temp2) {

        if(!item->timeout) {
            OSAL_LIST_HEAD_DELETE(ACT.head, item);
            OSAL_LIST_HEAD_APPEND(head, item);
        }
    }

    OSAL_LIST_HEAD_SAFE(head, item, temp1, temp2) {
        int t = -1;
        OSAL_LIST_HEAD_DELETE(head, item);
        
        if(item->cb) {
            t = item->cb(item->args);
        }

        if(t < 0) {
            continue;
        } else if(t == 0) {
            item->timeout = item->period;
        } else {
            item->period = item->timeout = t;
        }
        OSAL_LIST_HEAD_APPEND(ACT.head, item);
    }
}

static void osal_timer_update(uint32_t updatetime)
{
    osal_etimer_t* item;

    ACT.next_expiration = 0;

    OSAL_LIST_HEAD_FOREACH(ACT.head, item) {

        if(item->timeout > updatetime) {
            item->timeout -= updatetime;
        } else {
            item->timeout = 0;
        }

        if(!ACT.next_expiration) {
            ACT.next_expiration = item->timeout ;
        } else if(ACT.next_expiration > item->timeout) {
            ACT.next_expiration = item->timeout;
        }
    }
}


int  osal_timer_start(osal_etimer_t* ctx, osal_timer_cb cb, void* args, uint32_t timeout)
{
    if(ctx == NULL) {
        return -1;
    }

    if(cb == NULL) {
        return -1;
    }

    ctx->cb      = cb;
    ctx->args    = args;
    ctx->timeout = ctx->period = timeout;

    OSAL_LIST_HEAD_APPEND(ACT.head, ctx);
    return 0;
}

int  osal_timer_stop(osal_etimer_t* ctx)
{
    osal_etimer_t* item, *temp1, *temp2;

    if(ctx == NULL) {
        return -1;
    }
    OSAL_LIST_HEAD_SAFE(ACT.head, item, temp1, temp2) {

        if(ctx == item) {
            OSAL_LIST_HEAD_DELETE(ACT.head, item);
            return 0;
        }
    }
    return -1;
}


uint32_t osal_timer_poll(uint32_t updatetime)
{
    osal_timer_update(updatetime);
    osal_timer_executecb();
    return ACT.next_expiration;
}

void osal_timer_relcease(void)
{
    osal_etimer_t* item, *temp1, *temp2;

    OSAL_LIST_HEAD_SAFE(ACT.head, item, temp1, temp2) {
        OSAL_LIST_HEAD_DELETE(ACT.head, item);
    }
}

void osal_timer_init(void)
{
    ACT.head            = NULL;
    ACT.next_expiration = 0;
}


