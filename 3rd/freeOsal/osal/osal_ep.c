

/*
// Copyright (c) 2019 Intel Corporation
//
//endpoint manager
// james
*/
#include "task_event.h"

#include <sys/time.h>
#include "osal_ep.h"

struct osal_ep_manage {
    fd_set rfds;
    fd_set sfds;
    struct osal_ep* fdlist;
};

static struct osal_ep_manage EP;

//--------------------------------------------------------------------------------------
static void _zero_fd_to_fds(fd_set* fds)
{
    FD_ZERO(fds);
}

static void _add_fd_to_fds(int sockfd, fd_set* fds)
{
    FD_SET(sockfd, fds);
}

static void _clr_fd_to_fds(int sockfd, fd_set* fds)
{
    FD_CLR(sockfd, fds);
}

static int _isset_fd_to_fds(int sockfd, fd_set* fds)
{
    return FD_ISSET(sockfd, fds);
}

static void osal_ep_tcp_add_select(int fd)
{
    _clr_fd_to_fds(fd, &EP.sfds);
    _add_fd_to_fds(fd, &EP.rfds);
}

static void osal_ep_recive_process(fd_set* fds)
{
    struct osal_ep* node, *tmp1, *tmp2;

    OSAL_LIST_HEAD_SAFE(EP.fdlist, node, tmp1, tmp2) {

        if(_isset_fd_to_fds(node->fd, fds)) {
            _clr_fd_to_fds(node->fd, fds);

            if(node->cb_recv) {
                node->cb_recv(node->args, node->fd);
            } else {
                _clr_fd_to_fds(node->fd, &EP.sfds);
            }
            return;
        }
    }
}

static void osal_ep_send_process(fd_set* fds)
{
    struct osal_ep* node, *tmp1, *tmp2;
    OSAL_LIST_HEAD_SAFE(EP.fdlist, node, tmp1, tmp2) {
        if(_isset_fd_to_fds(node->fd, fds)) {
            _clr_fd_to_fds(node->fd, fds);

            if(node->cb_send) {
                node->cb_send(node->args, node->fd);
            } else {
                _clr_fd_to_fds(node->fd, &EP.sfds);
            }
            return;
        }
    }
}
//--------------------------------------------------------------
/**
 * _select_fd_to_fds
 * select fd
*/
static void _select_fd_to_fds(int time)
{
    int i;
    fd_set setfds = EP.sfds;
    fd_set readfds = EP.rfds;
    struct timeval to = {0};
    struct timeval* sletime = NULL;

    sletime = &to;

    if(time > 10) {
        to.tv_sec = time / 1000;
        to.tv_usec = (time % 1000) * 1000;

    } else {
        to.tv_usec = 10 * 1000;//10ms
    }

    int n = select(FD_SETSIZE, &readfds, &setfds, NULL, sletime);
    if(n) {

        osal_ep_recive_process(&readfds);
        osal_ep_send_process(&setfds);
    }
}

void osal_ep_poll(int time)
{
    _select_fd_to_fds(time);
}


static struct osal_ep* _find_ep(int fd)
{
    struct osal_ep* node;

    OSAL_LIST_HEAD_FOREACH(EP.fdlist, node) {
        if(node->fd == fd) {
            return node;
        }
    }
    return NULL;
}

int osal_ep_bind(struct osal_ep* ep)
{
    if(ep == NULL) {
        OSAL_ERR("ep error");
        return -1;
    }
    if(ep->fd < 0) {
        OSAL_ERR("fd error");
        return -1;
    }

    if(_find_ep(ep->fd)) {
        OSAL_ERR("fd is set");
        return -1;
    }
    if(ep->cb_recv) {
        _add_fd_to_fds(ep->fd, &EP.rfds);
    }
    if(ep->cb_send) {
        _add_fd_to_fds(ep->fd, &EP.sfds);
    }

    OSAL_LIST_HEAD_APPEND(EP.fdlist, ep);
    return 0;
}

void osal_ep_unbind(struct osal_ep* ep)
{
    if(ep == NULL) {
        return ;
    }

    if(ep->fd < 0) {
        return;
    }

    OSAL_LIST_HEAD_DELETE(EP.fdlist, ep);

    if(ep->cb_recv) {
        _clr_fd_to_fds(ep->fd, &EP.rfds);
    }
    if(ep->cb_send) {
        _clr_fd_to_fds(ep->fd, &EP.sfds);
    }
}

void osal_ep_init(void)
{
    _zero_fd_to_fds(&EP.rfds);
    _zero_fd_to_fds(&EP.sfds);

    EP.fdlist = NULL;
}
