#include "task_event.h"
#include "unix_time.h"

struct osal_task {
    uint32_t prevSysTick;
};

struct osal_task Task;

static uint32_t osal_task_timer(void)
{
    uint32_t currSysTick = unix_get_tick_count();


    if(currSysTick != Task.prevSysTick) {
        uint32_t updatetime = (uint32_t)(currSysTick - Task.prevSysTick);

        Task.prevSysTick = currSysTick;

        return updatetime;
    }
    return 0;
}

void osal_task_poll(void)
{
    osal_timer_poll(osal_task_timer());
}


void osal_task_init(void)
{
    Task.prevSysTick = unix_get_tick_count();

    osal_log_start();
}
