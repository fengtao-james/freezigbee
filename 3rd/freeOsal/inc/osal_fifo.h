#ifndef FREEOSAL_FIFO_H
#define FREEOSAL_FIFO_H

typedef struct cycle_fifo {
    uint8_t buffer[KFIFO_MAX];/* the buffer holding the data*/
    uint32_t size;/* the size of the allocated buffer*/
    uint32_t in;/* data is added at offset (in % size)*/
    uint32_t out;/* data is extracted from off. (out % size)*/
} cycle_fifo_t;


static inline void cycle_fifo_init(cycle_fifo_t* fifo)
{

    memset(fifo, 0, sizeof(*fifo));

    fifo->size = KFIFO_MAX;
    fifo->in = fifo->out = 0;
}

static inline void cycle_fifo_reset(cycle_fifo_t* fifo)
{
    fifo->in = fifo->out = 0;
}

static inline uint32_t cycle_fifo_len(cycle_fifo_t* fifo)
{
    return fifo->in - fifo->out;
}

static inline  uint32_t cycle_fifo_put(cycle_fifo_t* fifo, uint8_t* buffer, uint32_t len)
{
    uint32_t l;

    len = osal_min(len, fifo->size - fifo->in + fifo->out);

    l = osal_min(len, fifo->size - (fifo->in & (fifo->size - 1)));
    memcpy(fifo->buffer + (fifo->in & (fifo->size - 1)), buffer, l);

    memcpy(fifo->buffer, buffer + l, len - l);

    fifo->in += len;
    return len;
}

static inline  uint32_t cycle_fifo_get(cycle_fifo_t* fifo, uint8_t* buffer, uint32_t len)
{
    uint32_t l;

    len = osal_min(len, fifo->in - fifo->out);

    /* first get the data from fifo->out until the end of the buffer */
    l = osal_min(len, fifo->size - (fifo->out & (fifo->size - 1)));
    memcpy(buffer, fifo->buffer + (fifo->out & (fifo->size - 1)), l);

    /* then get the rest (if any) from the beginning of the buffer */
    memcpy(buffer + l, fifo->buffer, len - l);
    fifo->out += len;

    return len;
}
#endif
