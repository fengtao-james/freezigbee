/**
 * james
 * osal_ep.h
 *
*/
#ifndef FREEOSAL_EPP_H
#define FREEOSAL_EPP_H

#ifdef __cplusplus
extern "C"
{
#endif

struct osal_ep {
    struct osal_ep* prev, * next;
    int fd;
    void (*cb_send)(void* args, int fd);
    void (*cb_recv)(void* args, int fd);
    void* args;
};

void osal_ep_init(void);
void osal_ep_poll(int time);
int osal_ep_bind(struct osal_ep* ep);
void osal_ep_unbind(struct osal_ep* ep);

#ifdef __cplusplus
}
#endif

#endif /* FO_ENDPOINT_H */