#ifndef OSAL_STRING_H
#define OSAL_STRING_H

#define osal_min(a,b) ((a) < (b) ? (a):(b))

void* osal_memdup(const void* src, unsigned int len);

void osal_strfree(string* pstr);
void osal_strclear(string* pstr);
void osal_strinit(string* pstr,  size_t sz);
int osal_strncpy(void* data, const string* pstr);
int osal_strset(string* pstr, const void* data, size_t sz);
string*  osal_strnew(const void* data, size_t sz);

int osal_stringtohex(void* out, const void* data, size_t sz);
int osal_hextostring(char* buffer, const void* data, size_t sz);
int osal_b64encode(char* buffer, const void* data, size_t sz);
int osal_b64decode(void* out, const void* data, size_t sz);
#endif
