#ifndef FREEOSAL_TYPE_H
#define FREEOSAL_TYPE_H

//芯片硬件字长
typedef unsigned int        halDataAlign_t;

// Unsigned numbers
typedef unsigned char       byte;

typedef struct {
    size_t len;
    uint8_t data[];
} string;


#ifndef ARRAY_NULL
#define ARRAY_NULL '\0'
#endif

#ifndef NULL
#define NULL       ((void*) 0 )
#endif


//#define CLI()         __set_PRIMASK(1)              // Disable Interrupts
//#define SEI()         __set_PRIMASK(0)              // Enable Interrupts
#define CLI()         ;              // Disable Interrupts
#define SEI()         ;              // Enable Interrupts

#define HAL_ENABLE_INTERRUPTS()         SEI()       // Enable Interrupts
#define HAL_DISABLE_INTERRUPTS()        CLI()       // Disable Interrupts
#define HAL_INTERRUPTS_ARE_ENABLED()    SEI()       // Enable Interrupts

#define HAL_ENTER_CRITICAL_SECTION()    CLI()
#define HAL_EXIT_CRITICAL_SECTION()     SEI()


#define HI_UINT32(a)  (((a) >> 24) & 0xFF)
#define HI_UINT24(a)  (((a) >> 16) & 0xFF)
#define HI_UINT16(a)  (((a) >> 8) & 0xFF)
#define LO_UINT16(a)  ((a) & 0xFF)

#define BUILD_UINT16(loByte, hiByte) \
          ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))

#define BUILD_UINT32(Byte0, Byte1, Byte2, Byte3) \
          ((uint32_t)((uint32_t)((Byte0) & 0x00FF) \
          + ((uint32_t)((Byte1) & 0x00FF) << 8) \
          + ((uint32_t)((Byte2) & 0x00FF) << 16) \
          + ((uint32_t)((Byte3) & 0x00FF) << 24)))

#define ZNC_RTN_U16( BUFFER, i ) \
        ( ( ( uint16_t ) (BUFFER)[ i ])\
        |( ( uint16_t ) (BUFFER)[ i+1 ] << 8))

#define ZNC_RTN_U24( BUFFER, i ) \
        ( ( ( uint32_t ) (BUFFER)[ i ])\
        |( ( uint32_t ) (BUFFER)[ i+1 ] << 8)\
        |( ( uint32_t ) (BUFFER)[ i+2 ] << 16))

#define ZNC_RTN_U32( BUFFER, i ) \
        ( ( ( uint32_t ) (BUFFER)[ i ])\
        |( ( uint32_t ) (BUFFER)[ i+1 ] << 8)\
        |( ( uint32_t ) (BUFFER)[ i+2 ] << 16)\
        |( ( uint32_t ) (BUFFER)[ i+3 ] << 24))

#undef offsetof
#define offsetof(type,member) ((size_t)&((type *)0)->member)

/**
 * ptr:point
 * type: struct
 * member:struct->member
*/

#ifndef container_of
#define container_of(ptr,type,member)({\
const typeof(((type *)0)->member)*__mptr=(ptr);\
(type*)((char*)__mptr - offsetof(type,member));})
#endif
#endif
