#ifndef OSAL_TIMER_H
#define OSAL_TIMER_H

typedef int (*osal_timer_cb)(void* args);


struct osal_etimer {
    struct osal_etimer* prev;
    struct osal_etimer* next;
    uint32_t period;
    uint32_t timeout;
    osal_timer_cb cb;
    void* args;
};

typedef struct osal_etimer  osal_etimer_t;


int  osal_timer_start(osal_etimer_t* ctx,osal_timer_cb cb,void *args, uint32_t timeout);
int  osal_timer_stop(osal_etimer_t* ctx);

uint32_t  osal_timer_poll(uint32_t updatetime);
void osal_timer_relcease(void);
void osal_timer_init(void);

//-------------------------------------------------------------------
#endif

