#ifndef FREEOSAL_CONFIG_H
#define FREEOSAL_CONFIG_H

#define KFIFO_MAX (1<<10)

#define HASH_MAX 1<<16

#define  MAX_LOG_BUF      1024//4096file
#define  OSAL_DEBUG

// #define OSAL_MEM_WATCH

#endif