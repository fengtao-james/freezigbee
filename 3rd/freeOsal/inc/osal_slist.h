/*
 *list
 *
 * This file is part of the freeOsal operating system.
 *
 * Author: james
 *
 */
#ifndef FREEOSAL_SLIST_H
#define FREEOSAL_SLIST_H

#define OSAL_SLIST_HEAD_FOREACH(head,el)                                                       \
    for ((el) = (head); el; (el) = (el)->next)

#define OSAL_SLIST_HEAD_FOREACH_SAFE(head,el,tmp)                                              \
  for ((el) = (head); (el) && ((tmp) = (el)->next, 1); (el) = (tmp))

#define OSAL_SLIST_HEAD_PREPEND(head,add)                                                      \
do {                                                                                           \
  (add)->next = (head);                                                                        \
  (head) = (add);                                                                              \
} while (0)


#define OSAL_SLIST_HEAD_APPEND(head,add,tmp)                                                   \
do {                                                                                           \
  (add)->next=NULL;                                                                            \
  if (head) {                                                                                  \
    tmp = (head);                                                                              \
    while(tmp->next) { tmp = tmp->next; }                                                      \
    tmp->next=(add);                                                                           \
  } else {                                                                                     \
    (head)=(add);                                                                              \
  }                                                                                            \
} while (0)


#define OSAL_SLIST_HEAD_DELETE(head,del,tmp)                                                   \
do {                                                                                           \
  if ((head) == (del)) {                                                                       \
    (head)=(head)->next;                                                                       \
  } else {                                                                                     \
    tmp = (head);                                                                              \
    while (tmp->next && (tmp->next != (del))) {                                                \
      tmp = tmp->next;                                                                         \
    }                                                                                          \
    if (tmp->next) {                                                                           \
      tmp->next = (del)->next;                                                                 \
    }                                                                                          \
  }                                                                                            \
} while (0)


#define OSAL_SLIST_HEAD_DHEAD(head,dhead)                                                      \
do {                                                                                           \
  if ((head) == (dhead)) {                                                                       \
    (head)=(head)->next;                                                                       \
  }                                                                                            \
} while (0)

#define OSAL_SLIST_HEAD_COUNT(head,el,counter)                                                 \
do {                                                                                           \
  (counter) = 0;                                                                               \
  OSAL_SLIST_HEAD_FOREACH(head,el) { ++(counter); }                                            \
} while (0)

//---------------------------------------------------------------------------------------------
/**
 * The linked list type.
 *
 */
// typedef void** osal_slist_t;

struct osal_slist {
    struct osal_slist* next;
};

typedef struct osal_slist  osal_slist_t;

#define OSAL_SLIST_FOR_EACH(item, list)   \
    for ((item) = (list);(item) != NULL; (item) = ((osal_slist_t*)(item))->next)

#define OSAL_SLIST_FOR_EACH_SAFE(item,next, list)   \
    for ((item) = (list);(item) && ((next)=((osal_slist_t*)(item))->next,1); (item) = (next))
/*---------------------------------------------------------------------------*/
/**
 * Initialize a list.
 *
 */
static inline void osal_slist_init(osal_slist_t* list)
{
    list->next = NULL;
}

/*---------------------------------------------------------------------------*/
/**
 * Get the tail of a list.
 *osal_slist_tail
 */
static inline void* osal_slist_tail(osal_slist_t* list)
{

    if(list == NULL) {
        return NULL;
    }
    osal_slist_t* l;

    for(l = list; l->next != NULL; l = l->next);

    return l;
}
/*---------------------------------------------------------------------------*/
/**
 * Add an item at the end of a list.
 *
 *osal_slist_add_tail
 */
static inline void osal_slist_add_tail(osal_slist_t** list, osal_slist_t* item)
{
    item->next = NULL;
    osal_slist_t* l = osal_slist_tail(*list);

    if(l == NULL) {
        *list = item;
    } else {
        l->next = item;
    }
}
/*---------------------------------------------------------------------------*/

/**
 * Add an item to the start of the list.
 */
static inline void osal_slist_add(osal_slist_t** list, osal_slist_t* item)
{
    item->next = *list;
    *list = item;
}

/*---------------------------------------------------------------------------*/
/**
 * Add an item to the start of the list.
 */
static inline void osal_slist_insert(osal_slist_t** head, osal_slist_t** tail, osal_slist_t* item)
{
    osal_slist_t* l = *tail;
    item->next = NULL;

    if(l && *head) {
        l->next = item;
        *tail = item;
    } else {
        *head = *tail = item;
    }
}

/*---------------------------------------------------------------------------*/
/**
 * Remove the first object on a list.
 *
 *
 *osal_slist_pop
 */
/*---------------------------------------------------------------------------*/
static inline void* osal_slist_pop(osal_slist_t** list)
{
    osal_slist_t* l;
    l = *list;
    if(*list != NULL) {
        *list = (*list)->next;
        l->next = NULL;
    }
    return l;
}

/*---------------------------------------------------------------------------*/
/**
 * Remove a specific element from a list.
 *
 *osal_slist_remove
 */
/*---------------------------------------------------------------------------*/
static inline void osal_slist_remove(osal_slist_t** list, void* item)
{
    osal_slist_t** l;

    for(l = list; *l != NULL; l = &(*l)->next) {
        if(*l == item) {
            *l = (*l)->next;
            return;
        }
    }
}
/*---------------------------------------------------------------------------*/
/**
 * Get the length of a list.
 *
 *osal_slist_length
 */
/*---------------------------------------------------------------------------*/
static inline int osal_slist_length(osal_slist_t* list)
{
    osal_slist_t* l;
    int n = 0;
    for(l = list; l != NULL; l = l->next) {
        ++n;
    }
    return n;
}

#endif /* osal_LIST_H */
