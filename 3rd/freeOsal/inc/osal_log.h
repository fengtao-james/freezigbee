/*
// Copyright (c) 2016 Intel Corporation
//LOG
*/
/**
  @file
*/
#ifndef OSAL_LOG_H
#define OSAL_LOG_H


#ifdef __cplusplus
extern "C"
{
#endif


#ifdef OSAL_DEBUG
#define PRINT(...) printf(__VA_ARGS__)

#define LOG_COLOR_BLACK   "30"
#define LOG_COLOR_RED     "31"
#define LOG_COLOR_GREEN   "32"
#define LOG_COLOR_BROWN   "33"
#define LOG_COLOR_BLUE    "34"
#define LOG_COLOR_PURPLE  "35"
#define LOG_COLOR_CYAN    "36"
#define LOG_COLOR_WRITE   "37"

#define LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
#define LOG_RESET_COLOR   "\033[0m"
#define LOG_COLOR_E       LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W       LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I       LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D       LOG_COLOR(LOG_COLOR_WRITE)
#define LOG_COLOR_V

void osal_log_debug(const char* level ,const char* file ,int line,const char* color, const char* msg, ...);
void osal_log_printf(const char* level, const char* data);
void osal_log_start(void);


#define OSAL_DBG(fmt, args...) { osal_log_debug("D",__FILE__, __LINE__,LOG_RESET_COLOR, fmt, ## args); }
#define OSAL_INFO(fmt, args...) { osal_log_debug("I",__FILE__, __LINE__,LOG_COLOR_I, fmt, ## args); }
#define OSAL_WRN(fmt, args...) { osal_log_debug("W",__FILE__, __LINE__,LOG_COLOR_W, fmt, ## args); }
#define OSAL_ERR(fmt, args...) { osal_log_debug("E",__FILE__, __LINE__,LOG_COLOR_E, fmt, ## args); }

#else
#define OSAL_LOG(...)
#define OSAL_INFO(...)
#define OSAL_DBG(...)
#define OSAL_WRN(...)
#define OSAL_ERR(...)

#define LOG_COLOR_E   " "
#define LOG_COLOR_W   " "
#define LOG_COLOR_I   " "
#define LOG_COLOR_D   " "
#define LOG_COLOR_V   " "
#define LOG_RESET_COLOR   " "
#endif

void osal_log(char* log, size_t len);
void osal_logstart(void (*func)(const char* log, size_t len));
void osal_logstop(void);
char* osal_log_hexstr(char* out, const uint8_t* in, size_t sz);
#ifdef __cplusplus
}
#endif

#endif /* OSAL_LOG_H */
