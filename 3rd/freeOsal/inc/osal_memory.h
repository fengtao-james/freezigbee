#ifndef OSALMEM_METRICS_H
#define OSALMEM_METRICS_H

//-------------------------------

#define MAXMEMHEAP  0

#define osal_mem_alloc(size) _osal_mem_alloc(__func__,size)
#define osal_mem_free(ptr) _osal_mem_free(__func__,ptr)

void osal_mem_init(void);
void osal_mem_kick(void);
void* _osal_mem_alloc(const char* func, halDataAlign_t size);
void _osal_mem_free(const char* func, void* ptr);

void* osal_mem_lalloc(void* ptr, halDataAlign_t nsize);
void* osal_mem_calloc(halDataAlign_t numElements, halDataAlign_t sizeOfElement);

#ifdef OSAL_MEM_WATCH
void osal_mem_watch_check(void);
#endif

#endif
