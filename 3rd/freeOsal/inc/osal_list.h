#ifndef FREEOSAL_LIST_H
#define FREEOSAL_LIST_H
#include "type.h"
/**
 * @brief osal_list
 */
#define OSAL_LIST_HEAD_DELETE(head,del)                                                             \
do {                                                                                           \
  if (((head)==(del)) && ((head)->next == (head))) {                                           \
      (head) = NULL;                                                                           \
  } else {                                                                                     \
     (del)->next->prev = (del)->prev;                                                          \
     (del)->prev->next = (del)->next;                                                          \
     if ((del) == (head)) (head)=(del)->next;                                                  \
  }                                                                                            \
} while (0)

/**
 * ADD end
*/
#define OSAL_LIST_HEAD_APPEND(head,add)                                                        \
do {                                                                                           \
 if (head) {                                                                                   \
   (add)->prev = (head)->prev;                                                                 \
   (add)->next = (head);                                                                       \
   (head)->prev = (add);                                                                       \
   (add)->prev->next = (add);                                                                  \
 } else {                                                                                      \
   (add)->prev = (add);                                                                        \
   (add)->next = (add);                                                                        \
   (head) = (add);                                                                             \
 }                                                                                             \
} while (0)

/**
 * ADD head
 * */
#define OSAL_LIST_HEAD_PREPEND(head,add)                                                       \
do {                                                                                           \
 if (head) {                                                                                   \
   (add)->prev = (head)->prev;                                                                 \
   (add)->next = (head);                                                                       \
   (head)->prev = (add);                                                                       \
   (add)->prev->next = (add);                                                                  \
 } else {                                                                                      \
   (add)->prev = (add);                                                                        \
   (add)->next = (add);                                                                        \
 }                                                                                             \
 (head) = (add);                                                                               \
} while (0)

#define OSAL_LIST_HEAD_FOREACH(head,el)                                                        \
   for ((el)=(head);el;(el)=(((el)->next==(head)) ? NULL : (el)->next))

#define OSAL_LIST_HEAD_SAFE(head,el,tmp1,tmp2)                                                 \
  for ((el) = (head), (tmp1) = (head) ? (head)->prev : NULL;                                   \
       (el) && ((tmp2) = (el)->next, 1);                                                       \
       (el) = ((el) == (tmp1) ? NULL : (tmp2)))

#define OSAL_LIST_HEAD_COUNT(head, el, counter)                                                \
do {                                                                                           \
  (counter) = 0;                                                                               \
  OSAL_LIST_HEAD_FOREACH(head,el) { ++(counter); }                                             \
} while (0)

#define OSAL_LIST_HEAD_ADD(el,prev,next)                                                      \
do {                                                                                          \
    (prev)->next = (el);                                                                      \
    (el)->prev = (prev);                                                                      \
    (el)->next = (next);                                                                      \
    (next)->prev = (el);                                                                      \
} while (0)


//----------------------------------------------------------------------------------------------
struct osal_list {
    struct osal_list* prev;
    struct osal_list* next;
};

typedef struct osal_list osal_list_t;

#define OSAL_LIST_FOR_EACH(item, list)   \
    for ((item) = (list)->next; \
        (item) != (list); \
        (item) = (item)->next)

#define OSAL_LIST_FOR_EACH_SAFE(item, next, list) \
    for (item = (list)->next, next = item->next; item != (list); \
        item = next, next = item->next)

#define OSAL_LIST_FIRST(list) ((list)->next)
#define OSAL_LIST_LAST(list) ((list)->prev)

#define OSAL_LIST_HEAD(list) \
            osal_list_t list = { &(list), &(list) }

/**
 * @brief osal_list_init
 */

static inline void osal_list_init(osal_list_t* head)
{
    head->prev = head;
    head->next = head;
}

/**
 * @brief osal_list_empty
 */

static inline int osal_list_empty(const osal_list_t* head)
{
    return head == head->next;
}

/**
 * @brief osal_list_add
 */

static inline osal_list_t* osal_list_add(osal_list_t* node, osal_list_t* prev, osal_list_t* next)
{
    if(!node) {
        return NULL;
    }

    prev->next = node;
    node->prev = prev;
    node->next = next;
    next->prev = node;

    return node;
}

/**
 * @brief osal_list_remove
 */

static inline osal_list_t* osal_list_remove(osal_list_t* node)
{
    osal_list_t* next = node->next;

    node->prev->next = node->next;
    node->next->prev = node->prev;

    node->prev = NULL;
    node->next = NULL;
    return next;
}

/**
 * @brief osal_list_pop_last
 */

static inline osal_list_t* osal_list_pop_last(osal_list_t* head)
{
    osal_list_t* node = head->prev;

    if(head != node) {
        osal_list_remove(node);
    }

    return node;
}

/**
 * @brief osal_list_pop_first
 */

static inline osal_list_t* osal_list_pop_first(osal_list_t* head)
{
    osal_list_t* node = head->next;

    if(head != node) {
        osal_list_remove(node);
    }
    return node;
}

/**
 * @brief osal_list_push_last
 */

static inline void* osal_list_push_last(osal_list_t* head, osal_list_t* node)
{
    return osal_list_add(node, head->prev, head);
}

/**
 * @brief osal_list_push_head
 */

static inline void* osal_list_push_first(osal_list_t* head, osal_list_t* node)
{
    return osal_list_add(node, head, head->next);
}


/**
 * @brief osal_list_count
 */

static inline int osal_list_count(osal_list_t* head)
{
    int count = 0;
    osal_list_t* node;

    OSAL_LIST_FOR_EACH(node, head) {
        ++count;
    }
    return count;
}

#endif	/* _MACRO_PRIVATE_LIBRARY_COMMON_C_LIST_H */