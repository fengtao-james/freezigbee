#ifndef TASK_EVENT_H
#define TASK_EVENT_H
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdarg.h>

#include "osal_config.h"
#include "type.h"
#include "osal_slist.h"
#include "osal_list.h"
#include "osal_memory.h"
#include "osal_string.h"
#include "osal_timer.h"
#include "osal_log.h"
#include "osal_fifo.h"
#include "osal_ep.h"

// #include "app_cfg.h"

void osal_os_init(void);

uint32_t osal_get_tick(void);
void osal_task_poll(void);
void osal_task_init(void);
#endif
