#LUA 源码阅读

##./freelua -I ./lua/app/lib test.lua

## 栈结构简介
+ Lua的栈结构主要由两部分组成：数据栈和调用栈
+ 数据栈主要由一个StkId结构的双向链表组成。
+ 调用栈主要由一个CallInfo的结构组成。

### 数据栈结构

#### Lua的栈结构由一个StkId结构存储.lua中的数据分为值类型和引用类型。引用类型需要引入GC机制管理。

##### typedef TValue *StkId;  /* index to stack elements*/

### 调用栈结构

#### Lua把调用栈和数据栈分离了，调用栈放在一个叫做 CallInfo 的结构中，以数组的形式储存在虚拟机对象里。其中CallInfo中的ci->func指向正在执行的函数在数据栈上的位置l->top

##### typedef struct CallInfo {}CallInfo


## 1. 虚拟机核心功能部分
## core
|文件|作用|
|--|--|
|lua.c|lua的可执行入口 main函数|
|lapi.c|C语言接口|
|ldebug.c|Debug 接口 |
|ldo.c|函数调用以及栈管理 |
|lfunc.c|函数原型及闭包管理 |
|lgc.c|垃圾回收机制|
|lmem.c|内存管理接口|
|lobject.c|对象操作函数|
|lopcodes.c|虚拟机字节码定义|
|lstate.c|全局状态机 管理全局信息|
|lstring.c|字符串池 |
|ltable.c|表类型的相关操作|
|ltm.c|元方法 |
|lvm.c|虚拟机 |
|lzio.c|输入流接口|

### lapi.c 

#### LUA 数据类型

+ #define LUA_TNIL		0
+ #define LUA_TBOOLEAN		1
+ #define LUA_TLIGHTUSERDATA	2
+ #define LUA_TNUMBER		3
+ #define LUA_TSTRING		4
+ #define LUA_TTABLE		5
+ #define LUA_TFUNCTION		6
+ #define LUA_TUSERDATA		7
+ #define LUA_TTHREAD		8
+ #define LUA_NUMTAGS		9

#### 数据插入：
##### 栈插入不消耗内存

+ lua_pushinteger() 栈插入
+ lua_pushstring() 需要申请内存
+ lua_pushfstring()把一个格式化过的字符串压内存栈。

+ lua_State：主线程栈结构
+ global_State：全局状态机，维护全局字符串表、内存管理函数、gc等信息
+ lua_State和global_State结构，通过LG的方式进行链接，将全局状态机global_State挂载到lua_State数据结构上
+ lua_State *L整个LG结构贯穿整个LUA线程解析的生命周期的始终。
+ LUA语言没有实现独立的线程，但是实现了协程序，关于协程后续会单独开一章节讲解。

## gcc
|文件|作用|
|--|--|
|lcode.c|代码生成器 |
|ldump.c|序列化预编译的Lua 字节码|
|llex.c|词法分析器  |
|lparser.c|解析器 |
|lundump.c|还原预编译的字节码 |

## lib
|文件|作用|
|--|--|
|lauxlib.c|库编写用到的辅助函数库|
|lbaselib.c|基础库|
|ldblib.c|Debug 库 |
|linit.c|内嵌库的初始化 |
|liolib.c|IO 库 |
|lmathlib.c|数学库 |
|loadlib.c|动态扩展库管理|
|loslib.c|OS 库|
|lstrlib.c|字符串库|
|ltablib.c|表处理库|


