/*File : lua_task.h
 *Auth : james
 *Date : 2019.8.22
 */
#ifndef _LUA_TASK_H
#define _LUA_TASK_H
#include "task_event.h"
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

void* lua_task_get_vm(void);
void lua_task_poll(void);
int  lua_task_event_init(int argc, char* argv[]);
void lua_task_setcb(lua_State* L, uint32_t session, uint32_t cmd);
#endif