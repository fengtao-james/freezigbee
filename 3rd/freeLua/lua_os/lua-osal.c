#define LUA_LIB

#include "task_event.h"
#include "lua_task.h"
#include "lua.h"
#include "lauxlib.h"
#include "unix_time.h"

//----------------------------------------------------------------------------
struct lua_osal_timer {
    osal_etimer_t t;
    uint32_t session;
};

static int
ltimeout_cb(void* args)
{
    struct lua_osal_timer* t = args;

    lua_State* L = lua_task_get_vm();
    lua_task_setcb(L, t->session, 0);

    // OSAL_DBG("time cb :%x",t->session);

    int r = lua_pcall(L, 2, 0, 0);
    if(r != LUA_OK) {
        OSAL_ERR("lua pcall error : %p::%s", t , lua_tostring(L, -1));
        lua_pop(L, 1);
    }
    osal_mem_free(t);
    return -1;
}

static int
ltimeout(lua_State* L)
{
    uint32_t session = luaL_checkinteger(L, 1);
    int timeout      = luaL_checkinteger(L, 2);

    struct lua_osal_timer* t = osal_mem_alloc(sizeof(*t));
    if(t) {
        t->session = session;
        osal_timer_start(&t->t, ltimeout_cb, t, timeout);
        lua_pushlightuserdata(L, t);
        return 1;
    }
    return 0;
}

static int
lexittime(lua_State* L)
{
    struct lua_osal_timer* t = lua_touserdata(L, 1);
    osal_timer_stop(&t->t);
    osal_mem_free(t);
    return 0;
}

static int
lbindcb(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TFUNCTION);
    lua_rawsetp(L, LUA_REGISTRYINDEX, L);
    return 0;
}

static int
llog(lua_State* L)
{
    size_t leavsz = 0;
    size_t datasz = 0;

    const char* leave = lua_tolstring(L, 1, &leavsz);
    const char* data  = lua_tolstring(L, 2, &datasz);
    osal_log_printf(leave, data);
    return 0;
}

static int ltick(lua_State* L)
{
    lua_pushnumber(L, unix_get_tick_count());
    return 1;
}

static int lb64_hex(lua_State* L)
{
    uint8_t hex[128] = {0};
    size_t sz = 0;

    const char* str = lua_tolstring(L, 1, &sz);
    lua_pushlstring(L, hex, osal_b64decode(hex, str, sz));
    return 1;
}

static int lb64_str(lua_State* L)
{
    uint8_t str[128] = {0};
    size_t sz = 0;

    const char* hex = lua_tolstring(L, 1, &sz);
    lua_pushlstring(L, str, osal_b64encode(str, hex, sz));
    return 1;
}


LUAMOD_API int luaopen_osal_core(lua_State* L)
{
    luaL_checkversion(L);

    luaL_Reg l[] = {
        { "timeout" , ltimeout },
        { "exittime", lexittime},
        { "bindcb"  , lbindcb},
        { "log"     , llog },
        { "tick"    , ltick},
        { "b64_hex",  lb64_hex},
        { "b64_str",  lb64_str},
        { NULL, NULL },
    };
    luaL_newlib(L, l); // when set func  need a table
    return 1;
}
