#include <string.h>
#include <stdlib.h>
#include "lua_task.h"

//8M

#define MEMORY_WARNING_REPORT (1024 * 1024*8)
#define MEMORY_LIMIT_SIZE     (1024 * 1024*8)

typedef struct lua_task_manage {
    uint32_t mem;
    uint32_t mem_report;
    uint32_t mem_limit;
    lua_State* L;
} lua_task_manage_t;


static lua_task_manage_t LUA;

static void* l_alloc(void* ud, void* ptr, size_t osize, size_t nsize)
{
    (void)ud;
    (void)osize;  /* not used */
    if(nsize == 0) {
        free(ptr);
        return NULL;
    } else {
        return realloc(ptr, nsize);
    }
}

static void* lalloc(void* ud, void* ptr, size_t osize, size_t nsize)
{
    lua_task_manage_t* l = &LUA;

    size_t mem = l->mem;
    l->mem += nsize;
    if(ptr) {
        l->mem -= osize;
    }
    if(l->mem_limit != 0 && l->mem > l->mem_limit) {
        OSAL_DBG("Memory LIMIT %u B", l->mem);
        if(ptr == NULL || nsize > osize) {
            l->mem = mem;
            return NULL;
        }
    }
    if(l->mem > l->mem_report) {
        lua_gc(LUA.L, LUA_GCCOLLECT, 0); // gc the collect
        OSAL_DBG("Memory warning  l->mem_report:%u::%u B", l->mem_report, l->mem);
    }
//	return osal_mem_lalloc(ptr,nsize);
    return l_alloc(ud, ptr, osize, nsize);
}

int lua_task_mem(void)
{
    return LUA.mem;
}

static int
traceback(lua_State* L)
{
    const char* msg = lua_tostring(L, 1);
    if(msg) {
        luaL_traceback(L, L, msg, 1);
    } else {
        lua_pushliteral(L, "(no error message)");
    }
    return 1;
}

/**
 * @brief LUA TASK INIT
 * @param
 * @param
 * @return
 */
static void lua_task_start(const char* file)
{
    lua_State* L = LUA.L;

    lua_pushcfunction(L, traceback);

    int r = luaL_loadfile(L, file);
    if(r != LUA_OK) {
        OSAL_ERR("Can't load %s : %s", file, lua_tostring(L, -1));
        return ;
    }
    r = lua_pcall(L, 0, 0, lua_gettop(L) - 1);

    if(r != LUA_OK) {
        OSAL_ERR("lua pcall error : %s::%s", file , lua_tostring(L, -1));
        return ;
    }
    lua_settop(L, 0);
    lua_gc(LUA.L, LUA_GCRESTART, 0);
}

static void createargtable(lua_State* L, char** argv, int argc)
{
    int i;
    lua_createtable(L, argc, 0);

    for(i = 0; i < argc; i++) {

        lua_pushstring(L, argv[i]);
        lua_rawseti(L, -2, i + 1);

    }
    lua_setglobal(L, "arg");
}
//-------------------------------------------------------------------------------------------------

void* lua_task_get_vm(void)
{
    return LUA.L;
}

//lua vm cb
void lua_task_setcb(lua_State* L, uint32_t session, uint32_t cmd)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, L);
    lua_pushinteger(L, session);
    lua_pushinteger(L, cmd);
}

#if 0
static void lua_task_addpath(char* name, char* value)
{
    lua_State* L = LUA.L;
    lua_getglobal(L, "package");
    lua_pushstring(L, value);
    lua_setfield(L, -2, name);
    lua_pop(L, 2);
}
#endif

// ./lua  -I path  test.lua
int lua_task_event_init(int argc, char* argv[])
{
    char* largv    = NULL;

    if(!strstr(argv[1], ".lua")) {
        OSAL_WRN("ERR ARG:%s", argv[3]);
        return -1;
    } else {
        largv = argv[1];
    }

    memset(&LUA, 0, sizeof(LUA));
    LUA.mem_report = MEMORY_WARNING_REPORT;
    LUA.mem_limit  = MEMORY_LIMIT_SIZE;
    LUA.L          = lua_newstate(lalloc, NULL);

    lua_gc(LUA.L, LUA_GCSTOP, 0);

    luaL_openlibs(LUA.L);

    if(argc > 2) {
        // createargtable(LUA.L, &argv[2], argc - 2);    // add args data
    }

    LUAMOD_API int luaopen_osal_profile(lua_State * L);
    luaL_requiref(LUA.L, "profile", luaopen_osal_profile, 1);

    LUAMOD_API int luaopen_osal_core(lua_State * L);
    luaL_requiref(LUA.L, "osal_core", luaopen_osal_core, 1);

    int luaopen_cjson(lua_State * l);
    luaL_requiref(LUA.L, "cjson", luaopen_cjson, 1);

    LUAMOD_API int luaopen_zigbee_obj(lua_State * L);
    luaL_requiref(LUA.L, "zbmgr", luaopen_zigbee_obj, 1);

    LUAMOD_API int luaopen_udp_obj(lua_State * L);
    luaL_requiref(LUA.L, "udp", luaopen_udp_obj, 1);

    LUAMOD_API int luaopen_tcp_obj(lua_State * L);
    luaL_requiref(LUA.L, "tcp", luaopen_tcp_obj, 1);

    LUAMOD_API int luaopen_http_obj(lua_State * L);
    luaL_requiref(LUA.L, "http", luaopen_http_obj, 1);

    LUAMOD_API int luaopen_modbus_obj(lua_State * L);
    luaL_requiref(LUA.L, "modbus", luaopen_modbus_obj, 1);

    lua_task_start(largv);
    return 0;
}