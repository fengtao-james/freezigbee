package.path ="./freeSproto/app/lib/?.lua;./freeSproto/app/microservice/include/?.lua"

if _VERSION ~= "Lua 5.3" then
	error "Use lua 5.3"
end

local osal=require "osal"
local sproto = require "lsproto"
local coap= require "coap_proto"
local proto = require "proto_test"

local host = sproto.new(proto.s2c):host "package"
local request = host:attach(sproto.new(proto.c2s))

local function print_request(name, args)
	print("REQUEST", name)
	if args then
		for k,v in pairs(args) do
			print(k,v)
		end
	end
end

local function print_response(session, args)
	print("RESPONSE", session)
	if args then
		for k,v in pairs(args) do
			print(k,v)
		end
	end
end

-- type,name ,args,response
local function print_package(t, ...)
	print(t)
	if t == "REQUEST" then
		print_request(...)
	else
		assert(t == "RESPONSE")
		print_response(...)
	end
end

local fd

local function sproto_msg(name,args,session)

	return {type=coap.CON,
			code=coap.GET,
			[coap.OP_URI_PATH]="sproto/resource",
			data=request(name,args,session)}
end

local function sproto_call(name,args)
	local session =osal.session()
	osal.unicast(fd,session,sproto_msg(name,args,session))

	local ack=osal.wait(session)

	if(ack.event==coap.EVENT_ACK)then
		return host:dispatch(ack.data)
	else
		error(ack.event)
	end
end



osal.start(function()
	local id =osal.open("udp://127.0.0.1:5683")
	local ack_msg=osal.call(id,
			{	
				type=coap.CON,
				code=coap.GET,
				[coap.OP_URI_PATH]="center/get",
				data=cjson.encode({server="test"})
			})
	
	print("server::",ack_msg.data)

	osal.close(id)

	if(ack_msg.code==coap.code_205 and ack_msg.data~=nil)then
		local info=cjson.decode(ack_msg.data)

		print("server::",info.url)
		fd=osal.open(info.url)
		sproto_call("set", { what = "hello", value = "world" })



	end
	error("server is ont online")
end)



