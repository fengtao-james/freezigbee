package.path ="./freeSproto/app/lib/?.lua"

local osal=require "osal"
local coap= require "coap_proto"
local cjson=cjson

if _VERSION ~= "Lua 5.3" then
	error "Use lua 5.3"
end

local db = {}
local observe_id

local function GET(key)
	return db[key]
end
--[[
    key:server
    url : udp://ip:port
    apilist:{"a_api","b_api","c_api"}
]]
local function SET(id,server,url,apilist)
	local ser= {}
	ser.url=url
	ser.apilist=apilist
	ser.id=id
	ser.server=server

	db[ser.server]=ser
	db[ser.id]=ser
end

local function CLEAR(server)
	local name=server.server
	local id=server.id

	db[name]=nil
	db[id]=nil

end
---------------------------------------------------------
--server  api  args
local function server_call(id,pdu)


end


-------------------------------------------------------
local function get_center(id,pdu)

	if(pdu.data==nil)then
		error("data is nil")
	end

	print(id,pdu[11],pdu.data,pdu.code,pdu.type)

	local args=cjson.decode(pdu.data)
	local ser=GET(args.server)
	if(ser)then
	
		return {code=coap.code_205,
				token=pdu.token,
				data=cjson.encode({url=ser.url,apilist=ser.apilist})}
	else
		return {code=coap.code_400,token=pdu.token}
	end

end

------------------------------------------------------
local function regist_center(id,pdu)
	local ip,port=osal.observeip(id)
	if(ip==nil)then
		print("--------------------------ip is nil")
		return {code=coap.code_400,token=pdu.token}
	end
	print("hello sproto",ip,port)


	if(pdu.data==nil)then
		error("data is nil")
	end

	print(id,pdu[11],pdu.data,pdu.code,pdu.type)

	local args=cjson.decode(pdu.data)
	print(args.server,args.port)

	local info
	if(GET(args.server)~=nil)then
		info={ result = "error" }
	else
		local url="udp://" .. ip .. ":" .. args.port
		print(url)
		
		SET(id,args.server,url,args.apilist)
		info= { result = "ok" }
	end

	return {code=coap.code_205,
			token=pdu.token,
			data=cjson.encode(info)}
end

-------------------------------------------------------
local function center_offlinepush(ser)

	osal.obpush(observe_id,{type=coap.NON,code=coap.code_205,data=cjson.encode({server=ser.server})})

end

local function center_offline(id)
	local ser=GET(id)
	if(ser)then
		print("server",ser.server,ser.id)
		center_offlinepush(ser)
		CLEAR(ser)
	end
end
-------------------------------------------------------
-------------------------------------------------------
local LUA_COAP_GET_EVENT=0x50
local LUA_COAP_REGIST_EVENT=0x51

osal.start(function()

	osal.openserve(5683,5684)
    observe_id = osal.observe("hello")

	osal.dispatch("center/regist",LUA_COAP_REGIST_EVENT,regist_center)
	osal.dispatch("center/get",LUA_COAP_GET_EVENT,get_center)

	osal.event(osal.LUA_HEAT_EVENT,center_offline)
end)