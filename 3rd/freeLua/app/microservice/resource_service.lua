package.path ="./freeSproto/app/lib/?.lua;./freeSproto/app/microservice/include/?.lua"

local osal=require "osal"
local luasproto = require "lsproto"
local coap= require "coap_proto"
local proto = require "proto_test"
local cjson=cjson

if _VERSION ~= "Lua 5.3" then
	error "Use lua 5.3"
end

local host = luasproto.new(proto.c2s):host "package"
local request = host:attach(luasproto.new(proto.s2c))
-----------------------------------------------------------------------
-----------------------------CALL--------------------------------------
-----------------------------------------------------------------------
local function sproto_msg(name,args,session)

	return {type=coap.CON,
			code=coap.GET,
			[coap.OP_URI_PATH]="sproto",
			data=request(name,args,session)}
end

local function sproto_call(name,args)
	local session =osal.session()
	osal.unicast(fd,session,sproto_msg(name,args,session))

	local ack=osal.wait(session)

	if(ack.event==coap.EVENT_ACK)then
		return host:dispatch(ack.data)
	else
		error(ack.event)
	end
end

-----------------------------------------------------------------------
-------------------------------PROCESS---------------------------------
-----------------------------------------------------------------------
local db = {}
local REQUEST = {}
local function GET(key)
	return db[key]
end

local function SET(key, value)
	local last = db[key]
	db[key] = value
	return last
end

--server  api  args
function REQUEST:call()
	
end

function REQUEST:get()
	print("get name", self.what)
	local r = GET(self.what)
	print("get data",r)
	return { result = r }
end

function REQUEST:set()
	print("set", self.what, self.value)
	local r = SET(self.what, self.value)
end

-- req api
local function process_request(name, args, response)
	local f = assert(REQUEST[name])
	local r = f(args)
	print "request!!!!"
	if response then
		print("response!!!")
		return response(r)
	end
end

-------------------------------------------------------
-- req api
local function process_package(t, ...)
	if t == "REQUEST" then
		local ok, result  = pcall(process_request, ...)
		if ok then
			if result then
				return result --string.pack(">s2", result)
			end
		end
	else
		assert(type == "RESPONSE")
		error "This example doesn't support request client"
	end
end

-----------------------------------------------------------------------
local function dispatch(id,pdu)

	if(pdu.data==nil)then
		error("data is nil")
	end

	print(id,pdu[11],pdu.data,pdu.code,pdu.type)

	local info=process_package(host:dispatch(pdu.data))

	return {code=coap.code_205,
			token=pdu.token,
			data=info}
end

-----------------------------------------------------------------------
-----------------------------OBSERVE-----------------------------------
-----------------------------------------------------------------------
local function center_observe(id,token,uri)

	osal.push(id,token,{type=coap.CON,
				code=coap.GET,
				[coap.OP_OBSERVE]=0,
				[coap.OP_URI_PATH]=uri}) --"heat/beat"
	
	return osal.wait(token)
end

local function regist_center(url)
	local fd =osal.open(url)

	local token=osal.token()
	local ack=center_observe(fd,token,"hello")
	print(ack.code)

	local pdu=osal.call(fd,
			{	
				type=coap.CON,
				code=coap.POST,
				[coap.OP_URI_PATH]="center/regist",
				data=cjson.encode({port="8888",server="test",apilist={"hello"}})
			})

    print(pdu.code,pdu.data)
	-- ping
	osal.fork(function()	
	
			print("sleep ok")
			osal.ping(fd)
			
			return 0
			end,5000)

	while true do
		local ack=osal.wait(token)
		print(ack.event,ack.code,ack.data)

		print(ack[coap.OP_URI_PATH])
	end
end

-----------------------------------------------------------------------
osal.start(function()
	
	osal.openserve(8888,8889)
	osal.dispatch("sproto/resource",osal.LUA_SPROTO_EVENT,dispatch)
	regist_center("udp://127.0.0.1:5683")

end)



