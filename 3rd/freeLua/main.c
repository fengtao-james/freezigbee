/**
 * @file main.c
 * @brief freeOsal main
 * @version 0.1
 * @date 2019-08-16
 * @author james
 */

#include "task_event.h"
#include "lua_task.h"
#include "unix_time.h"


int main(int argc, char* argv[])
{
    osal_os_init();
    osal_log_start();
    osal_timer_init();

    if(lua_task_event_init(argc, argv)) {
        return -1;
    }

    while(1) {
        usleep(1000 * 50);
        osal_task_poll();
    }

}



