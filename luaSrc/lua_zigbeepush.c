
#include "lua_task.h"
#include "lua_zigbeepush.h"

enum zcl_type {
    zcl_data_report = 1,
    zcl_dev_left,
    zcl_dev_ready,
    zcl_dev_online,
    zcl_dev_next_ep,
    zcl_dev_ota_end,
    zcl_dev_info,
    zcl_dev_ota_update,
    zcl_dev_rejoin,
    zcl_bind_info
};

static uint32_t zcl_session = 0xF6768941;


//--------------------------------------
//report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

void lua_zcl_report(const uint8_t* mac,
                    uint8_t ep,
                    uint16_t cluster,
                    uint8_t rssi,
                    uint8_t control,
                    uint16_t manucode,
                    uint8_t sql,
                    uint8_t cmd,
                    const uint8_t* data,
                    int len)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_data_report);

    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, ep);
    lua_pushinteger(L, cluster);
    lua_pushinteger(L, rssi);
    lua_pushinteger(L, control);
    lua_pushinteger(L, manucode);
    lua_pushinteger(L, sql);
    lua_pushinteger(L, cmd);
    lua_pushlstring(L, (char*)data, len);

    if(lua_pcall(L, 11, 0, 0)) {
        OSAL_ERR("lua_zcl_report error=%s\n", lua_tostring(L, -1));
    }
}

void lua_ota_end(const uint8_t* mac,
                 uint8_t ep, uint8_t status)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_ota_end);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, ep);
    lua_pushinteger(L, status);

    if(lua_pcall(L, 5, 0, 0)) {
        OSAL_ERR("lua_ota_end error=%s\n", lua_tostring(L, -1));
    }
}

void lua_ota_update(const uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_ota_update);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, ep);
    lua_pushinteger(L, max);
    lua_pushinteger(L, now);

    if(lua_pcall(L, 6, 0, 0)) {
        OSAL_ERR("lua_ota_update error=%s\n", lua_tostring(L, -1));
    }

    // OSAL_DBG(" update :max %d now %d",max,now);
}

void lua_dev_info(const uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_info);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushlstring(L, (char*)mfc, mlen);
    lua_pushlstring(L, (char*)mode, len);

    if(lua_pcall(L, 5, 0, 0)) {
        OSAL_ERR("lua_dev_info error=%s\n", lua_tostring(L, -1));
    }
}

void lua_dev_rejoin(const uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_rejoin);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushlstring(L, (char*)mfc, mlen);
    lua_pushlstring(L, (char*)mode, len);

    if(lua_pcall(L, 5, 0, 0)) {
        OSAL_ERR("lua_dev_info error=%s\n", lua_tostring(L, -1));
    }
}


void lua_bind_info(uint8_t index, const uint8_t* in, const uint16_t inlen, const uint8_t* out, const uint16_t outlen)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_bind_info);
    lua_pushinteger(L, index);
    lua_pushlstring(L, (char*)in, inlen);
    lua_pushlstring(L, (char*)out, outlen);

    if(lua_pcall(L, 5, 0, 0)) {
        OSAL_ERR("lua_dev_info error=%s\n", lua_tostring(L, -1));
    }
}

void lua_dev_left(const uint8_t* mac)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_left);
    lua_pushlstring(L, (char*)mac, 8);

    if(lua_pcall(L, 3, 0, 0)) {
        OSAL_ERR("lua_dev_left error=%s\n", lua_tostring(L, -1));
    }
}

void lua_dev_ready(const uint8_t* mac, uint8_t channel)
{
    lua_State* L = lua_task_get_vm();
    lua_task_setcb(L, zcl_session, zcl_dev_ready);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, channel);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("lua_dev_ready error=%s\n", lua_tostring(L, -1));
    }
}

void lua_dev_online(const uint8_t* mac, uint8_t ep)
{
    lua_State* L = lua_task_get_vm();
    lua_task_setcb(L, zcl_session, zcl_dev_online);
    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, ep);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("lua_dev_online error=%s\n", lua_tostring(L, -1));
    }
}

void lua_dev_next_ep(const uint8_t* mac, uint8_t ep)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, zcl_session, zcl_dev_next_ep);

    lua_pushlstring(L, (char*)mac, 8);
    lua_pushinteger(L, ep);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("lua_dev_online error=%s\n", lua_tostring(L, -1));
    }
}



