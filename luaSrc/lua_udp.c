#include "lua_task.h"
#include "net_config.h"
#include "net_udp.h"
#include "net_endpoint.h"

uint32_t udp_sessin_id  = 0xF6768942;


static void __udp_recv(net_udp_t* ctx, union sockaddr_all* addr, uint8_t* data, int len)
{
    char ip[22] = {0};
    char port[8] = {0};

    char uri[32] = {0};

    lua_State* L = lua_task_get_vm();
    lua_task_setcb(L, udp_sessin_id, 0);

    net_endpoint_to_string(addr, ip, port);
    sprintf(uri, "udp://%s:%s", ip, port);

    // OSAL_DBG("udp recv :%s", uri);

    lua_pushlstring(L, (char*)uri, strlen(uri));
    lua_pushlstring(L, (char*)data, len);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("lua_dev_online error=%s\n", lua_tostring(L, -1));
    }
}

static int c_lua_udp_client_open(lua_State* L)
{
    size_t len          = 0;
    const char* ip      = lua_tolstring(L, 1, &len);
    net_udp_t* cli      = net_udp_cli_open(ip, __udp_recv, NULL);

    if(cli == NULL) {
        lua_pushnil(L);
        return 1;
    }


    lua_pushlightuserdata(L, cli);
    return 1;
}

static int c_lua_udp_close(lua_State* L)
{
    net_udp_t* ctx      = lua_touserdata(L, 1);

    net_udp_close(ctx);
    return 0;
}

static int c_lua_udp_client_send(lua_State* L)
{
    net_udp_t* cli      = lua_touserdata(L, 1);
    size_t datalen      = 0;
    uint8_t* data       = (uint8_t*)lua_tolstring(L, 2, &datalen);

    lua_pushinteger(L, net_udp_cli_send(cli, data, datalen));

    return 1;
}

static int c_lua_udp_srv_open(lua_State* L)
{
    int port            = luaL_checkinteger(L, 1);
    net_udp_t* srv      = net_udp_srv_open(port, __udp_recv, NULL);
    if(srv == NULL) {
        lua_pushnil(L);
        return 1;
    }
    lua_pushlightuserdata(L, srv);
    return 1;
}


static int c_lua_udp_srv_send(lua_State* L)
{
    net_udp_t* srv         = lua_touserdata(L, 1);
    size_t len             = 0;
    const char* addr       = lua_tolstring(L, 2, &len);
    size_t datalen         = 0;
    const uint8_t* data    = (const uint8_t*)lua_tolstring(L, 3, &datalen);

    lua_pushinteger(L, net_udp_srv_straddr_send(srv, addr, data, datalen));

    return 1;
}

static const luaL_Reg call[] = {

    {"ucliopen",         c_lua_udp_client_open},
    {"uclisend",         c_lua_udp_client_send},
    {"uclose",	 	     c_lua_udp_close},
    {"usrvopen",         c_lua_udp_srv_open},
    {"usrvsend",         c_lua_udp_srv_send},
    {NULL, NULL}
};

LUAMOD_API int luaopen_udp_obj(lua_State* L)
{
    luaL_newlib(L, call);
    return 1;
}