#include "lua_task.h"
#include "http_parser.h"

#include "mbedtls/platform_util.h" // for mbedtls_platform_zeroize
#include "base64.h"
#include "sha1.h"
#include "lua_http.h"

typedef struct lua_http_handle {
    const char* key;
    size_t klen;
} lua_http_handle_t;

int on_message_begin(http_parser* parser)
{

    OSAL_DBG("***MESSAGE BEGIN******");
    return 0;
}

int on_headers_complete(http_parser* parser)
{
    OSAL_DBG("***HEADERS COMPLETE***");
    return 0;
}

int on_message_complete(http_parser* parser)
{
    OSAL_DBG("***MESSAGE COMPLETE***");
    return 0;
}

int on_url(http_parser* parser, const char* at, size_t length)
{
    OSAL_DBG("Url: %.*s", (int)length, at);
    return 0;
}


int on_status(http_parser* parser, const char* at, size_t length)
{
    OSAL_DBG("status: %.*s", (int)length, at);
    return 0;
}

int on_header_field(http_parser* parser, const char* at, size_t length)
{
    lua_http_handle_t* kv = parser->data;

    // OSAL_DBG("key: %.*s", (int)length, at);
    kv->key  = at;
    kv->klen = length;

    return 0;
}

int on_header_value(http_parser* parser, const char* at, size_t length)
{
    lua_State* L = lua_task_get_vm();
    lua_http_handle_t* kv = parser->data;

    lua_rawgetp(L, LUA_REGISTRYINDEX, &kv->klen);

    lua_pushlstring(L, kv->key, kv->klen);
    lua_pushlstring(L, at, length);

    if(lua_pcall(L, 2, 0, 0)) {
        OSAL_ERR("on_header_value error=%s\n", lua_tostring(L, -1));
    }
    //OSAL_DBG("key :%.*s ::value: %.*s", (int)kv->klen, kv->key, (int)length, at);
    return 0;
}

int on_body(http_parser* parser, const char* at, size_t length)
{
    OSAL_DBG("Body: %.*s", (int)length, at);
    lua_State* L = lua_task_get_vm();
    lua_rawgetp(L, LUA_REGISTRYINDEX, parser->data);
    lua_pushlstring(L, at, length);

    if(lua_pcall(L, 1, 0, 0)) {
        OSAL_ERR("on_body error=%s\n", lua_tostring(L, -1));
    }
    return 0;
}

static http_parser_settings settings_cb = { // http_parser cb
    .on_message_begin  = on_message_begin
    , .on_header_field = on_header_field
    , .on_header_value = on_header_value
    , .on_url          = on_url
    , .on_status       = on_status
    , .on_body         = on_body
    , .on_headers_complete = on_headers_complete
    , .on_message_complete = on_message_complete
};

int c_lua_http_open(lua_State* L)
{
    http_parser* parser = osal_mem_alloc(sizeof(*parser));

    if(parser == NULL) {
        lua_pushnil(L);
        return 1;
    }
    parser->data = osal_mem_alloc(sizeof(lua_http_handle_t));
    if(parser->data == NULL) {
        lua_pushnil(L);
        osal_mem_free(parser);
        return 1;
    }
    http_parser_init(parser, HTTP_BOTH);//  init parser=Response type
    lua_pushlightuserdata(L, parser);
    return 1;
}

int c_lua_http_close(lua_State* L)
{
    http_parser* parser      = lua_touserdata(L, 1);
    osal_mem_free(parser->data);
    osal_mem_free(parser);
    return 0;
}

int c_lua_http_decode(lua_State* L)
{
    http_parser* parser      = lua_touserdata(L, 1);

    size_t len               = 0;
    const char* data         = lua_tolstring(L, 2, &len);

    int rc = http_parser_execute(parser, &settings_cb, data, len);

    OSAL_INFO("http decode code:%d nread:%d code:%s", rc,
              parser->nread,
              http_errno_name(parser->http_errno));

    return 1;
}

int c_lua_http_bind(lua_State* L)
{
    http_parser* parser      = lua_touserdata(L, 1);

    lua_http_handle_t* kv = parser->data;
    //key val
    luaL_checktype(L, 2, LUA_TFUNCTION);
    lua_pushvalue(L, 2);
    lua_rawsetp(L, LUA_REGISTRYINDEX, &kv->klen);

    luaL_checktype(L, 3, LUA_TFUNCTION);
    lua_pushvalue(L, 3);
    lua_rawsetp(L, LUA_REGISTRYINDEX, parser->data);

    return 1;
}



int c_lua_base64(lua_State* L)
{
    size_t len               = 0;
    const uint8_t* data      = (const uint8_t*)lua_tolstring(L, 1, &len);
    size_t  olen             = 0;

    mbedtls_base64_encode(NULL, 0, &olen, data, len);

    unsigned char* dst       = osal_mem_alloc(olen * 1);

    // OSAL_DBG("encode_sz:%d::len %d", olen, len);

    if(dst == NULL) {
        lua_pushnil(L);
        return 1;
    }
    mbedtls_base64_encode(dst, olen, &olen, data, len);
    lua_pushlstring(L, (char*)dst, olen);

    osal_mem_free(dst);
    return 1;
}

int c_lua_sha1(lua_State* L)
{
    uint8_t res[20]          = {0};
    size_t len               = 0;
    const char* data         = lua_tolstring(L, 1, &len);

    mbedtls_sha1(data, len, res);

    lua_pushlstring(L, (char*)res, 20);
    return 1;
}

static const luaL_Reg call[] = {
    {"open",               c_lua_http_open},
    {"close",              c_lua_http_close},
    {"decode",             c_lua_http_decode},
    {"bind",               c_lua_http_bind},
    {"base64",             c_lua_base64},
    {"sha1",               c_lua_sha1},
    {NULL, NULL}
};

LUAMOD_API int luaopen_http_obj(lua_State* L)
{
    luaL_newlib(L, call);
    return 1;
}

