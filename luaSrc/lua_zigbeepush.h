#ifndef _LUA_ZIGBEE_PUSH_H
#define _LUA_ZIGBEE_PUSH_H
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

void lua_dev_ready(const uint8_t* mac, uint8_t channel);

void lua_dev_online(const uint8_t* mac, uint8_t ep);
void lua_dev_left(const uint8_t* mac);

void lua_zcl_report(const uint8_t* mac,
                    uint8_t ep,
                    uint16_t cluster,
                    uint8_t rssi,
                    uint8_t control,
                    uint16_t manucode,
                    uint8_t sql,
                    uint8_t cmd,
                    const uint8_t* data,
                    int len);

void lua_dev_next_ep(const uint8_t* mac, uint8_t ep);

void lua_ota_end(const uint8_t* mac,
                 uint8_t ep, uint8_t status);

void lua_dev_info(const uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len);
void lua_dev_rejoin(const uint8_t* mac, const uint8_t* mfc, uint8_t mlen, const uint8_t* mode, uint8_t len);
void lua_ota_update(const uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now);
void lua_bind_info(uint8_t index, const uint8_t* in, const uint16_t inlen, const uint8_t* out, const uint16_t outlen);
#endif
