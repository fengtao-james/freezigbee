#include "lua_task.h"
#include "net_config.h"
#include "net_tcp.h"
#include "net_endpoint.h"
#include "lua_tcp.h"

uint32_t tcp_sessin_id      = 0xF6768943;
uint32_t tcp_srv_sessin_id  = 0xF6768944;

static void __tcp_cli_recv(net_tcp_t* tcp,  uint8_t* data, int len)
{
    lua_State* L = lua_task_get_vm();

    lua_task_setcb(L, tcp_sessin_id, 0);

    lua_pushinteger(L, tcp->ep.fd);
    lua_pushlstring(L, (char*)data, len);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("__tcp_recv error=%s\n", lua_tostring(L, -1));
    }
}

static void __tcp_cli_report(net_tcp_t* tcp, enum net_type type)
{
    lua_State* L = lua_task_get_vm();
    lua_task_setcb(L, tcp_sessin_id, 1);

    lua_pushinteger(L, tcp->ep.fd);
    lua_pushinteger(L, (int)type);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("__tcp_cli_report error=%s\n", lua_tostring(L, -1));
    }
}
//----------------------------------------------------------------------------
static void __tcp_srv_report(net_tcp_t* tcp, int fd, union sockaddr_all* addr)
{
    lua_State* L = lua_task_get_vm();
    char ip[22]  = {0};
    char port[8] = {0};
    char uri[32] = {0};

    net_endpoint_to_string(addr, ip, port);
    sprintf(uri, "tcp://%s:%s", ip, port);

    lua_task_setcb(L, tcp_srv_sessin_id, 1);

    lua_pushlstring(L, (char*)uri, strlen(uri));
    lua_pushinteger(L, (int)fd);

    if(lua_pcall(L, 4, 0, 0)) {
        OSAL_ERR("__tcp_srv_report error=%s\n", lua_tostring(L, -1));
    }
}

static int c_lua_tcp_client_open(lua_State* L)
{
    size_t len          = 0;
    const char* ip      = lua_tolstring(L, 1, &len);
    net_tcp_t* cli      = net_tcp_client_open(ip, __tcp_cli_recv, __tcp_cli_report, NULL);

    if(cli == NULL) {
        lua_pushnil(L);
        return 1;
    }

    lua_pushlightuserdata(L, cli);
    return 1;
}


static int c_lua_tcp_client_close(lua_State* L)
{
    net_tcp_t* ctx      = lua_touserdata(L, 1);

    if(ctx == NULL) {
        OSAL_ERR("ctx child error");
        return 0;
    }
    net_tcp_close(ctx);
    return 0;
}

static int c_lua_tcp_srv_open(lua_State* L)
{
    int port            = luaL_checkinteger(L, 1);
    net_tcp_t* srv      = net_tcp_srv_open(port, __tcp_srv_report, NULL);
    if(srv == NULL) {
        lua_pushnil(L);
        return 1;
    }
    lua_pushlightuserdata(L, srv);
    return 1;
}


static int c_lua_tcp_child_open(lua_State* L)
{
    int fd            = luaL_checkinteger(L, 1);
    net_tcp_t* cli    = net_tcp_child_open(fd, __tcp_cli_recv, __tcp_cli_report, NULL);

    if(cli == NULL) {
        OSAL_ERR("child error");
        lua_pushnil(L);
        return 1;
    }

    lua_pushlightuserdata(L, cli);
    return 1;
}

static int c_lua_tcp_client_send(lua_State* L)
{
    net_tcp_t* cli      = lua_touserdata(L, 1);
    size_t datalen      = 0;
    uint8_t* data       = (uint8_t*)lua_tolstring(L, 2, &datalen);

    lua_pushinteger(L, net_tcp_send(cli, data, datalen));

    return 1;
}

static int c_lua_tcp_client_reset(lua_State* L)
{
    net_tcp_t* cli      = lua_touserdata(L, 1);
    lua_pushinteger(L, net_tcp_client_reset(cli));

    return 1;
}

int c_lua_ppmq_send(lua_State* L);
int c_lua_ppmq_decode(lua_State* L);

static const luaL_Reg call[] = {
    {"tcliopen",             c_lua_tcp_client_open},
    {"tclose",               c_lua_tcp_client_close},
    {"tsrvopen",             c_lua_tcp_srv_open},
    {"tsrvaccept",           c_lua_tcp_child_open},
    {"tsend",                c_lua_tcp_client_send},
    {"tclireset",            c_lua_tcp_client_reset},
    // {"ppmqsend",              c_lua_ppmq_send},
    // {"ppmqdecode",            c_lua_ppmq_decode},
    {NULL, NULL}
};

LUAMOD_API int luaopen_tcp_obj(lua_State* L)
{
    luaL_newlib(L, call);
    return 1;
}