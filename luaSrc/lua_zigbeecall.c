#include "lua_task.h"
#include "ncp_network.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "discover.h"
#include "device-table.h"
#include "lua_zigbeepush.h"

static uint8_t       sg_data_sql_num           =  1;

static uint8_t __get_sql(void)
{
    return sg_data_sql_num++;
}


static int c_lua_dev_ep_list(lua_State* L)
{
    uint8_t eplist[8] = {0};
    size_t len = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    int n = zcl_get_dev_ep(eplist, 8, mac);

    if(n > 0) {
        lua_pushlstring(L, (char*)eplist, n);
    }

    return 1;
}


static int c_lua_zcl_send(lua_State* L)
{
    size_t len = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    int ep       = luaL_checkinteger(L, 2);
    int cid      = luaL_checkinteger(L, 3);
    int control  = luaL_checkinteger(L, 4);
    int manucode = luaL_checkinteger(L, 5);
    int cmd      = luaL_checkinteger(L, 6);

    unsigned char* data = (unsigned char*)lua_tolstring(L, 7, &len);

    uint8_t sql = __get_sql();

    int rc = zcl_send(mac, ep, cid, control, manucode, sql, cmd, data, len);
    if(rc) {
        OSAL_ERR("zcl send error");
        lua_pushinteger(L, -1);

    } else {
        lua_pushinteger(L, sql);
    }

    return 1;
}

static int c_lua_ota_start(lua_State* L)
{
    size_t len = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    int ep       = luaL_checkinteger(L, 2);

    int rc = ota_start(mac, ep);
    if(rc) {
        OSAL_ERR("ota  error");

    }
    lua_pushinteger(L, rc);
    return 1;
}

static int c_lua_ota_code(lua_State* L)
{
    size_t len = 0;
    const char* path = lua_tolstring(L, 1, &len);

    int rc = zcl_ota_info(path);
    if(rc) {
        OSAL_ERR("ota  file error");

    }
    lua_pushinteger(L, rc);
    return 1;
}

static int c_lua_zcl_broadcast(lua_State* L)
{
    size_t len = 0;

    int ep       = luaL_checkinteger(L, 1);
    int cid      = luaL_checkinteger(L, 2);
    int control  = luaL_checkinteger(L, 3);
    int manucode = luaL_checkinteger(L, 4);
    int cmd      = luaL_checkinteger(L, 5);

    unsigned char* data = (unsigned char*)lua_tolstring(L, 6, &len);

    uint8_t sql = __get_sql();

    int rc =  zcl_broadcast(ep, cid, control, manucode, sql, cmd, data, len);
    if(rc) {
        OSAL_ERR("zcl send error");
        lua_pushinteger(L, -1);

    } else {
        lua_pushinteger(L, sql);
    }
    return 1;
}

static int c_lua_zcl_groupsend(lua_State* L)
{
    size_t len = 0;
    int gid      = luaL_checkinteger(L, 1);
    int ep       = luaL_checkinteger(L, 2);
    int cid      = luaL_checkinteger(L, 3);
    int control  = luaL_checkinteger(L, 4);
    int manucode = luaL_checkinteger(L, 5);
    int cmd      = luaL_checkinteger(L, 6);

    unsigned char* data = (unsigned char*)lua_tolstring(L, 7, &len);

    OSAL_DBG("group len:%d", len);
    uint8_t sql = __get_sql();

    int rc =  zcl_groupsend(gid, ep, cid, control, manucode, sql, cmd, data, len);
    if(rc) {
        OSAL_ERR("zcl send error");
        lua_pushinteger(L, -1);

    } else {
        lua_pushinteger(L, sql);
    }
    return 1;
}

static int c_lua_zcl_is_global(lua_State* L)
{
    int control  = luaL_checkinteger(L, 1);

    lua_pushboolean(L, zcl_is_global(control));
    return 1;
}

static int c_lua_zcl_is_specfic(lua_State* L)
{
    int control  = luaL_checkinteger(L, 1);

    lua_pushboolean(L, zcl_is_specfic(control));
    return 1;
}


static int c_lua_network_del(lua_State* L)
{
    size_t len = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    if(len == 8) {
        zcl_deldev(mac);
    }

    return 0;
}

static int c_lua_network_info(lua_State* L)
{
    ncp_info_t info = {0};
    ncp_get_netinfo(&info);

    lua_pushlstring(L, (char*)info.mac, 8);
    lua_pushinteger(L, info.Channel);
    return 2;
}


static int c_lua_network_leave(lua_State* L)
{
    ncp_NetworkLeave();
    return 0;
}

static int c_lua_network_close(lua_State* L)
{
    ncp_CloseNetwork();
    return 0;
}


static int c_lua_network_open(lua_State* L)
{
    ncp_OpenNetwork();
    return 0;
}

//--------------------------------------------
//dev info
static int c_lua_dev_ep_info(lua_State* L)
{
    ep_info_t info ;
    size_t len         = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    int ep             = luaL_checkinteger(L, 2);

    int rc = zcl_get_ep_dev_info(&info, mac, ep);
    if(rc < 0) {
        OSAL_ERR("ep list is error");
        lua_pushnil(L);
        return 1;
    }
    lua_pushlstring(L, (char*)info.mac, 8);
    lua_pushinteger(L, info.addr);
    lua_pushinteger(L, info.ep);
    lua_pushinteger(L, info.devid);
    lua_pushlstring(L, (char*)info.inclust, info.inclust_len * 2);
    lua_pushlstring(L, (char*)info.outclust, info.outclust_len * 2);
    return 6;
}

static int c_lua_dev_info(lua_State* L)
{
    dev_info_t info ;
    size_t len         = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);

    int rc = zcl_get_dev_info(&info, mac);
    if(rc < 0) {
        OSAL_ERR("ep list is error");
        lua_pushnil(L);
        return 1;
    }

    lua_pushlstring(L, (char*)&info.mfd[1], info.mfd[0]);
    lua_pushlstring(L, (char*)&info.mid[1], info.mid[0]);
    return 2;
}

static int c_lua_dev_all(lua_State* L)
{
    freezgb_mac list[128] = {0};

    int n = ncp_get_all_devmac(list, 128);

    for(int i = 0; i < n; i++) {
        lua_pushlstring(L, (char*)list[i], 8);
    }
    return n;
}

static int c_lua_dev_cnt(lua_State* L)
{
    lua_pushinteger(L,  ncp_get_devnum());
    return 1;
}

static int c_lua_dev_mac(lua_State* L)
{
    freezgb_mac mac = {0};
    int index       = luaL_checkinteger(L, 1);

    int r = ncp_get_devmac(mac, index);
    if(r < 0) {
        OSAL_ERR("index is error");
        lua_pushnil(L);
        return 1;
    }

    lua_pushlstring(L, (char*)mac, 8);
    return 1;
}
//----------------------------------------------------------
static int c_lua_sys_reset(lua_State* L)
{
    ncp_netreset();
    return 0;
}

static int c_lua_set_panid(lua_State* L)
{
    OSAL_ERR("set panid");
    int panid = luaL_checkinteger(L, 1);
    ncp_setpanid(panid);
    return 0;
}

static int c_lua_set_channel(lua_State* L)
{
    OSAL_ERR("set channel");
    int channel = luaL_checkinteger(L, 1);
    ncp_setchannel(channel);
    return 0;
}

static int c_lua_sys_start(lua_State* L)
{
    size_t len = 0;
    size_t psz = 0;

    const char* dev  = lua_tolstring(L, 1, &len);
    const char* path = lua_tolstring(L, 2, &len);
    int bps          = luaL_checkinteger(L, 3);

    lua_pushinteger(L, ncp_uart_start(dev, path, bps));

    return 1;
}

static int c_lua_dev_online(lua_State* L)
{
    ncp_all_online();
    return 0;
}

static int c_lua_cid_bind(lua_State* L)
{
    size_t len         = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    int ep             = luaL_checkinteger(L, 2);
    int cid            = luaL_checkinteger(L, 3);

    zcl_zdo_bind(mac, ep, cid);
    return 0;
}

static int c_lua_dev_index(lua_State* L)
{
    size_t len         = 0;
    const char* dev    = lua_tolstring(L, 1, &len);
    lua_pushinteger(L, zcl_get_dev_index(dev));
    return 1;
}

static int c_lua_net_repair(lua_State* L)
{
    size_t len         = 0;
    unsigned char* mac = (unsigned char*)lua_tolstring(L, 1, &len);
    ncp_net_nodeidreq(mac);
    return 0;
}

static int c_lua_bind_info(lua_State* L)
{
    size_t inlen = 0;
    size_t outlen = 0;
    int index       = luaL_checkinteger(L, 1);
    unsigned char* input  = (unsigned char*)lua_tolstring(L, 2, &inlen);
    unsigned char* output = (unsigned char*)lua_tolstring(L, 3, &outlen);

    bind_info_set(index, input, inlen, output, outlen);
}

static const luaL_Reg call[] = {
    {"sys_reset",         c_lua_sys_reset},
    {"set_channel",       c_lua_set_channel},
    {"set_panid",         c_lua_set_panid},
    {"network_open",      c_lua_network_open},
    {"network_close",     c_lua_network_close},
    {"network_leave",     c_lua_network_leave},
    {"network_del",	 	  c_lua_network_del},
    {"network_info",	  c_lua_network_info},
    {"zcl_send",          c_lua_zcl_send},
    {"ota_start",         c_lua_ota_start},
    {"zcl_broadcast",     c_lua_zcl_broadcast},
    {"zcl_group",         c_lua_zcl_groupsend},
    {"zcl_isglobal",      c_lua_zcl_is_global},
    {"zcl_isspecfic",     c_lua_zcl_is_specfic},
    {"dev_eplist",        c_lua_dev_ep_list},
    {"dev_ep_info",       c_lua_dev_ep_info},
    {"dev_info",       c_lua_dev_info},
    {"dev_all",        c_lua_dev_all},
    {"dev_online",     c_lua_dev_online},
    {"dev_cnt",        c_lua_dev_cnt},
    {"dev_mac",        c_lua_dev_mac},
    {"dev_index",      c_lua_dev_index},
    {"ota_code",       c_lua_ota_code},
    {"sys_start",      c_lua_sys_start},
    {"cid_bind",       c_lua_cid_bind},
    {"net_repair",     c_lua_net_repair},
    {"bind_table",     c_lua_bind_info},
    {NULL, NULL}
};

LUAMOD_API int luaopen_zigbee_obj(lua_State* L)
{
    luaL_newlib(L, call);
    return 1;
}


