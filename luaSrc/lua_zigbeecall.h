#ifndef _LUA_ZIGBEE_CALL_H
#define _LUA_ZIGBEE_CALL_H
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

LUAMOD_API int luaopen_zigbee_obj(lua_State* L) ;

#endif
