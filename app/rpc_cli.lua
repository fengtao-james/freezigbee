local rpc      =  require("osal_rpc") 
local osal     = require "osal"
local lua_udp  = require("osal_udp") 

local function  hello_cb(data)
    print("-----------",data)
end

osal.start(function()

    local cli = lua_udp.cli_new("udp://127.0.0.1:12345")

    if(cli~=nil)then
        
        cli:bind(function(ip,data)
            print("cli recv :",ip,data)

            rpc.scheduler(data)
        end)
    end

 
    osal.fork(function()
        local strcall = rpc.call(123,nil,0,"hello",1,2)

        print("call:",strcall)
        cli:send(strcall)
        return 0
    end,2000)
  --  print("--------------rpc call")
 end)