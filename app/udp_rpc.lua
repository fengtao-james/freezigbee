
package.path ="./lualib/?.lua"

local osal     = require "osal"

osal.include("./")
local aux_core = require "aux_core"
local lua_udp  = require("osal_udp") 

local rpc      = require("osal_rpc") 
local ncp      = require("ncp")

local cli_ip = nil

osal.start(function()

   ncp.start()

   local srv = lua_udp.srv_new(12321)

    if(srv~=nil)then
        srv:bind(function(ip,data)
            print("srv recv :",ip,data)
            -- srv:send(ip,"hello cli im srv")
            cli_ip = ip
            local sr = rpc.scheduler(data,nil)

            print("result data",sr)

            srv:send(ip,sr)
        end)
    end
    --mac,ep,cluster,rssi,control,menu,sql,cmd,data
    function ncp.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
        
        srv:send(cli_ip,ncp.call(nil,nil,0,nil,{method ="report",params =  {mac = aux_core.HexToStr(mac),ep = ep,cluster = cluster,rssi = rssi,control = control,cmd = cmd,data = aux_core.HexToStr(data)}}))
    end

    function ncp.on_join(mac,mfd,mode)
        
        srv:send(cli_ip,ncp.call(nil,nil,0,nil, {method ="join", params = {mac = aux_core.HexToStr(mac),mfd = mfd,mode = mode}}))
    end

    function ncp.on_leave(mac)
        
        srv:send(cli_ip,ncp.call(nil,nil,0,nil,{method = "leave",params = {mac = aux_core.HexToStr(mac)}}))
    end
end)