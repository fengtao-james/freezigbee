local osal     = require "osal"
local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 

s_cli = {}


osal.start(function()

    local srv = lua_tcp.srv_new(12345)

    local function srv_report(ip,fd)
        print("srv recv :",ip)
    
        local cli = srv:accept(fd)
        if(cli~= nil)then
           print( "accept :",fd)
    
           

           local http_srv = lua_tcp.http_new(cli)

            if(http_srv ==nil)then
                print("http srv is nil")
            end
            
            local function cli_recv(fd,data)

                print("-----msg recv",fd,#data)
                http_srv:decode(data)
            end

            local function cli_report(fd,ntype)

                print("-----cli net",fd,ntype)
                
                if(ntype == 1) then

                    lua_tcp.cli_free(s_cli[fd])
                    s_cli[fd] = nil
                end
            end
            cli:bind(cli_recv,cli_report)
            
            local function kv_func(k,v)
                print(k,v)
                if(k == "Sec-WebSocket-Key")then

                    osal.timecall(function()
                        print("acckey::",http_srv:rspconnect(v))
                        http_srv:send(http_srv:rspconnect(v))
                    end,1000)
                end
            end

            local function body_func(v)
                print("-------",v)
            end

            http_srv:bind(kv_func,body_func)
        end
        s_cli[fd] = cli
    end

     if(srv~=nil)then
        print("srv ok!!")
         srv:bind(srv_report)
     end
 end)