local osal     = require "osal"

osal.include("app")

local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local mqtt     = require("osal_mqtt")
local rpc      = require("osal_rpc") 
local ncp      = require("ncp")
-- 260a31ea2738c1a4
local mqtt_cli  = nil

local dev_topic = "/rpc/12345678"
local srv_topic = "/rpc/87654321"

local topic_cb = {}

local pcallx      = osal.pcall

local  function _on_connect(flags,status)
        
    if(status == 0) then

        print("mqtt connect ok!!!!")

        mqtt_cli:subscribe(srv_topic,0)
     end
end

local  function _on_recv_topic(topic,qos,payload)
        
    if topic_cb[topic] then

        pcallx(topic_cb[topic],qos,payload)
    end
end
local function tcp_mqtt_srv_connect()

 
    local cli = lua_tcp.cli_new("tcp://114.215.187.216:1883")

    if(cli == nil )then
        print("tcp cli is nil")
        return
    end

    local function tcp_mqtt_send(data)

     --   print("-----send",aux_core.HexToStr(data))
        cli:send(data)
    end

    local heat_num  = 0
    local mqtt_cli  =  mqtt.new(cli,_on_connect,_on_recv_topic)
    local mqttt_msg = ""

    topic_cb[srv_topic] = function(qos,data)

        print("recv data",data)
        
        local sr = rpc.scheduler(data,{sender = "87654321"})
        
        print("result data",sr)
    
        mqtt_cli:publish(dev_topic,0,sr)
    end

    cli:bind(function(fd,data)
       -- print("-----msg recv",aux_core.HexToStr(data),strmac)
            mqttt_msg = data
            mqtt_cli:decode(data)
        end,
        function(fd,ntype)
            print("-----cli net",ntype)
            
            if(ntype == 1) then
                cli:reset()
            else
                
                mqtt_cli:connect("admin","{13510470342}","12345678",180)
            end
        end)
    --[[
    local function ncp.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
        
        tcp_mqtt_send(ncp.call(nil,nil,0,{sender = "87654321"},{mac = mac,ep = ep,cluster = cluster,rssi = rssi,control = control,cmd = cmd,data = data}))
    end
    ]]
    osal.fork(function()

        heat_num = heat_num+1

        if(heat_num  == 100) then

            heat_num = 0
            mqtt_cli:ping()
        end
        return 0
        end,1000)
end

osal.start(function()

    tcp_mqtt_srv_connect()
    ncp.start()
end)