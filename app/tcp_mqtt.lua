local osal     = require "osal"

osal.include("lualib/modbus")
osal.include("lualib/cluster")
osal.include("lualib/device")

local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local mqtt     = require("osal_mqtt")
local zcl      = require("osal_zcl")
local map      = require("dev_485map")
local mgr      = require("dev_mgr")
local osal_kv  = require( "osal_kv" )

-- 260a31ea2738c1a4
local zgb_kv_db = nil

local function tcp_mqtt_srv_connect(mac)

    local strmac = aux_core.HexToStr(mac)
    print("strmac :::",strmac)
    local cli = lua_tcp.cli_new("tcp://www.ai-fsd.cn:1883")

    
    if(cli == nil )then
        print("tcp cli is nil")
        return
    end

    local function tcp_mqtt_send(data)

     --   print("-----send",aux_core.HexToStr(data))
        cli:send(data)
    end

    local srv_topic = "/server/coo/" .. strmac
    local dev_topic = "/dev/coo/" .. strmac
    local heat_num  = 0
    local mqtt_dev  =  mqtt.new()
    local mqttt_msg = ""

    mqtt_dev.connect_cb = function(flags,status)
        
        if(status == 0) then
            
            print("mqtt connect ok!!!!")

            tcp_mqtt_send(mqtt.subscribe(srv_topic,0))
        end
    end

    mqtt_dev.topic_cb[srv_topic] = function(qos,data)
       tcp_mqtt_send(mqttt_msg)

        local r = osal.json_decode(data)

        print("recv data",data,r.port_id,r.sdata)
        
        mgr.set(mac,r.port_id,r.sdata)
    end

    cli:bind(function(fd,data)
       -- print("-----msg recv",aux_core.HexToStr(data),strmac)
            mqttt_msg = data
            mqtt_dev:decode(data)
        end,
        function(fd,ntype)
            print("-----cli net",ntype)
            
            if(ntype == 1) then
                cli:reset()
            else
                local connect = mqtt.connect(nil,nil,strmac,180)

            -- print("-----connect",aux_core.HexToStr(connect))
                tcp_mqtt_send(connect)
            end
        end)
    
    osal.fork(function()
        heat_num = heat_num+1

        if(heat_num  == 100) then

            heat_num = 0
            tcp_mqtt_send(mqtt.ping())
        else
            local strdata = mgr.get_update(mac)
            
          --  print("strdata::",strdata)
            
            if(strdata ~= nil) then
                tcp_mqtt_send(mqtt.publish(dev_topic,0,strdata))
            end
        end
        return 0
    end,1000)
end
--[[
    NCP  CB
]]
local function _on_ready(mac,channel)
   
    osal.DBG("gw mac:%s channel:%d",aux_core.HexToStr(mac),channel)

    -- zcl.allow_join()
end

local function _on_online(mac,ep)

    osal.DBG("online mac:%s ep:%d",aux_core.HexToStr(mac),ep)

    zcl.unallow_join()

    local r = map.info(zgb_kv_db,mac,ep)

    if(r) then

        osal.timecall(function()
    
            osal.fork(tcp_mqtt_srv_connect,mac)
        end,1000)
    end

end

local function _on_next(mac,ep)

    
end

local function _on_left(mac)

    map.leave(zgb_kv_db,mac)
end

osal.start(function()

    zgb_kv_db = osal_kv.open("/bin/gw_8258/dev/devinfo",128,12800)
    if(zgb_kv_db == nil) then
    
        print("error db")
    end

    local list = {
        [zcl.dev_ready]   = _on_ready,
        [zcl.dev_online]  = _on_online,
        [zcl.dev_next_ep] = _on_next,
        [zcl.dev_left]    = _on_left,
    }
    zcl.recv_thread(list)

    osal.timecall(zcl.sys_reboot,500)

end)