package.path ="./lualib/?.lua"

local osal     = require "osal"
local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local mqtt     = require("osal_mqtt")
local zcl      = require("osal_zcl")

local ha_ui    = require("ha_ui")

local to_json     = osal.json_encode
local to_table    = osal.json_decode

-- 260a31ea2738c1a4
local dev_tcp_cli = nil
local mqtt_cli    = nil


local function _push_join()

    mqtt_cli:publish("cmnd/5c725269d838c1a4/ZbPermitJoin",0,0,to_json({Id = "123456",ZbPermitJoin = 1}))
end

local function _push_get()

    mqtt_cli:publish("cmnd/5c725269d838c1a4/Cmd",0,0,to_json({Id = "12345",Device = "077d2b4c1638c1a4",Endpoint =1,Cmd = {control = 0,cmd = 0,cluster = 0x0b04,data = "050508050b05"}}))
end

local function _report_sub()

    mqtt_cli:subscribe("stat/+/ZbPermitJoinAck",0)
    mqtt_cli:subscribe("stat/+/GwInfoAck",0)
    mqtt_cli:subscribe("stat/+/GpcmdAck",0)
    mqtt_cli:subscribe("stat/+/CmdAck",0)
    mqtt_cli:subscribe("stat/+/ZbSendAck",0)
    mqtt_cli:subscribe("stat/+/LeftAck",0)
    mqtt_cli:subscribe("stat/+/ZbLeaveAck",0)
    mqtt_cli:subscribe("stat/+/ZbInfoAck",0)
    mqtt_cli:subscribe("tele/+/join",0)
    mqtt_cli:subscribe("tele/+/LWT",0)
end

local function _report_tasmota_sub()

end


local function _set_tasmota_onoff()
    local onoff = 1

    osal.fork(function()
        
        if onoff == 1 then
            onoff = 0
            mqtt_cli:publish("cmnd/smartplug_7CE213/POWER",0,0,'OFF')

        else
            onoff = 1
            mqtt_cli:publish("cmnd/smartplug_7CE213/POWER",0,0,'ON')
        end

        return 0
    end,4000)
   
end

----------------------------------------------------------
local function _send_cmd(addr,ep,cmd)
    print("-----------push onoff devide")
    mqtt_cli:publish("cmnd/5c725269d838c1a4/ZbSend",0,0,to_json({Device = addr,Endpoint = ep,Cmd = cmd}))
end

local function _send_ts_cmd(addr,ep,cmd)
    -- print("-----------push onoff devide")
    mqtt_cli:publish("cmnd/5c725269d838c1a4/Cmd",0,0,to_json({Device = addr,Endpoint = ep,Cmd = cmd}))
end

local function _send_gp_cmd(addr,ep,cmd)
    -- print("-----------push onoff devide")
    mqtt_cli:publish("cmnd/5c725269d838c1a4/Gpcmd",0,0,to_json({Group = addr,Endpoint = ep,Cmd = cmd}))
end

local function _send_reset_cmd()
    -- print("-----------push onoff devide")
    mqtt_cli:publish("cmnd/5c725269d838c1a4/Left",0,0,"")
end

local function _find_all_device(void)
    mqtt_cli:publish("cmnd/5c725269d838c1a4/ZbInfo",0,0,to_json({Id = "1111111111"}))
    -- mqtt_cli:publish("cmnd/zbeacons/GwInfo",0,"")
end

--addr :077d2b4c1638c1a4
local function _del_dev(addr)
    -- mqtt_cli:publish("cmnd/zbeacons/ZbLeave",0,addr)
    mqtt_cli:publish("cmnd/5c725269d838c1a4/ZbLeave",0,addr)
end

local sign_trigger_info    = nil
local long_trigger_info    = nil
local double_trigger_info  = nil


local sensor_door_info  = nil
local sensor_misture_info  = nil
local sensor_motion_info  = nil

local function _ha_sign()
    local topic_cfg,trigger_info   = ha_ui.trigger("012345678","zbscen","button_1",1)

    sign_trigger_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local function _ha_double()
    local topic_cfg,trigger_info   = ha_ui.trigger("012345678","zbscen","button_2",2)

    double_trigger_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local function _ha_long()
    local topic_cfg,trigger_info   = ha_ui.trigger("012345678","zbscen","button_3",3)

    long_trigger_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local function _ha_door()

    local topic_cfg,trigger_info   = ha_ui.sensor_door("012345678","zbscen")

    sensor_door_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local function _ha_misture()

    local topic_cfg,trigger_info   = ha_ui.sensor_moisture("012345678","zbscen")

    sensor_misture_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end


local function _ha_motion()

    local topic_cfg,trigger_info   = ha_ui.sensor_motion("012345678","zbscen")

    sensor_motion_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local sensor_point_info  = nil

local function _ha_point()

    local topic_cfg,trigger_info   = ha_ui.point("012345678","zbscen","point")
    sensor_point_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

local function _ha_ontime()

    local topic_cfg,trigger_info   = ha_ui.ontime("012345678","zbscen")
    -- sensor_point_info = trigger_info
    mqtt_cli:publish(topic_cfg,0,0,to_json(trigger_info))
end

----------------------------------------------------------------------------
local function _ha_online()

    mqtt_cli:publish(sensor_point_info.availability.topic,0,0,"online")
end

----------------------------------------------------------------------------
local function _ha_set_sign()

    mqtt_cli:publish(sign_trigger_info.topic,0,0,sign_trigger_info.payload)
end

local function _ha_set_double()

    mqtt_cli:publish(double_trigger_info.topic,0,0,double_trigger_info.payload)
end

local function _ha_set_long()

    mqtt_cli:publish(long_trigger_info.topic,0,0,long_trigger_info.payload)
end

local function _ha_set_on_bsensor()

    mqtt_cli:publish(sensor_door_info.stat_t,0,0,to_json({door = "ON",motion = "ON",moisture = "ON"}))
end

local function _ha_set_off_bsensor()

    mqtt_cli:publish(sensor_door_info.stat_t,0,0,to_json({door = "OFF",motion = "OFF",moisture = "OFF"}))
end


local function _on_tcp_recv(fd,data)
    -- osal.WRN("RECV len:%d",#data)
    mqtt_cli:decode(data)
end


local function _on_tcp_notify()

    print("tcp is error ------------------")

  --  mqtt_cli:connect("mqtt","mqtt","8888888",180)
end


local  function _on_connect(flags,status)
    
    local onoff = 1

    if(status == 0) then
        
        print("mqtt connect ok!!!!")

        -- _ha_point()
        -- _ha_ontime()
        -- _ha_sign()
        -- _ha_double()
        -- _ha_long()
        -- _ha_door()
        -- _ha_misture()
        -- _ha_motion()
        -- _report_tasmota_sub()
        _report_sub()
        -- _set_tasmota_onoff()
        _push_join()
        -- _del_dev("0xD0A6")
        -- _del_dev("0xA4C1381ECA543A09")
       

        --  osal.fork(function()
                --  _ha_set_on_bsensor()
                --  return 0
                --  end,3000)

        -- osal.fork(function()
            --     _ha_set_off_bsensor()
            --     return 0
            --     end,4000)

        osal.fork(function()
                        -- _ha_online()
                        -- _find_all_device()
                        -- _send_cmd("077d2b4c1638c1a4",1,{Power = onoff})
                        _push_get()
                        if onoff == 1 then
                            onoff = 0
                        else
                            onoff =1
                        end
                        return 0
                        end,5000)

        --  _send_gp_cmd(1,1,{cluster = 6,control = 1,cmd = 1})
        -- _send_reset_cmd()
        -- _del_dev("077d2b4c1638c1a4")
    else
        print("connect error")
    end
end

local  function _on_recv_topic(topic,qos,payload)

    print("mqtt recv :",topic,payload)
end

local function tcp_mqtt_srv_connect()
-- 2rn0690348.zicp.fun:56139
    dev_tcp_cli = lua_tcp.cli_new("tcp://10.10.10.1:8883")

    if not dev_tcp_cli then
        error("error ip -------")
        return
    end
    dev_tcp_cli:bind(_on_tcp_recv,_on_tcp_notify)
    

    mqtt_cli  =  mqtt.new(dev_tcp_cli,_on_connect,_on_recv_topic)
    
    mqtt_cli:connect("zbeacon","zbeacon123","8888888",180,"/hello","hellp")
    
    osal.fork(function() 
                        print("----------------------mqtt   ping")
                        mqtt_cli:ping() 
                        return 0
                        end,
                    30000)
end

osal.start(function()

    tcp_mqtt_srv_connect()
end)