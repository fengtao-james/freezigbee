
local osal     = require "osal"
local aux_core = require "aux_core"
local lua_udp  = require("osal_udp") 


osal.start(function()

   local srv = lua_udp.srv_new(12345)

    if(srv~=nil)then
        srv:bind(function(ip,data)
            print("srv recv :",ip,data)
            srv:send(ip,"hello cli im srv")
        end)
    end
end)