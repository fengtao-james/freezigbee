package.path ="./lualib/?.lua"

local osal     = require "osal"
local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 

cli_ip = nil

local function cli_recv(fd,data)
    print("-----msg recv",fd,data)

    cli_ip:send("1223333")
end

local function cli_report()

    print("-----cli net lost")
    lua_tcp.cli_free(cli_ip)
    cli_ip = nil
end


osal.start(function()

    local srv = lua_tcp.srv_new(12345)

    local function srv_report(ip,fd)
        print("srv recv :",ip)
    
        local cli = srv:accept(fd)
        if(cli~= nil)then
           print( "accept :",fd)
    
           cli:bind(cli_recv,cli_report)
        end
        cli_ip = cli
    end

     if(srv~=nil)then
        srv:bind(srv_report)
     end
 end)