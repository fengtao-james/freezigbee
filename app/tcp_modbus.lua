local osal     = require "osal"

osal.include("lualib/modbus")
osal.include("lualib/cluster")
osal.include("lualib/device")

local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local zcl      = require("osal_zcl")
local map      = require("dev_map")
local mgr      = require("dev_mgr")


local bus  = require("modbus_rtusrv") 

local function bus_tcp_srv_connect(mac)

    local strmac = aux_core.HexToStr(mac)
    local cli = lua_tcp.cli_new("tcp://47.111.164.255:50001")

    if(cli == nil)then
        print("tcp error ............")
        return
    end

    local bushandle = bus.new()

    bushandle.read_onoff = function(addr,star,num)
 
        local r = bus.pack(addr,bushandle.cmd,string.pack("B",mgr.get(addr,star)))

       
        cli:send(r)
    end

    bushandle.set_onoff = function(data,addr,id,onoff)
        print("set onoff addr cmd",addr,id,onoff)
        cli:send(data)

        if(addr == 0) then
            mgr.set(mac,id,onoff)
        else
            print("485 cmd ....")
        end
    end


    cli:bind(function(fd,data)
        print("-----msg recv",aux_core.HexToStr(data))

        bushandle:handle(data)

        end,function(fd,ntype)

        print("-----cli net",ntype)
        if(ntype == 1) then
            cli:reset()
        else
            cli:send(strmac)
        end
    end)

    osal.fork(cli:send(strmac),30000)
end
--[[
    NCP  CB
]]
local function _on_ready(mac,channel)
   
    osal.DBG("gw mac:%s channel:%d",aux_core.HexToStr(mac),channel)

    zcl.allow_join()
end

local function _on_online(mac,ep)

    osal.DBG("online mac:%s ep:%d",aux_core.HexToStr(mac),ep)

    zcl.unallow_join()

    mgr.info(mac,ep)

    osal.timecall(function()
      
        map.device_identify(mac)

        osal.fork(bus_tcp_srv_connect,mac)
    end,5000)
end

local function _on_next(mac,ep)

    
end

local function _on_left(mac)

    
end

osal.start(function()

    local list = {
        [zcl.dev_ready]   = _on_ready,
        [zcl.dev_online]  = _on_online,
        [zcl.dev_next_ep] = _on_next,
        [zcl.dev_left]    = _on_left,
    }
    zcl.recv_thread(list)

    osal.timecall(zcl.sys_reboot,500)
    osal.fork(mgr.change,50)
end)