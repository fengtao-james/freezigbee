package.path ="./lualib/?.lua"

local osal        = require "osal"
local aux_core    = require "aux_core"
local lua_tcp     = require("osal_tcp") 
local mqtt        = require("osal_mqtt")
local zcl         = require("osal_zcl")
local ha_ui       = require("ha_ui")

local to_json     = osal.json_encode
local to_table    = osal.json_decode

-- 260a31ea2738c1a4
local dev_tcp_cli = nil
local mqtt_cli    = nil


local function _push_join()
    mqtt_cli:publish("cmnd/zbeacons/ZbPermitJoin",0,to_json({ZbPermitJoin = 1}))
end

local function _report_sub()
    mqtt_cli:subscribe("tele/+/SENSOR",0)
end

local function _find_all_device(void)
    mqtt_cli:publish("cmnd/zbeacons/ZbInfo",0,"")
end

--to_json({Device = addr}
local function _del_dev(addr)
    mqtt_cli:publish("cmnd/zbeacons/ZbLeave",0,addr)
    mqtt_cli:publish("cmnd/zbeacons/ZbForget",0,addr)
end

local function _send_cmd(addr,ep,cmd)
    mqtt_cli:publish("cmnd/zbeacons/ZbSend",0,to_json({Device = addr,Endpoint = ep,Send = cmd}))
end

local function _on_tcp_recv(fd,data)
    mqtt_cli:decode(data)
end

local function _on_tcp_notify()

    print("tcp is error ------------------")
  --  mqtt_cli:connect("mqtt","mqtt","8888888",180)
end


local  function _on_connect(flags,status)
        
    if status ~= 0 then

        return
    end

    print("mqtt connect ok!!!!")
    _report_sub()
    _find_all_device()
    
 
    osal.fork(function()
            _send_cmd("0xA4C1387904E50A4F",1,{power = 0})
            return 0
            end,2000)
   --[[
    osal.fork(function()
                _find_all_device()
                return 0
                end,10000)
    ]]
end

local  function _on_recv_topic(topic,qos,payload)

    print("mqtt recv :",topic,payload)

end
--emqx.dx1628.com 1883
--tcp://192.168.8.118:1883
local function tcp_mqtt_srv_connect()

    dev_tcp_cli = lua_tcp.cli_new("tcp://emqx.dx1628.com:1883")

    if not dev_tcp_cli then
        error("error ip -------")
        return
    end
    dev_tcp_cli:bind(_on_tcp_recv,_on_tcp_notify)
    
    mqtt_cli  =  mqtt.new(dev_tcp_cli,_on_connect,_on_recv_topic)
    
    mqtt_cli:connect("mqtt12","mqtt12","8888888",180)
    
    osal.fork(function() 
            print("----------------------mqtt   ping")
            mqtt_cli:ping() 
            return 0
            end, 30000)
end

osal.start(function()
    tcp_mqtt_srv_connect()
end)