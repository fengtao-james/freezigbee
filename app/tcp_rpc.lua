
package.path ="./lualib/?.lua"

local osal     = require "osal"

osal.include("./")
local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local rpc      = require("osal_rpc") 
local ncp      = require("ncp")

local cli_ip = nil

local function cli_recv(fd,data)
    print("-----msg recv",fd,data)

    local sr = rpc.scheduler(data,nil)
    print("result data",sr)

    cli_ip:send(sr)

end

local function cli_lost()
  
    lua_tcp.cli_free(cli_ip)
    print("-----cli lost")

    cli_ip = nil
end


osal.start(function()

   ncp.start()

   local srv = lua_tcp.srv_new(12321)

    local function srv_report(ip,fd)
        print("srv recv :",ip,fd)

        local cli = srv:accept(fd)

        if(cli~= nil)then

        cli:bind(cli_recv,cli_lost)
        
        cli_ip = cli
        end
    end


    if(srv~=nil)then
  
        srv:bind(srv_report)
     end

    --mac,ep,cluster,rssi,control,menu,sql,cmd,data
    function ncp.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
        if not cli_ip then
        
            return
        end

        cli_ip:send(ncp.call(nil,nil,0,nil,{method ="report",params =  {mac = aux_core.HexToStr(mac),ep = ep,cluster = cluster,rssi = rssi,control = control,cmd = cmd,data = aux_core.HexToStr(data)}}))
    end

    function ncp.on_join(mac,mfd,mode)
        if not cli_ip then
        
            return
        end

        cli_ip:send(ncp.call(nil,nil,0,nil, {method ="join", params = {mac = aux_core.HexToStr(mac),mfd = mfd,mode = mode}}))
    end

    function ncp.on_leave(mac)
        if not cli_ip then
        
            return
        end

        cli_ip:send(ncp.call(nil,nil,0,nil,{method = "leave",params = {mac = aux_core.HexToStr(mac)}}))
    end

    function ncp.on_ready(mac,channel)
        if not cli_ip then
        
            return
        end

        cli_ip:send(ncp.call(nil,nil,0,nil,{method = "ready",params = {mac = aux_core.HexToStr(mac),channel = channel}}))
    end
end)