
local osal     = require "osal"
local aux_core = require "aux_core"
local zcl      = require("osal_zcl")
local rpc      = require("osal_rpc") 

local pcallx   = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local ncp = {}

rpc.register("ncp.reboot",function()

    zcl.sys_reboot()
    return true
end)

--ncp.allowjoin( enable, sec )
rpc.register( "ncp.allowjoin", function(en,t)

    print("en  t",en,t)
    if(en) then
        zcl.allow_join()
    else
        zcl.unallow_join()
    end
    return true
 end)

 --ncp.reset()
 rpc.register( "ncp.reset", function()
    zcl.net_reset()
    return true
 end)

 --ncp.get_details()
 rpc.register( "ncp.remove", function(mac)

    osal.DBG("del mac:%s",mac)
    zcl.del_dev(aux_core.StrToHex(mac))
    return true
 end)

 --ncp.get_device_list()
 rpc.register( "ncp.get_details", function()
    local mac,channel = zcl.net_info()

    return {mac = aux_core.HexToStr(mac),channel = channel}
 end)

 --ncp.remove( addr )
 rpc.register( "ncp.get_device_list", function()
    
    local list = osal.array_pack(zcl.all_dev())

    if 0 == #list then
        return {}
    end
    local strlist = {}

    for i = 1, list.n ,1 do
        table.insert(strlist,aux_core.HexToStr(list[i]))
    end

    return strlist
 end)

----------------------------------------------
-- zcl.get_device_details( addr )
rpc.register("zcl.get_device_details",function(addr)
    local mac = aux_core.StrToHex(addr)
    local list = zcl.get_eplist(mac)
    local info = {}


    for i = 1,#list,1 do
        local outlist = {}
        local intlist = {}
        -- print("date",list:byte(i))
        local mac,addr,ep,did,incid,outcid =  zcl.get_ep_info(mac,list:byte(i))

        while #incid ~=0 do
     
        pcallx(table.insert,intlist,(string.unpack("<H",incid)))

        incid = string.sub(incid,3)
    
        end

        while #outcid ~= 0 do

        pcallx(table.insert,outlist,(string.unpack("<H",outcid)))
        outcid = string.sub(outcid,3)
        
        end

        table.insert(info,{mac = aux_core.HexToStr(mac),addr = addr,ep = ep,did = did,outcid = outlist,incid = intlist})
    end

    return info
end)

--zcl.send( addr, ep, cluster, control, command, buffer )
rpc.register("zcl.send",function(addr,ep,cluster,control,command,buffer)

    print("zcl.send ",addr,ep,cluster,control,command,buffer)

    local r,rssi =   zcl.send(aux_core.StrToHex(addr),ep,cluster,control,command,aux_core.StrToHex(buffer))

    if r then
        return aux_core.HexToStr(r),rssi
    else
        osal.ERR("send error mac :%s",addr)
    end
    return nil
end)

--zcl.multicast( addr, ep, cluster, control, command, buffer )
rpc.register("zcl.multicast",function(addr,ep,cluster,control,command,buffer)

    local r = zcl.group(addr,ep,cluster,control,command,aux_core.StrToHex(buffer))
    if r<0  then
        return false
    end
    return true
end)

--zcl.broadcast( ep, cluster, control, command, buffer )

rpc.register("zcl.broadcast",function(ep,cluster,control,command,buffer)
    local r = zcl.broadcast(ep,cluster,control,command,aux_core.StrToHex(buffer))
    if r<0  then
        return false
    end
    return true
end)

local on_report_cb = nil
--zcl.subscribe( fnname )
rpc.register("zcl.subscribe",function(fnname)

    on_report_cb = fnname
   
    return true
end)

--zcl.unsubscribe( fnname )
rpc.register("zcl.unsubscribe",function(fnname)

    on_report_cb = nil

    return true
end)

-- (session,result ,timeout,extend, method, ...)
function ncp.call(session,result ,timeout,extend, ...)
    return rpc.call(session,result,timeout,extend,on_report_cb,...)
end

----------------------------------------------------------------------
local function _on_ready(mac,channel)
   
    osal.DBG("gw mac:%s channel:%d",aux_core.HexToStr(mac),channel)
    pcallx(ncp.on_ready,mac,channel)
    
    zcl.online()
end

local function _on_online(mac,ep)

    zcl.unallow_join()
end

local function _on_next(mac,ep)

    
end

local function _on_left(mac)
    osal.DBG("left mac:%s",aux_core.HexToStr(mac))
    
    zcl.unbind(mac)

    pcallx(ncp.on_leave,mac)
end

local ota_up = {}

local function _on_ota_end(mac,ep,st)

    osal.DBG("ota end mac:%s",aux_core.HexToStr(mac))

    ota_up[mac] = nil
    
    zcl.allow_join()

    osal.fork(function()
        osal.DBG("del mac:%s",aux_core.HexToStr(mac))
        zcl.del_dev(mac)
        return 0
    end,80000)
end


local function _on_ota_update(mac,ep,max,now)
   
    if(ota_up[mac] == nil) then
        ota_up[mac] = now
        return 
    end

    local off = now - ota_up[mac]
    
    if(off > 20000) then
        ota_up[mac] = now

        print("ota update:",aux_core.HexToStr(mac),now/max)
    end
end

local function _on_dev_info(mac,mfc,mode)

    osal.DBG("dev online mac :%s mfc: %s mode:%s",aux_core.HexToStr(mac),mfc,mode)

    zcl.bind(mac,ncp.on_report)

    pcallx(ncp.on_join,mac,mfc,mode)
end

local function _on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
    zcl.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
end

local list = {
    [zcl.dev_ready]   = _on_ready,
    [zcl.dev_online]  = _on_online,
    [zcl.dev_left]    = _on_dev_left,
    [zcl.dev_next_ep] = _on_next,
    [zcl.dev_ota_end] = _on_ota_end,
    [zcl.dev_info]    = _on_dev_info,
    [zcl.dev_ota_up]  = _on_ota_update,
    [zcl.data_report] = _on_report
}

osal.start(function()
    print(">>NCP START")
    zcl.sys_start("/dev/ttyUSB0",".",115200)
    zcl.recv_thread(list)
    osal.timecall(zcl.sys_reboot,500)
end)

return ncp