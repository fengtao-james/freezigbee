
local osal     = require "osal"

osal.include("app")
local aux_core = require "aux_core"
local lua_udp  = require("osal_udp") 

local rpc      = require("osal_rpc") 
local ncp      = require("ncp")

osal.start(function()

    ncp.start()

    local cli = lua_udp.cli_new("udp://192.168.8.117:12321")

    if(cli~=nil)then
        
        cli:bind(function(ip,data)
            -- print("cli recv :",ip,data)

            local sr = rpc.scheduler(data,nil)
            if sr then
                -- print("result data",sr)
                cli:send(sr)
            end
        end)
    end

--mac,ep,cluster,rssi,control,menu,sql,cmd,data
    function ncp.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
        
        cli:send(ncp.call(nil,nil,0,nil,"report", {mac = aux_core.HexToStr(mac),ep = ep,cluster = cluster,rssi = rssi,control = control,cmd = cmd,data = aux_core.HexToStr(data)}))
    end

    function ncp.on_join(mac,mfd,mode)
        
        cli:send(ncp.call(nil,nil,0,nil, "join",{mac = aux_core.HexToStr(mac),mfd = mfd,mode = mode}))
    end

    function ncp.on_leave(mac)
        
        cli:send(ncp.call(nil,nil,0,nil, "leave",{mac = aux_core.HexToStr(mac)}))
    end

  
   -- local strcall = cli:send(rpc.call(11234,nil ,0,nil, "register"))
   -- print("recv strcal",strcall)
    local tick = osal.tick()

    osal.fork(function()
        local ping =  rpc.call(11234,nil ,0,nil, "register",tick)

        -- print("ping :",ping)

        cli:send(ping)
        return 0
    end,5000)
end)