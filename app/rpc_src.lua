local rpc  =  require("osal_rpc") 
local osal     = require "osal"
local lua_udp  = require("osal_udp") 

rpc.register( "hello", function(r,c)
    print("-----hellp api",r,c)
    return r+c
 end)

 rpc.register( "world", function(r,c)
    print(r,c)
 end)

osal.start(function()
    local srv = lua_udp.srv_new(12345)

    if(srv~=nil)then
        srv:bind(function(ip,data)
            print("srv recv :",ip,data)
            local strrst = rpc.scheduler(data)
            print("result:",strrst)

            srv:send(ip,strrst)
        end)
    end
    
 end)
