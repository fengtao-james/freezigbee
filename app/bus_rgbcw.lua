package.path ="/usr/lib/lualib/?.lua"
-- package.path ="./lualib/?.lua"

local osal     = require "osal"

osal.include("/root/fzigbee")

local aux_core = require("aux_core")
local bus      = require("bus_cli")

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr

local function bus_recv_data(addr,cmd,data)
   if cmd > bus.read_input then
    return
   end

   local len,pid,add,temp,hui =  string.unpack("B >H >H >H >H",data)
    print("bus recv:",addr,cmd,#data,len,pid,add,temp,hui)
end
-- // "/dev/ttyUSB0"
-- // "/dev/ttyS0"
osal.start(function()
    
    local bus_index = 1

    bus.sys_start("/dev/ttyS0",57600)
    bus.recv_thread(bus_recv_data)

    bus.send(0,bus.set_acdata,1,1)

    osal.fork(function()
        bus.send(bus_index,bus.read_acdata,1,20)

        bus_index = bus_index + 1

        if bus_index > 20  then
            bus_index = 1
        end

        return 0
    end,100)
end)