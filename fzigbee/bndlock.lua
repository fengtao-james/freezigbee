local osal     = require "osal"
local aux_core = require "aux_core"
local zcl      = require "osal_zcl"

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr
local to_json     = osal.json_encode
local to_table    = osal.json_decode


local bndlock = {}

local function lockchecksun(hex)

    local sun = 0

    for i=1, #hex ,1 do
        sun = sun + hex:byte(i)
    end
    
    return hex .. string.pack("B",sun & 0xff)
end

function bndlock.unlock(ackfn,mac,ep,cmd)
    
    if is_string(cmd.key) then

        local phead   = "\x00\x02\x0E\x08\x00\x08\x63"
        local skey    = "\x55\xaa\x03\x00\x01\x04\x00\x0A\x15\x00\x00"
        
        skey  = skey .. string.pack("B",#cmd.key)
        skey  = skey .. cmd.key
        skey  = lockchecksun(skey)

        phead = phead .. string.pack("B",#skey)

        return zcl.menuwrite(ackfn,mac,ep,0xfcc0,0x115f,0xfff1,zcl.OCTET_STR,phead .. skey)
    end

    return false
end

local function locktime(mac,ep)
    local phead   = "\x00\x02\x0D\x08\x00\x08\x63\x11"
    local payload = "\x55\xaa\x03\x00\x02\x24\x00\x08"

    local baset  = string.pack(">I",os.time())
    local lbaset = string.pack(">I",os.time() + 28800)

    timebase = payload .. baset .. lbaset
    timebase = phead .. lockchecksun(timebase)

    local function _ackfn(control,cmd,data,rssi)
        if rssi then
            print(" locktime set ok!")
        else
            print(" locktime set error!")
        end
    end

    zcl.menuwrite(_ackfn,mac,ep,0xfcc0,0x115f,0xfff1,zcl.OCTET_STR,timebase)
end

function bndlock.on_report(mac,ep,cluster,control,cmd,data)
    
    local lockpy = nil

    if cluster ~= 0xfcc0 then
        return lockpy
    end

    local r = false

    zcl.report_unpack(data,function(attrid,dtype,atrdata)
    
        if attrid ~= 0xfff1 then
            return lockpy
        end
        r = true

        local dlen = string.unpack("B",atrdata)
        local payload = string.sub(atrdata,1+5)

        local cmd = string.unpack("B",payload)

        payload = string.sub(payload,4)
        
        if cmd == 0x24 then
            locktime(mac,ep)
        end
    
        if cmd == 5 then

            local dpid,dptp ,dplen= string.unpack("B B >H",payload)

            payload = string.sub(payload,5)
            if dpid == 10 then
                lockpy = {BatteryPercentage = string.unpack(">I",payload)}
            end
            if dpid == 14 then
                lockpy = {doorbell = 1}
            end
            if dpid == 22 then
                lockpy = {Remotelock = string.unpack("B",payload)}
            end
        end

        if cmd == 0x23 then
            payload = string.sub(payload,6)

            local dpid,dptp ,dplen= string.unpack("B B >H",payload)
            
            if dpid == 2 then
                lockpy = {actlock = "password"}
            end
            if dpid == 5 then
                lockpy = {actlock = "card"}
            end
            if dpid == 41 then
                lockpy = {actlock = "Remote"}
            end
        end
    end)

    return lockpy,r
end

return bndlock
