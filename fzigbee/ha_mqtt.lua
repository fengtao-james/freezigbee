local osal     = require "osal"
local aux_core = require "aux_core"
local lua_tcp  = require("osal_tcp") 
local mqtt     = require("osal_mqtt")
local zcl      = require "osal_zcl"


local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr
local to_json     = osal.json_encode
local to_table    = osal.json_decode

local dev_tcp_cli    = nil
local mqtt_cli = nil 

local instance_list = {}
local tocpic_list   = {}
local net_info_st   = false
local namespace     = {}
local mqgw            = nil
local mqgwlist        = {}

local tcp_last        = ""

local function _push_online_info(dev)
    if not mqtt_cli then
        return
    end
    if dev.set_cb  then
        mqtt_cli:subscribe(dev.info.cmd_t,0,dev.set_cb)
    end

    if dev.reg_cb then
        pcallx(dev.reg_cb)
    end
end

local function _push_regist_info(dev)
    if not mqtt_cli then
        return
    end
    mqtt_cli:publish(dev.cfg,0,0,to_json(dev.info))
    _push_online_info(dev)
end

local function _push_offline_info(dev)
    if not mqtt_cli then
        return
    end
    if dev.set_cb then     
        mqtt_cli:unsubscribe(dev.info.cmd_t,0)
    end

    if dev.unreg_cb then
        pcallx(dev.unreg_cb)
    end
    mqtt_cli:publish(dev.cfg,0,0,nil)
end


local function _devlist_regist(list)

    osal.table_foreach(list,function(k,v)

        osal.array_foreach(v,function(dev)
            _push_regist_info(dev)
        end)
    end)
end

local function _devlist_left(list)

    osal.table_foreach(list,function(k,v)
        osal.array_foreach(v,function(dev)
            _push_offline_info(dev)
        end)
    end)
end

function namespace.gw(dev)
    mqgw = dev
end

function namespace.online()

    net_info_st = true

    if not mqgw then
        return
    end

    osal.array_foreach(mqgw,function(dev)
        _push_regist_info(dev)
    end)

    _devlist_regist(mqgwlist)
    _devlist_regist(instance_list)
end


function namespace.regist(dev,cfg,info,set_cb,reg_cb,unreg_cb)
    
    if not is_table(dev) then
        error("reg error dev")
    end

    table.insert(dev,{ 
                        info      = info,
                        set_cb    = set_cb,
                        reg_cb    = reg_cb,
                        unreg_cb  = unreg_cb,
                        cfg       = cfg
                    })
end

function namespace.gbind(mac,dev)

    if not is_table(dev) then
        error("bind error dev")
    end

    mqgwlist[mac] = dev
end


function namespace.gunbind()

    _devlist_left(mqgwlist)
end

-- instance
function namespace.bind(mac,dev)

    if not is_table(dev) then
        error("bind error dev")
    end

    instance_list[mac] = dev

    print("bind:",net_info_st)

    if net_info_st  then
        
        osal.array_foreach(dev,function(v)
            _push_regist_info(v)
        end)
    end
end

function namespace.unbind(mac)

    local dev = instance_list[mac]

    osal.array_foreach(dev,function(v)
        _push_offline_info(v)
    end)
    instance_list[mac] = nil
end


function namespace.offline()

    _devlist_left(instance_list)

end

function namespace.subscribe(topic,cb)
    if not mqtt_cli then
        return
    end

    mqtt_cli:subscribe(topic,0,cb)
end

function namespace.unsubscribe(topic)
    if not mqtt_cli then
        return
    end
    mqtt_cli:unsubscribe(topic,0)
end
--{"mac": "012345678"}

function namespace.report(topic,payload)
    if not mqtt_cli then
        return
    end
    mqtt_cli:publish(topic,0,0,payload)
end

function namespace.push_retain(topic,payload)
    if not mqtt_cli then
        return
    end
    mqtt_cli:publish(topic,0,1,payload)
end

--------------------------------------------------------------------------
local function rmqttcfg(name)

    local info = nil

    local file  = io.popen("uci get mqtt_file.mqtt." .. name,"r")

    info = file:read("a")

    file:close()

    return string.match( info, "[%w%p]+" )
end

local function unpack_package(text)
    local size = #text
    
	if size < 2 then
		return nil, text
    end
    
    local l,s = mqtt_cli:plen(text)
    
	if not l then
		return nil, text
    end
	return text:sub(1,l-1+s), text:sub(l+s)
end

function namespace.start(connectfn,willtp,willmsg,mac)

    if dev_tcp_cli then
        print("mqtt is set")
        return
    end

    local uri    = rmqttcfg("url")
    local user   = rmqttcfg("user")
    local passwd = rmqttcfg("passwd")

    if not uri then
        error("uri is error")
    end

    if not is_function(connectfn) then
        error("connectfn is nil")
    end

    if not is_string(willtp) then

        error("willtp is nil")
    end

    if not is_string(willmsg) then

        error("willmsg is nil")
    end

    osal.DBG("mqtt start:%s %s %s",uri,user,passwd)
    
    local mac_str = aux_core.HexToStr(mac)

    local function _on_tcp_recv(fd,data)
        
        tcp_last = tcp_last..data

        while true do
            local v
            v, tcp_last = unpack_package(tcp_last)
            if not v then
                break
            end
            mqtt_cli:decode(v)
        end
    end

    local function _re_connect()

        osal.fork(function()
            dev_tcp_cli:connect(uri)

            if not dev_tcp_cli.ctx then
                return 0
            end
            mqtt_cli:connect(user,passwd,mac_str,180,willtp,willmsg)

            return -1
        end,3000)
    end

    local function _on_tcp_lost()
        net_info_st   = false
        print("tcp lost--------------------")
        pcallx(_re_connect)
    end

    local  function _on_recv_topic(topic,qos,payload)

        local cb = tocpic_list[topic]

        if cb then
            osal.DBG("recv %s %s",topic,payload)
            pcallx(cb,payload)
        end
    end

    osal.fork(function()
    
        dev_tcp_cli = lua_tcp.cli_new(uri)

        if not dev_tcp_cli then
            print("error ip --")
            return 5000
        end

        dev_tcp_cli:bind(_on_tcp_recv,_on_tcp_lost)

        mqtt_cli    =  mqtt.new(dev_tcp_cli,connectfn,_on_recv_topic)
        mqtt_cli:connect(user,passwd,mac_str,180,willtp,willmsg)
        return -1
    end,50)

    osal.fork(function()
        if mqtt_cli then
            mqtt_cli:ping()
        end
        return 0
    end,30000)
end


return namespace

