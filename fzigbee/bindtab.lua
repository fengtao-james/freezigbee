local osal     = require "osal"
local aux_core = require "aux_core"
local zcl      = require "osal_zcl"

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr
local to_json     = osal.json_encode
local to_table    = osal.json_decode


local bind_tab = {
    -- [1] = {input = {Device = "f14c006f6338c1a4",Endpoint = 1,name = "Alarm",data = 1}
    --      ,output = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = "C4E3BAC35B7035305DBFAACABCD0B4B4FAC2EBB0C95B7035305DD3EFD2F4C1AAB6AF5B7035305DC8C3C9FABBEEB8FCBCD3D6C7C4DC"}}},
    
    -- [2] = {input = {Device = "f14c006f6338c1a4",Endpoint = 1,name = "Alarm",data = 0}
    -- ,output = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = "BFAACABCB9A4D7F7B0C95B7035305DBCD3D3CD"}}},

    -- [3] = {input = {Device = "acfef202e838c1a4",Endpoint = 1,name = "Alarm",data = 1}
    -- ,output = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = "C3C5BFAAC1CB5B7035305DA3ACBBB6D3ADBBD8C0B4"}}},

    -- [4] = {input = {Device = "acfef202e838c1a4",Endpoint = 1,name = "Alarm",data = 0}
    -- ,output = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = "C3C5B9D8C1CB5B7035305DC2FDD7DF5B7035305DD7A2D2E2B0B2C8AB"}}},

    -- [5] = {input = {Device = "141946469938c1a4",Endpoint = 1,name = "Alarm",data = 1}
    -- ,output = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {IASWD = 3,Blink = 1,Duration = 1}}},
}


local temphum_tab = {
-- 'B6C8'
[1] = {input  = {Device = "7a790666dd38c1a4",Endpoint = 1,name = "Temperature"}
    ,output   = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = ""},info = "CFD6D4DACEC2B6C85B6E325D"}},
[2] =  {input = {Device = "7a790666dd38c1a4",Endpoint = 1,name = "Humidity"}
    ,output   = {Device = "3fc11f19cf38c1a4",Endpoint = 1,Cmd = {TTS_Girl = ""},info = "CFD6D4DACAAAB6C8B0D9B7D6D6AE5B6E325D"}},
}

local bindapi = {}


local function _action(acfn,output)

    local function _ackfn(control,cmd,data,rssi)

        if rssi then
            print("action ok")
        else
            print("action time out")
        end
    end

    local r = acfn(_ackfn,str_to_hex(output.Device),output.Endpoint,output.Cmd)

    if not r then
        print("cmd is Invalid")
    end
end

local function bind_action(acfn,mac,ep,name,code)

    osal.array_foreach(bind_tab,function(tab)
        local input = tab.input

        if input.Device == mac and input.Endpoint == ep  and input.name == name and input.data == code then

            osal.timecall(function()
                _action(acfn,tab.output)
            end,10)
        end
    end)
end


local function temphum_action(acfn,mac,ep,name,code)

    osal.array_foreach(temphum_tab,function(tab)
        local input = tab.input

        if input.Device == mac and input.Endpoint == ep  and input.name == name then

           local scode = hex_to_str(string.format("%d", code//100))
           
            osal.timecall(function()
               local output = tab.output

               output.Cmd.TTS_Girl = output.info .. scode
                _action(acfn,output)
            end,3500*osal.random(0,2))
        end
    end)
end

function bindapi.on_bind(acfn,mac,ep,name,code)

    temphum_action(acfn,mac,ep,name,code)
    bind_action(acfn,mac,ep,name,code)
end

function bindapi.set_tab(index,input,output)

    bind_tab[index+1] = {input = to_table(input),output = to_table(output)}
end

function bindapi.appbind(index,input,output)

    if not is_string(input) then
        zcl.bind_table(index,"","")
        bind_tab[index+1] = nil
        return
    end

    zcl.bind_table(index,input,output)
    bind_tab[index+1] = {input = to_table(input),output = to_table(output)}
end

return bindapi
