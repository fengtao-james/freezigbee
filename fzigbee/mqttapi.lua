local osal     = require "osal"
local aux_core = require "aux_core"
local zcl      = require "osal_zcl"
local rpc      = require "osal_rpc"
local bindtab  = require "bindtab"

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_bool     = aux_core.IsBoolean

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr

local b64_to_str  = osal.b64_encode
local b64_to_hex  = osal.b64_decode

local to_json     = osal.json_encode
local to_table    = osal.json_decode

local mqttapi = {}


local attr_list = {
    -- Power Configuration cluster
    {tp = zcl.UINT8,   cid = 0x0001, atid = 0x0020,  name = "BatteryVoltage",         },   -- divide by 10
    {tp = zcl.UINT8,   cid = 0x0001, atid = 0x0021,  name = "BatteryPercentage",      },   -- divide by 2
            
    -- On/off cluster
    {tp = zcl.BOOLEAN,    cid = 0x0006, atid = 0x0000,  name = "Power",                 },
    {tp = zcl.ENUM8,      cid = 0x0006, atid = 0x4003,  name = "StartUpOnOff",          },
    {tp = zcl.BOOLEAN,    cid = 0x0006, atid = 0x4000,  name = "OnOff",                 },
    {tp = zcl.UINT16,     cid = 0x0006, atid = 0x4001,  name = "OnTime",                },
    {tp = zcl.UINT16,     cid = 0x0006, atid = 0x4002,  name = "OffWaitTime",           },
    {tp = zcl.ENUM8,      cid = 0x0006, atid = 0x8002,  name = "tyStartUpOnOff",        },
    {tp = zcl.BOOLEAN,    cid = 0x0006, atid = 0x8000,  name = "CLock",        },
    {tp = zcl.UINT8,       cid = 0x0006,atid = 0xF010,  name = "Volume",        },

    -- Level Control cluster
    {tp = zcl.UINT8,   cid = 0x0008, atid = 0x0000,  name = "Dimmer",                 },
    {tp = zcl.UINT16,  cid = 0x0008, atid = 0xF000,  name = "TYDimmer",                 },
    {tp = zcl.UINT16,  cid = 0x0008, atid = 0x0001,  name = "DimmerRemainingTime",    },
    {tp = zcl.UINT16,  cid = 0x0008, atid = 0x0010,  name = "OnOffTransitionTime",    },
    {tp = zcl.UINT8,   cid = 0x0008, atid = 0x0011,  name = "OnLevel",                },
    {tp = zcl.UINT16,  cid = 0x0008, atid = 0x0012,  name = "OnTransitionTime",       },
    {tp = zcl.UINT16,  cid = 0x0008, atid = 0x0013,  name = "OffTransitionTime",      },
    {tp = zcl.UINT8,   cid = 0x0008, atid = 0x4000,  name = "DimmerStartUpLevel",     },

    -- Time cluster
    {tp = zcl.UTC,     cid = 0x000A, atid = 0x0000,  name = "Time",                     },
    {tp = zcl.UINT32,  cid = 0x000A, atid = 0x0007,  name = "LocalTime",                },
    
    -- Window Covering cluster  0:open  1:stop  2:close
    {tp = zcl.ENUM8,   cid = 0x0102, atid = 0x0000,  name = "WindowCoveringType",    },
    {tp = zcl.ENUM8,   cid = 0x0102, atid = 0xF000,  name = "WindowState",    },
    {tp = zcl.UINT8,   cid = 0x0102, atid = 0x0008,  name = "CurrentPositionLiftPercentage", },
    {tp = zcl.UINT8,   cid = 0x0102, atid = 0x0009,  name = "CurrentPositionTiltPercentage", },
    {tp = zcl.ENUM8,   cid = 0x0102, atid = 0xF002,  name = "WindowMto",    },
    {tp = zcl.UINT16,   cid = 0x0102, atid = 0xF003, name = "WindowTbase",    },
    
    -- Color Control cluster
    {tp = zcl.UINT8,     cid = 0x0300, atid = 0x0000,  name = "Hue",                   },
    {tp = zcl.UINT8,     cid = 0x0300, atid = 0x0001,  name = "Sat",                   },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0x0002,  name = "RemainingTime",         },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0x0003,  name = "X",                     },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0x0004,  name = "Y",                     },
    {tp = zcl.ENUM8,     cid = 0x0300, atid = 0x0005,  name = "DriftCompensation",     },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0x0007,  name = "colorTemperatureMireds",     },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0xE000,  name = "tycolorTemperatureMireds",   },
    {tp = zcl.UINT16,    cid = 0x0300, atid = 0x4010,  name = "ColorStartUpColorTempireds", },
    {tp = zcl.ARRAY,     cid = 0x0300, atid = 0xF00E,  name = "TYOnOffTime",     },

    -- Illuminance Measurement cluster
    {tp = zcl.UINT16,  cid = 0x0400, atid = 0x0000,  name = "Illuminance",           }, 
    -- Temperature Measurement cluster
    {tp = zcl.INT16,   cid = 0x0402, atid = 0x0000,  name = "Temperature",           },
    -- Relative Humidity Measurement cluster
    {tp = zcl.UINT16,  cid = 0x0405, atid = 0x0000,  name = "Humidity",              },   -- Humidity 
    -- IAS Cluster (Intruder Alarm System)
    {tp = zcl.ENUM8,   cid = 0x0500, atid = 0x0000,  name = "ZoneState",              },    -- Occupancy (map8)
    {tp = zcl.ENUM16,  cid = 0x0500, atid = 0x0001,  name = "ZoneType",               },    -- Zone type for sensor
    {tp = zcl.BITMAP16,   cid = 0x0500, atid = 0x0002,  name = "ZoneStatus",          },    -- Zone status for sensor
    {tp = zcl.IEEE_ADDR,  cid = 0x0500, atid = 0x0010,  name = "IASCIEAddress",       },
    {tp = zcl.UINT8,      cid = 0x0500, atid = 0x0011,  name = "ZoneID",              },
    -- Metering (Smart Energy) cluster
    {tp = zcl.UINT48,     cid = 0x0702, atid = 0x0000,  name = "CurrentSummationDelivered", },
    -- Electrical Measurement cluster
    {tp = zcl.UINT16,  cid = 0x0B04, atid = 0x0505,  name = "RMSVoltage",             },
    {tp = zcl.UINT16,  cid = 0x0B04, atid = 0x0508,  name = "RMSCurrent",             },
    {tp = zcl.INT16,   cid = 0x0B04, atid = 0x050B,  name = "ActivePower",            },

    --0XE001 tuya config
    --conf_switch_flip  conf_switch_sync  conf_switch_button
    {tp = zcl.ENUM8,  cid = 0XE001, atid = 0xD030,  name = "SwitchType",              },
    --0XE001 tuya timer
    {tp = zcl.CHAR_STR,  cid = 0XE000, atid = 0xD003,  name = "PointTime",            },
}


local function zcl_tab_2_name(cid,attid,dtype)
    local tb = nil

    osal.array_foreach(attr_list,function(tab)
        if cid == tab.cid  and dtype == tab.tp and attid == tab.atid then
            tb = tab
        end
    end)
    return tb
end

local function zcl_name_2_tab(name)
    local tb = nil

    osal.array_foreach(attr_list,function(tab)
        if name == tab.name  then
            tb = tab
        end
    end)
    return tb
end



local function get_attr_val(tb,dtype,atrdata)

    local rp,code = nil,nil 

    if tb then
        if zcl.tplen[dtype] == nil then
            rp   =  tb.name
            code =  hex_to_str(atrdata)
        end

        if zcl.tplen[dtype] == 1 then
            rp   =  tb.name
            code = string.unpack("<B",atrdata)
        end

        if zcl.tplen[dtype] == 2 then
            rp   =  tb.name
            code = string.unpack("<H",atrdata)
        end

        if zcl.tplen[dtype] >= 4 then
            rp   =  tb.name
            code = string.unpack("<I",atrdata)
        end
    end

    return rp,code
end

local function report_val(clust,data)

    local rp,code = nil,nil 

    zcl.report_unpack(data,function(attrid,dtype,atrdata)
    
        local tb = zcl_tab_2_name(clust,attrid,dtype)

        rp,code = get_attr_val(tb,dtype,atrdata)
    end)
    return rp,code
end

function mqttapi.on_report(mac,ep,cluster,control,cmd,data)

    if control == 1 then
        if cluster == 0x0500 and cmd == 0 then
            local code = string.unpack("<H",data)

            bindtab.on_bind(mqttapi.cmd,hex_to_str(mac),ep,"Alarm",code)
            return {Alarm = code}
        end
        
        if cluster == 0x0501 and cmd == 2 then
            bindtab.on_bind(mqttapi.cmd,hex_to_str(mac),ep,"Alarm",1)
            return {Alarm = 1}
        end

        return nil
    end

    if cmd ~= zcl.REPORT then
        return nil
    end
    local name,code = report_val(cluster,data)

    if name then
        bindtab.on_bind(mqttapi.cmd,hex_to_str(mac),ep,name,code)
        
        return {[name] = code}
    end
    return nil
end

function mqttapi.info(ackfn,mac,ep,info)

    local tb = zcl_name_2_tab(info.name)

    --ackfn(control,cmd,data,rssi)
    local function fnreadcb(control,cmd,data,rssi)

        if not rssi then
            pcallx(ackfn,nil)
        end

        zcl.readack_unpack(data, function(attrid,status,dtype,atrdata)
            if status == 0 then
                pcallx(ackfn,get_attr_val(tb,dtype,atrdata))
                return
            end
            pcallx(ackfn,nil)
        end)
    end

    if not tb then
        return nil
    else
        return zcl.read(fnreadcb,mac,ep,tb.cid,{tb.atid});
    end
end

function mqttapi.cmd(ackfn,mac,ep,cmd)
    
    if cmd.addGroup then
        return zcl.cmd(ackfn,mac,ep,0x0004,0x00,string.pack("<H",cmd.addGroup))
    end

    if cmd.delGroup then
        return zcl.cmd(ackfn,mac,ep,0x0004,0x03,string.pack("<H",cmd.delGroup))
    end

    if cmd.Power then
        return zcl.cmd(ackfn,mac,ep,0x0006,cmd.Power,nil)
    end
    if cmd.IASWD and cmd.Blink and cmd.Duration then

        if cmd.IASWD > 15 then
            return false
        end

        if cmd.Blink >1 then
            return false
        end
        return zcl.cmd(ackfn,mac,ep,0x0502,0,string.pack("B <H B B",cmd.IASWD + cmd.Blink*16,cmd.Duration,0,0))
    end

    --5B6D335D
    if cmd.TTS_Girl then
        return zcl.exwrite(ackfn,mac,ep,0x0006,str_to_hex("5B6D335D" .. cmd.TTS_Girl))
    end

    if cmd.TTS_Boy then
        return zcl.exwrite(ackfn,mac,ep,0x0006,str_to_hex("5B6D35315D" .. cmd.TTS_Boy))
    end

    if cmd.TTS_Child then
        return zcl.exwrite(ackfn,mac,ep,0x0006,str_to_hex("5B6D35355D" .. cmd.TTS_Child))
    end

    if cmd.Volume then
        return zcl.write(ackfn,mac,ep,0x0006,0xF010,zcl.UINT8,string.pack("B",cmd.Volume))
    end

    if cmd.CLock then
        return zcl.write(ackfn,mac,ep,0x0006,0x8000,zcl.BOOLEAN,string.pack("B",cmd.CLock))
    end
    -- 0: off  1: on  0xff: memory
    if cmd.StartUpOnOff then
        return zcl.write(ackfn,mac,ep,0x0006,0x4003,zcl.ENUM8,string.pack("B",cmd.StartUpOnOff))
    end
    
    -- 0: off  1: on  2: memory
    if cmd.tyStartUpOnOff then
        return zcl.write(ackfn,mac,ep,0x0006,0x8002,zcl.ENUM8,string.pack("B",cmd.StartUpOnOff))
    end

    if cmd.OnOffTime then
        return zcl.cmd(ackfn,mac,ep,0x0006,0x42,string.pack("B <H <H",0,cmd.OnOffTime,cmd.OnOffTime))
    end

    if cmd.OffWaitTime then
        return zcl.cmd(ackfn,mac,ep,0x0006,0x42,string.pack("B <H <H",0,cmd.OffWaitTime,cmd.OffWaitTime))
    end
    --1 - 254
    if cmd.Dimmer then
        return zcl.cmd(ackfn,mac,ep,0x0008,0,string.pack("B <H",cmd.Dimmer,8))
    end

    -- 10-1000
    if cmd.TYDimmer then
        return zcl.cmd(ackfn,mac,ep,0x0008,0xf0,string.pack("<H",cmd.TYDimmer))
    end

    -- 153 -370
    if cmd.colorTemperatureMireds then
        return zcl.cmd(ackfn,mac,ep,0x0300,0x0A,string.pack("<H <H",cmd.colorTemperatureMireds,8))
    end

    -- 0 - 1000
    if cmd.tycolorTemperatureMireds then
        return zcl.cmd(ackfn,mac,ep,0x0300,0xE0,string.pack("<H",cmd.tycolorTemperatureMireds))
    end

    if cmd.tyontime and cmd.tyofftime then
        return zcl.cmd(ackfn,mac,ep,0x0300,0xFB,string.pack("B B >H B >H",0,0,cmd.tyontime*1000,0,cmd.tyofftime*1000))
    end

    if cmd.WindowCoveringType then
        return zcl.cmd(ackfn,mac,ep,0x0102,cmd.WindowCoveringType,nil)
    end
    --[[0-100]]
    if cmd.CurrentPositionLiftPercentage then
        return zcl.cmd(ackfn,mac,ep,0x0102,0x05,cmd.CurrentPositionLiftPercentage)
    end

    if cmd.WindowMto then
        return zcl.write(ackfn,mac,ep,0x0102,0xF002,zcl.ENUM8,string.pack("B",cmd.WindowMto))
    end

    if cmd.WindowTbase then
        return zcl.write(ackfn,mac,ep,0x0102,0xF003,zcl.UINT16,string.pack("<H",cmd.WindowTbase))
    end

    if cmd.SwitchType then
        return zcl.write(ackfn,mac,ep,0xE001,0xD030,zcl.ENUM8,string.pack("B",cmd.SwitchType))
    end

    --PointTime : 0-65535 S
    if cmd.PointTime then
        return zcl.cmd(ackfn,mac,ep,0xE000,0xfb,b64_to_str(string.pack("B >H",1,cmd.PointTime)))
    end


    return false
end

function mqttapi.group(gid,ep,cmd)

    if cmd.Power then
        zcl.group_cmd(gid,ep,0x0006,1,cmd.Power,nil)
    end

    if cmd.Dimmer then
        zcl.group_cmd(gid,ep,0x0008,1,0,string.pack("B <H",cmd.Dimmer,8))
    end

    if cmd.TYDimmer then
        zcl.group_cmd(gid,ep,0x0008,1,0xf0,string.pack("<H",cmd.TYDimmer))
    end

    if cmd.colorTemperatureMireds then
        zcl.group_cmd(gid,ep,0x0300,1,0x0A,string.pack("<H <H",cmd.colorTemperatureMireds,8))
    end

    if cmd.tycolorTemperatureMireds then
        zcl.group_cmd(gid,ep,0x0300,1,0xE0,string.pack("<H",cmd.colorTemperatureMireds))
    end

    if cmd.WindowCoveringType then
        zcl.group_cmd(gid,ep,0x0102,1,cmd.WindowCoveringType,nil)
    end
    --[[0-100]]
    if cmd.CurrentPositionLiftPercentage then
        zcl.group_cmd(gid,ep,0x0102,1,0x05,cmd.CurrentPositionLiftPercentage)
    end

    if cmd.SceneStore then
        zcl.group_cmd(gid,0xff,0x0005,0x04,string.pack("B",cmd.SceneStore))
    end

    if cmd.SceneCall then
        zcl.group_cmd(gid,0xff,0x0005,0x05,string.pack("B",cmd.SceneCall))
    end
    return true
end

function mqttapi.bdcmd(ep,cmd)

    if cmd.Power then
        zcl.broadcast_cmd(ep,0x0006,1,cmd.Power,nil)
    end

    if cmd.Dimmer then
        zcl.broadcast_cmd(ep,0x0008,1,0xf0,string.pack("<H",cmd.Dimmer))
    end

    if cmd.colorTemperatureMireds then
        zcl.broadcast_cmd(ep,0x0300,1,0xE0,string.pack("<H",cmd.colorTemperatureMireds))
    end

    if cmd.WindowCoveringType then
        zcl.broadcast_cmd(ep,0x0102,1,cmd.WindowCoveringType,nil)
    end
    --[[0-100]]
    if cmd.CurrentPositionLiftPercentage then
        zcl.broadcast_cmd(ep,0x0102,1,0x05,cmd.CurrentPositionLiftPercentage)
    end

    return true
end
--[[
    rpc api 
]]

rpc.register("ncp.reboot",function()

    zcl.sys_reboot()
    return true
end)

--ncp.allowjoin( enable, sec )
rpc.register( "ncp.allowjoin", function(en,t)

    if not is_bool(en) then
        error("en is bool")
    end
    
    if not en then
        zcl.unallow_join()
    else
        zcl.allow_join()
    end
    return true
 end)

 --ncp.reset()
 rpc.register( "ncp.reset", function()
    zcl.net_reset()
    return true
 end)

 --ncp.get_details()
 rpc.register( "ncp.remove", function(mac)

    if not is_string(mac) then
        error("mac is string")
    end

    zcl.del_dev(str_to_hex(mac))

    return true
 end)

 --ncp.get_device_list()
 rpc.register( "ncp.get_details", function()

    local mac,channel = zcl.net_info()

    return hex_to_str(mac),channel
 end)

 --ncp.get_device_list( mac = addr ,mfd = mfd,mid = mid)
 rpc.register( "ncp.get_device_list", function()
    
    local list = osal.array_pack(zcl.all_dev())

    if 0 == #list then
        return nil
    end

    local strlist = {}

    for i = 1, list.n ,1 do
        local mfd,mid = zcl.get_dev_info(list[i])
        table.insert(strlist,{mac = hex_to_str(list[i]),mfd = mfd,mid = mid})
    end

    return strlist
 end)

--------------------------------------------------------------------------------
-- zcl.get_device_details( addr )
rpc.register("zcl.get_device_details",function(addr)

    if not is_string(addr) then
        error("addr is string")
    end

    local mac = str_to_hex(addr)
    local list = zcl.get_eplist(mac)
    local info = {}

    for i = 1,#list,1 do
        local outlist = {}
        local intlist = {}
        -- print("date",list:byte(i))
        local mac,addr,ep,did,incid,outcid =  zcl.get_ep_info(mac,list:byte(i))

        while #incid ~=0 do
     
            pcallx(table.insert,intlist,(string.unpack("<H",incid)))
            incid = string.sub(incid,3)
        end

        while #outcid ~= 0 do
            pcallx(table.insert,outlist,(string.unpack("<H",outcid)))
            outcid = string.sub(outcid,3)
        end

        table.insert(info,{addr = addr,ep = ep,did = did,outcid = outlist,incid = intlist})
    end

    return info
end)

rpc.register("zcl.bind",function(addr,ep,cid)

    if not is_string(addr) then
        error("addr is string")
    end
    if not is_integer(ep) then
        error("ep is integer")
    end
    if not is_integer(cid) then
        error("cid is integer")
    end

    zcl.cbind(str_to_hex(addr),ep,cid)
    return true
end)

rpc.register("zcl.cmd",function(addr,ep,cmd)

    if not is_string(addr) then
        error("addr is string")
    end

    if not is_integer(ep) then
        error("ep is integer")
    end

    if not is_table(cmd) then
        error("cmd is table")
    end

    local syncfn = rpc.async_switch()

    local function __ackfn(control,cmd,data,rssi)

        if not is_function(syncfn) then
            return
        end
        if not rssi then
            syncfn(false)
            -- zcl.net_repair(str_to_hex(addr))
        end
        if rssi then
            syncfn(true,control,cmd,hex_to_str(data),rssi)
        end
    end

    local r = mqttapi.cmd(__ackfn,str_to_hex(addr),ep,cmd)

    if not r then
        error("mqttapi.cmd error")
    end
    return true
end)

--zcl.send(addr, ep, cluster, control, command, buffer )
rpc.register("zcl.send",function(addr,ep,cluster,control,command,buffer)

    if not is_string(addr) then
        error("addr is string")
    end

    if not is_integer(ep) then
        error("ep is integer")
    end
    
    if not is_integer(cluster) then
        error("cluster is integer")
    end

    if not is_integer(control) then
        error("control is integer")
    end

    if not is_integer(command) then
        error("command is integer")
    end

    local syncfn = rpc.async_switch()

    local function __ackfn(control,cmd,data,rssi)

        if not is_function(syncfn) then
            return
        end

        if not rssi then
            -- zcl.net_repair(str_to_hex(addr))
            syncfn(false)
        end

        if rssi then
            syncfn(true,control,cmd,hex_to_str(data),rssi)
        end
    end

    local r = zcl.send(__ackfn,str_to_hex(addr),ep,cluster,control,command,str_to_hex(buffer))

    if not r then
        error("zcl send error")
    end

    return true
end)

--zcl.multicast( addr, ep, cluster, control, command, buffer )
rpc.register("zcl.multicast",function(addr,ep,cluster,control,command,buffer)

    if not is_integer(addr) then
        error("addr is integer")
    end

    if not is_integer(ep) then
        error("ep is integer")
    end
    
    if not is_integer(cluster) then
        error("cluster is integer")
    end

    if not is_integer(control) then
        error("control is integer")
    end

    if not is_integer(command) then
        error("command is integer")
    end
    
    zcl.group_cmd(addr,ep,cluster,control,command,str_to_hex(buffer))
  
    return true
end)

--zcl.broadcast( ep, cluster, control, command, buffer )

rpc.register("zcl.broadcast",function(ep,cluster,control,command,buffer)

    zcl.broadcast(ep,cluster,control,0,command,str_to_hex(buffer))

    return true
end)

local ackfn_cb = nil

--zcl.subscribe( fnname )
rpc.register("zcl.subscribe",function(fnname)

    ackfn_cb = rpc.reflect(fnname)
   
    return true
end)

--zcl.unsubscribe( fnname )
rpc.register("zcl.unsubscribe",function(fnname)

    ackfn_cb = nil

    return true
end)

-- (mac,ep,rssi,cluster,control,cmd,data)
function mqttapi.report(...)

    if not is_function(ackfn_cb) then
        return
    end

    ackfn_cb(...)
end

function mqttapi.handle(...)

    rpc.scheduler(...)
end


return mqttapi
