local osal     = require "osal"
local aux_core = require "aux_core"
local zcl      = require "osal_zcl"
local mapi     = require("mqttapi")
local hmqtt    = require("ha_mqtt")
local bind     = require "bindtab"
local bndlock  = require "bndlock"
local lua_udp  = require("osal_udp")


local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray

local is_function = aux_core.IsFunction
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr
local to_json     = osal.json_encode
local to_table    = osal.json_decode

local topicapi = {}

local version = "1.1.1"


local function _rpc_report(gmac,data)
    hmqtt.report("/rpc/" .. gmac,data)
end

local function _gw_online()
    hmqtt.push_retain("tele/".. hex_to_str(zcl.net_info()) .."/LWT","Online")
end


local function _sensor_push(api,data)

    if not is_string(api) then

        error("sensor api error")
        return
    end

    hmqtt.report("tele/".. hex_to_str(zcl.net_info()) .. api,to_json(data))
end


function topicapi.devleft(mac)
    _sensor_push("/leave",{Device = hex_to_str(mac)})
    mapi.report("leave",{mac = hex_to_str(mac)})
end

function topicapi.devinfo(mac,mfd,mid)
    _sensor_push("/join",{Device = hex_to_str(mac),mfd = mfd,mid = mid})
    mapi.report("join",{mac = hex_to_str(mac),mfd = mfd,mid = mid})
end

function topicapi.devrejoin(mac,mfd,mid)
    _sensor_push("/rejoin",{Device = hex_to_str(mac),mfd = mfd,mid = mid})
    mapi.report("rejoin",{mac = hex_to_str(mac),mfd = mfd,mid = mid})
end

function topicapi.net_ok(mac,channel)
    _sensor_push("/ready",{Device = hex_to_str(mac),channel = channel})
    mapi.report("ready",{mac = hex_to_str(mac),channel = channel})
end


function topicapi.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

    local r = zcl.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

        -- time read 2000 - 2023
    if  control&1 == 0 and cmd == zcl.READ  and cluster  == 0x000a then
        zcl.readrsp(mac,ep,cluster,data .. string.pack("B B <I",0,zcl.UINT32,os.time()-725760000))
        return
    end
 
    if r then
        return
    end
    
    mapi.report("report",{mac = hex_to_str(mac),ep = ep,rssi = rssi,cluster = cluster,control = control&1,cmd = cmd,payload = hex_to_str(data)})

    local report = mapi.on_report(mac,ep,cluster,control,cmd,data)

    if not report then
        report,r = bndlock.on_report(mac,ep,cluster,control,cmd,data)
        if not report and r then
            return
        end
    end

    if report then
        _sensor_push("/report", {Device = hex_to_str(mac),Endpoint = ep,LinkQuality = rssi,report = report})
    else
        _sensor_push("/transmit",{Device = hex_to_str(mac),Endpoint = ep,LinkQuality = rssi,payload = {control = control&1,cluster = cluster,cmd = cmd,data = hex_to_str(data)}})
    end
end

local function _topic_ack(api,data)

    if not is_string(api) then

        error("sensor api error")
        return
    end

    hmqtt.report("stat/".. hex_to_str(zcl.net_info()) .. api,to_json(data))
end

local function _sub_topic_fn()

    local gmacstr    = hex_to_str(zcl.net_info())

    hmqtt.subscribe("/rpc/" .. gmacstr,function(data)

        local packet = to_table(data)

        if not is_table(packet) then
            return
        end

        if not is_table(packet.extend) then
            return
        end

        if not is_string(packet.extend.sender) then
            return
        end

        mapi.handle({sendfn = function(data)
            _rpc_report(packet.extend.sender,to_json(data))
        end,
        extend = {sender = gmacstr}},packet)
    end)

    --[[Id]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/GwInfo",function(data)

        local cmd = to_table(data)

        if not cmd then
            return 
        end

        local gmac ,channel  = zcl.net_info()
        _topic_ack("/GwInfoAck", {Id = cmd.Id,Device = hex_to_str(gmac),channel = channel,version = version})
    end)

    hmqtt.subscribe("zbeacon/discovery",function(data)
        _gw_online()
    end)

    --[[Id,ZbPermitJoin:1]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/ZbPermitJoin",function(data)

        local cmd = to_table(data)

        if not cmd then
            return 
        end
        if not cmd.ZbPermitJoin then

            return
        end
        
        if cmd.ZbPermitJoin == 0 then
            zcl.unallow_join()
        else
            zcl.allow_join()
        end

        _topic_ack("/ZbPermitJoinAck", {Id = cmd.Id,result = "ok"})
    end)

    --[[Id]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/ZbInfo",function(data)

        local cmd = to_table(data)

        if not cmd then
            print("data error")
            return 
        end
        if not ( is_nil( cmd.Id ) or is_integer( cmd.Id ) or is_string( cmd.Id ) ) then

            print("id is nil")
            return
        end

        local list = osal.array_pack(zcl.all_dev())

        if 0 == #list then
            return
        end
 
        for i = 1, list.n ,1 do
            local mfd,mid = zcl.get_dev_info(list[i])
            _topic_ack("/ZbInfoAck",{Id = cmd.Id,Device = hex_to_str(list[i]),mfd = mfd,mid = mid})
        end
    end)

    --[[Device,Id]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/ZbLeave",function(data)
        local cmd = to_table(data)

        if not cmd then
            return 
        end

        if not is_string(cmd.Device) then
            return
        end

        zcl.del_dev(str_to_hex(cmd.Device))

        _topic_ack("/ZbLeaveAck",{Id = cmd.Id,result = "ok"})
    end)

    --[[Id]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/Left",function(data)
        local cmd = to_table(data)

        if not cmd then
            return 
        end

        zcl.net_reset()
        _topic_ack("/LeftAck",{Id = cmd.Id,result = "ok"})
    end)

    --[[Device,Endpoint,Id,Cmd ={}]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/ZbSend",function(data)

        local order = to_table(data)

        if not is_string(order.Device) then
            return
        end
        if not is_integer(order.Endpoint) then
            return
        end

        if not is_table(order.Cmd) then
            return
        end
        
        -- ackfn(control,cmd,data,rssi)
        local function _ackfn(control,cmd,data,rssi)
            if rssi then
                _topic_ack("/ZbSendAck", {Id = order.Id,LinkQuality = rssi,result = "ok"})
            else
                _topic_ack("/ZbSendAck", {Id = order.Id,result = "timeout"})
            end
        end

        local r = mapi.cmd(_ackfn,str_to_hex(order.Device),order.Endpoint,order.Cmd)

        if not r then
            r = bndlock.unlock(_ackfn,str_to_hex(order.Device),order.Endpoint,order.Cmd)
        end

        if not r then
            _topic_ack("/ZbSendAck",{Id = order.Id,result = "Invalid"})
        end
    end)

    --[[Id]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/Bind",function(data)

        local cmd = to_table(data)

        
        if not is_integer(cmd.index) then
           
            return
        end

        if cmd.index >128 then
            _topic_ack("/BindAck", {Id = cmd.Id,result = "Invalid"})
        else
            _topic_ack("/BindAck", {Id = cmd.Id,result = "ok"})
        end
       
        if not is_string(cmd.input) then
            bind.appbind(cmd.index,nil,nil)
            return
        end

        if not is_string(cmd.output) then
            bind.appbind(cmd.index,nil,nil)
            return
        end

        bind.appbind(cmd.index,cmd.input,cmd.output)
    end)

    --[[Device,Endpoint,Id,Info ={name = "Power"}]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/Info",function(data)

        local order = to_table(data)

        if not is_string(order.Device) then
            return
        end

        if not is_integer(order.Endpoint) then
            return
        end

        if not is_table(order.Info) then
            return
        end
        if 500 > zcl.offset() then

            _topic_ack("/InfoAck",{Id = order.Id,result = "busy"})
            return
        end
      
        local function _ackfn(readdata)
            if readdata then
                _topic_ack("/InfoAck", {Id = order.Id,Device = order.Device,Endpoint = order.Endpoint,result = readdata})
            else
                _topic_ack("/InfoAck", {Id = order.Id,result = "timeout"})
            end
        end

        local r = mapi.info(_ackfn,str_to_hex(order.Device),order.Endpoint,order.Info)

        if not r then
            _topic_ack("/InfoAck",{Id = order.Id,result = "Invalid"})
        end
    end)

    --[[Group,Endpoint,Id,Cmd ={}]]

    hmqtt.subscribe("cmnd/".. gmacstr .."/Gpcmd",function(data)

        local cmd = to_table(data)

        if not is_integer(cmd.Group) then
            return
        end

        if not is_integer(cmd.Endpoint) then
            return
        end

        if not is_table(cmd.Cmd) then
            return 
        end
        -- str_to_hex(cmd.Cmd.data)
        mapi.group(cmd.Group,cmd.Endpoint,cmd.Cmd)

        _topic_ack("/GpcmdAck",{Id = cmd.Id,result = "ok"})
    end)
    
    --[[Id,Endpoint,Cmd ={}]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/Bdcmd",function(data)

        local cmd = to_table(data)

        if not is_integer(cmd.Endpoint) then
            return
        end

        if not is_table(cmd.Cmd) then
            return 
        end
        -- str_to_hex(cmd.Cmd.data)
        mapi.bdcmd(cmd.Endpoint,cmd.Cmd)

        _topic_ack("/BdcmdAck",{Id = cmd.Id,result = "ok"})
    end)

    --[[Id,Endpoint,Cmd ={}]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/CGchannel",function(data)

        local cmd = to_table(data)

        if not is_table(cmd.channel) then
            return 
        end
        
        zcl.setchannel(cmd.channel)
        _topic_ack("/CGchannelAck",{Id = cmd.Id,result = "ok"})
    end)

    --[[Device,Endpoint,Id,Cmd ={control,cluster,cmd,data}]]
    hmqtt.subscribe("cmnd/".. gmacstr .."/Cmd",function(data)

        local order = to_table(data)

        if not is_string(order.Device) then
            return
        end

        if not is_integer(order.Endpoint) then
            return
        end

        if not is_table(order.Cmd) then
            return 
        end

        -- ackfn(control,cmd,data,rssi)
        local function _ackfn(control,cmd,data,rssi)

            if rssi then
                _topic_ack("/CmdAck",{Id = order.Id,LinkQuality = rssi,ack = {control = control,cmd = cmd,data = hex_to_str(data)}})
            else
                _topic_ack("/CmdAck",{Id = order.Id,result = "timeout"})
            end
        end

        local r = zcl.send(_ackfn,str_to_hex(order.Device),order.Endpoint,order.Cmd.cluster,order.Cmd.control,order.Cmd.cmd,str_to_hex(order.Cmd.data))

        if not r then
            _topic_ack("/CmdAck",{Id = order.Id,result = "Invalid"})
        end
    end)
end

function topicapi.start(mac)

    hmqtt.start(function(flags,status)

        if status == 0  then

            print("mqtt connect ok!!!!")
            
            _sub_topic_fn()
            hmqtt.online()
            _gw_online()
        else
            print("mqtt connect error!!!!")
        end
    end,
    "tele/".. hex_to_str(zcl.net_info()) .."/LWT",
    "Offline", mac)
end

return topicapi

