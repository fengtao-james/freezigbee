
-- /usr/lib
--/etc/config/mqttcfg
package.path ="/usr/lib/lualib/?.lua"
-- package.path ="./lualib/?.lua"

local osal     = require "osal"

osal.include("/root/fzigbee")
-- osal.include("./fzigbee")

local aux_core = require("aux_core")
local zcl      = require("osal_zcl")
local tpapi    = require("topicapi")
local bind     = require "bindtab"

local pcallx      = osal.pcall

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local str_to_hex  = aux_core.StrToHex
local hex_to_str  = aux_core.HexToStr

-- zcl.net_reset()
-- zcl.ota_code("./1141-0201-10023607-bnd_m6_6_switch_test_v3.bin")

local function _on_ready(mac,channel)
   
    osal.DBG("gw mac:%s channel:%d",hex_to_str(mac),channel)
    
    pcallx(tpapi.net_ok,mac,channel)
    pcallx(tpapi.start,mac)
    
    osal.timecall(zcl.online,1000)
end

local ota_up = {}

local function _on_ota_end(mac,ep,st)
    osal.DBG("ota end mac:%s",hex_to_str(mac))
    ota_up[mac] = nil
end

local function _on_ota_update(mac,ep,max,now)
   
    if(ota_up[mac] == nil) then
        ota_up[mac] = now
        -- print("mac error ota update :",hex_to_str(mac))
        return 
    end

    local off = now-ota_up[mac]
    
    if off > 2000  then
        ota_up[mac] = now
        osal.INFO("OTA update:%s ------->[%d %%]",hex_to_str(mac),math.floor(100*now/max))
    end
end

local function _on_dev_info(mac,mfc,mode)
    osal.DBG("dev online mac :%s mfc: %s mode:%s",hex_to_str(mac),mfc,mode)
    tpapi.devinfo(mac,mfc,mode)
end

-- dev left
local function _on_dev_left(mac)
    osal.DBG("_on_dev_left :%s",hex_to_str(mac))
    tpapi.devleft(mac)
end

local function _on_dev_rejoin(mac,mfc,mode)
    osal.DBG("dev rejoin mac :%s mfc: %s mode:%s",hex_to_str(mac),mfc,mode)
    tpapi.devrejoin(mac,mfc,mode)
end

local function _on_online(mac,ep)
end

local function _on_next(mac,ep)
end

local function _on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
    tpapi.on_report(mac,ep,cluster,rssi,control&1,menu,sql,cmd,data)
end

local function _on_bind_info(index,input,output)
    osal.DBG("bind info :%d input: %s output:%s",index,input,output)

    bind.set_tab(index,input,output)
end

local list = {
    [zcl.dev_ready]   = _on_ready,
    [zcl.dev_online]  = _on_online,
    [zcl.dev_left]    = _on_dev_left,
    [zcl.dev_next_ep] = _on_next,
    [zcl.dev_ota_end] = _on_ota_end,
    [zcl.dev_info]    = _on_dev_info,
    [zcl.dev_rejoin]  = _on_dev_rejoin,
    [zcl.dev_ota_up]  = _on_ota_update,
    [zcl.bind_info]   = _on_bind_info,
    [zcl.data_report] = _on_report
}
-- // "/dev/ttyUSB0"
-- // "/dev/ttyS0"
osal.start(function()
    
    zcl.sys_start("/dev/ttyS0","/root/",115200)
    -- zcl.sys_start("/dev/ttyUSB0",".",115200)
    zcl.recv_thread(list)
    osal.timecall(zcl.sys_reboot,500)
end)
