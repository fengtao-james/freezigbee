local osal       = require("osal")
local zcl        = require("osal_zcl")
local aux_core   = require("aux_core")
local hamqtt     = require("ha_mqtt")
local UI         = require("ha_ui")
local onoff    = require("0006")
local level    = require("0008")
local color    = require("0300")
local group    = require("0004")

local light    = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local to_num      = tonumber
local to_str      = tostring


--[[
    get
        {
            switch : 0/1
            level  : 1-1000
            temp   : 1-1000
            scene  : 1-8
            ontime  : 3000 - 60000
            offtime : 3000 - 60000
        }
    ]]

--[[
    set
        {
            switch : 0/1
            switch : 0/1
            level  : 1-1000
            temp   : 1-1000
            scene  : 1-8
            delay  : 123
        }
    ]] 

function light.bind(mac,mfc,mode)

    local mac_str    = aux_core.HexToStr(mac)
    
    local topic_cfg,cw_info   = UI.cw_light(mac_str,mode)
    local scene_cfg,scene_ino = UI.scene(mac_str,mode)
    local on_cfg,on_info      = UI.onofftime(mac_str,mode)
    local add_cfg,add_info    = UI.group(mac_str,mode,"add")
    local del_cfg,del_info    = UI.group(mac_str,mode,"del")
    local rest_cfg,rest_info  = UI.reset(mac_str,mode)

    local ep  = string.unpack("B",zcl.get_eplist(mac))

    local ep_onoff   = onoff.new(mac,ep)
    local ep_level   = level.new(mac,ep)
    local ep_temp    = color.newtemp(mac,ep)
    
    local  function _report()

        local report = {
            state      = "ON",
            color_mode = "color_temp",
            color_temp = 0,
            brightness = 0,
            ontime     = 0,
            offtime    = 0
        }

        report.state      = UI.sw_to_str(ep_onoff.onoff)
        report.color_temp = UI.cw_ty_to_temp(ep_temp.tytemp)
        report.brightness = UI.cw_ty_to_level(ep_level.tylevel)
        report.ontime     = ep_temp.ontime
        report.offtime    = ep_temp.offtime

        hamqtt.report(cw_info.stat_t ,to_json(report))   
    end

    local  function _report_onoff()

        local report = {
            state      = "ON",
        }

        report.state = UI.sw_to_str(ep_onoff.onoff)

        hamqtt.report(cw_info.stat_t ,to_json(report))   
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
 
        -- osal.DBG("cw handle cluster:%x",cluster)
        ep_onoff:report(ep,control,cmd,cluster,data)
        ep_level:report(ep,control,cmd,cluster,data)
        ep_temp:report(ep,control,cmd,cluster,data)
        _report()
    end
    
    local function to_set_zb(data)

        local function _ackfn(control,cmd,data,rssi)

            if rssi == nil then
                print("set point error")
            end
        end

        print("onoff light data ",data)

        local set = to_table(data)

        if set.state == "ON"  then

            if ep_onoff.onoff == 0 then
                ep_onoff:set(_ackfn,1)
            end
        else
            ep_onoff:set(_ackfn,0)
            return
        end

        if set.color_temp then
            ep_temp:tyset(_ackfn,UI.cw_temp_to_ty(set.color_temp))
        end

        if set.brightness then
            ep_level:tyset(_ackfn,UI.cw_level_to_ty(set.brightness))
        end
    end


    local function to_on_time(data)

        local function _ackfn(control,cmd,data,rssi)
            if rssi == nil then
                print("set time error")
            end
        end
        print("on light data ",data)

        ep_temp:settime(_ackfn,to_num(data))
    end


    local function to_scene_cb(data)

        local function _ackfn(control,cmd,data,rssi)
            if rssi == nil then
                print("set time error")
            end
        end
        print("to_scene_cb data ",data)

        ep_temp:scene(_ackfn,data)
    end

    local function to_add_group(data)

        print("add light data ",data)
        group.add(mac,ep,to_num(data))
    end
    
    local function to_del_group(data)

        print("del light data ",data)
        group.remove(mac,ep,to_num(data))
    end

    local function _reset(data)

        print("reset light data ",data)

        if data == "restart" then
            zcl.del_dev(mac)
        end
    end

    print("light bind :",aux_core.HexToStr(mac))
    
    zcl.bind(mac,from_zb)

    local dev = {}
    
    hamqtt.regist(dev,topic_cfg,cw_info,to_set_zb)
    hamqtt.regist(dev,scene_cfg,scene_ino,to_scene_cb)
    hamqtt.regist(dev,on_cfg,on_info,to_on_time)
    hamqtt.regist(dev,add_cfg,add_info,to_add_group )
    hamqtt.regist(dev,del_cfg,del_info,to_del_group )
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)

end

return light









