local aux_core  = require ("aux_core")

local osal      = require("osal")
local zcl       = require("osal_zcl")
local hamqtt    = require("ha_mqtt")
local UI        = require("ha_ui")

local onoff     = require("0006")

local switch = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode

function switch.bind(mac,mfc,mode)

    local mac_str      = aux_core.HexToStr(mac)

    local topic_cfg1,switch_info1  = UI.switch(mac_str,mode,"sw1")
    local topic_cfg2,switch_info2  = UI.switch(mac_str,mode,"sw2")
    local rest_cfg,rest_info       = UI.reset(mac_str,mode)

    local e1,e2 = string.unpack("B B",zcl.get_eplist(mac))

    local ep1      = onoff.new(e1)
    local ep2      = onoff.new(e2)

    local  function _report1()
        hamqtt.report(switch_info1.stat_t ,{sw1 = UI.sw_to_str(ep1.onoff)})
    end

    local  function _report2()
        hamqtt.report(switch_info1.stat_t ,{sw2 = UI.sw_to_str(ep2.onoff)})
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

        if ep1:report(ep,control,cmd,cluster,data)  then
            
            _report1()
        end

        if ep2:report(ep,control,cmd,cluster,data) then
            
            _report2()
        end 
    end

    local function _onoff1()
        ep1:set(mac)
    end

    local function _onoff2()
        ep2:set(mac)
    end

    local function to_set_zb1(onoff)

        ep1:change(hamqtt.sw_to_hex(onoff))
        _report1()
        poll.push(_onoff1)
    end

    local function to_set_zb2(onoff)

        ep2:change(hamqtt.sw_to_hex(onoff))
        _report2()
        poll.push(_onoff2)
    end

    local function _reset(data)
        if data == "restart" then
            zcl.del_dev(mac)
        end
    end

    zcl.bind(mac,from_zb)

    local dev = {}

    hamqtt.regist(dev,topic_cfg1,switch_info1,to_set_zb1)
    hamqtt.regist(dev,topic_cfg2,switch_info2,to_set_zb2)
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)
end

return switch



