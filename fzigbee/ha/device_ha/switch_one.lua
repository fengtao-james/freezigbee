local onoff   = require("0006")
local osal    = require("osal")
local zcl     = require("osal_zcl")
local aux_core = require ("aux_core")
local hamqtt    = require("ha_mqtt")
local UI        = require("ha_ui")

local switch = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode

--[[
    get
        {
            switch : 0/1
            rssi:0
        }
    ]]

--[[
    set
        {
            switch : 0/1
            delay  : 123
        }
    ]] 

function switch.bind(mac,mfc,mode)

    local mac_str      = aux_core.HexToStr(mac)

    local topic_cfg1,switch_info1  = UI.switch(mac_str,mode,"sw1")
    local rest_cfg,rest_info       = UI.reset(mac_str,mode)
    local ep                       = onoff.new(string.unpack("B",zcl.get_eplist(mac)))

    local  function _report()
        hamqtt.report(switch_info1.stat_t ,to_json({sw1 = UI.sw_to_str(ep.onoff)}))
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
        ep:report(ep,control,cmd,cluster,data)
        _report()
    end

    local function _onoff()
        ep:set(mac)
    end
    
    local function to_set_zb(onoff)
        ep:change(hamqtt.sw_to_hex(onoff))
        _report()

        poll.push(_onoff)
    end

    local function _reset(data)
        if data == "restart" then
            zcl.del_dev(mac)
        end
    end
    zcl.bind(mac,from_zb)

    local dev = {}
    
    hamqtt.regist(dev,topic_cfg1,switch_info1,to_set_zb)
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)
end


return switch



