local osal       = require("osal")
local zcl        = require("osal_zcl")
local aux_core   = require("aux_core")
local hamqtt     = require("ha_mqtt")
local UI         = require("ha_ui")

local zone       = require("0500")
local battery    = require("0001")

local sensor    = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode


function sensor.bind(mac,mfc,mode)

    local mac_str      = aux_core.HexToStr(mac)

    local topic_cfg,door_info = UI.sensor_door(mac_str,mode)
    local vat_cfg,bat_power   = UI.battery(mac_str,mode)
    local rest_cfg,rest_info  = UI.reset(mac_str,mode)
    
    local ep       = string.unpack("B",zcl.get_eplist(mac))
    local ep_bat   = battery.new(ep)

    local function _ararm(code)

        if((status & 0x03) == 0) then
            
            hamqtt.report(door_info.stat_t ,to_json({state = "OFF"}))
        else
           
            hamqtt.report(door_info.stat_t ,to_json({state = "ON"}))
        end
    end

    local function _battery(void)
        hamqtt.report(bat_power.stat_t ,to_json({power = ep_bat.battery/2}))
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

        zone.change(function(status)
            _ararm(status)
                    end,cluster,control,cmd,data)

                    
        if cmd ~= zcl.REPORT then
            return
        end

        if(cluster == 0x0001) then    
            ep_bat:report(data)
            _battery()
        end
    end

    local function _reset(data)
        if data == "restart" then
            zcl.del_dev(mac)
        end
    end
    zcl.bind(mac,from_zb)

    local dev = {}
    hamqtt.regist(dev,topic_cfg,door_info)
    hamqtt.regist(dev,vat_cfg,bat_power)
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)
end

return sensor
