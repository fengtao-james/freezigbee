
local osal     = require("osal")
local zcl      = require("osal_zcl")
local aux_core = require ("aux_core")
local hamqtt   = require("ha_mqtt")
local UI       = require("ha_ui")

local onoff   = require("0006")
local swtype  = require("E001")
local ptime   = require("E000")
--[[
    E001
    0xD030
]]
local switch = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local to_num      = tonumber
local to_str      = tostring

function switch.bind(mac,mfc,mode)

    local mac_str      = aux_core.HexToStr(mac)

    local topic_cfg1,switch_info1  = UI.switch(mac_str,mfc,"sw1")
    local type_cfg ,type_info      = UI.select(mac_str,mfc,"sw-type",{"flip","sync","button"})
    local start_cfg ,start_info    = UI.start(mac_str,mfc,"start")
    local lock_cfg,lock_info       = UI.lock(mac_str,mfc,"lock")
    local point_cfg,point_info     = UI.point(mac_str,mode,"point")
    local rest_cfg,rest_info       = UI.reset(mac_str,mfc)

    local e1                          = string.unpack("B",zcl.get_eplist(mac))
    local ep_sw                       = onoff.new(e1)
    local ep_sw_type                  = swtype.new(e1)
    local ep_ptime                    = ptime.new(e1)

    local  function _report()
        hamqtt.report(switch_info1.stat_t ,to_json({sw1 = UI.sw_to_str(ep_sw.onoff),
                                                        start = UI.start_str(ep_sw.start),
                                                        lock  = UI.lock_to_str(ep_sw.lock),
                                                        point = ep_ptime.ptime}))
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

        ep_sw:report(ep,control,cmd,cluster,data)
        ep_ptime:report(ep,control,cmd,cluster,data)
        _report()
    end
    
    local function _onoff()
        ep_sw:set(mac)
    end

    local function to_set_zb(onoff)
        print("to zb onoff",onoff)

        ep_sw:change(UI.sw_to_hex(onoff))
  
        _report()
        poll.push(_onoff)
    end

    local function to_set_type(swtype)

        if swtype == "flip" then
        ep_sw_type:change(0)
        end

        if swtype == "sync" then
        ep_sw_type:change(1)
        end

        if swtype == "button" then
        ep_sw_type:change(2)
        end

        ep_sw_type:set(mac)
    end

    local function to_start_zb(onoff)


        if onoff == "on" then
        ep_sw:start_set(1)
        end

        if onoff == "off" then
        ep_sw:start_set(0)
        end

        if onoff == "memory" then
        ep_sw:start_set(2)
        end

        ep_sw:onoff_start(mac)
    end

    local function to_lock_zb(lock)
        ep_sw:lock_set(UI.lock_to_hex(lock))
        
        _report()
        ep_sw:lock_start(mac)
    end

    local function to_point_zb(ti)

       ep_ptime:change(to_num(ti))
       _report()
       ep_ptime:set(mac)
    end

    local function _reset(data)
        if data == "restart" then
            zcl.del_dev(mac)
        end
    end
    zcl.bind(mac,from_zb)

    local dev = {}
    
    hamqtt.regist(dev,topic_cfg1,switch_info1,to_set_zb)
    hamqtt.regist(dev,type_cfg ,type_info ,to_set_type)
    hamqtt.regist(dev,start_cfg ,start_info,to_start_zb)
    hamqtt.regist(dev,lock_cfg,lock_info,to_lock_zb)
    hamqtt.regist(dev,point_cfg,point_info,to_point_zb)
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)

end


return switch

