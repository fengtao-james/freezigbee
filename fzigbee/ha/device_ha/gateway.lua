local osal     = require("osal")
local zcl      = require("osal_zcl")
local aux_core = require("aux_core")
local hamqtt   = require("ha_mqtt")
local UI       = require("ha_ui")
local group    = require("0004")
local scene    = require("0005")

local pcallx      = osal.pcall

local to_num      = tonumber
local to_str      = tostring
local to_json     = osal.json_encode
local to_table    = osal.json_decode

local gw    = {}


gw.id       = 0
gw.ids      = nil
gw.ids_list = {}


local function _allhome(gname)

    local join_cfg ,join_info   = UI.permit_join(nil,gname,gname .. "/join","allhome")
    local rest_cfg,rest_info    = UI.reset(nil,gname,gname .. "/rest","allhome")

    gw.ids_list["allhome"] = join_info.dev.ids[1]

    local function _on_permit_join(data)
            
        print("allhome on permit",data)
    
        if(data == "join") then

            gw.id  = 0
            gw.ids = nil
            zcl.allow_join()
        end
    end

    local function to_reset(data)

        if data == "restart" then
            hamqtt.gunbind()
        end
    end

    ---------------------------------------------------------------------------------------------
    local dev = {}
    hamqtt.regist(dev,join_cfg,join_info,_on_permit_join)
    hamqtt.regist(dev,rest_cfg,rest_info,to_reset)
    hamqtt.gw(dev)
end


local function _keting()

    local join_cfg ,join_info       = UI.permit_join(gw.ids_list["allhome"],"客厅","rom1/join","rom1")
    local scene1_cfg ,scene1_info   = UI.scene(gw.ids_list["allhome"],"客厅","全开","rom1/act1","rom1","mdi:home-account")
    local scene2_cfg ,scene2_info   = UI.scene(gw.ids_list["allhome"],"客厅","全关","rom1/act2","rom1","mdi:home-alert-outline")
    local scene3_cfg ,scene3_info   = UI.scene(gw.ids_list["allhome"],"客厅","早起","rom1/act3","rom1","mdi:home-edit-outline")
    local scene4_cfg ,scene4_info   = UI.scene(gw.ids_list["allhome"],"客厅","晚安","rom1/act4","rom1","mdi:home-edit-outline")
    local scene5_cfg ,scene5_info   = UI.scene(gw.ids_list["allhome"],"客厅","观影","rom1/act5","rom1","mdi:home-edit-outline")
    local topic_cfg,cw_info         = UI.cw_light(gw.ids_list["allhome"],"客厅","rom1/light","rom1")

    gw.ids_list["keting"] = join_info.dev.ids[1]

    local function _on_permit_join(data)
            
        print("keting on permit",data)
    
        if(data == "join") then

            gw.id  = 1
            gw.ids = gw.ids_list["keting"]

            zcl.allow_join()
        end
    end

    local function to_scene1_cb(data)

        print("keting scene1_cb data ",data)

    end

    local function to_scene2_cb(data)

        print("keting scene2_cb data ",data)

    end

    local function to_scene3_cb(data)

        print("keting scene3_cb data ",data)

    end

    local function to_scene4_cb(data)

        print("keting scene4_cb data ",data)

    end

    local function to_scene5_cb(data)

        print("keting scene5_cb data ",data)

    end

    local function to_set_zb(data)

        print("keting light data ",data)
    end

    local dev = {}
    hamqtt.regist(dev,join_cfg,join_info,_on_permit_join)
    hamqtt.regist(dev,scene1_cfg,scene1_info,to_scene1_cb)
    hamqtt.regist(dev,scene2_cfg,scene2_info,to_scene2_cb)
    hamqtt.regist(dev,scene3_cfg,scene3_info,to_scene3_cb)
    hamqtt.regist(dev,scene4_cfg,scene4_info,to_scene4_cb)
    hamqtt.regist(dev,scene5_cfg,scene5_info,to_scene5_cb)
    hamqtt.regist(dev,topic_cfg,cw_info,to_set_zb)
    hamqtt.gbind(gw.ids_list["keting"],dev)
end


local function _woshi()

    local join_cfg ,join_info   = UI.permit_join(gw.ids_list["allhome"],"卧室","rom2/join","rom2")
    local scene1_cfg ,scene1_info   = UI.scene(gw.ids_list["allhome"],"卧室","全开","rom2/act1","rom2","mdi:home-account")
    local scene2_cfg ,scene2_info   = UI.scene(gw.ids_list["allhome"],"卧室","全关","rom2/act2","rom2","mdi:home-alert-outline")
    local scene3_cfg ,scene3_info   = UI.scene(gw.ids_list["allhome"],"卧室","早起","rom2/act3","rom2","mdi:home-edit-outline")
    local scene4_cfg ,scene4_info   = UI.scene(gw.ids_list["allhome"],"卧室","晚安","rom2/act4","rom2","mdi:home-edit-outline")
    local scene5_cfg ,scene5_info   = UI.scene(gw.ids_list["allhome"],"卧室","观影","rom2/act5","rom2","mdi:home-edit-outline")
    local topic_cfg,cw_info         = UI.cw_light(gw.ids_list["allhome"],"卧室","rom2/light","rom2")

    gw.ids_list["woshi"] = join_info.dev.ids[1]

    local function _on_permit_join(data)
            
        print("woshi on permit",data)
    
        if(data == "join") then

            gw.id  = 2
            gw.ids = gw.ids_list["woshi"]
            zcl.allow_join()
        end
    end
    local function to_scene1_cb(data)

        print("woshi scene1_cb data ",data)

    end

    local function to_scene2_cb(data)

        print("woshi scene2_cb data ",data)

    end

    local function to_scene3_cb(data)

        print("woshi scene3_cb data ",data)

    end

    local function to_scene4_cb(data)

        print("woshi scene4_cb data ",data)

    end

    local function to_scene5_cb(data)

        print("woshi scene5_cb data ",data)

    end

    local function to_set_zb(data)

        print("woshi light data ",data)
    end

    local dev = {}
    hamqtt.regist(dev,join_cfg,join_info,_on_permit_join)
    hamqtt.regist(dev,scene1_cfg,scene1_info,to_scene1_cb)
    hamqtt.regist(dev,scene2_cfg,scene2_info,to_scene2_cb)
    hamqtt.regist(dev,scene3_cfg,scene3_info,to_scene3_cb)
    hamqtt.regist(dev,scene4_cfg,scene4_info,to_scene4_cb)
    hamqtt.regist(dev,scene5_cfg,scene5_info,to_scene5_cb)
    hamqtt.regist(dev,topic_cfg,cw_info,to_set_zb)
    hamqtt.gbind(gw.ids_list["woshi"],dev)
end

local function _chufang()

    local join_cfg ,join_info       = UI.permit_join(gw.ids_list["allhome"],"厨房","rom3/join","rom3")
    local scene1_cfg ,scene1_info   = UI.scene(gw.ids_list["allhome"],"厨房","全开","rom3/act1","rom3","mdi:home-account")
    local scene2_cfg ,scene2_info   = UI.scene(gw.ids_list["allhome"],"厨房","全关","rom3/act2","rom3","mdi:home-alert-outline")
    local scene3_cfg ,scene3_info   = UI.scene(gw.ids_list["allhome"],"厨房","早起","rom3/act3","rom3","mdi:home-edit-outline")
    local scene4_cfg ,scene4_info   = UI.scene(gw.ids_list["allhome"],"厨房","晚安","rom3/act4","rom3","mdi:home-edit-outline")
    local scene5_cfg ,scene5_info   = UI.scene(gw.ids_list["allhome"],"厨房","观影","rom3/act5","rom3","mdi:home-edit-outline")
    local topic_cfg,cw_info         = UI.cw_light(gw.ids_list["allhome"],"厨房","rom3/light","rom3")

    gw.ids_list["chufang"] = join_info.dev.ids[1]

    local function _on_permit_join(data)
            
        print("chufang on permit",data)
    
        if(data == "join") then

            gw.id  = 3
            gw.ids = gw.ids_list["chufang"]
            zcl.allow_join()
        end
    end

    local function to_scene1_cb(data)

        print("chufang scene1_cb data ",data)

    end

    local function to_scene2_cb(data)

        print("chufang scene2_cb data ",data)

    end

    local function to_scene3_cb(data)

        print("chufang scene3_cb data ",data)

    end

    local function to_scene4_cb(data)

        print("chufang scene4_cb data ",data)

    end

    local function to_scene5_cb(data)

        print("chufang scene5_cb data ",data)

    end
    local function to_set_zb(data)

        print("chufang light data ",data)
    end

    local dev = {}
    hamqtt.regist(dev,join_cfg,join_info,_on_permit_join)
    hamqtt.regist(dev,scene1_cfg,scene1_info,to_scene1_cb)
    hamqtt.regist(dev,scene2_cfg,scene2_info,to_scene2_cb)
    hamqtt.regist(dev,scene3_cfg,scene3_info,to_scene3_cb)
    hamqtt.regist(dev,scene4_cfg,scene4_info,to_scene4_cb)
    hamqtt.regist(dev,scene5_cfg,scene5_info,to_scene5_cb)
    hamqtt.regist(dev,topic_cfg,cw_info,to_set_zb)

    hamqtt.gbind(gw.ids_list["chufang"],dev)
end


function gw.group(mac,ep)
    group.add(mac,ep,gw.id)
end

function gw.pname()
    return gw.ids
end

function gw.bind(mac,mfc,mode)

    local mac_str    = aux_core.HexToStr(mac)

    _allhome(mac_str)
    _keting()
    _woshi()
    _chufang()
end


return gw
