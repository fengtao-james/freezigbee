
local osal     = require("osal")
local zcl      = require("osal_zcl")
local aux_core = require("aux_core")
local hamqtt     = require("ha_mqtt")
local UI         = require("ha_ui")
local window     = require("0102")

        -- payload_open          = "open",
        -- payload_close         = "close",
        -- payload_stop          = "stoped",
        -- state_open            = "open",
        -- state_closed          = "closed",
        -- state_opening         = opening
        -- state_closing         = closing 
local cover    = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode

function cover.bind(mac,mfc,mode)

    local mac_str = aux_core.HexToStr(mac)

    local cover_cfg ,cover_info  = UI.cover(mac_str,mode,"cover")
    local set_cfg ,set_info      = UI.set(mac_str,mode,"Q_CA",1,180)
    local rest_cfg,rest_info     = UI.reset(mac_str,mode)

    local e1       = string.unpack("B",zcl.get_eplist(mac))
    local ep_wind  = window.new(e1)

    local  function _report()

        local report = {
            state      = "open",
            position   = 0
        }

        if ep_wind.percent == 0 then
            report.state  = cover_info.state_closed
        else 
            report.state  = cover_info.state_open
        end

        report.position   = ep_wind.percent

        hamqtt.report(cover_info.stat_t ,to_json(report))
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
    
        ep_wind:report(ep,control,cmd,cluster,data)
        _report()
        
    end
    
    local function to_state_zb(data)

        print("recv state ",data)

        if data == "OPEN" then
            ep_wind:change(window.UP_OPEN)
        end
        if data == "STOP" then
            ep_wind:change(window.STOP)
        end
        if data == "CLOSE" then
            ep_wind:change(window.DOWN_CLOSE)
        end

        _report()

        ep_wind:set(mac)
    end

    local function to_percent_zb(data)

        print("recv percent ",data)
        local set = to_table(data)

        ep_wind:lift(set.position)
        _report()

        ep_wind:start(mac)
    end
    

    local function reg_handle()

        hamqtt.subscribe(cover_info.command_topic,to_state_zb)
        hamqtt.subscribe(cover_info.set_position_topic,to_percent_zb)
    end

    local function unreg_handle()

        hamqtt.unsubscribe(cover_info.command_topic)
        hamqtt.unsubscribe(cover_info.set_position_topic)
    end

    local function _reset(data)

        if data == "restart" then
            zcl.del_dev(mac)
        end
    end

    local function _set(data)

        print("num set :",data)
    end

    print("window bind :",aux_core.HexToStr(mac))
    
    zcl.bind(mac,from_zb)
    
    local dev = {}

    hamqtt.regist(mac,cover_cfg,cover_info,nil,reg_handle,unreg_handle)
    hamqtt.regist(mac,set_cfg,set_info,_set)
    hamqtt.regist(mac,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)
end

return cover
