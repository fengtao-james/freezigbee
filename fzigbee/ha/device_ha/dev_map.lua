

local osal        = require("osal")
local zcl         = require("osal_zcl")
local aux_core    = require("aux_core")

local gateway     = require("gateway")
local switch1     = require("switch_one")
local switch2     = require("switch_two")
local switch3     = require("switch_three")
local switch4     = require("switch_four")
local plug        = require("plug")
local cw          = require("light_cw")
local cover       = require("cover")
local door        = require("sensor_door")
local swbox       = require("switch_box")

local pcallx      = osal.pcall

local map    = {
                {mfc = "_TZ3000_3xaagfmk",mode = "SM0001",bind  = switch3.bind},
                {mfc = "_TZ3000_skueekg3",mode = "TS0001",bind  = swbox.bind},
                {mfc = nil,mode = "TS011F",bind  = plug.bind},
                {mfc = nil,mode = "TS0001",bind  = switch1.bind},
                {mfc = nil,mode = "TS0002",bind  = switch2.bind},
                {mfc = nil,mode = "TS0003",bind  = switch3.bind},
                {mfc = nil,mode = "TS0004",bind  = switch4.bind},
                {mfc = nil,mode = "TS0502B",bind = cw.bind},
                {mfc = nil,mode = "TS130F", bind = cover.bind},
                {mfc = nil,mode = "TS0203", bind = door.bind},
                {mfc = nil,mode = "TS0000", bind = gateway.bind},
               }
local namespace = {}

function namespace.device_identify(mac,mfc,mode)

    for i=1, #map ,1 do
        local dev = map[i]

        if dev.mode == mode  then
            
            if dev.mfc == nil  then
                return dev.bind(mac,mfc,mode)
            end

            if dev.mfc == mfc then
                return dev.bind(mac,mfc,mode)
            end
        end
    end

    osal.DBG("identify error info mac %s %s %s",mac,mfc,mode)

    return nil
end


return namespace

