
local osal     = require("osal")
local zcl      = require("osal_zcl")
local aux_core = require("aux_core")
local hamqtt   = require("ha_mqtt")
local UI       = require("ha_ui")

local onoff   = require("0006")
local elec    = require("0b04")
local energy  = require("0702")
local ptime   = require("E000")

local plug    = {}

local to_json     = osal.json_encode
local to_table    = osal.json_decode
local to_num      = tonumber

--     {switch  = ep_onoff.nowonoff,
--     voltage = ep_elec.voltage,
--     current = ep_elec.current,
--     power   = ep_elec.power,
--     energy  = ep_energy.energy,
--     rssi    = rssi
-- }

function plug.bind(mac,mfc,mode)

    local mac_str = aux_core.HexToStr(mac)

    local sw_cfg,plug_info       = UI.switch(nil,mac_str,mode,"onoff")
    local power_cfg ,plug_power  = UI.sensor(nil,mac_str,mode,"power","power","W")
    local v_cfg  ,plug_v         = UI.sensor(nil,mac_str,mode,"voltage","voltage","V")
    local c_cfg  ,plug_c         = UI.sensor(nil,mac_str,mode,"current","current","A")
    local energy_cfg,plug_energy = UI.sensor(nil,mac_str,mode,"energy","energy","kw/h")
    local lock_cfg,lock_info     = UI.switch_lock(nil,mac_str,mode,"lock")
    local start_cfg ,start_info  = UI.power_start(nil,mac_str,mode,"start")
    local delay_cfg,delay_info   = UI.switch_point(nil,mac_str,mode,"delay")
    local rest_cfg,rest_info     = UI.reset(nil,mac_str,mode)

    local ep         = string.unpack("B",zcl.get_eplist(mac))
    local ep_onoff   = onoff.new(mac,ep)
    local ep_elec    = elec.new(mac,ep)
    local ep_energy  = energy.new(mac,ep)

    local  function _report()

        hamqtt.report(plug_info.stat_t ,to_json({power   = ep_elec.power,
                                                      voltage = ep_elec.voltage,
                                                      current = ep_elec.current/1000,
                                                      energy  = ep_energy.energy/100,
                                                      onoff   = UI.sw_to_str(ep_onoff.onoff),
                                                      lock    = UI.lock_to_str(ep_onoff.lock),
                                                      start   = UI.start_to_str(ep_onoff.start),
                                                      delay   = ep_onoff.onofftime}))
    end

    local function from_zb(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

        ep_onoff:report(ep,control,cmd,cluster,data)
        ep_elec:report(ep,control,cmd,cluster,data)
        ep_energy:report(ep,control,cmd,cluster,data)

        _report()
    end
    
    local function to_set_zb(onoff)

        local r = ep_onoff.onoff

        local function _ackfn(control,cmd,data,rssi)

            if rssi == nil then
                ep_onoff.onoff = r
            end
        end
        ep_onoff:set(_ackfn,UI.str_to_sw(onoff))
    end

    local function to_lock_zb(onoff)

        local lock = ep_onoff.lock

        local function _ackfn(control,cmd,data,rssi)

            if rssi == nil then
                ep_onoff.lock = lock
            end
        end

        ep_onoff:lock_set(_ackfn,UI.str_to_lock(onoff))
    end

    local function to_start_zb(onoff)

        local start = ep_onoff.start

        local function _ackfn(control,cmd,data,rssi)

            if rssi == nil then
                ep_onoff.start = start
            end
        end

        ep_onoff:start_set(_ackfn,UI.str_to_start(onoff))
    end

    local function to_delay_zb(ti)
        
        local function _ackfn(control,cmd,data,rssi)

            if rssi == nil then
                print("set point error")
            end
        end
        ep_onoff:delay(_ackfn,ti)
    end

    local function _reset(data)

        if data == "restart" then
            zcl.del_dev(mac)
        end
    end


    print("plug bind :",aux_core.HexToStr(mac))
    
    zcl.bind(mac,from_zb)
    local dev = {}

    hamqtt.regist(dev,sw_cfg,plug_info,to_set_zb)
    hamqtt.regist(dev,power_cfg,plug_power)
    hamqtt.regist(dev,v_cfg,plug_v)
    hamqtt.regist(dev,c_cfg,plug_c)
    hamqtt.regist(dev,energy_cfg,plug_energy)
    hamqtt.regist(dev,lock_cfg,lock_info,to_lock_zb)
    hamqtt.regist(dev,start_cfg ,start_info,to_start_zb)
    hamqtt.regist(dev,delay_cfg,delay_info,to_delay_zb)
    hamqtt.regist(dev,rest_cfg,rest_info,_reset)

    hamqtt.bind(mac,dev)
end

return plug



