local aux_core = require("aux_core")
local zcl      = require("osal_zcl")
local map      = require("dev_map")
local hamqtt   = require("ha_mqtt")

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local ctx = {}


function ctx.gwstart(mac)
    map.device_identify(mac,nil,"TS0000")
end

function ctx.devstart(mac,mfc,mode)
    map.device_identify(mac,mfc,mode)
end

function ctx.devleft(mac)
    hamqtt.unbind(mac)
    zcl.unbind(mac)
end


return ctx
