/**
 * @file main.c
 * @brief osal运行例程入口
 * @version 0.1
 * @date 2019-07-25
 * @author WatWu
 */
#include "task_event.h"
#include "lua_task.h"
#include "ncp_api.h"
#include "zcl_api.h"
#include "host_main.h"
#include "lua_zigbeepush.h"
#include "mbedtls/aes.h"



static void _ready(void* args)
{
    ncp_info_t info = {0};
    ncp_get_netinfo(&info);

    lua_dev_ready(info.mac, info.Channel);

    ncp_getjoineddev();
}

static void _close(void* args)
{
    OSAL_DBG("zigbee close");
    ncp_creat();
}

static void _online(void* args, const uint8_t* mac, uint8_t ep)
{
    // OSAL_DBG("zigbee dev online");
    lua_dev_online(mac, ep);
}

static void _next_ep(void* args, const uint8_t* mac, uint8_t ep)
{
    // OSAL_DBG("next_ep ============= ep :%d", ep);
    lua_dev_next_ep(mac, ep);
}

static void _leave(void* args, const uint8_t* mac)
{
    lua_dev_left(mac);
}

static void _rejoin(void* args, const uint8_t* mac, const uint8_t* mfc,
                    uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    lua_dev_rejoin(mac, mfc, mlen, mode, len);
}

static void _recv(void* args,
                  const uint8_t* mac,
                  uint8_t ep,
                  uint16_t clusterId,//clusterId
                  uint8_t rssi,
                  const zcldata_t* cmd,
                  const void* data,
                  uint8_t len)
{
    lua_zcl_report(mac, ep, clusterId, rssi, cmd->control, cmd->manucode,
                   cmd->sql, cmd->cmd, data, len);
}

static void _bind(void* args, const uint8_t* mac, uint8_t status)
{

}

static void _ota_st(void* args, const uint8_t* mac, uint8_t ep, uint8_t status)
{
    //  OSAL_DBG("ota st %d", index, zcl_ota_block(index, len));
    lua_ota_end(mac, ep, status);
}

static void _ota_block(void* args, uint32_t index, uint8_t len)
{
    OSAL_DBG("index code :%u::%d", index, zcl_ota_block(index, len));
}


static void _ota_update(void* args, const uint8_t* mac, uint8_t ep, uint32_t max, uint32_t now)
{
    lua_ota_update(mac, ep, max, now);
}

static void _dev_info(void* args, const uint8_t* mac, const uint8_t* mfc,
                      uint8_t mlen, const uint8_t* mode, uint8_t len)
{
    lua_dev_info(mac, mfc, mlen, mode, len);
}

static void _bind_info(void* args, uint8_t index, const uint8_t* input, uint16_t inlen, const uint8_t* output, uint8_t outlen)
{
    lua_bind_info(index, input, inlen, output, outlen);
}

static zcl_cb_t zclcb = {
    .zcl_ready  = _ready,
    .zcl_close  = _close,
    .dev_online = _online,
    .next_ep    = _next_ep,
    .dev_rejoin = _rejoin,
    .dev_leave  = _leave,
    .zcl_recv   = _recv,
    .zdo_bind   = _bind,
    .ota_st     = _ota_st,
    .ota_block  = _ota_block,
    .ota_update = _ota_update,
    .dev_info   = _dev_info,
    .bind_info  = _bind_info,
};

int ncp_uart_start(const char* dev, const char* dev_path, int bps)
{
    return host_start(&zclcb, dev, dev_path, bps);
}

// char* argv[] = {"11","-I","lualib/","app/mqtt_test.lua"};
// int argc     = 4;

int main(int argc, char* argv[])
{
    osal_os_init();
    osal_ep_init();

    if(lua_task_event_init(argc, argv)) {
        return -1;
    }

    while(1) {
        host_poll();
    }

    return 0;
}



