
local lcoap={}

--[[
 coap = {
	 V =1,
	 T = CON,
	 C = GET,
	 ID   = 123.
	 token = 123456,
	 
	 opt = {
		 [11] = "helloword"
	 },
	 payload = "0123456789"
 }
]]

lcoap.VERSION = 1
--/* CoAP message types */
lcoap.CON =   0 --/* confirmable message (requires ACK/RST) */
lcoap.NON =   1 --/* non-confirmable message (one-shot message) */
lcoap.ACK =   2 --/* used to acknowledge confirmable messages */
lcoap.RST =   3 --/* indicates error in received messages */

--/* CoAP request methods */

lcoap.GET    =   1
lcoap.POST   =   2
lcoap.PUT    =   3
lcoap.DELETE =   4

--/* CoAP option types (be sure to update check_critical when adding options */
lcoap.OP_IF_MATCH        = 1  --/* C, opaque, 0-8 B, (none) */
lcoap.OP_URI_HOST        = 3  --/* C, String, 1-255 B, destination address */
lcoap.OP_ETAG            = 4  --/* E, opaque, 1-8 B, (none) */
lcoap.OP_IF_NONE_MATCH   = 5  --/* empty, 0 B, (none) */
lcoap.OP_URI_PORT        = 7  --/* C, uint, 0-2 B, destination port */
lcoap.OP_LOCATION_PATH   = 8  --/* E, String, 0-255 B, - */
lcoap.OP_URI_PATH        =11  --/* C, String, 0-255 B, (none) */
lcoap.OP_CONTENT_FORMAT  =12  --/* E, uint, 0-2 B, (none) */
lcoap.OP_CONTENT_TYPE    =12  --/*
lcoap.OP_MAXAGE          =14  --/* E, uint, 0--4 B, 60 Seconds */
lcoap.OP_URI_QUERY       =15  --/* C, String, 1-255 B, (none) */
lcoap.OP_ACCEPT          =17  --/* C, uint,   0-2 B, (none) */
lcoap.OP_LOCATION_QUERY  =20  --/* E, String,   0-255 B, (none) */
lcoap.OP_PROXY_URI       =35  --/* C, String, 1-1034 B, (none) */
lcoap.OP_PROXY_SCHEME    =39  --/* C, String, 1-255 B, (none) */
lcoap.OP_SIZE1           =60  --/* E, uint, 0-4 B, (none) */
lcoap.OP_SIZE2           =28  --/* E, uint, 0-4 B, (none) */
lcoap.OP_OBSERVE         =6   --/* E, empty/uint, 0 B/0-3 B, (none) */
lcoap.OP_BLOCK2          =23  --/* C, uint, 0--3 B, (none) */
lcoap.OP_BLOCK1          =27  --/* C, uint, 0--3 B, (none) */


lcoap.code_200      = 200  --/* 2.00 OK */
lcoap.code_201      = 201  --/* 2.01 Created */
lcoap.code_202      = 202  --/* 2.03 Valid */
lcoap.code_203      = 203  --/* 2.03 Valid */
lcoap.code_204      = 204  --/* 2.04 change */
lcoap.code_205      = 205  --/* 2.05 change */
lcoap.code_231      = 231  --/* 2.31 continue */
lcoap.code_400      = 400  --/* 4.00 Bad Request */
lcoap.code_404      = 404  --/* 4.04 Not Found */
lcoap.code_405      = 405  --/* 4.05 Method Not Allowed */
lcoap.code_413      = 413  --/* 4.05 Method Not Allowed */
lcoap.code_415      = 415  --/* 4.15 Unsupported Media Type */
lcoap.code_500      = 500  --/* 5.00 Internal Server Error */
lcoap.code_501      = 501  --/* 5.01 Not Implemented */
lcoap.code_503      = 503  --/* 5.03 Service Unavailable */
lcoap.code_504      = 504  --/* 5.04 Gateway Timeout */
lcoap.code_702      = 702  --/* 702 TCP ping */
lcoap.code_703      = 703  --/* 7.03 TCP pong */

lcoap.TNUMBER		=3
lcoap.TSTRING		=4

lcoap.EVENT_ACK      =1
lcoap.EVENT_CHANGE   =2
lcoap.EVENT_RESET    =3
lcoap.EVENT_ERROR    =4

local proto={
    [lcoap.OP_URI_HOST]    =lcoap.TSTRING,
    [lcoap.OP_URI_PORT]    =lcoap.TNUMBER,
    [lcoap.OP_URI_PATH]    =lcoap.TSTRING,
    [lcoap.OP_CONTENT_TYPE]=lcoap.TNUMBER,
    [lcoap.OP_OBSERVE]     =lcoap.TNUMBER,
    [lcoap.OP_SIZE1]       =lcoap.TNUMBER,
    [lcoap.OP_SIZE2]       =lcoap.TNUMBER,
}

--update option order 
local function pairs_by_keys(t)
	local arry = {}

	for n in pairs(t) 
	do 
		arry[#arry + 1] = n 
	end

	table.sort(arry)
	local i = 0
	
    return function ()
        i = i + 1
        return arry[i], t[arry[i]]
	end
end

local function coap_option_decode(value)

	if(#value < 1)
	then
		return 0
	end

	local clen = 1
	local k = (value:byte(1) & 0xf0)>>4
	local vlen = value:byte(1) & 0x0f

	if(k == 15)
	then
		return 0
	elseif(k == 14)
	then
		k = string.unpack(">H",value,clen+1)  +269
		clen = clen+2

	elseif(k == 13)
	then
		k = k + string.unpack(">B",value,clen+1)
		clen = clen+1
	end
	
	if(vlen == 15) then
		return 0

	elseif(vlen == 14) then
		vlen = string.unpack(">H",value,clen+1)  +269
		clen = clen+2
	elseif(vlen == 13) then
		vlen = vlen + string.unpack(">B",value,clen+1)
		clen = clen+1
	end

	if((vlen+clen)>#value) then
		return 0
	end

	return (vlen+clen),k,value:sub(clen+1,vlen+clen)
end

local function coap_option_encode(idtle,optid,data)
	local opt = {}
	local k    = optid - idtle
	local vlen = #data
	local h = 0

	local k_str = nil
	local vlen_str = nil

	if(k < 13)
	then
		h = k << 4
	elseif(k < 270)
	then
		h = 13 << 4
		k_str = string.pack(">B",k -13)
	else
		h = 14 << 4
		k_str = string.pack(">H",k -269)
	end

	if(vlen < 13)
	then
		h = h | vlen
	elseif(vlen < 270)
	then
		h = h | 13
		vlen_str = string.pack(">B",k -13)
	else
		h = h | 14
		vlen_str = string.pack(">B",k -13)
	end

	table.insert(opt,string.pack(">B",h))
	table.insert(opt,k_str)
	table.insert(opt,vlen_str)
	table.insert(opt,data)

	return table.concat(opt)
end

--[[V = ,T= ,C= ,ID = ,token = ,opt[1] = "",payload = ""]]
function lcoap.decode( value )
	local coap = {}

	local tlen   = (value:byte(1)) & 0x0f

	coap.V	  = (value:byte(1) >>6) & 3
	coap.T    = (value:byte(1) >>4) & 3

	coap.C    =  value:byte(2)
	coap.ID   =  string.unpack(">H",value,3)

	if tlen > 0 then
		coap.token   =  value:sub(5,4+tlen)
	end
	coap.opt = {}

    if #value > 4+tlen then
        
		local data   = value:sub(5+tlen)
		local i      = 1
		local opt_id = 0
--		print("data",data,#data)
	
		while((i<#data) and (data:byte(i) ~= 0xff)) do
			
			local len,id,v = coap_option_decode(data:sub(i))

			opt_id = opt_id + id

			if(len ~= 0)
			then
				coap.opt[opt_id] = v
			end
			i = i+len
		end

		print(data)

		if((data:byte(i) == 0xff) and (i<#data))
		then
			coap.payload = data:sub(i+2)
		end
		
	end

	return coap
end


function lcoap.encode( coap )
	local pdu = {}
	local head = (coap.V & 3)<<6

	head = head | ((coap.T & 3)<<4)
	head = head | (#coap.token)

	table.insert(pdu,string.pack(">B",head))
	table.insert(pdu,string.pack(">B",coap.C))
	table.insert(pdu,string.pack(">H",coap.ID))
	table.insert(pdu,coap.token) --string

    if coap.opt then
        
		local idtle = 0

		for k,v in pairs_by_keys(coap.opt) do
			table.insert(pdu,coap_option_encode(idtle,k,v))
			idtle = k
		end
	end

	if coap.payload then
		table.insert(pdu,string.pack(">B",0xff))
		table.insert(pdu,coap.payload)
	end

	return table.concat(pdu)
end

return lcoap
