local c 		=  osal_core
local profile   =  profile
local tostring  =  tostring
local coroutine =  coroutine
local assert    =  assert
local pairs     =  pairs
local pcall     =  pcall
local table     =  table
local JSON      =  cjson

local osal		 = {}
local fork_queue = {}      -- work task queue

local session_id_coroutine  = {} -- switch task queue

local coroutine_session_num = 1

local to_num      = tonumber
local to_str      = tostring
local random_cnt  = 0

local function debug_table()
	print("------------------co::",#session_id_coroutine)
	print("------------------pool::",#coroutine_pool)
	print("------------------fork_queue::",#fork_queue)
end

function Is_Nil( value )
	return "nil" == type( value )
end

local function _pcall(r,...)
	assert(r, (...))
	return ...
end

function osal.pcall(func, ...)
	return _pcall(xpcall(func, debug.traceback, ...))
end

function osal.random(sc,ec)

    local n = math.random(sc,ec)

    while n == random_cnt do
        n = math.random(sc,ec)
    end

    random_cnt = n

    return random_cnt
end
------------------------------------------------------------------------
--[[
	call dispatch message to call
]]--
local function raw_dispatch_message(session, ...)

	local co = session_id_coroutine[session]  -- session ---> running coroutine
	
	if co then
		if 0 == co.v then
			session_id_coroutine[session] = nil -- clear session_id_coroutine 
		end
		osal.pcall(co.f, ...)
	end
end

------------------------------------------------------------------------------
osal.pstart = assert(profile.start)
------------------------------------------------------------------------------

function osal.WRN(fmt,...)
	
	c.log("WARNING",string.format(fmt,...))
end
function osal.INFO(fmt,...)
	c.log("INFO",string.format(fmt,...))
end
function osal.DBG(fmt,...)
	c.log("DEBUG",string.format(fmt,...))
end
function osal.ERR(fmt,...)
	c.log("ERROR",string.format(fmt,...))
end

function osal.session()
	local co = session_id_coroutine[coroutine_session_num]
		  
	while co ~= nil do
		coroutine_session_num = coroutine_session_num+1
		co = session_id_coroutine[coroutine_session_num]
	end

	local n  = coroutine_session_num
	coroutine_session_num = coroutine_session_num+1

	return  n
end
-----------------------------------------------------------------------
function osal.include(path)
	package.path = package.path .. ";" .. path .."/?.lua"
end


function osal.tick()
	return c.tick()
end

-----------------------------------------------------------------------
--[[
	flag:0  once
	     1  long
]]
function osal.wait(session,func,flag)
	assert(session_id_coroutine[session] == nil)
	session_id_coroutine[session] = {f = func,v = flag}
end


function osal.awake(session)
	session_id_coroutine[session] = nil
end

-----------------------------------------------------------------------
--[[
	it is time for task to do
]]--
function osal.timecall(ackfn,ti)
	if ti == nil  then
		return 0
	end

	local session = osal.session()
	local timer   = c.timeout(session,ti)

	assert(timer)
	
	osal.wait(session,ackfn,0)

	return session	-- for debug
end

function osal.fork(ackfn,ti)

	local session = osal.session()

	local function _cycle()
		local r = osal.pcall(ackfn)
		if r == nil then
			osal.awake(session)
			return
		end
		
		if r < 0 then
			osal.awake(session)
			return
		end
		c.timeout(session,ti)
	end

	osal.wait(session,_cycle,1)
	c.timeout(session,ti)
end

function osal.sessioncall(session,ackfn,ti)
	
	if ti == nil  then
		return
	end

	local timer = c.timeout(session,ti)
	assert(session)
	assert(timer)
	
	osal.wait(session,ackfn,0)
end

-----dispatch_message
function osal.dispatch(...)
	osal.pcall(raw_dispatch_message, ...)
end

function osal.start(start_func)  -- start lua core
	c.bindcb(osal.dispatch) -- poll fork queue

	osal.timecall(function()
		osal.pcall(start_func)
		collectgarbage("collect")  --clear all mem
	end,0)
end


function osal.load(name)
	return loadfile(name .. ".lua")
end

-- for each tab list
function osal.table_foreach(tab,func)
	for k,v in  pairs(tab) do
		func(k,v)
	end
end

-- for each array list
function osal.array_foreach(array,func)
	for i=1, #array ,1 do
		osal.pcall(func,array[i])
	end
end

function osal.tab_to_hex(fmt,tab)
	local strtab = {}

	osal.array_foreach(tab,function(v)
		table.insert(strtab,string.pack(fmt,v))	
	end)
	return table.concat(strtab)
end


function osal.json_encode(tab)

	return JSON.encode(tab)
end

function osal.json_decode(str)

	return JSON.decode(str)
end

function osal.b64_encode(hex)
	return c.b64_str(hex)
end

function osal.b64_decode(str)
	return c.b64_hex(str)
end

function osal.array_pack(...)
	
	local r = table.pack( ... )

	for i = 1, r.n ,1 do
		if Is_Nil( r[ i ] ) then
			r[ i ] = null
		end
	end
	return r
end

return osal