local osal        = require("osal")
local aux_core    = require("aux_core" )
local modbus      = modbus

local bus = {}

bus.read_onoff   = 1
bus.read_gonoff  = 2
bus.read_acdata  = 3
bus.read_input   = 4
bus.set_onoff    = 5
bus.set_acdata   = 6
bus.set_gonoff   = 15

function bus.pack(addr,cmd,data)

    local str = ""
   -- print("-----msg pack",aux_core.HexToStr(data),addr,cmd)

    str = str .. string.pack("B B B",addr,cmd,#data)

  
    str = str .. data

    local crc = modbus.crc16(str)

    return str .. string.pack(">H",crc)
end
---------------------------------------------------------
--[[
    8  4   2  1
]]
function bus.ack_onoff(addr,tab_onoff)

    return bus.pack(addr,bus.read_onoff,osal.tab_to_hex(">H",tab_onoff))
end

--[[
    {0x0001,0x0002,0x0003,0x0004}
]]
function bus.ack_acdata(addr,acdata)

    return bus.pack(addr,bus.read_acdata,osal.tab_to_hex(">H",acdata))
end

--[[
    {0x0001,0x0002,0x0003,0x0004}
]]
function bus.ack_input(addr,tab_input)

    return bus.pack(addr,bus.read_input,osal.tab_to_hex(">H",tab_input))
end

local bussrv = {}
bussrv.__index = bussrv

local function _bus_srv_unpack(func,data)

    local dlen  = #data
    local crc   = string.unpack(">H",string.sub(data,dlen-1))
    local crc_r = modbus.crc16(string.sub(data,1,dlen-2))

    --osal.DBG("crc::%x  %x %d",crc,crc_r,dlen)
    if(crc_r ~= crc) then
        return false
    end

    local addr = data:byte(1)
    local cmd  = data:byte(2)

    osal.pcall(func,addr,cmd,string.sub(data,3,dlen-2))

    return true
end

function bussrv:handle(data)

    local cblist = {
        [bus.read_onoff]  = function(addr,payload)
        
            local star,num = string.unpack(">H >H",payload)
            osal.pcall(self.read_onoff,addr,star,num) 
        end,
        [bus.set_onoff]   = function(addr,payload)

            local id,onoff = string.unpack(">H >H",payload)
            osal.pcall(self.set_onoff,data,addr,id,onoff) 
        end,
        [bus.read_acdata] = function(addr,payload)

            local star,num = string.unpack(">H >H",payload)
            osal.pcall(self.read_acdata,addr,star,num) 
        end
    }
    local function _srccall(addr,cmd,data)
        --print("-----msg recv",aux_core.HexToStr(data))

        local cbcall = cblist[cmd]

        self.cmd = cmd

        if(cbcall ~= nil) then
            osal.pcall(cbcall,addr,data) 
        else
            print("cmd is not set...............",cmd)   
        end
    end

    osal.pcall(_bus_srv_unpack,_srccall,data)
end

function bus.new() 
   
    local Object = {}

    setmetatable( Object, bussrv )
    return Object
end
return bus