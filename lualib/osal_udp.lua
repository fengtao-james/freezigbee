local osal        = require( "osal" )
local udp         = udp
local mod = {}

local pcallx      = osal.pcall

local cli = {}
cli.__index = cli

function cli:send(data)
    return udp.uclisend(self.ctx,data)
end

function cli:bind(recvfunc)

    local function _handle(type,...)
        local args = {...}
       
        pcallx(recvfunc,table.unpack(args))
    end
    osal.wait(0xF6768942,_handle,1)

    -- udp.uclibind(self.ctx,recvfunc)
end

function mod.cli_new(ip)
    local Object = {}
    Object.ctx   = udp.ucliopen(ip)

    if(Object.ctx == nil) then
        return nil
    end

    setmetatable( Object, cli )
    
	return Object
end

function mod.cli_free(object)

    udp.uclose(object.ctx)
    object = nil
end

local srv = {}
srv.__index = srv

function srv:send(uri,data)
    return udp.usrvsend(self.ctx,uri,data)
end

function srv:bind(recvfunc)

    local function _handle(type,...)
        local args = {...}
       
        pcallx(recvfunc,table.unpack(args))
    end

    osal.wait(0xF6768942,_handle,1)
end

function mod.srv_new(port)
    local Object = {}
    Object.ctx = udp.usrvopen(port)

    if(Object.ctx == nil) then
        return nil
    end

    setmetatable( Object, srv )

	return Object
end

function mod.srv_free(object)

    udp.uclose(object.ctx)
end

return mod

