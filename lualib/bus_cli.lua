local osal        = require("osal")
local aux_core    = require("aux_core" )
local modbus      = modbus

local pcallx         = osal.pcall
local str_to_hex     = aux_core.StrToHex
local hex_to_str     = aux_core.HexToStr

local bus = {}

bus.sys_start       = modbus.sys_start 

bus.read_onoff   = 1
bus.read_gonoff  = 2
bus.read_acdata  = 3
bus.read_input   = 4
bus.set_onoff    = 5
bus.set_acdata   = 6
bus.set_gonoff   = 15

function bus.send(id,cmd,rid,code)
    modbus.busend(id,cmd,string.pack(">H >H",rid,code))
end

-- fn(id,cmd,data)
function bus.recv_thread(fn)

    local function _handle(type,...)
        local args = {...}
        pcallx(fn,table.unpack(args))
    end
    osal.wait(0xF6768945,_handle,1)
end

return bus