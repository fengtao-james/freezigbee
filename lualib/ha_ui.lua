local osal     = require "osal"
local aux_core = require "aux_core"


local pcallx      = osal.pcall

local to_json     = osal.json_encode
local to_table    = osal.json_decode

local ui = {}

-- pmac : ids



function ui.availability(tpkey)
    return {topic =  "zbeacon/" .. tpkey .. "/status"}
end


function ui.str_to_sw(onoff)
    if onoff == "ON" then
        return 1
    else
        return 0
    end
end

function ui.sw_to_str(onoff)
    if onoff == 1 then
        return "ON"
    else
        return "OFF"
    end
end

function ui.lock_to_str(onoff)
    if onoff == 1 then
        return "LOCK"
    else
        return "UNLOCK"
    end
end

function ui.str_to_lock(onoff)

    if onoff == "LOCK" then
        return 1
    else
        return 0
    end
end

function ui.start_to_str(code)

    if code == 0 then
        return "off"
    end
    if code == 1 then
        return "on"
    end
    if code == 2 then
        return "memory"
    end
end

function ui.str_to_start(code)
    
    if code == "off" then
        return 0
    end
    if code == "on" then
        return 1
    end
    if code == "memory" then
        return 2
    end
end

function ui.cw_level_to_ty(level)

    return level*1000//254
end

function ui.cw_ty_to_level(level)

    return level*254//1000
end

function ui.cw_temp_to_ty(temp)

    return (temp-143)*1000//(500-143)
end

function ui.cw_ty_to_temp(temp)

    return (500-143)*temp//1000 + 143
end


function ui.device(pname,dname,mode)

    return {
                ids          = {mode},
                mf           = "zbeacon",
                mdl          = mode,
                name         = dname,
                sw           = "1.3.0",
                via_device   = pname
            }
end

function ui.number(pname,dname,cname,tpkey,mode,min,max,class)

    local cfg = "homeassistant/number/" .. tpkey .. "/config"

    local info  = {    
        name                = cname,
        cmd_t               = tpkey .. "/set",
        stat_t              = tpkey .. "/state",
        uniq_id             = tpkey .. "cfg",
        val_tpl             = "{{ value_json.number }}",
        mode                = "box",  --- box
        min                 = min,
        max                 = max,
        ent_cat             = "config",
        dev                 = ui.device(pname,dname,mode)
    }

    return cfg,info
end
-------------------------------------------------------------------------
function ui.sensor(pname,dname,cname,tpkey,mode,class,unit)

    local cfg = "homeassistant/sensor/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        stat_t              = tpkey  .. "/state",
        uniq_id             = tpkey,
        dev_cla             = class,
        unit_of_meas        = unit,
        val_tpl             = "{{ value_json.sensor}}",
        dev                 = ui.device(pname,dname,mode)
    }   
    return cfg,info
end

--[[
    class:door  
          gas
          lock
          tamper
          motion
          moisture
]]
function ui.bsensor(pname,dname,cname,tpkey,mode,class)

    local cfg = "homeassistant/binary_sensor/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        stat_t              = tpkey  .. "/state",
        uniq_id             = tpkey,
        dev_cla             = class,
        pl_on               = "ON",
        pl_off              = "OFF",
        val_tpl             = "{{ value_json.sensor}}",
        dev                 = ui.device(pname,dname,mode)
    }   
    return cfg,info
end


function ui.cw_light(pname,dname,tpkey,mode)

    local cfg  = "homeassistant/light/" .. tpkey .. "_light/config"

    local info    = {    
        schema                    = "json",
        name                      = "cw调光灯",
        cmd_t                     = tpkey .. "/light/set",
        stat_t                    = tpkey .. "/state",
        uniq_id                   = tpkey .. "_light",
        obj_id                    = tpkey .. "_light",
        bri_scl                   = 254,
        brightness                = true,
        clrm                      = true,
        max_mirs                  = 500,
        min_mirs                  = 143,
        opt                       = false,
        sup_clrm                  = {"color_temp"},
        dev                       = ui.device(pname,dname,mode)
    }
    return cfg,info
end


function ui.cover(pname,dname,cname,tpkey,mode)

    local cfg          = "homeassistant/cover/" .. tpkey .. "/config"

    local info  = {    
        name                  = cname,
        cmd_t                 = tpkey .. "/set",
        stat_t                = tpkey .. "/state",
        pos_t                 = tpkey .. "/state",
        set_pos_t             = tpkey .. "/position/set",
        uniq_id               = tpkey,
        val_tpl               = '{{ value_json.state }}',
        pos_tpl               = '{{ value_json.position }}',
        set_pos_tpl           = '{ "position": {{ position }}}',
        state_closed          =  "CLOSE",
        state_open            =  "OPEN",
        dev_cla               =  "curtain",
        dev                   = UI.device(pname,dname,mode)
    }
    return cfg,info
end

--name :button

--SINGLE
--DOUBLE
--HOLD

function ui.trigger(pname,dname,cname,tpkey,mode,dtype)
    local pl = {"SINGLE","DOUBLE","HOLD"}
    local tp = {"button_short_press","button_double_press","button_long_press"}

    local cfg = "homeassistant/device_automation/" .. tpkey .. "/config"

    local info = {    
        atype               = "trigger",
        type                = tp[dtype],
        subtype             = tpkey,
        pl                  = pl[dtype],
        topic               = tpkey .. "/action",
        dev                 = ui.device(pname,dname,mode)
    }   
    return cfg,info
end

function ui.cfgselect(pname,dname,cname,tpkey,mode,icon,list)

    local cfg = "homeassistant/select/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        cmd_t               = tpkey .. "/set",
        stat_t              = tpkey .. "/state",
        uniq_id             = tpkey,
        ops                 = list,
        icon                = icon,
        ent_cat             = "config",
        val_tpl             = "{{ value_json.select}}",
        dev                 = ui.device(pname,dname,mode)
    } 
    return cfg,info
end

function ui.select(pname,dname,cname,tpkey,mode,icon,list)

    local cfg = "homeassistant/select/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        cmd_t               = tpkey .. "/set",
        stat_t              = tpkey .. "/state",
        uniq_id             = tpkey,
        ops                 = list,
        icon                = icon,
        val_tpl             = "{{ value_json.select }}",
        dev                 = ui.device(pname,dname,mode)
    } 
    return cfg,info
end

function ui.switch(pname,dname,cname,tpkey,mode)

    local cfg = "homeassistant/switch/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        cmd_t               = tpkey .. "/set",
        stat_t              = tpkey .. "/state",
        uniq_id             = tpkey,
        pl_on               = "ON",
        pl_off              = "OFF",
        stat_on             = "ON",
        stat_off            = "OFF",
        dev_cla             = "switch",
        val_tpl             = "{{ value_json.switch}}",
        dev                 = ui.device(pname,dname,mode)
    }   
    return cfg,info
end

--[[
    None
    restart
    update
]]
function ui.cfgbutton(pname,dname,cname,tpkey,mode,payload,class)

    local cfg = "homeassistant/button/" .. tpkey .. "/config"

    local info    = {    
        name                      = cname,
        uniq_id                   = tpkey,
        cmd_t                     = tpkey .. "/set",
        pl_prs                    = payload,
        dev_cla                   = class,
        ent_cat                   = "config",
        dev                       = ui.device(pname,dname,mode)
    }

    return cfg,info
end

function ui.button(pname,dname,cname,tpkey,mode,payload,class)

    local cfg = "homeassistant/button/" .. tpkey .. "/config"

    local info    = {    
        name                      = cname,
        uniq_id                   = tpkey,
        cmd_t                     = tpkey .. "/set",
        pl_prs                    = payload,
        dev_cla                   = class,
        dev                       = ui.device(pname,dname,mode)
    }

    return cfg,info
end


function ui.switch_lock(pname,dname,cname,tpkey,mode)

    local cfg = "homeassistant/lock/" .. tpkey .. "/config"

    local info = {    
        name                = cname,
        cmd_t               = tpkey .. "/set",
        stat_t              = tpkey .. "/state",
        uniq_id             = tpkey,
        pl_lock             = "LOCK",
        pl_unlk             = "UNLOCK",
        stat_locked         = "LOCK",
        stat_unlocked       = "UNLOCK",
        ent_cat             = "config",
        val_tpl             = "{{ value_json.slock}}",
        dev                 = ui.device(pname,dname,mode)
    }   
    return cfg,info
end

--------------------------------------------------------------------------------
function ui.reset(pname,dname,tpkey,mode)
    return ui.cfgbutton(pname,dname,"reset",tpkey,mode,"restart","restart")
end

function ui.ota(pname,dname,tpkey,mode)
    return ui.cfgbutton(pname,dname,"OTA",tpkey,mode,"update","update")
end

function ui.permit_join(pname,dname,tpkey,mode)
    return ui.button(pname,dname,"join",tpkey,mode,"join","update")
end

function ui.sensor_door(pname,dname,tpkey,mode)
    return ui.bsensor(pname,dname,"door",tpkey,mode,"door")
end

--water
function ui.sensor_moisture(pname,dname,tpkey,mode)
    return ui.bsensor(pname,dname,"moisture",tpkey,mode,"moisture")
end

--motion
function ui.sensor_motion(pname,dname,tpkey,mode)
    return ui.bsensor(pname,dname,"motion",tpkey,mode,"motion")
end

function ui.power_start(pname,dname,tpkey,mode,icon)
    return ui.cfgselect(pname,dname,"通电配置",tpkey,mode,icon,{"关","开","记忆"})
end

function ui.switch_type(pname,dname,tpkey,mode,icon)
    return  ui.cfgselect(pname,dname,"开关类型",tpkey,mode,icon,{"flip","sync","button"})
end

-- point1
function ui.switch_point(pname,dname,tpkey,mode)
    return ui.number(pname,dname,"点动",tpkey,mode,0,3600)
end

function ui.onofftime(pname,dname,tpkey,mode)

    return ui.number(pname,dname,"缓起缓灭",tpkey,mode,"onofftime",1,10)
end

function ui.power(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"功率",tpkey,mode,"power","power","W")
end

function ui.voltage(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"电压",tpkey,mode,"voltage","voltage","V")
end

function ui.current(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"电流",tpkey,mode,"current","current","A")
end

function ui.energy(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"千瓦时",tpkey,mode,"energy","energy","kw/h")
end

function ui.battery(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"电量",tpkey,mode,"battery","battery","%")
end

function ui.humidity(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"湿度",tpkey,mode,"humidity","humidity","%")
end

function ui.temperature(pname,dname,tpkey,mode)

    return ui.sensor(pname,dname,"温度",tpkey,mode,"temperature","temperature","°C")
end

function ui.scene(pname,dname,cname,tpkey,mode,icon)

    return ui.select(pname,dname,cname,tpkey,mode,icon,{"active","store"})
--    return ui.select(pname,dname,"scene",tpkey,mode,{"night","read","work","leisure","jump","breath","ladder","rhythm"})
end


return ui