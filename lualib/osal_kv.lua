local aux_core = require ("aux_core")

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction

local kv_db = kv_db

local db = {}

local function GET(key)
	return db[key]
end

local function SET(key, value)
	local last = db[key]
	db[key] = value
	return last
end

function db.get(k)
	return GET(k)
end

function db.set(k,v)
	return SET(k, v)
end

function db.isset(k,v)
	return v==GET(k)
end

local Context = {}
Context.__index = Context

function Context:insert(key,value)

	if is_string( value ) then
		return kv_db.insert_string(self.db,key,value)
	end

	if is_integer(value) then
		return kv_db.insert_int(self.db,key,value)
	end
end

function Context:del(key)
	return kv_db.del(self.db,key)
end

function Context:get_str(key)
	return kv_db.get_string(self.db,key)
end

function Context:get_int(key)
	return kv_db.get_int(self.db,key)
end

function Context:close()
	kv_db.close(self.db)
end

function db.open(path,sz)
	local Object  = {}

	Object.db = kv_db.open(path,sz)

	if(Object.db == nil)then
		print("error db",path,sz)
		return nil
	end

	setmetatable( Object, Context )
	return Object
end

function db.del(path)
	kv_db.remove(path)
end


return db
