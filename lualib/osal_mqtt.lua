local osal           = require( "osal" )
local aux_core       = require( "aux_core" )

local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_bool     = aux_core.IsBoolean

local is_function = aux_core.IsFunction

local pcallx      = osal.pcall

local MQTT = {}

MQTT.VERSION = 0x03

-- MQTT 3.1 Specification: Section 2.1: Fixed header, Message type
MQTT.TYPE_CONNECT     = 0x01
MQTT.TYPE_CONACK      = 0x02
MQTT.TYPE_PUBLISH     = 0x03
MQTT.TYPE_PUBACK      = 0x04
MQTT.TYPE_PUBREC      = 0x05
MQTT.TYPE_PUBREL      = 0x06
MQTT.TYPE_PUBCOMP     = 0x07
MQTT.TYPE_SUBSCRIBE   = 0x08
MQTT.TYPE_SUBACK      = 0x09
MQTT.TYPE_UNSUBSCRIBE = 0x0a
MQTT.TYPE_UNSUBACK    = 0x0b
MQTT.TYPE_PINGREQ     = 0x0c
MQTT.TYPE_PINGRESP    = 0x0d
MQTT.TYPE_DISCONNECT  = 0x0e

local pack_id = 1

local function _get_pack_id()

  local n  = pack_id
  pack_id = pack_id+1
  return pack_id
end

local function _mqtt_encode(type,dup,qos,retain,payload)

  local ecode = {}
  local tlen = #payload

  table.insert(ecode,string.pack("B",((type & 0x0f) << 4) | ((dup & 1) << 3) | ((qos & 3) << 1) | (retain & 1)))

  repeat
    local encodedByte = tlen % 128

    tlen = tlen >> 7

    if tlen > 0  then
      encodedByte = encodedByte | 0x80
    end
    table.insert(ecode,string.pack(">B",encodedByte))
  until(tlen == 0)

  table.insert(ecode,payload)

  return table.concat(ecode)
end

--[[
  mqtt start connect 
]]

local mqttcli = {}
mqttcli.__index = mqttcli

function mqttcli:connect(username,key,client,keep,willtp,willmsg)

  if not is_string(willtp) then
    error("error willtp")
  end
  if not is_string(willmsg) then
    error("error willmsg")
  end

  local con = {}
  table.insert(con,string.pack(">H",4)) -- version
  table.insert(con,"MQTT")              -- MQTT
  table.insert(con,string.pack("B",4))  -- Protocol Level

  local flag = 0x26

  if(client == nil) then
    print("error connect client")
  end

  if(username ~= nil) then
    flag = flag | 0x80
  end

  if(key ~= nil) then
    flag = flag | 0x40
  end

  table.insert(con,string.pack("B",flag))
  table.insert(con,string.pack(">H",keep))

  table.insert(con,string.pack(">H",#client))
  table.insert(con,client)
  
  table.insert(con,string.pack(">H",#willtp))
  table.insert(con,willtp)

  table.insert(con,string.pack(">H",#willmsg))
  table.insert(con,willmsg)

  if(username ~= nil) then
    table.insert(con,string.pack(">H",#username))
    table.insert(con,username)
  end
  
  if(key ~= nil) then
    table.insert(con,string.pack(">H",#key))
    table.insert(con,key)
  end

  self.tcp:send(_mqtt_encode(MQTT.TYPE_CONNECT,0,0,0,table.concat(con))) 
end

--[[
  mqtt publish
]]
function mqttcli:publish(topic,qos,retain,data)
  local pub = {}
  table.insert(pub,string.pack(">H",#topic))
  table.insert(pub,topic)

  if qos >0 then
    table.insert(pub,string.pack(">H",_get_pack_id()))
  end
  table.insert(pub,data)

  print("push:",topic,data)
  self.tcp:send( _mqtt_encode(MQTT.TYPE_PUBLISH,0,qos,retain,table.concat(pub)))
end



function mqttcli:subscribe(topic,qos,ackfn)

  if  is_function(ackfn) then
    self.list[topic] = ackfn
  end

  local sub = {}

  table.insert(sub,string.pack(">H",_get_pack_id()))

  table.insert(sub,string.pack(">H",#topic))
  table.insert(sub,topic)

  table.insert(sub,string.pack("B",qos))

  self.tcp:send(_mqtt_encode(MQTT.TYPE_SUBSCRIBE,0,1,0,table.concat(sub))) 
end

function mqttcli:unsubscribe(topic)

  self.list[topic] = nil

  local sub = {}

  table.insert(sub,string.pack(">H",_get_pack_id()))

  table.insert(sub,string.pack(">H",#topic))
  table.insert(sub,topic)

  self.tcp:send( _mqtt_encode(MQTT.TYPE_UNSUBSCRIBE,0,1,0,table.concat(sub)))
end

function mqttcli:ping()

  self.tcp:send(_mqtt_encode(MQTT.TYPE_PINGREQ,0,0,0,""))
end

function mqttcli:plen(value)

  local encodedByte   = 0
  local multiplier    = 1
  local tlen          = 0
  local i             = 2

  repeat
    encodedByte = value:byte(i)

    if not encodedByte then
      return nil
    end
    
    tlen        = tlen + ((encodedByte & 0x7f) * multiplier)
    multiplier  = multiplier << 7
    i           = i + 1
  until((encodedByte & 0x80) == 0)

  if #value < tlen+i-1 then
    return nil
  end
  return i,tlen
end

local function _pushack(id)
  local sub = {}

  table.insert(sub,string.pack(">H",id))
  
  return _mqtt_encode(MQTT.TYPE_PUBACK,0,0,0,table.concat(sub))
end

function mqttcli:decode(value)

  local mqtt_unpack = {
    [MQTT.TYPE_CONACK]   = function(qos,payload)
                        pcallx(self.connect_cb,string.unpack("B B",payload))
                        end,
    [MQTT.TYPE_PUBLISH]  = function(qos,payload)
                        local tlen   = string.unpack(">H",payload)
                        local topic  = payload:sub(3,2+tlen)
                        local data   = payload:sub(3+tlen)

                        if qos == 1 then
                          local id = string.unpack(">H",data)

                          self.tcp:send(_pushack(id))

                          data   = payload:sub(5+tlen)
                          print("recv push",qos,id,#data)
                        end

                        print("recv: mqtt:",topic,data)
                        local fn = self.list[topic]
                        if fn then
                          pcallx(fn,data)
                          return
                        end

                        fn = self.topic_cb
                          if fn  then
                          pcallx(fn,topic,qos,data)
                          end
                        end,

    [MQTT.TYPE_PINGRESP] = function(qos,payload)
      -- print(".......ping resp")
                          end,
  }

    local i             = 1

    local tlen          = 0
    local encodedByte   = 0
    local multiplier    = 1

    local type          = (value:byte(i) >> 4) & 0x0f
    --local dup         = (value:byte(i) >> 3) & 0x01
    local qos           = (value:byte(i) >> 1) & 0x03
    --local retain      = (value:byte(i) >> 0) & 0x01

    i = i + 1

    repeat
      encodedByte = value:byte(i)
      tlen        = tlen + ((encodedByte & 0x7f) * multiplier)
      multiplier  = multiplier << 7
      i           = i + 1
    until((encodedByte & 0x80) == 0)
    
    if #value < tlen then

      osal.ERR("error mqtt recv tlen:%d len :%d",#value,tlen)
      return
    end

    local payload = value:sub(i,i-1+tlen) --[i,i-1+tlen]

    -- print("recv :tlen",i,tlen,#value,#payload)
    
    local ackfn = mqtt_unpack[type]

    if ackfn then
      pcallx(ackfn,qos,payload)
    end
end

function MQTT.new(tcp,connet_cb,topic_cb)

  local Object = {}
  Object.tcp        = tcp
  Object.topic_cb   = topic_cb
  Object.connect_cb = connet_cb
  Object.list       = {}

  setmetatable( Object, mqttcli )
  return Object
end

return MQTT