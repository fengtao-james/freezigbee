local osal           = require( "osal" )
local aux_core       = require( "aux_core" )
local zgbmgr         = zbmgr
local zcl_sql_pool   = {}
local zcl_mac_poll   = {}

local pcallx         = osal.pcall
local str_to_hex     = aux_core.StrToHex
local hex_to_str     = aux_core.HexToStr

local zcl = {}

zcl.data_report = 1
zcl.dev_left    = 2
zcl.dev_ready   = 3
zcl.dev_online  = 4
zcl.dev_next_ep = 5
zcl.dev_ota_end = 6
zcl.dev_info    = 7
zcl.dev_ota_up  = 8
zcl.dev_rejoin  = 9
zcl.bind_info   = 10

zcl.CHANGED            = 1
zcl.UPDATE             = 2
zcl.FINISH             = 3

zcl.rftick             = 0

zcl.sys_start       = zgbmgr.sys_start      --(dev,bps)
zcl.sys_reboot      = zgbmgr.sys_reset      --nil
zcl.allow_join      = zgbmgr.network_open   --nil
zcl.unallow_join    = zgbmgr.network_close  --nil
zcl.net_reset       = zgbmgr.network_leave  --nil
-- zcl.del_dev         = zgbmgr.network_del    -- mac
zcl.get_eplist      = zgbmgr.dev_eplist     -- eplist
zcl.get_ep_info     = zgbmgr.dev_ep_info    -- mac ,ep ,did,inclust,outclust
zcl.get_dev_info    = zgbmgr.dev_info       -- mfd  mid
zcl.broadcast       = zgbmgr.zcl_broadcast  -- ep,cid,control,menu,cmd,data
zcl.group           = zgbmgr.zcl_group      -- gid,ep,cid,control,menu,cmd,data
zcl.setpanid        = zgbmgr.set_panid
zcl.setchannel      = zgbmgr.set_channel
zcl.net_info        = zgbmgr.network_info
zcl.all_dev         = zgbmgr.dev_all
zcl.online          = zgbmgr.dev_online
zcl.get_dev_index   = zgbmgr.dev_index
zcl.cbind           = zgbmgr.cid_bind       --mac,ep,cid
zcl.net_repair      = zgbmgr.net_repair
zcl.bind_table      = zgbmgr.bind_table

-- Foundation Command IDs
zcl.READ                                   = 0x00
zcl.READ_RSP                               = 0x01
zcl.WRITE                                  = 0x02
zcl.WRITE_UNDIVIDED                        = 0x03
zcl.WRITE_RSP                              = 0x04
zcl.WRITE_NO_RSP                           = 0x05
zcl.CONFIG_REPORT                          = 0x06
zcl.CONFIG_REPORT_RSP                      = 0x07
zcl.READ_REPORT_CFG                        = 0x08
zcl.READ_REPORT_CFG_RSP                    = 0x09
zcl.REPORT                                 = 0x0a
zcl.DEFAULT_RSP                            = 0x0b
zcl.DISCOVER_ATTRS                         = 0x0c
zcl.DISCOVER_ATTRS_RSP                     = 0x0d
zcl.DISCOVER_CMDS_RECEIVED                 = 0x11
zcl.DISCOVER_CMDS_RECEIVED_RSP             = 0x12
zcl.DISCOVER_CMDS_GEN                      = 0x13
zcl.DISCOVER_CMDS_GEN_RSP                  = 0x14
zcl.DISCOVER_ATTRS_EXT                     = 0x15
zcl.DISCOVER_ATTRS_EXT_RSP                 = 0x16
zcl.EXWRITE                                = 0xF0
--/*** Data Types ***/
zcl.NO_DATA                            = 0x00
zcl.DATA8                              = 0x08
zcl.DATA16                             = 0x09
zcl.DATA24                             = 0x0a
zcl.DATA32                             = 0x0b
zcl.DATA40                             = 0x0c
zcl.DATA48                             = 0x0d
zcl.DATA56                             = 0x0e
zcl.DATA64                             = 0x0f
zcl.BOOLEAN                            = 0x10
zcl.BITMAP8                            = 0x18
zcl.BITMAP16                           = 0x19
zcl.BITMAP24                           = 0x1a
zcl.BITMAP32                           = 0x1b
zcl.BITMAP40                           = 0x1c
zcl.BITMAP48                           = 0x1d
zcl.BITMAP56                           = 0x1e
zcl.BITMAP64                           = 0x1f
zcl.UINT8                              = 0x20
zcl.UINT16                             = 0x21
zcl.UINT24                             = 0x22
zcl.UINT32                             = 0x23
zcl.UINT40                             = 0x24
zcl.UINT48                             = 0x25
zcl.UINT56                             = 0x26
zcl.UINT64                             = 0x27
zcl.INT8                               = 0x28
zcl.INT16                              = 0x29
zcl.INT24                              = 0x2a
zcl.INT32                              = 0x2b
zcl.INT40                              = 0x2c
zcl.INT48                              = 0x2d
zcl.INT56                              = 0x2e
zcl.INT64                              = 0x2f
zcl.ENUM8                              = 0x30
zcl.ENUM16                             = 0x31
zcl.SEMI_PREC                          = 0x38
zcl.SINGLE_PREC                        = 0x39
zcl.DOUBLE_PREC                        = 0x3a
zcl.OCTET_STR                          = 0x41
zcl.CHAR_STR                           = 0x42
zcl.LONG_OCTET_STR                     = 0x43
zcl.LONG_CHAR_STR                      = 0x44
zcl.ARRAY                              = 0x48
zcl.STRUCT                             = 0x4c
zcl.SET                                = 0x50
zcl.BAG                                = 0x51
zcl.TOD                                = 0xe0
zcl.DATE                               = 0xe1
zcl.UTC                                = 0xe2
zcl.CLUSTER_ID                         = 0xe8
zcl.ATTR_ID                            = 0xe9
zcl.BAC_OID                            = 0xea
zcl.IEEE_ADDR                          = 0xf0
zcl.BIT_SEC_KEY                        = 0xf1
zcl.UNKNOWN                            = 0xff

zcl.tplen =
{
	[0x00] = 0,
	[0xff] = 0,
	[0x08] = 1,
	[0x10] = 1,
	[0x18] = 1,
	[0x20] = 1,
	[0x28] = 1,
	[0x30] = 1,
	[0x09] = 2,
	[0x19] = 2,
	[0x21] = 2,
	[0x29] = 2,
	[0x31] = 2,
	[0x38] = 2,
	[0xe8] = 2,
	[0xe9] = 2,
	[0x0a] = 3,
	[0x1a] = 3,
	[0x22] = 3,
	[0x2a] = 3,
	[0x0b] = 4,
	[0x1b] = 4,
	[0x23] = 4,
	[0x2b] = 4,
	[0x39] = 4,
	[0xe0] = 4,
	[0xe1] = 4,
	[0xe2] = 4,
	[0xea] = 4,
	[0x24] = 5,
	[0x2c] = 5,
	[0x25] = 6,
	[0x2d] = 6,	
	[0x26] = 7,
	[0x2e] = 7,	
	[0x3a] = 8,
	[0xf0] = 8,
	[0x27] = 8,
	[0x2f] = 8,
}

local function attr_handle(data)

    local dtype = string.unpack("B",data)
    local buff  = string.sub(data,2)

    if dtype == zcl.LONG_CHAR_STR or dtype == zcl.LONG_OCTET_STR  then
        
		local dlen   = string.unpack("<H",buff)
        local strbuf = string.sub(buff,3)
		return string.sub(strbuf,1,dlen),string.sub(strbuf,dlen+1),dtype
    end
    	
    if dtype == zcl.CHAR_STR or dtype == zcl.OCTET_STR or dtype == zcl.ARRAY  then
        
        local dlen   = string.unpack("B",buff)
        local strbuf = string.sub(buff,2)
		return string.sub(strbuf,1,dlen),string.sub(strbuf,dlen+1),dtype
    end
    
    if zcl.tplen[dtype] ~= nil  then
        local dlen = zcl.tplen[dtype]
        return string.sub(buff,1,dlen),string.sub(buff,dlen+1),dtype
    else
        error(dtype)
        return nil    
	end
end


local function write_pack(attr,dtype,data)

    if dtype==zcl.LONG_CHAR_STR or dtype==zcl.LONG_OCTET_STR  then
        return string.pack("<H B <H",attr,dtype,#data) .. data
    end

    if dtype==zcl.CHAR_STR or dtype==zcl.OCTET_STR or dtype==zcl.ARRAY  then
        return string.pack("<H B B",attr,dtype,#data) .. data
    end

    if zcl.tplen[dtype] ~=nil  then
        return string.pack("<H B",attr,dtype) .. data
    else
        error(dtype)
        return nil
    end
end


local function read_pack(attrlist)
    return osal.tab_to_hex("<H",attrlist)
end

-- func(attrid,status ,dtype, atrdata)
function zcl.readack_unpack(data,func)
    local str           = data
    local atrdata       = nil
    local dtype         = nil

    while #str >= 3 do
        local attrid,status = string.unpack("<H B",str)
        
        str = string.sub(str,4)  -- 2 + 1

        if status == 0  then
            atrdata ,str ,dtype = attr_handle(str)
            pcallx(func,attrid,status,dtype,atrdata) 
        else
            pcallx(func,attrid,status) 
        end
    end
end

function zcl.report_unpack(data,func)
    local str     = data
    local atrdata = nil
    local dtype   = nil

    while #str > 0  do

        local attrid  = string.unpack("<H",str)

        str                = string.sub(str,3)  -- 2 + 1
        atrdata ,str ,dtype = attr_handle(str)
        
        pcallx(func,attrid,dtype,atrdata) 
    end
end

-------------------------zcl ota start--------------------
function zcl.ota(mac,ep)

    local r = zgbmgr.ota_start(mac,ep)

    if r < 0  then
        print("ota error!!!!!")
    end
end
-------------------------zcl--ota---code-------------------
function zcl.ota_code(path)

    local r = zgbmgr.ota_code(path)

    if r < 0  then
        print("ota error!!!!!",path)
    end
end

-----------------------------------------------------------
local function __wait_ack(ackfn,mac,ep,clust,sql,ti)

    local session         = osal.session()
    zcl_sql_pool[sql]     = {sid = session,mac = mac}
    
    -- print("sid :",session,sql)
    osal.sessioncall(session,ackfn,ti)
end

-- control  :0  attr    1 ctrl

function zcl.send(ackfn,mac,ep,clust,control,menu,cmd,data,t)
    
    local sql = zgbmgr.zcl_send(mac,ep,clust,control,menu,cmd,data)
    
    if sql < 0 then
        return false
    end

    if not ackfn then
        return true
    end

    pcallx(__wait_ack,ackfn,mac,ep,clust,sql,t or 500 + math.random(200,512))
    return true
end

-- ackfn(control,cmd,data,rssi)
function zcl.cmd(ackfn,mac,ep,clust,cmd,data)
    return zcl.send(ackfn,mac,ep,clust,1,0,cmd,data)
end

function zcl.read(ackfn,mac,ep,clust,list)
    return zcl.send(ackfn,mac,ep,clust,0,0,zcl.READ,read_pack(list))
end

function zcl.readrsp(mac,ep,clust,data)
    return zcl.send(nil,mac,ep,clust,0,0,zcl.READ_RSP,data)
end

function zcl.write(ackfn,mac,ep,clust,attrid,dtype,data)
    return zcl.send(ackfn,mac,ep,clust,0,0,zcl.WRITE,write_pack(attrid,dtype,data))
end

function zcl.group_cmd(gid,ep,cluster,control,cmd,data)
    pcallx(zcl.group,gid,ep,cluster,control,0,cmd,data)
end

function zcl.broadcast_cmd(ep,cluster,control,cmd,data)
    pcallx(zcl.broadcast,ep,cluster,control,0,cmd,data)
end

function zcl.exwrite(ackfn,mac,ep,clust,data)
    return zcl.send(ackfn,mac,ep,clust,0,0,zcl.EXWRITE,string.pack("B",#data) .. data)
end

function zcl.menuwrite(ackfn,mac,ep,clust,menu,attrid,dtype,data)
    return zcl.send(ackfn,mac,ep,clust,4,menu,zcl.WRITE,write_pack(attrid,dtype,data),5000)
end

--mac,ep,cluster,rssi,control,menu,cmd,data
function zcl.bind(mac,recvfunc)
    zcl_mac_poll[mac]     = recvfunc
end

function zcl.unbind(mac)
    zcl_mac_poll[mac]     = nil
end

function zcl.del_dev(mac)
    zgbmgr.network_del(mac)
end

-------------------------------------------------------------------------------------

local function resp(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

    local ctx = zcl_sql_pool[sql]

    zcl_sql_pool[sql] = nil

    if ctx == nil then
        return false
    end

    if ctx.mac ~= mac then
        print("resp status:mac is error")
        return false
    end

    -- osal.DBG("resp :mac:%s ep:%d sql :%d",aux_core.HexToStr(mac),ep,sql)
    -- ackfn(control,cmd,data,rssi)
    osal.dispatch(ctx.sid,control&1,cmd,data,rssi)

    return true
end

function zcl.on_report(mac,ep,cluster,rssi,control,menu,sql,cmd,data)
    
    local rc = resp(mac,ep,cluster,rssi,control,menu,sql,cmd,data)

    if rc == true  then
        return true
    end 

    local ackfn = zcl_mac_poll[mac]

    if ackfn == nil  then
        return false
    end

    pcallx(ackfn,mac,ep,cluster,rssi,control,menu,sql,cmd,data)
    return true
end

function zcl.offset()
    return osal.tick() - zcl.rftick
end

function zcl.tick()
    zcl.rftick = osal.tick()
end

--  zcl  recv  handle
function zcl.recv_thread(list)

    local function _handle(type,...)

        local cb = list[type]
        if not cb then
            return
        end
        zcl.rftick = osal.tick()

        local args = {...}
        pcallx(cb,table.unpack(args))
    end

    osal.wait(0xF6768941,_handle,1)
end

return zcl
