
local osal     = require ("osal")
local aux_core = require ("aux_core")
local namespace   = {}


local is_integer  = aux_core.IsInteger
local is_nil      = aux_core.IsNil
local is_string   = aux_core.IsString
local is_table    = aux_core.IsTable
local is_array    = aux_core.IsArray
local is_function = aux_core.IsFunction
local to_json     = osal.json_encode
local to_table    = osal.json_decode

local fn_timeout  = osal.sessioncall

local pcallx      = osal.pcall

local function is_null( v )
	return nil == v
end
--------------------------------------------------------------------------------

local function jrpc_request( id, method, params )

	if not ( is_nil( id ) or is_integer( id ) or is_string( id ) ) then

		return nil
	end

	if not is_string( method ) then

		return nil
	end

	if not ( is_nil( params ) or is_table( params ) ) then

		return nil
	end

	return { jsonrpc = "2.0", id = id, method = method, params = params}
end
--------------------------------------------------------------------------------

local function jrpc_error( id, code, message, data)

	if not ( is_null( id ) or is_integer( id ) or is_string( id ) ) then

		return nil
	end

	if not is_integer( code ) then

		return nil
	end

	if not is_string( message ) then

		return nil
	end

	return { jsonrpc = "2.0", id = id, error = { code = code, message = message, data = data }} 
end

--------------------------------------------------------------------------------

local function jrpc_response( id, result)

	if not ( is_null( id ) or is_integer( id ) or is_string( id ) ) then

		return nil
	end

	if is_nil( result ) then

		result = nil
	end

	return { jsonrpc = "2.0", id = id, result = result } 
end

--[[
	 method = method, params = params, extend = extend
]]
--------------------------------------------------------------------------------
-- Registry[ name ] = proc

local Registry = {}

local CodeMessage = {

	[ -32600 ] = "Invalid request",
	[ -32601 ] = "Method not found",
	[ -36602 ] = "Invalid parameter",
	[ -36603 ] = "Internal error",
	[ -32700 ] = "Parse error",
	[ -32099 ] = "Request timeout"
}


local function response( id, result)

	if is_nil( id ) then

		return nil
	end

	return jrpc_response( id, result)
end

local function response_error( id, code, message, data)

	if is_nil( id ) then

		return nil
	end

	if nil == message then
		message = CodeMessage[ code ] or "Unknown error"
	end

	return  jrpc_error( id, code, message, data) 
end

local function jrpc_params( ... )

	local r = table.pack( ... )

	for i = 1, r.n do
		if is_nil( r[ i ] ) then
			r[ i ] = nil
		end
	end
	r.n = nil
	return r
end

local function return_pack( ... )

	local r = table.pack( ... )

	for i = 1, r.n  do
		if is_nil( r[ i ] ) then
			r[ i ] = null
		end
	end
	return r
end

local function on_invoke( object,packet )

	local proc = object[ packet.method ]

	if is_function( proc ) then

		if is_array( packet.params ) then

			return return_pack( proc(table.unpack( packet.params ) ) )
		else
			return return_pack( proc(packet.params ) )
		end
	end
end

------------------------------- req--------------------------------------- 

local function on_call(packet)

	local  f,r = pcall( on_invoke, Registry,packet )

	if f then
		if is_table( r ) then

			if r.n > 1 then

				r.n = nil  -- n

				return response(packet.id, r)
			else
				return response(packet.id, r[ 1 ])
			end
		else
			return response_error(packet.id, -32601,nil,nil)
		end
	else
		return response_error(packet.id, -32600,nil,nil)
	end
end

------------------------------- rsp--------------------------------------- 
--{ jsonrpc = "2.0", id = id, result = result }

local function on_result( packet )
	osal.dispatch(packet.id, packet.result, packet.error )
	return nil
end

--------------------------------------------------------------------------------
--[[
	name :string
	proc : func
]]
function namespace.register( name, proc )

	if not is_string( name ) then
		print("name error",name)
		return
	end

	if not ( is_nil( proc ) or is_function( proc ) ) then
		print("proc error",proc)
		return
	end
	Registry[ name ] = proc
end

--------------------------------------------------------------------------------
--[[
	packet: { jsonrpc = "2.0", id = id, method = method, params = params, extend = extend } 
]]

local function _rpc_ackfn(sender,result)
	
	if not result then
		return
	end

	if not sender then
		return
	end

	local ackfn   = sender.sendfn

	result.extend = sender.extend

	pcallx(ackfn,result)
end

function namespace.scheduler(sender,packet)

	if not is_table( sender ) then
		return
	end

	if not is_table( packet ) then
		return
	end

	namespace.sender = sender
	namespace.id     = packet.id

	if packet.method then   -- req

		local r = on_call(packet)

		if not r then
			return
		end

		if  namespace.id or r.error then
			_rpc_ackfn(sender,r)
		end
	else
		on_result(packet)  	-- rsp
	end
	namespace.sender = nil
	namespace.id     = nil
end

--------------------------------------------------------------------------------
-- return
function namespace.call(sender,session,result ,timeout, method, ...)

	if not is_string( method ) then
		return
	end

	local params = jrpc_params(...)

	-- result fn
	if session then
		fn_timeout(session,result,timeout)
	end
	_rpc_ackfn(sender,jrpc_request( session, method, params))
end

function namespace.reflect(method)

	if not is_string( method ) then
		return nil
	end

	local sender = namespace.sender

	return function(...)
		_rpc_ackfn(sender,{ jsonrpc = "2.0", method = method, params = jrpc_params(...)})
	end
end

function namespace.async_switch()

	local id = namespace.id

	if is_nil( id ) then

		return nil
	end

	local sender = namespace.sender
	
	namespace.sender = nil
	namespace.id     = nil

	return function(r,...)
		if r then
		_rpc_ackfn(sender,response(id,jrpc_params(...)))
		else
		_rpc_ackfn(sender,response_error(id, -32099,nil,nil))	
		end
	end
end
return namespace

