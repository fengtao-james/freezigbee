local osal        = require( "osal" )

local tcp         = tcp
local http        = http

local mod = {}

local pcallx      = osal.pcall

local cli = {}
cli.__index = cli

function cli:send(data)
    return tcp.tsend(self.ctx,data)
end

function cli:bind(recvfunc,notify)

    local function _on_notify(fd,ntype)
        if ntype == 1  then
            pcallx(notify)
        end
    end

    local function _handle(type,...)
        local args = {...}
        local cb   = recvfunc

        if type == 1 then
            cb = _on_notify
        end
        
        pcallx(cb,table.unpack(args))
    end

    osal.wait(0xF6768943,_handle,1)
end

function cli:reset()
    tcp.tclireset(self.ctx)
end

function cli:connect(ip)
    self.ctx = tcp.tcliopen(ip)
end

function mod.cli_new(ip)

    local Object = {}
    
    Object.ctx = tcp.tcliopen(ip)

    if not Object.ctx then
        return nil
    end
    setmetatable( Object, cli )
	return Object
end

function mod.cli_free(object)
    if not object then
        return
    end

    tcp.tclose(object.ctx)
end


local srv = {}
srv.__index = srv

function srv:bind(notify)
    local function _handle(type,...)
        local args = {...}
        pcallx(notify,table.unpack(args))
    end

    osal.wait(0xF6768944,_handle,1)
end

function srv:accept(fd)
    local Object = {}
    Object.ctx = tcp.tsrvaccept(fd)

    if(Object.ctx == nil) then
        return nil
    end

    setmetatable( Object, cli )
	return Object
end

function mod.srv_new(port)
    local Object = {}
    Object.ctx = tcp.tsrvopen(port)

    if(Object.ctx == nil) then
        return nil
    end

    setmetatable( Object, srv )
	return Object

end

function mod.srv_free(object)
    tcp.tclose(object.ctx)
    object = nil
end
-------------------------websocket------------------------------
local guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"


local wbsock = {}
wbsock.__index = wbsock

function wbsock:decode(data)
    http.decode(self.ctx,data)
end

function wbsock:clode()
    http.close(self.ctx)
end

function wbsock:bind(kv_func,body_func)
    http.bind(self.ctx,kv_func,body_func)
end

function wbsock:send(data)
    self.tcp:send(data)
end

function wbsock:rspconnect(wbkey)
    local wbval  = wbkey..guid
    local acckey = http.base64(http.sha1(wbval))

    return string.format("HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: %s\r\n\r\n",acckey)
end

function mod.http_new(tcp_ctx)

    local Object = {}

    Object.ctx = http.open()
    Object.tcp = tcp_ctx

    if(Object.ctx == nil) then
        return nil
    end

    setmetatable( Object, wbsock )
	return Object
end


return mod

